const config = require('config');
const path = require('path');
const errorUtils = require('./models/services/error/errorUtils');
const transactionProcessor = require('./smart-contracts/services/cron/resolverLoop');
const claimVotersNotifier = require('./smart-contracts/services/cron/claimVotersNotifier');
const checkClaimsJob = require('./smart-contracts/services/cron/checkClaimsJob');
const checkVoteTxJob = require('./smart-contracts/services/cron/checkVoteTxJob');
const votingReminderJob = require('./smart-contracts/services/cron/votingReminderJob');


global.appRoot = path.resolve(__dirname);

let send_tick = config.get('cron.send_tick');
let process_tick = config.get('cron.process_tick');
let claims_notify_tick = config.get('cron.claims_notify_tick');
let check_claims_tick = config.get('cron.check_claims_tick');
let check_vote_tx_tick = config.get('cron.check_vote_tx_tick');

if(!send_tick || !process_tick){
    errorUtils.throwError("Unable to start cron job , configuration missing for send_tick or process_tick")
}

if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'integration'){
    var CronJob = require('cron').CronJob;    
    var sendJob = new CronJob(send_tick, async () => await transactionProcessor.sendTransactions(), null, true, 'GMT');     
    var job = new CronJob(process_tick, async () => await transactionProcessor.processTransactions(), null, true, 'GMT');
    var notifyClaimsJob = new CronJob(claims_notify_tick, async () => await claimVotersNotifier.notifyClaimVoters(), null, true, 'GMT');
    var claimResolution = new CronJob(check_claims_tick, async () => await checkClaimsJob.processOutstandingClaims(), null, true, 'GMT');
    var checkVoteTxCron = new CronJob(check_vote_tx_tick, async () => await checkVoteTxJob.checkVoteTxStatus(), null, true, 'GMT');
    new CronJob(check_vote_tx_tick, async () => await votingReminderJob.sendReminderEmails(), null, true, 'GMT');
}

console.log('started cron job succesfully');
