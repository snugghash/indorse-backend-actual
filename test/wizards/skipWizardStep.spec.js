process.env.NODE_ENV = 'test';

const chai = require('chai');
const mongooseWizardsRepo = require('../../models/services/mongo/mongoose/mongooseWizardsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const server = require('../../server');
const chaiHttp = require('chai-http')
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const authenticationWrapper = require('../authenticationWrapper');
const config = require('config');
chai.use(chaiHttp);

describe('wizards.skipWizardStep', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /wizards/:wizard_name/:user_id/skip/:step_number', () => {

        it('it should skip through the wizard.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;
            result.next_step.title.should.equal("interests");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("Connect social accounts");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/4')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.wizard_finished.should.equal(true);
        });

        it('it should skip through the wizard and get a badge.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;
            result.next_step.title.should.equal("interests");

            let randomInterests = [testUtils.generateRandomString(),testUtils.generateRandomString()];
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/interests')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({interests : randomInterests});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            let skillInDb = testUtils.generateRandomUserSkill();
            await mongooseSkillRepo.insert(skillInDb);
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/skills')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({name: skillInDb.name, level: 'intermediate'});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("Connect social accounts");

            await mongooseUserRepo.update({email: userFromMongo.email}, {$set: {isGoogleLinked: true}});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/4')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("You earned a badge");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/skip/5')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("You earned a badge");
        });

        it('it should return 404 for non-existing wizard', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            try {
                await chai.request(server)
                    .post('/wizards/skidaddle-skidoodle/' + userFromMongo._id.toString() + '/skip/1')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail when user is not logged in', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            try {
                await chai.request(server)
                    .post('/wizards/signUp/' + userFromMongo._id.toString() + '/skip/1')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    });

});