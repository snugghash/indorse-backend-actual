process.env.NODE_ENV = 'test';

const chai = require('chai');
const mongooseWizardsRepo = require('../../models/services/mongo/mongoose/mongooseWizardsRepo');
const server = require('../../server');
const chaiHttp = require('chai-http')
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const authenticationWrapper = require('../authenticationWrapper');
const config = require('config');
chai.use(chaiHttp);

describe('wizards.completeWizardStep', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /wizards/:wizard_name/:user_id/complete/step', () => {

        it('it should retrieve a wizard for  valid request', async (done) => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await mongooseWizardsRepo.insert(signupWizard);

                let res = await chai.request(server)
                    .get('/wizards/' +signupWizard.name)
                    .send();

                let retrievedWizard = res.body;

                retrievedWizard.name.should.equal(signupWizard.name);
                retrievedWizard.title.should.equal(signupWizard.title);
                retrievedWizard.description.should.equal(signupWizard.description);
                retrievedWizard.steps.length.should.equal(signupWizard.steps.length);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('it should return 404 for non-existing company', async (done) => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .get('/wizards/skidaddle-skidoodle')
                    .send();

                done(new Error('Expected to fail!'));
            } catch (error) {
                try {
                    error.status.should.equal(404);
                    done();
                } catch (error) {
                    done(error)
                }
            }
        });
    })
});