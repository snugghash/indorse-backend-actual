process.env.NODE_ENV = 'test';

const chai = require('chai');
const mongooseWizardsRepo = require('../../models/services/mongo/mongoose/mongooseWizardsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const server = require('../../server');
const chaiHttp = require('chai-http')
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const authenticationWrapper = require('../authenticationWrapper');
const config = require('config');
chai.use(chaiHttp);

describe('wizards.completeWizardStep', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /wizards/:wizard_name/:user_id/complete/:step_number', () => {

        it('it should complete signup wizard step by step. Redoing it should be possible.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;

            result.next_step.title.should.equal("interests");

            let randomInterests = [testUtils.generateRandomString(),testUtils.generateRandomString()];
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/interests')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({interests : randomInterests});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            let skillInDb = testUtils.generateRandomUserSkill();
            await mongooseSkillRepo.insert(skillInDb);
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/skills')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({name: skillInDb.name, level: 'intermediate'});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("Connect social accounts");

            await mongooseUserRepo.update({email: userFromMongo.email}, {$set: {isGoogleLinked: true}});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/4')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("You earned a badge");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/5')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;

            should.not.exist(result.next_step);
            result.wizard_finished.should.equal(true);

            // Redo the wizard

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("interests");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("Connect social accounts");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/4')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("You earned a badge");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/5')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;

            should.not.exist(result.next_step);
            result.wizard_finished.should.equal(true);
        });

        it('it should not go to step 3 if no interests are provided.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;

            result.next_step.title.should.equal("interests");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.failed.should.equal(true);
            result.currentStep.title.should.equal("interests");
            should.not.exist(result.next_step);
        });

        it('it should not go to step 4 if no skills are added.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;

            result.next_step.title.should.equal("interests");

            let randomInterests = [testUtils.generateRandomString(), testUtils.generateRandomString()];
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/interests')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({interests: randomInterests});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.failed.should.equal(true);
            result.currentStep.title.should.equal("skills");
            should.not.exist(result.next_step);
        });

        it('it should not complete step 4 if no social accounts are linked.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;

            result.next_step.title.should.equal("interests");

            let randomInterests = [testUtils.generateRandomString(), testUtils.generateRandomString()];
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/interests')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({interests: randomInterests});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            let skillInDb = testUtils.generateRandomUserSkill();
            await mongooseSkillRepo.insert(skillInDb);
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/skills')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({name: skillInDb.name, level: 'intermediate'});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("Connect social accounts");

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/4')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.failed.should.equal(true);
            result.currentStep.title.should.equal("Connect social accounts");
            should.not.exist(result.next_step);
        });

        it('it should complete wizard but not get the badge.', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            let result = res.body;

            result.next_step.title.should.equal("interests");

            let randomInterests = [testUtils.generateRandomString(), testUtils.generateRandomString()];
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/interests')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({interests: randomInterests});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("skills");

            let skillInDb = testUtils.generateRandomUserSkill();
            await mongooseSkillRepo.insert(skillInDb);
            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/skills')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({name: skillInDb.name, level: 'intermediate'});

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/3')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.next_step.title.should.equal("Connect social accounts");

            await mongooseUserRepo.update({email: userFromMongo.email}, {$set: {isGoogleLinked: true}});
            await mongooseUserRepo.update({email: userFromMongo.email}, {$set: {interests: null} });

            res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/4')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            result = res.body;
            result.wizard_finished.should.equal(true);
            should.not.exist(result.next_step);
        });

        it('it should fail to complete somebody else\'s step', async () => {

            let user = testUtils.generateRandomUser();
            let user2 = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};
            let tokenObj2 = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
            await authenticationWrapper.signupVerifyAuthenticate(user2, tokenObj2);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            try {
                await chai.request(server)
                    .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/1')
                    .set('Authorization', 'Bearer ' + tokenObj2.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should not allow skipping steps', async () => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await mongooseWizardsRepo.insert(signupWizard);

            let res = await chai.request(server)
                .post('/wizards/' + signupWizard.name + '/' + userFromMongo._id.toString() + '/complete/2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.failed.should.equal(true);
            should.exist(res.body.reason);
        });

        it('it should return 404 for non-existing wizard', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            try {
                await chai.request(server)
                    .post('/wizards/skidaddle-skidoodle/' + userFromMongo._id.toString() + '/complete/1')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail when user is not logged in', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            try {
                await chai.request(server)
                    .post('/wizards/signUp/' + userFromMongo._id.toString() + '/complete/1')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});