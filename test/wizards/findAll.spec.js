process.env.NODE_ENV = 'test';

const chai = require('chai');
const mongooseWizardsRepo = require('../../models/services/mongo/mongoose/mongooseWizardsRepo');
const server = require('../../server');
const chaiHttp = require('chai-http')
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const authenticationWrapper = require('../authenticationWrapper');
const config = require('config');
chai.use(chaiHttp);

describe('wizards.findAll', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /wizards', () => {

        it('should retrieve the test wizard', async (done) => {

            let user = testUtils.generateRandomUser();

            let signupWizard = testUtils.getSignupWizard();

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await mongooseWizardsRepo.insert(signupWizard);

                let res = await chai.request(server)
                    .get('/wizards')
                    .send();

                let retrievedWizard = res.body.wizards[0];

                retrievedWizard.name.should.equal(signupWizard.name);
                retrievedWizard.title.should.equal(signupWizard.title);
                retrievedWizard.description.should.equal(signupWizard.description);
                retrievedWizard.steps.length.should.equal(signupWizard.steps.length);

                done();
            } catch (error) {
                done(error);
            }
        });

    })
});