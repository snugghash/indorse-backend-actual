process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoBadgeRepo = require('../../models/services/mongo/mongoRepository')('badges');
const mongoUserRepo = require('../../models/services/mongo/mongoRepository')('users');
const badgeCreationWrapper = require('./badgeCreationWrapper');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('badges.findAll', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /badges/:pretty_id/users', () => {

        it('When no search param it should retrieve all users', async () => {

            let randomBadge = testUtils.generateRandomBadge();

            let adminUser = testUtils.generateRandomUser();
            let adminUser2 = testUtils.generateRandomUser();

            let pretty_id = randomBadge.pretty_id;

            let userCount = 2;
            

            let tokenObj = {};
            let tokenObj1 = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser2, tokenObj1, 'admin');

            await mongoUserRepo.update({'username' : adminUser.username},{$addToSet : {badges : pretty_id}});
            await mongoUserRepo.update({'username' : adminUser2.username},{$addToSet : {badges : pretty_id}});

            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

            let res = await chai.request(server)
                .get('/badges/' + randomBadge.pretty_id + '/users')
                .send();

            res.body.users.length.should.equal(userCount);
            res.body.totalUsers.should.equal(userCount);
            should.not.exist(res.body.matchingUsers);
        });

        it('Pagination should work', async () => {

            let randomBadge = testUtils.generateRandomBadge();

            let adminUser = testUtils.generateRandomUser();
            let adminUser2 = testUtils.generateRandomUser();

            let pretty_id = randomBadge.pretty_id;
            

            let userCount = 2;
            

            let tokenObj = {};
            let tokenObj1 = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser2, tokenObj1, 'admin');

            await mongoUserRepo.update({'username' : adminUser.username},{$addToSet : {badges : pretty_id}});
            await mongoUserRepo.update({'username' : adminUser2.username},{$addToSet : {badges : pretty_id}});

            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

            let res = await chai.request(server)
                .get('/badges/' + randomBadge.pretty_id + '/users?pageNumber=' + 1 + '&pageSize=1')
                .send();

            res.body.users.length.should.equal(1);

            res = await chai.request(server)
                .get('/badges/' + randomBadge.pretty_id + '/users?pageNumber=' + 2 + '&pageSize=2')
                .send();

            res.body.users.length.should.equal(0);

            res = await chai.request(server)
                .get('/badges/' + randomBadge.pretty_id + '/users?pageNumber=' + 2 + '&pageSize=1')
                .send();

            res.body.users.length.should.equal(1);
            should.not.exist(res.body.matchingUsers);
        });
    })
});
