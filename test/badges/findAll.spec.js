process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoBadgeRepo = require('../../models/services/mongo/mongoRepository')('badges');
const badgeCreationWrapper = require('./badgeCreationWrapper');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('badges.findAll', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /badges', () => {

        it('When no search param it should retrieve all created badges', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await mongoBadgeRepo.insert({this_badge : 'should be ignored'});

            let badgeCount = 10;

            for (let index = 0; index < badgeCount; index++) {
                let randomBadge = testUtils.generateRandomBadge();
                await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
            }

            let res = await chai.request(server)
                .get('/badges')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(badgeCount);
            res.body.totalBadges.should.equal(badgeCount);
            should.not.exist(res.body.matchingBadges);
        });

        it('Pagination should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let badgeCount = 10;

            for (let index = 0; index < badgeCount; index++) {
                let randomBadge = testUtils.generateRandomBadge();
                await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
            }

            let res = await chai.request(server)
                .get('/badges?pageNumber=' + 1 + '&pageSize=2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(2);

            res = await chai.request(server)
                .get('/badges?pageNumber=' + badgeCount + '&pageSize=2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(0);

            res = await chai.request(server)
                .get('/badges?pageNumber=' + 2 + '&pageSize=9')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(1);
        });

        it('Search should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomBadge = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

            let res = await chai.request(server)
                .get('/badges?search=' + randomBadge.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(1);
            res.body.totalBadges.should.equal(1);
            res.body.matchingBadges.should.equal(1);
        });

        it('Fields should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomBadge = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

            let res = await chai.request(server)
                .get('/badges?fields=id,name')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(1);
            res.body.totalBadges.should.equal(1);

            let badge = res.body.badges[0];

            should.exist(badge.name);
            should.exist(badge.id);
            should.not.exist(badge.description);
        });

        it('Sort should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomBadge = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

            let res = await chai.request(server)
                .get('/badges?sort=pretty_id')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(1);
        });

        it('Reverse sort should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomBadge = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

            let res = await chai.request(server)
                .get('/badges?sort=-pretty_id')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.badges.length.should.equal(1);
        });
    })
});
