process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../authenticationWrapper');
chai.use(chaiHttp);

exports.authenticateAndCreateBadge = async function authenticateAndCreateBadge(adminUser, badge, tokenObj) {
    let createdUser =  await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

    await createBadge(badge,tokenObj);

    return createdUser;
};

async function createBadge(badge, tokenObj) {
    await chai.request(server)
        .post('/badges')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send(badge);
}

exports.createBadge = createBadge;