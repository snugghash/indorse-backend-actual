const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
const fbUtils = require("../../../models/services/social/facebookService");
const sinon = require('sinon')

chai.use(chaiHttp);

/**
*
* @param user to signup
* @param accessToken tokenObj.token will hold auth token
* @returns {Promise.<created user obj>}
*/
exports.facebookAuthenticate = async function facebookAuthenticate(user,email,tokenObj) {
    let mockImageUrl = false;

    let stubFacebook = sinon.stub(fbUtils, "validateAndGetEmail").callsFake(function validatePass(){
        return [email,mockImageUrl]
    });

    let response = await chai.request(server)
    .post('/signup/facebook')
    .send(user);

    expect(response).to.have.status(200);

    response = await chai.request(server)
    .post('/auth/facebook')
    .send({facebook:user.facebook});

    expect(response).to.have.status(200);
    should.exist(response.body.token)
    tokenObj.token = response.body.token;

    response = await chai.request(server)
    .post('/me')
    .send({email: email})
    .set('Authorization', 'Bearer ' + tokenObj.token)

    expect(response).to.have.status(200);
    stubFacebook.restore();
    return response.body.profile;

};
