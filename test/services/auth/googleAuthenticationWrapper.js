const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
const gUtils = require("../../../models/services/social/googleService");
const sinon = require('sinon')

chai.use(chaiHttp);

/**
*
* @param user to signup
* @param accessToken tokenObj.token will hold auth token
* @returns {Promise.<created user obj>}
*/
exports.googleAuthenticate = async function googleAuthenticate(user,inputEmail,googleUid,tokenObj) {
    let stubGoogle = sinon.stub(gUtils, "validateAndGetEmail").callsFake(function validatePass() {
        return [inputEmail, googleUid]
    });

    let response = await chai.request(server)
    .post('/signup/google')
    .send(user);

    expect(response).to.have.status(200);

    response = await chai.request(server)
    .post('/auth/google')
    .send({google:user.google});

    expect(response).to.have.status(200);
    should.exist(response.body.token)
    tokenObj.token = response.body.token;

    response = await chai.request(server).post('/me').send({email: inputEmail}).set('Authorization', 'Bearer ' + tokenObj.token)

    expect(response).to.have.status(200);
    stubGoogle.restore();
    return response.body.profile;

};
