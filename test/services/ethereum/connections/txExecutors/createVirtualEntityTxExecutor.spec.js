process.env.NODE_ENV = 'test';

const mongooseTransactionQueueRepo = require('../../../../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const mongooseCompanyRepo = require('../../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const chai = require('chai');
const DB = require('../../../../db');
const testUtils = require('../../../../testUtils');
const config = require('config');
const createVirtualEntityTxExecutor = require('../../../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const should = chai.should();
const expect = chai.expect;
const connectionsDeployer = require('../../../../smart-contracts/connections/connectionsDeployer')
const resolverLoopWrapper = require('../../../../smart-contracts/cron/resolverLoopWrapper');
const connectionsFacade = require('../../../../../smart-contracts/services/connections/connectionsContract');


describe('executor.createVirtualEntityTxExecutor', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        await connectionsDeployer.deploy();
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should create a virtual entity', async () => {
        let createCompanyRequest = testUtils.generateRandomCompany();

        let companyCreated = await mongooseCompanyRepo.insert(createCompanyRequest)

        let metaData = {
            contract: "Connections",
            function_name: 'createVirtualEntity',
            function_args: [],
            callback_data: {
                company_id: companyCreated
            },
        }
        let initiatingUserId = "user123"

        await createVirtualEntityTxExecutor.execute(companyCreated, initiatingUserId)

        let transactionQueueDoc = await mongooseTransactionQueueRepo.findOne({
            initiating_user_id: initiatingUserId,
            'tx_metadata.function_name': 'createVirtualEntity',
            'tx_metadata.contract': 'Connections',
            'tx_metadata.callback_data.company_id': companyCreated,
            status : 'QUEUED'
        })

        should.exist(transactionQueueDoc, "Could not find the transaction in the queue")

        await resolverLoopWrapper.processTransactions();

        let companyDoc = await mongooseCompanyRepo.findOneById(companyCreated)
        let entityAddress = companyDoc.entity_ethaddress
        should.exist(entityAddress, "Ethereum address was not saved to company")

        let entity = await connectionsFacade.getEntity(entityAddress)
        entity.active.should.equal(true, "Entity was not created")
    });

    it('should create a virtual entity when there is already a transaction pending', async () => {
        let createCompanyRequest = testUtils.generateRandomCompany();

        let companyCreated = await mongooseCompanyRepo.insert(createCompanyRequest)

        let initiatingUserId = "user123"

        await createVirtualEntityTxExecutor.execute(companyCreated, initiatingUserId)
        await createVirtualEntityTxExecutor.execute(companyCreated, initiatingUserId)

        let transactionCount = await mongooseTransactionQueueRepo.count({
            initiating_user_id: initiatingUserId,
            'tx_metadata.function_name': 'createVirtualEntity',
            'tx_metadata.contract': 'Connections',
            'tx_metadata.callback_data.company_id': companyCreated,
            status : 'QUEUED'
        })

        transactionCount.should.equal(1, "There should only be one transaction in the queue")

        await resolverLoopWrapper.processTransactions();

        let companyDoc = await mongooseCompanyRepo.findOneById(companyCreated)
        let entityAddress = companyDoc.entity_ethaddress
        should.exist(entityAddress, "Ethereum address was not saved to company")

        let entity = await connectionsFacade.getEntity(entityAddress)
        entity.active.should.equal(true, "Entity was not created")
    });
});
