process.env.NODE_ENV = 'test';
const chai = require('chai');
const config = require('config');
const should = chai.should();
const expect = chai.expect;
const publishToExchange = require('../../../models/services/rabbitMq/publishToExchange');
const subscribeToQueue = require('../../../models/services/rabbitMq/subscribeToQueue');

describe('services.rabbitMq', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach(async () => {
    });

    afterEach(() => {
    });

    it('should publish and consume a message', async (done ) => {
        let message = {
            data: "I am some data"
        }
        let exchangeName = "TestExchangeName";

        try{
            await publishToExchange.publishToExchange(exchangeName, message)
            let queueName = exchangeName;

            let messageReceived = await new Promise((res, rej) => {
                try {
                    let watchOneMessage = function (message, headers, deliveryInfo, messageObject) {
                        res(message);
                    }
                    subscribeToQueue.subscribeToQueue(queueName, watchOneMessage)
                } catch(error) {
                    rej(error)
                }
            })
            messageReceived.data.should.equal(message.data)
            done();
        }catch (error){
            done(error);
        }

    })
})