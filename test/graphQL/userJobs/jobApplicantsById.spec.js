process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoUserJobsRepo = require('../../../models/services/mongo/mongoose/mongooseUserJobsRepo');


chai.use(chaiHttp);

describe('UserJobs.jobApplicantsById', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive the job applicants with the job by Id', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            let id = await mongoSkillsRepo.insert({
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation: {
                    chatbot: true
                }
            });
            skillIds.push({id: id, level: 'beginner'});
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
        let updateObj = [];
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "tmp",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                },
                    {
                        type: "tmp",
                        level: "expert",
                        id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                        validated: true
                    }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({email: email}, {$set: {skills: updateObj}})

        let operationName = 'applyJobPostTest';
        const mutationGql = `
            mutation ${operationName}($id: String!) {
                applyJobPost(id: $id) {
                    url
                    email
                } 
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {id: jobId}};
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        operationName = 'jobApplicantsByIdTest';
        const queryGql = `
            query ${operationName}($jobId: String!, $pageNumber: Int, $pageSize: Int) {
                jobApplicantsById(jobId: $jobId, pageNumber: $pageNumber, pageSize: $pageSize) {
                    applicants {
                        id
                    }
                    totalJobApplicants
                }
            }
        `;

        let adminUser = testUtils.generateRandomUser();

        let adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, adminTokenObj, 'admin');

        body = {operationName: operationName, query: queryGql, variables: {jobId: jobId, pageNumber: 1, pageSize: 10}};
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + adminTokenObj.token)
            .send(body);

        res.status.should.equal(200)

        let userFromMong = await mongoUserRepo.findOneByEmail(user.email);

        const jobApplicantsById = res.body.data.jobApplicantsById;
        jobApplicantsById.applicants[0].id.should.equal(userFromMong._id.toString());
        jobApplicantsById.totalJobApplicants.should.equal(1);
    });

    it('should sort the jobs by applied date', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            let id = await mongoSkillsRepo.insert({
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation: {
                    chatbot: true
                }
            });
            skillIds.push({id: id, level: 'beginner'});
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
        let updateObj = [];
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "tmp",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                },
                    {
                        type: "tmp",
                        level: "expert",
                        id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                        validated: true
                    }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({email: user.email}, {$set: {skills: updateObj}})

        let operationName = 'applyJobPostTest';
        const mutationGql = `
            mutation ${operationName}($id: String!) {
                applyJobPost(id: $id) {
                    url
                    email
                } 
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {id: jobId}};
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        let anotherUser = testUtils.generateRandomUser();
        let anotherTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(anotherUser, anotherTokenObj, 'profile_access');
        await mongoUserRepo.update({email: anotherUser.email}, {$set: {skills: updateObj}})

        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + anotherTokenObj.token)
            .send(body);

        operationName = 'jobApplicantsByIdTest';
        const queryGql = `
            query ${operationName}($jobId: String!, $pageNumber: Int, $pageSize: Int) {
                jobApplicantsById(jobId: $jobId, pageNumber: $pageNumber, pageSize: $pageSize) {
                    applicants {
                        id
                    }
                }
            }
        `;

        let adminUser = testUtils.generateRandomUser();

        let adminTokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, adminTokenObj, 'admin');

        body = {operationName: operationName, query: queryGql, variables: {jobId: jobId, pageNumber: 1, pageSize: 10}};
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + adminTokenObj.token)
            .send(body);

        res.status.should.equal(200)

        let userFromMong = await mongoUserRepo.findOneByEmail(user.email);
        let anotherUserFromMong = await mongoUserRepo.findOneByEmail(anotherUser.email);

        const jobApplicantsById = res.body.data.jobApplicantsById;
        jobApplicantsById.applicants[0].id.should.equal(userFromMong._id.toString());
        jobApplicantsById.applicants[1].id.should.equal(anotherUserFromMong._id.toString());

    });

    it('should fail for non-admin', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            let id = await mongoSkillsRepo.insert({
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString(),
                validation: {
                    chatbot: true
                }
            });
            skillIds.push({id: id, level: 'beginner'});
        }

        skillIds.pop()
        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();

        let jobId = await mongoJobRepo.insert(jobPost);

        let user = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, 'profile_access');
        let updateObj = [];
        for (skill of skillIds) {
            let obj = {
                skill: {
                    name: "test",
                    category: "tmp",
                    _id: skill.id
                },
                level: 'beginner',
                validations: [{
                    type: "tmp",
                    level: "beginner",
                    id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                    validated: false
                },
                    {
                        type: "tmp",
                        level: "expert",
                        id: "fake_5ccd43a0-792f-11e8-8e45-2d7dae691d73",
                        validated: true
                    }]
            }
            updateObj.push(obj);
        }

        await mongoUserRepo.update({email: email}, {$set: {skills: updateObj}})

        let operationName = 'applyJobPostTest';
        const mutationGql = `
            mutation ${operationName}($id: String!) {
                applyJobPost(id: $id) {
                    url
                    email
                } 
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {id: jobId}};
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        operationName = 'jobApplicantsByIdTest';
        const queryGql = `
            query ${operationName}($jobId: String!, $pageNumber: Int, $pageSize: Int) {
                jobApplicantsById(jobId: $jobId, pageNumber: $pageNumber, pageSize: $pageSize) {
                    applicants {
                        id
                    }
                }
            }
        `;

        body = {operationName: operationName, query: queryGql, variables: {jobId: jobId, pageNumber: 1, pageSize: 10}};
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        should.exist(res.body.errors);
    });


});