process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo')
const githubService = require('../../../models/services/social/githubService');
const mongooseUserGithubRepo = require('../../../models/services/mongo/mongoose/mongooseUserGithubRepo')
const sinon = require('sinon')


chai.use(chaiHttp);
describe('Github Login test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    // This is the case when a user has email A, and that user try to signup/login with Github account with email A
    it('should login a user with existing account with email', async (done) => {
        let accessToken = 'ThisIsAccessToken';

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid_is_this';
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }
        let tokenObj = {};
        let stubGithubRedirect = sinon.stub(githubService, "exchangeAuthorizationCode").callsFake(function validatePass(){
            return accessToken
        });

        let stubGetResponseWithAccessToken = sinon.stub(githubService, "getGithubUserAndRepoData").callsFake(function validatePass(){
            return githubFakeResponse
        });

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');

        let githubLoginRequest = {
            code: 'abc',
            redirect_uri: 'https://github.com',
            state: 'state'
        }
        let loginRequest = `					 
            mutation (
                $code: String = "${githubLoginRequest.code}",
                $redirect_uri: String = "${githubLoginRequest.redirect_uri}",
                $state: String = "${githubLoginRequest.state}") {
					    githubLogin(code: $code, 
					                redirect_uri: $redirect_uri, 
					                state: $state
					                ){
					        userExists
					        token
					    }
					 }
            `;

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseData = res.body.data;

            should.exist(responseData);
            responseData.githubLogin.userExists.should.equal(true);

            stubGithubRedirect.restore();
            stubGetResponseWithAccessToken.restore();

            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            user.github_uid.should.equal(githubUidOfRandomUser);

            done();


        } catch (error) {
            done(error);
        }

    });

    it('should not login a user with corrupted access token', async (done) => {
        let accessToken = 'ThisIsCorruptedAccessTokenWhichWontbeStubbed';

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid_is_this';
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }
        let tokenObj = {};
        let stubGithubRedirect = sinon.stub(githubService, "exchangeAuthorizationCode").callsFake(function validatePass(){
            return accessToken
        });

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');

        let githubLoginRequest = {
            code: 'abc',
            redirect_uri: 'https://github.com',
            state: 'state'
        }
        let loginRequest = `					 
            mutation (
                $code: String = "${githubLoginRequest.code}",
                $redirect_uri: String = "${githubLoginRequest.redirect_uri}",
                $state: String = "${githubLoginRequest.state}") {
					    githubLogin(code: $code, 
					                redirect_uri: $redirect_uri, 
					                state: $state
					                ){
					        userExists
					        token
					    }
					 }
            `;

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseData = res.body.data;
            let errorResponse = res.body.errors[0];

            should.exist(errorResponse);
            errorResponse.message.should.equal("Github authentication failed")

            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            should.not.exist(user.github_uid);
            stubGithubRedirect.restore();
            done();


        } catch (error) {
            done(error);
        }

    });

    it('should not login a user with corrupted code / state / redirectUri', async (done) => {

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid_is_this';
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }
        let tokenObj = {};

        let stubGetResponseWithAccessToken = sinon.stub(githubService, "getGithubUserAndRepoData").callsFake(function validatePass(){
            return githubFakeResponse
        });

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');

        let githubLoginRequest = {
            code: 'abc',
            redirect_uri: 'https://github.com',
            state: 'state'
        }
        let loginRequest = `					 
            mutation (
                $code: String = "${githubLoginRequest.code}",
                $redirect_uri: String = "${githubLoginRequest.redirect_uri}",
                $state: String = "${githubLoginRequest.state}") {
					    githubLogin(code: $code, 
					                redirect_uri: $redirect_uri, 
					                state: $state
					                ){
					        userExists
					        token
					    }
					 }
            `;

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseData = res.body.data;
            let errorResponse = res.body.errors[0];

            should.exist(errorResponse);
            errorResponse.message.should.equal("Github authentication failed")

            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            should.not.exist(user.github_uid);
            stubGetResponseWithAccessToken.restore();
            done();


        } catch (error) {
            done(error);
        }

    });


    // 1. signup by email -> link -> then login.
    it('should login a user with github (1. signup by email 2. link with github 3. then login with github)', async (done) => {
        let accessToken = 'ThisIsAccessToken';

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid'
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }
        let tokenObj = {};
        let stubGithubRedirect = sinon.stub(githubService, "exchangeAuthorizationCode").callsFake(function validatePass(){
            return accessToken
        });

        let stubGetResponseWithAccessToken = sinon.stub(githubService, "getGithubUserAndRepoData").callsFake(function validatePass(){
            return githubFakeResponse
        });

        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');

        let githubLoginRequest = {
            code: 'abc',
            redirect_uri: 'https://github.com',
            state: 'state'
        }
        let loginRequest = `					 
            mutation (
                $code: String = "${githubLoginRequest.code}",
                $redirect_uri: String = "${githubLoginRequest.redirect_uri}",
                $state: String = "${githubLoginRequest.state}") {
					    githubLogin(code: $code, 
					                redirect_uri: $redirect_uri, 
					                state: $state
					                ){
					        userExists
					        token
					    }
					 }
            `;

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseData = res.body.data;

            should.exist(responseData);
            responseData.githubLogin.userExists.should.equal(true);

            res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseDataSecond = res.body.data;

            should.exist(responseDataSecond);
            responseData.githubLogin.userExists.should.equal(true);

            stubGithubRedirect.restore();
            stubGetResponseWithAccessToken.restore();

            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            user.github_uid.should.equal(githubUidOfRandomUser);

            done();


        } catch (error) {
            done(error);
        }

    });


})

