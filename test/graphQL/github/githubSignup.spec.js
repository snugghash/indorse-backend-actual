process.env.NODE_ENV = 'test';

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const DB = require('../../db');
const server = require('../../../server');
const testUtils = require('../../testUtils');
const should = chai.should();
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo')
const githubService = require('../../../models/services/social/githubService');
const mongooseUserGithubRepo = require('../../../models/services/mongo/mongoose/mongooseUserGithubRepo')
const sinon = require('sinon')
const randtoken = require('rand-token');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo')
const mongoValidatorsRepo = require('../../../models/services/mongo/mongoose/mongooseValidatorRepo');


chai.use(chaiHttp);
describe('Github Signup test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    /*
        Signup flow
        1. FE call githubLogin -> return userExists false
        2. FE call githubSignup with IND accesstoken -> then signup a user
     */
    it('should signup a user with Github', async (done) => {
        let accessToken = 'ThisIsAccessToken';

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid_is_this';
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }

        let stubGithubRedirect = sinon.stub(githubService, "exchangeAuthorizationCode").callsFake(function validatePass() {
            return accessToken
        });
        let stubGetResponseWithAccessToken = sinon.stub(githubService, "getGithubUserAndRepoData").callsFake(function validatePass() {
            return githubFakeResponse
        });


        let githubLoginRequest = {
            code: 'abc',
            redirect_uri: 'https://github.com',
            state: 'state'
        }
        let loginRequest = `					 
            mutation (
                $code: String = "${githubLoginRequest.code}",
                $redirect_uri: String = "${githubLoginRequest.redirect_uri}",
                $state: String = "${githubLoginRequest.state}") {
					    githubLogin(code: $code, 
					                redirect_uri: $redirect_uri, 
					                state: $state
					                ){
					        userExists
					        token
					    }
					 }
            `;

        try {
            let res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseData = res.body.data;

            should.exist(responseData);
            responseData.githubLogin.userExists.should.equal(false);
            let indToken = responseData.githubLogin.token;
            should.exist(indToken);
            let userNotExist = await mongooseUserRepo.findOneByEmail(randomUser.email);
            should.not.exist(userNotExist);
            let signupRequest = `
                mutation (
                        $token: String = "${indToken}",
                        $username: String = "${randomUser.username}",
                        $name: String = "${randomUser.name}") {
                                githubSignup(token: $token, 
                                            username: $username, 
                                            name: $name
                                            ){
                                    token
                                }
                             }
        `
            res = await chai.request(server)
                .post('/graphql')
                .send({query: signupRequest});
            responseData = res.body.data;
            should.exist(responseData);
            should.exist(responseData.githubSignup.token);

            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            user.github_uid.should.equal(githubUidOfRandomUser);

            stubGithubRedirect.restore();
            stubGetResponseWithAccessToken.restore();


            done();


        } catch (error) {
            done(error);
        }

    });

    it('should signup a user with Github and a claim', async (done) => {
        let accessToken = 'ThisIsAccessToken';

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid_is_this';
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }

        // Start of claim
        // Following lines are a copy of logics from uploadLinkedinArchive.js
        // If createClaimDraft logic changed, following lines need to be changed as well.
        let claimDraft = {
            title:'Claim title',
            desc:'Description',
            proof:'https://indorse.io',
            level:'intermediate',
            email: randomUser.email
        };

        claimDraft.token = randtoken.generate(16);
        let claimToken = claimDraft.token;
        await mongooseClaimDraftsRepo.insert(claimDraft);
        // End of claim

        let stubGithubRedirect = sinon.stub(githubService, "exchangeAuthorizationCode").callsFake(function validatePass() {
            return accessToken
        });
        let stubGetResponseWithAccessToken = sinon.stub(githubService, "getGithubUserAndRepoData").callsFake(function validatePass() {
            return githubFakeResponse
        });


        let githubLoginRequest = {
            code: 'abc',
            redirect_uri: 'https://github.com',
            state: 'state'
        }
        let loginRequest = `					 
            mutation (
                $code: String = "${githubLoginRequest.code}",
                $redirect_uri: String = "${githubLoginRequest.redirect_uri}",
                $state: String = "${githubLoginRequest.state}") {
					    githubLogin(code: $code, 
					                redirect_uri: $redirect_uri, 
					                state: $state
					                ){
					        userExists
					        token
					    }
					 }
            `;

        try {

            let skills_array = Array('Claim title');

            //Insert test validators for this skill
            await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
            await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
            await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
            await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
            await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});

            let res = await chai.request(server)
                .post('/graphql')
                .send({query: loginRequest});
            let responseData = res.body.data;

            should.exist(responseData);
            responseData.githubLogin.userExists.should.equal(false);
            let indToken = responseData.githubLogin.token;
            should.exist(indToken);
            let userNotExist = await mongooseUserRepo.findOneByEmail(randomUser.email);
            should.not.exist(userNotExist);
            let signupRequest = `
                mutation (
                        $token: String = "${indToken}",
                        $username: String = "${randomUser.username}",
                        $name: String = "${randomUser.name}",
                        $claimToken: String = "${claimToken}") {
                                githubSignup(token: $token, 
                                            username: $username, 
                                            name: $name,
                                            claimToken: $claimToken
                                            ){
                                    token
                                }
                             }
        `
            let resSecond = await chai.request(server)
                .post('/graphql')
                .send({query: signupRequest});

            let responseDataSecond = resSecond.body.data;
            should.exist(responseDataSecond);
            should.exist(responseDataSecond.githubSignup.token);

            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            user.github_uid.should.equal(githubUidOfRandomUser);

            let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(claimToken);
            claimDraft.finalized.should.equal(true);
            stubGithubRedirect.restore();
            stubGetResponseWithAccessToken.restore();


            done();


        } catch (error) {
            done(error);
        }

    });

    it('should not signup a user with fake JWT', async (done) => {
        let accessToken = 'ThisIsAccessToken';

        let randomUser = testUtils.generateRandomUser();
        let githubUidOfRandomUser = 'github_uid_is_this';
        let githubFakeResponse = {
            id: githubUidOfRandomUser,
            name: randomUser.name,
            email: randomUser.email,
            avatarUrl: 'avatar_url'
        }

        let stubGithubRedirect = sinon.stub(githubService, "exchangeAuthorizationCode").callsFake(function validatePass() {
            return accessToken
        });
        let stubGetResponseWithAccessToken = sinon.stub(githubService, "getGithubUserAndRepoData").callsFake(function validatePass() {
            return githubFakeResponse
        });




        try {

            let properIndTokenThatIsNotSigned = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiamFlIiwidXNlcm5hbWUiOiJ0ZWNoeWpheSIsImFjY2Vzc1Rva2VuIjoiYWNjZXNzIiwiZW1haWwiOiJlbWFpbC5pbmRvcnNlLmlvIiwiaWF0IjoxNTE2MjM5MDIyfQ.NaeJ-OiwtDhrcZvPVvlrrcemh3mIFQoI8Au4msszi_8';
            let signupRequest = `
                mutation (
                        $token: String = "${properIndTokenThatIsNotSigned}",
                        $username: String = "${randomUser.username}",
                        $name: String = "${randomUser.name}") {
                                githubSignup(token: $token, 
                                            username: $username, 
                                            name: $name
                                            ){
                                    token
                                }
                             }
        `
            let res = await chai.request(server)
                .post('/graphql')
                .send({query: signupRequest});


            let responseData = res.body.data;
            let errorResponse = res.body.errors[0];

            should.exist(errorResponse);
            should.not.exist(responseData.githubSignup);
            errorResponse.message.should.equal("Github indAccess Token error")


            let user = await mongooseUserRepo.findOneByEmail(randomUser.email);
            should.not.exist(user);

            stubGithubRedirect.restore();
            stubGetResponseWithAccessToken.restore();


            done();


        } catch (error) {
            done(error);
        }

    });


})

