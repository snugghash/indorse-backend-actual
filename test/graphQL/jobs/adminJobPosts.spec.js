process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp);

describe('Jobs.adminJobPosts', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should retreive all jobs', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let id = await mongoSkillsRepo.insert({ name: testUtils.generateRandomString(), category: testUtils.generateRandomString()});
            skillIds.push({ id: id, level:'beginner'});
        }

        jobPost.skills = skillIds;
        
        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();
        await mongoJobRepo.insert(jobPost);

        let getAllJobsQuery = 
        `query {
                adminJobPosts(pageNumber: 1, pageSize: 10) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                totalJobPosts                
                }
            }`;

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: getAllJobsQuery }); 


        res.body.data.adminJobPosts.jobPosts.length.should.equal(1);
    });

    it('should fail to return posts when requested by non admin', async () => {
        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let id = await mongoSkillsRepo.insert({ name: testUtils.generateRandomString(), category: testUtils.generateRandomString()});
            skillIds.push({ id: id, level:'beginner'});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);
        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();
        await mongoJobRepo.insert(jobPost);

        let getAllJobsQuery =
            `query {
                adminJobPosts(pageNumber: 1, pageSize: 10) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                totalJobPosts                
                }
            }`;

        let regularUser = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticate(regularUser, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ query: getAllJobsQuery });

        should.exist(res.body.errors);
    });

    it('should filter jobs', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let id = await mongoSkillsRepo.insert({ name: testUtils.generateRandomString(), category: testUtils.generateRandomString()});
            skillIds.push({ id: id, level:'beginner'});
        }

        jobPost.skills = skillIds;
        
        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);        
        

        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();
        await mongoJobRepo.insert(jobPost);

        let operationName = 'filterJobPosts';

        let getAllJobsQuery = 
        `query  ${operationName}($pageNumber: Int!, $pageSize: Int!, $search: String) {
                adminJobPosts(pageNumber: $pageNumber, pageSize: $pageSize, search: $search) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                    matchingJobPosts
                    totalJobPosts                
                }
            }`;

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        // Correct title
        let variables = { pageNumber: 1, pageSize: 1, search: jobPost.title };
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(1);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(1);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Wrong title
        variables = { pageNumber: 1, pageSize: 1, search: jobPost.title + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(0);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(0);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Correct location
        variables = { pageNumber: 1, pageSize: 1, search: jobPost.location };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(1);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(1);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Wrong location
        variables = { pageNumber: 1, pageSize: 1, search: jobPost.location + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(0);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(0);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Correct companyName
        variables = { pageNumber: 1, pageSize: 1, search: createCompanyRequest.companyName };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(1);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(1);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Wrong companyName
        variables = { pageNumber: 1, pageSize: 1, search: createCompanyRequest.companyName + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(0);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(0);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Correct skill name
        // Correct skill name
        let skill = mongoSkillsRepo.findOneById(skillIds[0].id);
        variables = { pageNumber: 1, pageSize: 1, search: skill.name };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });
        res.body.data.adminJobPosts.jobPosts.length.should.equal(1);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(1);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);

        // Wrong skill name
        variables = { pageNumber: 1, pageSize: 1, search: skill.name + testUtils.generateRandomString() };
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ operationName, query: getAllJobsQuery, variables });

        res.body.data.adminJobPosts.jobPosts.length.should.equal(0);
        res.body.data.adminJobPosts.matchingJobPosts.should.equal(0);
        res.body.data.adminJobPosts.totalJobPosts.should.equal(1);
    });

    it('should sort jobs', async () => {

        let jobPost1 = await testUtils.createRandomJob();
        let jobPost2 = await testUtils.createRandomJob();

        let skillIds = []
        for (i = 0; i < 3; ++i) {
            let id = await mongoSkillsRepo.insert({
                name: testUtils.generateRandomString(),
                category: testUtils.generateRandomString()
            });
            skillIds.push({id: id, level: 'beginner'});
        }

        jobPost1.skills = skillIds;
        jobPost2.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

        jobPost1.title = '1' + jobPost1.title;
        jobPost1.location = '1' + jobPost1.location;
        jobPost1.experienceLevel = 'intern';
        jobPost1.submitted.at = testUtils.getCurrentTimestamp();
        jobPost2.updated.at = jobPost1.submitted.at + 1;
        jobPost1.company = {};
        jobPost1.company.id = companyId;
        jobPost1.company.description = testUtils.generateRandomString();
        jobPost1._id = await mongoJobRepo.insert(jobPost1);

        jobPost2.title = '2' + jobPost2.title;
        jobPost2.location = '2' + jobPost2.location;
        jobPost2.experienceLevel = 'junior';
        jobPost2.submitted.at = jobPost1.submitted.at + 1;
        jobPost2.updated.at = jobPost2.submitted.at + 1;
        jobPost2.company = {};
        jobPost2.company.id = companyId;
        jobPost2.company.description = testUtils.generateRandomString();
        jobPost2._id = await mongoJobRepo.insert(jobPost2);

        let operationName = 'sortJobPosts';

        let getAllJobsQuery =
            `query  ${operationName}($pageNumber: Int!, $pageSize: Int!, $sort: JobsSortParam) {
                adminJobPosts(pageNumber: $pageNumber, pageSize: $pageSize, sort: $sort) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                    totalJobPosts                
                }
            }`;

        let adminUser = testUtils.generateRandomUser();

        let tokenObj = {};
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        // Default order: submitted at desc
        let variables = {pageNumber: 1, pageSize: 2};
        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost1._id);

        // Submitted at asc
        variables.sort = 'asc_submitted_at';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost2._id);


        // Title asc
        variables.sort = 'asc_title';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Title desc
        variables.sort = 'desc_title';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost1._id);


        // Experience asc
        variables.sort = 'asc_experience_level';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Experience desc
        variables.sort = 'desc_experience_level';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost1._id);


        // Location asc
        variables.sort = 'asc_location';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Location desc
        variables.sort = 'desc_location';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost2._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost1._id);


        // Updated at asc
        variables.sort = 'asc_updated_at';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost1._id);
        res.body.data.adminJobPosts.jobPosts[1].id.should.equal(jobPost2._id);

        // Updated at desc
        variables.sort = 'desc_updated_at';
        res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({operationName, query: getAllJobsQuery, variables});

        res.body.data.adminJobPosts.jobPosts[0].id.should.equal(jobPost2._id);
    })

    it('should fail if not logged in', async () => {
        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let id = await mongoSkillsRepo.insert({ name: testUtils.generateRandomString(), category: testUtils.generateRandomString()});
            skillIds.push({ id: id, level:'beginner'});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);

        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();
        await mongoJobRepo.insert(jobPost);

        let getAllJobsQuery =
            `query {
                adminJobPosts(pageNumber: 1, pageSize: 10) {
                    jobPosts {
                        id
                        title
                        experienceLevel
                        description
                        monthlySalary
                        company{
                            id
                            pretty_id
                            name
                            description
                        }
                        contactEmail
                        applicationLink
                        skills{
                            id
                            name
                            category
                            level
                        }
                        location
                        submitted{
                            at
                            by
                        }
                        approved{
                            at
                            by
                            approved
                        }
                        updated{
                            at
                            by
                        }
                    }
                totalJobPosts                
                }
            }`;

        let res = await chai.request(server)
            .post('/graphql')
            .send({ query: getAllJobsQuery });

        should.exist(res.body.errors);
    });
});