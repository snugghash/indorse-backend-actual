process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const mongooseJobsRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');

chai.use(chaiHttp);

describe('Jobs.createJobPost', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should create a new job', async () => {

        const skill = testUtils.generateRandomUserSkill();
        const skillInMongoId = await mongooseSkillRepo.insert(skill);

        const skillToAdd = {
            id: skillInMongoId,
            level: 'beginner'
        };

        const company = {
            name: testUtils.generateRandomString(),
            description: testUtils.generateRandomString(),
            logo: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QkODCkbOzrYpgAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAANElEQVR42u3BAQ0AAADCoPdPbQ43oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfgx1lAABLpXWfAAAAABJRU5ErkJggg=="
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], company);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                    monthlySalary
                    company {
                        id
                        pretty_id
                        name
                        logo {
                            s3Url
                            ipfsUrl
                        }
                        description
                    }
                    contactEmail
                    applicationLink
                    skills {
                        id
                        name
                        category
                        level
                    }
                    location
                    submitted {
                        by
                        at
                    }
                    approved {
                        by
                        at
                        approved
                    }
                    updated {
                        by
                        at
                    }
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        res.status.should.equal(200);
        const returnedJobPost = res.body.data.createJobPost;
        returnedJobPost.experienceLevel.should.equal(createJobRequest.experienceLevel);
        should.exist(returnedJobPost.submitted.at);
        returnedJobPost.skills[0].name.should.equal(skill.name);
        returnedJobPost.skills[0].category.should.equal(skill.category);
        
        const jobFromMongo = await mongooseJobsRepo.findOne({title: createJobRequest.title});
        jobFromMongo.description.should.equal(createJobRequest.description);
        let companyFromMongo = await mongooseCompanyRepo.findOneById(jobFromMongo.company.id);
        companyFromMongo.company_name.should.equal(createJobRequest.company.name);
    });

    it('should create a new with a company that is found by name', async () => {

        const skill = testUtils.generateRandomUserSkill();
        const skillInMongoId = await mongooseSkillRepo.insert(skill);

        const skillToAdd = {
            id: skillInMongoId,
            level: 'beginner'
        };

        const company = testUtils.generateRandomCompany();
        await mongooseCompanyRepo.insert(company);
        const companyToAdd = {
            name: company.company_name,
            description: company.description
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], companyToAdd);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                    company {
                        id
                        name
                        pretty_id
                        description
                    }
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        res.status.should.equal(200);
        const returnValue = res.body.data.createJobPost;
        returnValue.experienceLevel.should.equal(createJobRequest.experienceLevel);
        should.exist(returnValue.company.pretty_id);

        const jobFromMongo = await mongooseJobsRepo.findOne({title: createJobRequest.title});
        jobFromMongo.description.should.equal(createJobRequest.description);

    });

    it('should create a new with a company that is found by id', async () => {

        const skill = testUtils.generateRandomUserSkill();
        const skillInMongoId = await mongooseSkillRepo.insert(skill);

        const skillToAdd = {
            id: skillInMongoId,
            level: 'beginner'
        };

        const company = testUtils.generateRandomCompany();
        const company_id = await mongooseCompanyRepo.insert(company);
        const companyToAdd = {
            id: company_id.toString(),
            description: company.description
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], companyToAdd);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                    company {
                        id
                        name
                        pretty_id
                        description
                    }
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        res.status.should.equal(200);
        const returnValue = res.body.data.createJobPost;
        returnValue.experienceLevel.should.equal(createJobRequest.experienceLevel);
        should.exist(returnValue.company.pretty_id);

        const jobFromMongo = await mongooseJobsRepo.findOne({title: createJobRequest.title});
        jobFromMongo.description.should.equal(createJobRequest.description);

    });

    it('should fail to create a new with a company if the company cannot be found by id', async () => {

        const skill = testUtils.generateRandomUserSkill();
        const skillInMongoId = await mongooseSkillRepo.insert(skill);

        const skillToAdd = {
            id: skillInMongoId,
            level: 'beginner'
        };

        const company = testUtils.generateRandomCompany();
        const companyToAdd = {
            id: '5b509228ec80001758551a63',
            description: company.description
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], companyToAdd);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        let error = res.body.errors[0];
        should.exist(error.message);
    });

    it('should fail to create a job if the skill id cannot be found', async () => {

        const skillToAdd = {
            id: '5b509228ec80001758551a63',
            level: 'beginner'
        };

        const company = {
            name: testUtils.generateRandomString(),
            description: testUtils.generateRandomString(),
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], company);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        let error = res.body.errors[0];
        should.exist(error.message);
    });

    it('should sanitize malicious HTML coce NoSQL injection attack', async () => {

        const skill = testUtils.generateRandomUserSkill();
        const skillInMongoId = await mongooseSkillRepo.insert(skill);

        const skillToAdd = {
            id: skillInMongoId,
            level: 'beginner'
        };

        const company = testUtils.generateRandomCompany();
        const company_id = await mongooseCompanyRepo.insert(company);
        const xxsAttack = '<b>hello</b><img src="http://asdf"><a href="javascript:alert(0)"><script src="http://dfd"><\/script>';
        const santizedAttack = '<b>hello</b><img><a></a>';
        const companyToAdd = {
            id: company_id.toString(),
            description: xxsAttack
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], companyToAdd);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                    company {
                        id
                        name
                        pretty_id
                        description
                    }
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        res.status.should.equal(200);
        const returnedJobPost = res.body.data.createJobPost;
        returnedJobPost.company.description.should.equal(santizedAttack);

    });

    it('should fail due to malicious object in query', async () => {

        const skill = testUtils.generateRandomUserSkill();
        const skillInMongoId = await mongooseSkillRepo.insert(skill);

        const xxsAttack = {$ne : ''}
        const skillToAdd = {
            id: xxsAttack,
            level: 'beginner'
        };

        const company = testUtils.generateRandomCompany();
        const company_id = await mongooseCompanyRepo.insert(company);
        const companyToAdd = {
            id: company_id.toString(),
            description: company.description
        }
        const createJobRequest = testUtils.generateRandomJob([skillToAdd], companyToAdd);

        const operationName = 'createJobPostTest';
        const mutationGql = `
            mutation ${operationName}($form: JobPostForm!) {
                createJobPost(form: $form) {
                    id
                    title
                    experienceLevel
                    description
                    company {
                        id
                        name
                        pretty_id
                        description
                    }
                }
            }
        `;

        let body = {operationName: operationName, query: mutationGql, variables: {form: createJobRequest}};

        let res = await chai.request(server)
            .post('/graphql')
            .send(body);

        let error = res.body.errors[0];
        should.exist(error.message);

    });
});