process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoJobRepo = require('../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');


chai.use(chaiHttp);

describe('Jobs.deleteJobPost', function () {

    beforeEach(async (done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should delete a created job', async () => {

        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let id = await mongoSkillsRepo.insert({ name: testUtils.generateRandomString(), category: testUtils.generateRandomString()});
            skillIds.push({ id: id, level:'beginner'});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();
        let jobToDelete = await mongoJobRepo.insert(jobPost);

        console.log("Job to delete is " + jobToDelete);

        const operationName = 'deleteJobPostTest';
        const queryGql = `
            mutation ${operationName}($id: String!) {
                deleteJobPost(id: $id)
            }
        `;


        let body = { operationName: operationName, query: queryGql, variables: { id: jobToDelete } };
        let tokenObj = {};

        let adminUser = testUtils.generateRandomUser();
        let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        //Ensure job is deleted
        let returnVal = await mongoJobRepo.findOneById(jobToDelete);
        should.not.exist(returnVal);
    });

    it('should fail for a non-admin', async function() {
        let jobPost = await testUtils.createRandomJob();

        let skillIds =[]
        for (i=0;i<3;++i){
            let id = await mongoSkillsRepo.insert({ name: testUtils.generateRandomString(), category: testUtils.generateRandomString()});
            skillIds.push({ id: id, level:'beginner'});
        }

        jobPost.skills = skillIds;

        let createCompanyRequest = testUtils.generateRandomCompany();
        let companyId = await mongoCompanyRepo.insert(createCompanyRequest);


        jobPost.company = {};
        jobPost.company.id = companyId;
        jobPost.company.description = testUtils.generateRandomString();
        let jobToDelete = await mongoJobRepo.insert(jobPost);

        console.log("Job to delete is " + jobToDelete);

        const operationName = 'deleteJobPostTest';
        const queryGql = `
            mutation ${operationName}($id: String!) {
                deleteJobPost(id: $id)
            }
        `;


        let body = { operationName: operationName, query: queryGql, variables: { id: jobToDelete } };
        let tokenObj = {};

        let regularUser = testUtils.generateRandomUser();
        await authenticationWrapper.signupVerifyAuthenticate(regularUser, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(body);

        should.exist(res.body.errors);
        should.not.exist(res.body.data.deletedJobPost);
    })
});