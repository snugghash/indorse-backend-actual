process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const authenticationWrapper = require('../../authenticationWrapper');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo')
const mongoValidatorsRepo = require('../../../models/services/mongo/mongoRepository')('validators');

chai.use(chaiHttp);

describe('createClaimDraft test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should create draft for existing user', async () => {

        let adminUser = testUtils.generateRandomUser();
        let createDraftRequest = testUtils.generateRandomClaimDraftCreationRequest();
        let tokenObj = {};
        let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        let mutationRequest = `                  
            mutation {
                  createClaimDraft(email: "${createdUser.email}",title:"${createDraftRequest.title}",desc:"${createDraftRequest.desc}",proof:"${createDraftRequest.proof}",level:beginner){
                    userExists
                  }
                }
            `;

        let res = await chai.request(server)
            .post('/graphql')
            .send({query: mutationRequest});
        res.body.data.createClaimDraft.userExists.should.equal(true);

        let createdDraft = await mongooseClaimDraftsRepo.findOneByEmail(createdUser.email);
        let skills_array = Array(createDraftRequest.title);

        //Insert test validators for this skill
        await mongoValidatorsRepo.insert({"user_id" : createdUser._id,"skills" : skills_array});
        await mongoValidatorsRepo.insert({"user_id" : createdUser._id,"skills" : skills_array});
        await mongoValidatorsRepo.insert({"user_id" : createdUser._id,"skills" : skills_array});
        await mongoValidatorsRepo.insert({"user_id" : createdUser._id,"skills" : skills_array});
        await mongoValidatorsRepo.insert({"user_id" : createdUser._id,"skills" : skills_array});

        should.exist(createdDraft);
        should.exist(createdDraft.token);

        mutationRequest = `					 
            mutation (
                $token: String= "${createdDraft.token}"){
					    submitClaimDraftToken(token: $token){
                            email
		                    draftFinalized
                            userExists
					    }
					 }
            `;

        res = await chai.request(server)
            .post('/graphql')
            .send({query: mutationRequest});

        res.body.data.submitClaimDraftToken.email.should.equal(adminUser.email);
        res.body.data.submitClaimDraftToken.draftFinalized.should.equal(true);
    });
});
