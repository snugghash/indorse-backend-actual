process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../../server');
const should = chai.should();
const DB = require('../../db');
const config = require('config');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const testUtils = require('../../testUtils');
const createClaimDraftWrapper = require('./createClaimDraftWrapper');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');

chai.use(chaiHttp);

describe('airbitzWithToken.spec.js', function () {

    this.timeout(config.get('test.timeout'));
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should create a new user with claimToken', async () => {

        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4=';
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg=';
        let buf_v = '28';
        let signatureBuf = {buf_r, buf_s, buf_v};

        let claimDraft = await createClaimDraftWrapper.createClaimDraftNonExistingUser();

        let email = claimDraft.email;

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: email,
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf,
            claimToken : claimDraft.token
        };

        let res = await chai.request(server)
            .post('/signup/airbitz')
            .send(userSignup);

        res.should.have.status(200);
        let users = DB.getDB().collection('users');
        let item = await users.findOne({email: userSignup.email});
        item.name.should.equal(userSignup.name);
        item.email.should.equal(userSignup.email);
        item.ethaddress.should.equal(userSignup.ethaddress);
        item.is_airbitz_user.should.equal(true);
        item.ethaddressverified.should.equal(true);

        let mongoDoc = await mongoUserRepo.findOne({
            '$or': [{'email': userSignup.email},
                {'username': userSignup.username}]
        });

        mongoDoc.email.should.equal(userSignup.email);
        mongoDoc.username.should.equal(userSignup.username);

        let finalizedDraft = await mongooseClaimDraftsRepo.findOneByEmail(email);
        finalizedDraft.finalized.should.equal(true);
    })
});
