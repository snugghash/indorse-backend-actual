const chai = require('chai');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const mongooseClaimDraftsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongoValidatorsRepo = require('../../../models/services/mongo/mongoRepository')('validators');

exports.createClaimDraftNonExistingUser = async function createClaimDraftNonExistingUser() {

    let createDraftRequest = testUtils.generateRandomClaimDraftCreationRequest();

    let randomEmail = testUtils.generateRandomEmail();

    let mutationRequest = `                  
            mutation {
                  createClaimDraft(email: "${randomEmail}",title:"${createDraftRequest.title}",desc:"${createDraftRequest.desc}",proof:"${createDraftRequest.proof}",level:beginner){
                    userExists
                  }
                }
            `;

    await chai.request(server)
        .post('/graphql')
        .send({query: mutationRequest});

    let createdDraft = await mongooseClaimDraftsRepo.findOneByEmail(randomEmail);

    should.exist(createdDraft);
    should.exist(createdDraft.token);

    let skills_array = Array(createDraftRequest.title);

    //Insert test validators for this skill
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});
    await mongoValidatorsRepo.insert({"user_id" : 'some-id',"skills" : skills_array});

    return createdDraft;
};
