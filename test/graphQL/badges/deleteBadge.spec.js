process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');


chai.use(chaiHttp);

describe('deleteBadge test ', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('it should delete a badge given valid request', async () => {
        let adminUser = testUtils.generateRandomUser();

        let createBadgeRequest = testUtils.generateRandomBadge();

        let deleteBadgeQuery = `mutation ($id: String = "${createBadgeRequest.pretty_id}") {
                deleteBadge (id: $id)
                }
            `;

        let tokenObj = {};

        await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: deleteBadgeQuery});

        let deleteId = res.body.data.deleteBadge;
        deleteId.should.equal(createBadgeRequest.pretty_id);

        let deletedBadge = await mongoBadgeRepo.findOne({id :deleteId });
        should.not.exist(deletedBadge);
    });

    it('it should fail deleting a badge when user is not admin', async () => {
        let adminUser = testUtils.generateRandomUser();
        let normalUser = testUtils.generateRandomUser();

        let createBadgeRequest = testUtils.generateRandomBadge();

        let deleteBadgeQuery = `mutation ($id: String = "${createBadgeRequest.pretty_id}") {
                deleteBadge (id: $id)
                }
            `;

        let tokenObjNotUsing = {};
        let tokenObj = {};

        await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObjNotUsing);
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: deleteBadgeQuery});

        res.body.errors[0].message.should.equal("Unauthorized");

        let notDeletedBadge = await mongoBadgeRepo.findOne({id :createBadgeRequest.pretty_id });
        should.exist(notDeletedBadge);
    });

});