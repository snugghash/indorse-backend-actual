process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');


chai.use(chaiHttp);

describe('createBadge test', function () {
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('When no search param it should retrieve all users', async () => {

        let randomBadge = testUtils.generateRandomBadge();

        let adminUser = testUtils.generateRandomUser();

        let adminUser2 = testUtils.generateRandomUser();

        let pretty_id = randomBadge.pretty_id;

        let userCount = 2;


        let tokenObj = {};
        let tokenObj1 = {};

        let getBadgeUsersQuery = `query {
                allBadgeUsers (id: "${randomBadge.pretty_id}") {
                        users{
                            username
                            name
                            bio
                            img_url
                        }
                        totalUsers
                                    
                }
                }
            `;

        let createdAdmin1 = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
        let createdAdmin2 = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser2, tokenObj1, 'admin');

        await mongoUserRepo.update({'username' : adminUser.username},{$addToSet : {badges : pretty_id}});
        adminUser.bio = 'isBio';
        adminUser.img_url = 'indorse-staging-bucket.s3.amazonaws.com/5a4c7d8d16a975235235fdf3d?t=214241424';
        await mongoUserRepo.update({'username' : adminUser.username}, {$set : {bio : adminUser.bio, img_url: adminUser.img_url}})
        await mongoUserRepo.update({'username' : adminUser2.username},{$addToSet : {badges : pretty_id}});

        await badgeCreationWrapper.createBadge(randomBadge, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .send({query: getBadgeUsersQuery});

        let user1 = await mongoUserRepo.findOne({'username': adminUser.username});
        let respBody = res.body.data.allBadgeUsers;
        respBody.users.length.should.equal(userCount);
        respBody.totalUsers.should.equal(userCount);
        let findAdmin1;
        for(let user of respBody.users){
            if(user.username === createdAdmin1.username){
                findAdmin1 = user;
            }
        }
        should.exist(findAdmin1);
        findAdmin1.name.should.equal(createdAdmin1.name);
        findAdmin1.bio.should.equal(adminUser.bio);
        findAdmin1.img_url.should.equal(adminUser.img_url);
    });


});