process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');

chai.use(chaiHttp);

describe('updateBadge test', function () {

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('it should update badge name', async () => {

        let adminUser = testUtils.generateRandomUser();

        let createBadgeRequest = testUtils.generateRandomBadge();
        let badgeNameToUpdate = testUtils.generateRandomString();

        let updateBadgeQuery = `mutation ($id: String = "${createBadgeRequest.pretty_id}"
                                              $name: String = "${badgeNameToUpdate}"  ) {
                updateBadge (id: $id, name: $name) {
                    id
                    name
                    description
                }
                }
            `;

        let tokenObj = {};

        await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObj);

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: updateBadgeQuery});

        let updatedBadge = res.body.data.updateBadge;
        updatedBadge.name.should.equal(badgeNameToUpdate);
        updatedBadge.id.should.equal(createBadgeRequest.pretty_id);
    });


    it('it should not update badge name for a badge that doesnt exist', async () => {

        let adminUser = testUtils.generateRandomUser();

        let createBadgeRequest = testUtils.generateRandomBadge();
        let badgeNameToUpdate = testUtils.generateRandomString();

        let updateBadgeQuery = `mutation ($id: String = "${createBadgeRequest.pretty_id}"
                                              $name: String = "${badgeNameToUpdate}"  ) {
                updateBadge (id: $id, name: $name) {
                    id
                    name
                    description
                }
                }
            `;

        let tokenObj = {};

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: updateBadgeQuery});

        let updatedBadge = res.body.data.updateBadge;
        should.not.exist(updatedBadge);
    });

    it('it should not update badge name when it is not an admin', async () => {

        let adminUser = testUtils.generateRandomUser();
        let normalUser = testUtils.generateRandomUser();

        let createBadgeRequest = testUtils.generateRandomBadge();
        let badgeNameToUpdate = testUtils.generateRandomString();

        let updateBadgeQuery = `mutation ($id: String = "${createBadgeRequest.pretty_id}"
                                              $name: String = "${badgeNameToUpdate}"  ) {
                updateBadge (id: $id, name: $name) {
                    id
                    name
                    description
                }
                }
            `;

        let tokenObjAdmin = {};
        let tokenObj = {};

        await badgeCreationWrapper.authenticateAndCreateBadge(adminUser, createBadgeRequest, tokenObj);
        await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: updateBadgeQuery});

        res.body.errors[0].message.should.equal("Unauthorized");
    });

});