process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const config = require('config');
const testUtils = require('../../testUtils');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const authenticationWrapper = require('../../authenticationWrapper');
const mongoBadgeRepo = require('../../../models/services/mongo/mongoRepository')('badges');
const WebSocket = require('ws');
const {SubscriptionClient} = require('subscriptions-transport-ws');
const {ApolloClient} = require('apollo-client');
const {WebSocketLink} = require('apollo-link-ws');
const GRAPHQL_ENDPOINT = 'ws://localhost:3000/subscriptions';
const {HttpLink} = require('apollo-link-http');
const {InMemoryCache} = require('apollo-cache-inmemory');
const {setContext} = require('apollo-link-context');
const {split} = require('apollo-link');

const gql = require('graphql-tag');
const fetch = require('node-fetch');


chai.use(chaiHttp);

describe('massAssignBadge test', function () {


    let apollo;    // <<<  gets sets in the before()
    let networkInterface  // <<< same

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);

    });

    afterEach((done) => {
        console.log('dropping database');
        //networkInterface.close()
        DB.drop(done);
    });

    it('it should assign badges to two users', async () => {

        let adminUser = testUtils.generateRandomUser();
        let user1 = testUtils.generateRandomUser();
        let user2 = testUtils.generateRandomUser();

        let massAssignRequest = {
            emails: [user1.email, user2.email],
            badgeIds: [testUtils.generateRandomString(), testUtils.generateRandomString(), testUtils.generateRandomString()],
            assignType: "ASSIGN"
        };

        let massAssignBadgeQuery = `mutation ($emails: [EmailAddress] = ${JSON.stringify(massAssignRequest.emails)}
                                              $badgeIds: [String] = ${JSON.stringify(massAssignRequest.badgeIds)}) {
                massAssignBadges (emails: $emails, badgeIds: $badgeIds, assignType: ASSIGN )
                }
            `;

        let tokenObj = {};


        await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

        await authenticationWrapper.signupVerifyAuthenticate(user1, {});
        await authenticationWrapper.signupVerifyAuthenticate(user2, {});

        for (let badgeId of massAssignRequest.badgeIds) {
            let randomBadge = testUtils.generateRandomBadge();
            randomBadge.pretty_id = badgeId;
            await badgeCreationWrapper.createBadge(randomBadge, tokenObj);
        }

        let res = await chai.request(server)
            .post('/graphql')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({query: massAssignBadgeQuery});

        res.body.data.massAssignBadges.should.equal(true);


    });

});
