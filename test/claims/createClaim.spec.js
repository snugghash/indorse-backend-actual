process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseValidatorRepo = require('../../models/services/mongo/mongoose/mongooseValidatorRepo');
const checkClaimsJob = require('../../smart-contracts/services/cron/checkClaimsJob');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const claimsNotifier = require('../../smart-contracts/services/cron/claimVotersNotifier')
const resolverLoop = require('../../smart-contracts/services/cron/resolverLoop');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
chai.use(chaiHttp);
const ObjectID = require('mongodb').ObjectID;

describe('claims.createClaim', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /claims', () => {

        it('it should update notification flags for all created voting rounds', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            createdClaim = createdClaim[0];
            mongooseClaimsRepo.update({_id: ObjectID(createdClaim._id)}, {
                sc_claim_id: 0
            });

            await testUtils.wait(1000);
            await claimsNotifier.notifyClaimVoters();
            let votingRound = await mongooseVotingRoundRepo.findOne({claim_id: createdClaim._id});
            votingRound.voters_notified.should.equal(true);
        });

        it('it should create a claim given valid request', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            await resolverLoop.sendTransactions();
            await resolverLoop.processTransactions();
            let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            createdClaim = createdClaim[0];
            should.exist(createdUser._id);
            should.exist(createdClaim.sc_claim_id);
            createdClaim.title.should.equal(createClaimRequest.title);
            createdClaim.desc.should.equal(createClaimRequest.desc);
            createdClaim.proof.should.equal(createClaimRequest.proof);

            let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim._id);

            should.exist(votingRound);
            votingRound.claim_id.should.equal(createdClaim._id.toString());

            let votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

            votes.length.should.equal(settings.CLAIM_VALIDATOR_COUNT);

            for (let voteIndex = 0; voteIndex < votes.length; voteIndex++) {
                let vote = votes[voteIndex];
                vote.claim_id.should.equal(createdClaim._id.toString());
            }

            await mongooseVotingRoundRepo.update({_id: votingRound._id}, {$set: {end_voting: 1}});
            await testUtils.wait(4000);
            await checkClaimsJob.processOutstandingClaims();
            votingRound = await mongooseVotingRoundRepo.findOne({claim_id: createdClaim._id});

            votingRound.status.should.equal('finished');
            createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            createdClaim = createdClaim[0];
            createdClaim.final_status.should.equal(false);

            let updatedUser = await mongooseUserRepo.findOneByEmail(user.email);

            updatedUser.skills.length.should.equal(1);

            let skill = updatedUser.skills[0];

            skill.skill.name.should.equal(createClaimRequest.title.toLowerCase());
            skill.level.should.equal(createClaimRequest.level.toLowerCase());
            skill.validations.length.should.equal(1);

            let validation = skill.validations[0];

            validation.validated.should.equal(false);
            validation.id.should.equal(createdClaim._id.toString());
            validation.type.should.equal("claim");

            await mongooseUserRepo.setValidationResult(user.email, createdClaim._id.toString(), true);

            updatedUser = await mongooseUserRepo.findOneByEmail(user.email);
            updatedUser.skills[0].validations[0].validated.should.equal(true);
        });

        it('it should create a claim given valid request for admin', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            await resolverLoop.sendTransactions();
            await resolverLoop.processTransactions();
            let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            createdClaim = createdClaim[0];
            should.exist(createdUser._id);
            should.exist(createdClaim.sc_claim_id);
            createdClaim.title.should.equal(createClaimRequest.title);
            createdClaim.desc.should.equal(createClaimRequest.desc);
            createdClaim.proof.should.equal(createClaimRequest.proof);

            let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim._id);

            should.exist(votingRound);
            votingRound.claim_id.should.equal(createdClaim._id.toString());

            let votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

            votes.length.should.equal(settings.CLAIM_VALIDATOR_COUNT);

            for (let voteIndex = 0; voteIndex < votes.length; voteIndex++) {
                let vote = votes[voteIndex];
                vote.claim_id.should.equal(createdClaim._id.toString());
            }

            await mongooseVotingRoundRepo.update({_id: votingRound._id}, {$set: {end_voting: 1}});
            await testUtils.wait(5000);
            await checkClaimsJob.processOutstandingClaims();
            votingRound = await mongooseVotingRoundRepo.findOne({claim_id: createdClaim._id});

            votingRound.status.should.equal('finished');
            createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            createdClaim = createdClaim[0];
            createdClaim.final_status.should.equal(false);
        });

        it('it should fail to create a claim with the same title', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            try {
                await claimCreationWrapper.createClaim(createClaimRequest, tokenObj);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });


        it('should not return user id in the validator set', async () => {

            let skillsArray = ['Java', 'Solidity']
            let userToSkip = 'TestUserId123'
            let validator;
            for (let i = 0; i < 3; i++) {
                console.log("Insert")
                validator = {
                    user_id: testUtils.generateRandomString(),
                    skills: skillsArray
                };
                await mongooseValidatorRepo.insert(validator)
            }

            validator = {
                user_id: userToSkip,
                skills: skillsArray
            };

            await mongooseValidatorRepo.insert(validator);

            let result = await mongooseValidatorRepo.getRandomSampleBySkill(4, 'Java', userToSkip);
            result.length.should.equal(3);
        });

        it('it should not create a claim if a pending claim for that title exists', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            try {
                createClaimRequest.level = 'intermediate';
                await claimCreationWrapper.createClaim(createClaimRequest, tokenObj);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should not create a claim if a pending claim for that title and level exists', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            try {
                await claimCreationWrapper.createClaim(createClaimRequest, tokenObj);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should not create a claim if a successful claim for that title and level exists', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
            let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim[0]._id);
            await mongooseVotingRoundRepo.update({_id: votingRound._id}, {$set: {status: 'finished'}});
            await mongooseClaimsRepo.update({_id: votingRound.claim_id}, {$set: {final_status: true}});

            try {
                await claimCreationWrapper.createClaim(createClaimRequest, tokenObj);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail to create a claim if there are no validators for the title', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            try {
                await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });
    })
});

