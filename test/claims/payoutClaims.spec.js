process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
const resolverLoop = require('../smart-contracts/cron/resolverLoopWrapper') //TODO : Move to common
const claimsProxy = require('../../models/services/ethereum/claims/proxy/claimsProxy')
const mongooseTxQueueRepo = require('../../models/services/mongo/mongoose/mongooseTransactionQueueRepo')
const web3 = require('../smart-contracts/initializers/testWeb3Provider');
const settings = require('../../models/settings');
const userSimulator = require('../smart-contracts/claims/userSimulator');
const EVM_REVERT_ERR = 'VM Exception while processing transaction: revert';
const yesHash = '0x5950ebe73a617667a66f67d525282c827c82ef4d89ae8dcd8336d013773b6b7f';
const noHash = '0x1544e7eb480cc28803f7ff893bd999e62fb759c7d172fc27414594c5d9c925f2';
const claimCreationWrapper = require('./claimCreationWrapper');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');

chai.use(chaiHttp);

describe('payoutClaims', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach(async (done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('it should return payout data', async () => {

        let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
        let user = testUtils.generateRandomUser();

        let tokenObj = {};

        await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
        let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

        let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);
        let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(createdClaim[0]._id);
        let votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

        for (let voteIndex = 0; voteIndex < votes.length; voteIndex++) {
            await mongooseVoteRepo.update({_id: votes[voteIndex]._id.toString()}, {$set: {sc_vote_exists: true}})
        }

        votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id);

        let res = await chai.request(server)
            .post('/payoutClaims')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({pay : false});

        console.log(JSON.stringify(res.body));

        await chai.request(server)
            .post('/payoutClaims')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({pay : true});

        res = await chai.request(server)
            .post('/payoutClaims')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({pay : true});

        console.log(JSON.stringify(res.body));
    })
});
