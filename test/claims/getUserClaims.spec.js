process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const DB = require('../db');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
chai.use(chaiHttp);

describe('claims.getUserClaims', function () {
    this.timeout(config.get('test.timeout'));
    var naiveSC;
    var tallySC;

    before(async (done) => {
        [naiveSC, tallySC] = await contractSettingInitializer.initialize(); 
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /users/:user_id/claims', () => {

        it('it should return user claims', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            let res = await chai.request(server)
                .get('/users/' + userFromMongo._id.toString() + '/claims')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(1);

            let claimObj = res.body.claims[0];

            should.exist(claimObj.claim);
            should.exist(claimObj.votinground);
        });

        it('it should return no claims if user has none', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj,'full_access');

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);
            let res = await chai.request(server)
                .get('/users/' + userFromMongo._id.toString() + '/claims')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.claims.length.should.equal(0);
        });
    });
});