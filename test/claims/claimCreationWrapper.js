process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../authenticationWrapper');
const mongooseUserRepo = require('../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseValidatorRepo = require('../../models/services/mongo/mongoose/mongooseValidatorRepo');
const testUtils = require('../testUtils');
chai.use(chaiHttp);

exports.createValidatorSet = async function createValidatorSet(setSize,skillsArray) {
    let validators = [];

    for (let i = 0; i < setSize; i++) {
        let randomUser = testUtils.generateRandomUser();
        let tokenObj = {};
        await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
        let createdUser = await mongooseUserRepo.findOneByEmail(randomUser.email);
        let randomEthAddress = testUtils.generateRandomEthAddress();

        await mongooseUserRepo.update({email : createdUser.email}, {$set : {ethaddress : randomEthAddress}});

        let validator = {
            user_id : createdUser._id.toString(),
            skills : skillsArray
        };

        validators.push({user_id : validator.user_id,tokenObj : tokenObj});
        await mongooseValidatorRepo.insert(validator)
    }

    return validators;
};

exports.authenticateAndCreateClaim = async function authenticateAndCreateBadge(user, claim, tokenObj) {
    let createdUser =  await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj,'admin');
    await mongooseSkillRepo.insert({name : claim.title.toLowerCase(), category : "tech"});
    let randomEthAddress = testUtils.generateRandomEthAddress();

    await mongooseUserRepo.update({email : createdUser.email}, {$set : {ethaddress : randomEthAddress}});

    await createClaim(claim,tokenObj);

    return createdUser;
};

exports.authenticateAdminAndCreateClaim = async function authenticateAdminAndCreateClaim(user, claim, tokenObj) {
    let createdUser =  await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj,'admin');

    let randomEthAddress = testUtils.generateRandomEthAddress();

    await mongooseUserRepo.update({email : createdUser.email}, {$set : {ethaddress : randomEthAddress}});

    await createClaim(claim,tokenObj);

    return createdUser;
};

async function createClaim(claim, tokenObj) {
    let res = await chai.request(server)
        .post('/claims')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send(claim);

    let claimId = res.body.claim._id;

    await chai.request(server)
        .post('/claims/' + claimId + "/approve")
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send(claim);
}

exports.createClaim = createClaim;