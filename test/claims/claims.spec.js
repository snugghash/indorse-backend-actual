process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
const resolverLoop = require('../smart-contracts/cron/resolverLoopWrapper') //TODO : Move to common
const claimsProxy  = require('../../models/services/ethereum/claims/proxy/claimsProxy')
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo')
const mongooseTxQueueRepo =require('../../models/services/mongo/mongoose/mongooseTransactionQueueRepo')
const web3 = require('../smart-contracts/initializers/testWeb3Provider');
const userSimulator = require('../smart-contracts/claims/userSimulator');
const EVM_REVERT_ERR = 'VM Exception while processing transaction: revert';    
const yesHash = '0x5950ebe73a617667a66f67d525282c827c82ef4d89ae8dcd8336d013773b6b7f';
const noHash = '0x1544e7eb480cc28803f7ff893bd999e62fb759c7d172fc27414594c5d9c925f2';


chai.use(chaiHttp);

describe('claims', function () {
    this.timeout(config.get('test.timeout'));

    var naiveSC;
    var tallySC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        [naiveSC,tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach(async (done) => {
       console.log('dropping database');
       DB.drop(done);       
    });

    it('it should create the claim successfully and update address in mongo', async () => {
        let claim = testUtils.generateRandomClaim();
        let mongoClaimId = await mongooseClaimsRepo.insert(claim);
        let voters = [web3.eth.accounts[0], web3.eth.accounts[1]]

        await claimsProxy.createClaim(voters, testUtils.getDistantFutureTimestamp(), web3.eth.accounts[0], "test123", mongoClaimId);
        await resolverLoop.processTransactions();

        let claim2 = testUtils.generateRandomClaim();
        let mongoClaimId2 = await mongooseClaimsRepo.insert(claim);
        await claimsProxy.createClaim(voters, testUtils.getDistantFutureTimestamp(), web3.eth.accounts[0], "test122", mongoClaimId2);
        await resolverLoop.processTransactions();
        let mongoClaim = await mongooseClaimsRepo.findOneById(mongoClaimId2);
        should.exist(mongoClaim.sc_claim_id);
    });

    it('it should fail to create a duplicate claim', async () => {
        let claim = testUtils.generateRandomClaim();
        let mongoClaimId = await mongooseClaimsRepo.insert(claim);
        let voters = [web3.eth.accounts[0], web3.eth.accounts[1]]

        await claimsProxy.createClaim(voters, testUtils.getDistantFutureTimestamp(), web3.eth.accounts[0], "test123", mongoClaimId);

        await claimsProxy.createClaim(voters, testUtils.getDistantFutureTimestamp(), web3.eth.accounts[0], "test123", mongoClaimId);
        let count = await mongooseTxQueueRepo.count({'tx_metadata.callback_data.mongoClaimID': mongoClaimId})
        count.should.equal(1);

        resolverLoop.onlySendTransactions();

        await claimsProxy.createClaim(voters, testUtils.getDistantFutureTimestamp(), web3.eth.accounts[0], "test123", mongoClaimId);
        let count2 = await mongooseTxQueueRepo.count({ 'tx_metadata.callback_data.mongoClaimID': mongoClaimId })
        count.should.equal(1);
    });

    it('claim should be validated to false after voting ends', async () => {
        let claim = testUtils.generateRandomClaim();
        let mongoClaimId = await mongooseClaimsRepo.insert(claim);
        let voters = [userSimulator.senderAddress];
        let timeStamp = testUtils.getDistantFutureTimestamp();

        await claimsProxy.createClaim(voters, timeStamp, web3.eth.accounts[0], "test123", mongoClaimId);
        await resolverLoop.processTransactions();

        let mongoClaimObj = await mongooseClaimsRepo.findOneById(mongoClaimId);
        await testUtils.wait(3000);
        let result = await claimsProxy.getClaimTally(mongoClaimObj.sc_claim_id);
        result.should.equal(false);
    });

    it('exeption should be caught when an claim which has not ended and its tally is called', async () => {
        let existingUser = testUtils.generateRandomUser();
        let claim = testUtils.generateRandomClaim();
        let mongoClaimId = await mongooseClaimsRepo.insert(claim);
        let voters = [web3.eth.accounts[0], web3.eth.accounts[1]]
        await claimsProxy.createClaim(voters, testUtils.getOneDayFutureTimestamp(), web3.eth.accounts[0], "test123", mongoClaimId);
        await resolverLoop.processTransactions();
        try {
            await claimsProxy.getClaimTally(0);
            throw new Error("This should not execute")
        } catch (e) {
            e.message.should.equal(EVM_REVERT_ERR);

        }
    });
});
