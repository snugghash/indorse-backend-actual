process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('./claimCreationWrapper');
const DB = require('../db');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
chai.use(chaiHttp);

describe('claims.getClaim', function () {
    this.timeout(config.get('test.timeout'));
    var naiveSC;
    var tallySC;

    before(async (done) => {
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /claims/:claim_id', () => {

        it('it should return a claims', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let createdClaim = await mongooseClaimsRepo.findByOwnerId(createdUser._id);

            let res = await chai.request(server)
                .get('/claims/' + createdClaim[0]._id.toString())
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            should.exist(res.body.votes);
            should.exist(res.body.votinground);
            res.body.claim.title.should.equal(createClaimRequest.title);
            res.body.claim.desc.should.equal(createClaimRequest.desc);
            res.body.claim.proof.should.equal(createClaimRequest.proof);
        });
    });
});