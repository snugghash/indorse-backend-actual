process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const DB = require('../db');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
chai.use(chaiHttp);

describe('votes.getVote', function () {
    this.timeout(config.get('test.timeout'));
    var naiveSC;
    var tallySC;

    beforeEach(async(done) => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /votes/:vote_id', () => {

        it('it should return a vote', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            let createdUser = await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);
            let votes = await mongooseVoteRepo.findByVoterId(validators[0].user_id);

            let res = await chai.request(server)
                .get('/votes/' + votes[0]._id.toString())
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();
            res.body.voter_id.should.equal(validators[0].user_id);
        });
    });
});