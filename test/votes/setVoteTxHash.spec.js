process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const claimVotersNotifier = require('../../smart-contracts/services/cron/claimVotersNotifier');
const resolverLoop = require('../../smart-contracts/services/cron/resolverLoop');
const DB = require('../db');
const userSimulator = require('../smart-contracts/claims/userSimulator');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
const checkVoteTxJob = require('../../smart-contracts/services/cron/checkVoteTxJob');
chai.use(chaiHttp);

describe('votes.setVoteTxHash', function () {
    this.timeout(config.get('test.timeout'));
    var naiveSC;
    var tallySC;

    beforeEach(async(done) => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /votes/:vote_id/txHash', () => {

        it('it should submit set txHash', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user,createClaimRequest,tokenObj);

            await claimVotersNotifier.notifyClaimVoters();
            await resolverLoop.sendTransactions();
            await resolverLoop.processTransactions();

            let testedValidator = validators[0];
            let votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            let vote = votes[0];

            let res = await chai.request(server)
                .post('/votes/' + vote._id.toString())
                .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                .send({endorse : true});

            let votingToken = res.body.votingToken;

            let updatedVote = await mongooseVoteRepo.findOneById(vote._id);

            updatedVote.endorsed.should.equal(true);
            should.exist(updatedVote.voted_at);
            updatedVote.voting_token.should.equal(votingToken);

            //NOTE : This should be the cast vote hash submitted by the user
            //Since BE does not really use it as a means to ensure validity
            //of this transaction , using a dummy tx hash here which was successful
            let txHash = tallySC.transactionHash;

            await chai.request(server)
                .post('/votes/' + vote._id.toString())
                .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                .send({endorse : true});

            res = await chai.request(server)
                .post('/votes/' + vote._id.toString() + '/txHash')
                .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                .send({txHash : txHash});

            let returnedVote = res.body;
            returnedVote.tx_hash.should.equal(txHash);

            res = await chai.request(server)
                .get('/votes/' + vote._id.toString())
                .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                .send();

            returnedVote = res.body;
            returnedVote.tx_status.should.equal("SUCCESS");

            await checkVoteTxJob.checkVoteTxStatus();
        });
    })
});