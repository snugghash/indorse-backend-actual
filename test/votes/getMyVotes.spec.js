process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const DB = require('../db');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');

chai.use(chaiHttp);

describe('votes.getMyVotes', function () {
    this.timeout(config.get('test.timeout'));
    var naiveSC;
    var tallySC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /votes', () => {

        it('it should return user claims', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user, createClaimRequest, tokenObj);

            let res = await chai.request(server)
                .get('/votes')
                .set('Authorization', 'Bearer ' + validators[0].tokenObj.token)
                .send();

            res.body.results.length.should.equal(1);
        });

        it('it should return no votes if user has none', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj,'full_access');

            let res = await chai.request(server)
                .get('/votes')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.results.length.should.equal(0);
        });
    });
});