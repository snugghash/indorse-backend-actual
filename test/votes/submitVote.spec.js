process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongooseVoteRepo = require('../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const settings = require('../../models/settings');
const claimCreationWrapper = require('../claims/claimCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const contractSettingInitializer = require('../smart-contracts/claims/contractSettingInitializer');
chai.use(chaiHttp);

describe('votes.submitVote', function () {
    this.timeout(config.get('test.timeout'));
    var naiveSC;
    var tallySC;

    beforeEach(async(done) => {
        console.log('connecting to database');
        [naiveSC, tallySC] = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /votes/:vote_id', () => {

        it('it should submit a vote for a claim if user is one of voters', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user,createClaimRequest,tokenObj);

            let testedValidator = validators[0];
            await testUtils.wait(1000);

            let votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            let vote = votes[0];

            let res = await chai.request(server)
                .post('/votes/' + vote._id.toString())
                .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                .send({endorse : true});

            let votingToken = res.body.votingToken;

            let updatedVote = await mongooseVoteRepo.findOneById(vote._id);

            updatedVote.endorsed.should.equal(true);
            should.exist(updatedVote.voted_at);
            updatedVote.voting_token.should.equal(votingToken);

            res = await chai.request(server)
                .get('/votes/' + vote._id.toString())
                .set('Authorization', 'Bearer ' + testedValidator.tokenObj.token)
                .send();

            let retrievedVote = res.body;
        });

        it('it should fail to submit vote if user is not the voter', async () => {

            let createClaimRequest = testUtils.generateRndomClaimCreationRequest();
            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let validators = await claimCreationWrapper.createValidatorSet(settings.CLAIM_VALIDATOR_COUNT, ['Javascript']);
            await claimCreationWrapper.authenticateAndCreateClaim(user,createClaimRequest,tokenObj);

            let testedValidator = validators[0];
            let votes = await mongooseVoteRepo.findByVoterId(testedValidator.user_id);
            let vote = votes[0];

            try {
                await chai.request(server)
                    .post('/votes/' + vote._id.toString())
                    .set('Authorization', 'Bearer ' + validators[1].tokenObj.token)
                    .send({endorse : true});
                throw new Error('Expected to fail!');

            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});