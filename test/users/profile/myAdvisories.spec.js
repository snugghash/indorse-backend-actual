process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('../../companies/advisors/invitationWrapper');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('profile.myAdvisories', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /me/advisories', () => {


        it('it should return one invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,userTokenObj);
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            let res = await chai.request(server)
                .get('/me/advisories')
                .set('Authorization', 'Bearer ' + userTokenObj.token)
                .send();

            res.should.have.status(200);

            let advisories = res.body;

            advisories.length.should.equal(1);

            let advisory = advisories[0];

            should.exist(advisory._id);
            should.exist(advisory.signed_by_company);
            should.exist(advisory.signed_by_user);
            should.not.exist(advisory.verification_timestamp);
            should.exist(advisory.creation_timestamp);
            should.exist(advisory.company);
            should.exist(advisory.company.id);
            should.exist(advisory.company.pretty_id);
            should.exist(advisory.company.company_name);
        });

        it('it should fail if user is not logged in', async () => {

            try {
                await chai.request(server)
                    .get('/me/advisories')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });


    })
});