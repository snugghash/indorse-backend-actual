process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('users.findAll', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /users', () => {

        it('When no query param should return user mongo and bigchain profiles for admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let userCount = 10;

            let randomUsers = testUtils.generateRandomUsers(userCount);

            for (let index = 0; index < userCount; index++) {
                await authenticationWrapper.justSignUpNoVerify(randomUsers[index])
            }

            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

            let res = await chai.request(server)
                .get('/users/?pageNo=1&perPage=10')
                .set('Authorization', 'Bearer ' + token)
                .send({email: adminUser.email});

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(userCount + 1);
            res.body.users.length.should.equal(userCount);
            res.body.totalVerifiedUsers.should.equal(1);

            for (let index = 0; index < userCount; index++) {
                should.not.exist(res.body.users[index].keyPair);
                should.not.exist(res.body.users[index].pass);
                should.not.exist(res.body.users[index].password);
                should.not.exist(res.body.users[index].salt);
                should.not.exist(res.body.users[index].tokens);
                should.not.exist(res.body.users[index].pass_verify_token);
                should.not.exist(res.body.users[index].verify_token);
                should.not.exist(res.body.users[index].ga_id);
                should.not.exist(res.body.users[index].keyPair);
            }

            //Testing pagination

            for (let index = 1; index < 6; index++) {
                res = await chai.request(server)
                    .get('/users/?pageNo=' + index + '&perPage=2')
                    .set('Authorization', 'Bearer ' + token)
                    .send({email: adminUser.email});
                res.body.success.should.equal(true);
                res.body.totalUsers.should.equal(userCount + 1);
                res.body.users.length.should.equal(2);
                res.body.totalVerifiedUsers.should.equal(1);
                res.body.matchingVerifiedUsers.should.equal(1);

            }

            res = await chai.request(server)
                .get('/users/?pageNo=6&perPage=2')
                .set('Authorization', 'Bearer ' + token)
                .send({email: adminUser.email});

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(userCount + 1);
            res.body.users.length.should.equal(1);
            res.body.totalVerifiedUsers.should.equal(1);
        });

        it('When no query param and users don\'t have bigchain profiles should return user mongo and bigchain profiles for admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let userCount = 10;

            let randomUsers = testUtils.generateRandomUsers(userCount);

            for (let index = 0; index < userCount; index++) {
                await authenticationWrapper.justSignUpNoVerify(randomUsers[index]);
                await mongoUserRepo.update({email: randomUsers[index].email}, {$set: {keyPair: null}});
            }

            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

            let res = await chai.request(server)
                .get('/users/?pageNo=1&perPage=10')
                .set('Authorization', 'Bearer ' + token)
                .send({email: adminUser.email});

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(userCount + 1);
            res.body.users.length.should.equal(userCount);
            res.body.totalVerifiedUsers.should.equal(1);

            //Testing pagination

            for (let index = 1; index < 6; index++) {
                res = await chai.request(server)
                    .get('/users/?pageNo=' + index + '&perPage=2')
                    .set('Authorization', 'Bearer ' + token)
                    .send({email: adminUser.email});
                res.body.success.should.equal(true);
                res.body.totalUsers.should.equal(userCount + 1);
                res.body.users.length.should.equal(2);
                res.body.totalVerifiedUsers.should.equal(1);
                res.body.matchingVerifiedUsers.should.equal(1);

            }

            res = await chai.request(server)
                .get('/users/?pageNo=6&perPage=2')
                .set('Authorization', 'Bearer ' + token)
                .send({email: adminUser.email});

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(userCount + 1);
            res.body.users.length.should.equal(1);
        });

        it('When there is a query param should return user mongo and bigchain profiles for admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

            await mongoUserRepo.update({email: adminUser.email}, {
                $set: {
                    role: 'admin',
                    ethaddress: adminUser.ethaddress
                }
            })

            let res = await chai.request(server)
                .get('/users/?pageNo=1&perPage=10&search=' + adminUser.email)
                .set('Authorization', 'Bearer ' + token)
                .send({email: adminUser.email});

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(1);
            res.body.users.length.should.equal(1);
            res.body.totalVerifiedUsers.should.equal(1);
            res.body.matchingVerifiedUsers.should.equal(1);

            res = await chai.request(server)
                .get('/users/?pageNo=1&perPage=10&search=' + adminUser.name)
                .set('Authorization', 'Bearer ' + token)
                .send({email: adminUser.email});

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(1);
            res.body.users.length.should.equal(1);
            res.body.totalVerifiedUsers.should.equal(1);
            res.body.matchingVerifiedUsers.should.equal(1);
        });

        it('When there is field list and search query it should return only those fields and no bigchain profile', async () => {

            let adminUser = testUtils.generateRandomUser();
            let verifiedUser = testUtils.generateRandomUser();

            let tokenObj = {};
            let toeknObjVerifiedUser = {};

            let userCount = 3;

            let randomUsers = testUtils.generateRandomUsers(userCount);

            for (let index = 0; index < userCount; index++) {
                await authenticationWrapper.justSignUpNoVerify(randomUsers[index]);
                await mongoUserRepo.update({email: randomUsers[index].email}, {$set: {keyPair: null}});
            }

            await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);
            await authenticationWrapper.signupVerifyAuthenticate(verifiedUser, toeknObjVerifiedUser);

            await mongoUserRepo.update({email: adminUser.email}, {
                $set: {
                    role: 'admin',
                    ethaddress: adminUser.ethaddress
                }
            })

            let res = await chai.request(server)
                .get('/users/?search=' + adminUser.email + '&fields=email,username')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.success.should.equal(true);
            res.body.totalUsers.should.equal(5);
            res.body.users.length.should.equal(1);
            res.body.totalVerifiedUsers.should.equal(2);
            res.body.matchingVerifiedUsers.should.equal(1);

            let user = res.body.users[0];

            should.exist(user.username);
            should.exist(user.email);
            should.not.exist(user.name);
            should.not.exist(user.role);
        });


        it('should fail for non-admin user', async () => {

            let normalUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(normalUser, tokenObj);

            try {
                await chai.request(server)
                    .get('/users/?pageNo=1&perPage=10')
                    .set('Authorization', 'Bearer ' + token)
                    .send({email: normalUser.email});

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(403);
            }
        })

    })
});
