process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');

chai.use(chaiHttp);

describe('users.findUsername', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /findusername', () => {

        it('should find username if user exists', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let res = await chai.request(server)
                .post('/findusername')
                .send({username: user.username});

            res.should.have.status(200);
            res.body.success.should.equal(false);
        })

        it('should not find username if user does not exists', async () => {

            let res = await chai.request(server)
                .post('/findusername')
                .send({username: 'test'});

            res.should.have.status(200);
            res.body.success.should.equal(true);
        })

        it('should fail if request is invalid', async () => {

            try {
                 await chai.request(server)
                    .post('/findusername')
                    .send({user: 'test'});

                throw new Error("Expecting to fail!");
            } catch (error) {
                error.status.should.equal(422);
            }
        })

    });
});
