process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const testUtils = require('../../testUtils');
const config = require('config');
const googleAuthenticationWrapper = require('../../services/auth/googleAuthenticationWrapper')
const facebookAuthenticationWrapper = require('../../services/auth/facebookAuthenticationWrapper')
const fbUtils = require('../../../models/services/social/facebookService');
const sinon = require('sinon');
const badgeCreationWrapper = require('../../badges/badgeCreationWrapper');
const companyCreationWrapper = require('../../companies/companyCreationWrapper');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
chai.use(chaiHttp);

describe('users.profile', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /me', () => {
        it('should contain google_uid', async () => {
            let email = 'bob@inodrse.io'
            let googleUid = 'abc';

            let user = {
                username: 'tester',
                name: 'bob',
                google: {
                    id_token: 'tokenGoogle'
                }
            };

            let tokenObj = {};

            let createdUser = await googleAuthenticationWrapper.googleAuthenticate(user,email,googleUid,tokenObj);

            let response = await chai.request(server)
                .post('/me')
                .send({user_id: createdUser._id})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;
            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(email);
            mongoUser.isGoogleLinked.should.equal(true);
            should.not.exist(mongoUser.isFacebookLinked);
            should.not.exist(mongoUser.isAirbitzLinked);
            should.not.exist(mongoUser.isLinkedInLinked);
            should.not.exist(mongoUser.password);
            should.not.exist(mongoUser.pass);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.tokens);
            should.not.exist(mongoUser.salt);
            should.not.exist(mongoUser.pass_verify_token);
            should.not.exist(mongoUser.ga_id);
            should.not.exist(mongoUser.verify_token);
            should.not.exist(mongoUser.pass_verify_timestamp);
        })

        it('should contain facebook_uid', async () => {
            let email = 'bobtwo@inodrse.io'
            let facebookUid = 'def';

            let user = {
                username: 'testerse',
                name: 'bob',
                facebook: {
                    accessToken: 'token',
                    userID: 'userID'
                }
            };

            let testFaceBookParams = [{
                name: 'likes',
                value: 100
            },
                {
                    name: 'is_verfified',
                    value: true
                },
                {
                    name: 'verfified',
                    value: true
                },
                {
                    name: 'friends',
                    value: 215
                }
            ]
            let tokenObj = {};

            let stubFacebookVariable = sinon.stub(fbUtils, "getFacebookSocialParams").returns(testFaceBookParams);
            let createdUser = await facebookAuthenticationWrapper.facebookAuthenticate(user,email,tokenObj);
            stubFacebookVariable.restore();

            let response = await chai.request(server)
                .post('/me')
                .send({user_id: createdUser._id})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;
            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(email);
            mongoUser.isFacebookLinked.should.equal(true);
            should.not.exist(mongoUser.isAirbitzLinked);
            should.not.exist(mongoUser.isLinkedInLinked);
            should.not.exist(mongoUser.isGoogleLinked);
            should.not.exist(mongoUser.password);
            should.not.exist(mongoUser.pass);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.tokens);
            should.not.exist(mongoUser.salt);
            should.not.exist(mongoUser.pass_verify_token);
            should.not.exist(mongoUser.ga_id);
            should.not.exist(mongoUser.verify_token);
            should.not.exist(mongoUser.pass_verify_timestamp);
        })

        it('should not contain user credentials', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/me')
                .send({email: user.email})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;

            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(user.email);
            should.not.exist(mongoUser.password);
            should.not.exist(mongoUser.pass);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.tokens);
            should.not.exist(mongoUser.salt);
            should.not.exist(mongoUser.pass_verify_token);
            should.not.exist(mongoUser.ga_id);
            should.not.exist(mongoUser.verify_token);
            should.not.exist(mongoUser.pass_verify_timestamp);
        });

        it('should retrieve user by username with badges', async () => {

            let user = testUtils.generateRandomUser();
            let adminUser = testUtils.generateRandomUser();

            let randomBadge = testUtils.generateRandomBadge();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let adminTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, adminTokenObj, 'admin');

            await badgeCreationWrapper.createBadge(randomBadge, adminTokenObj);

            await mongoUserRepo.update({email : user.email},{$push : { badges : randomBadge.pretty_id}});

            let response = await chai.request(server)
                .post('/me')
                .send({username: user.username})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;

            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(user.email);
            mongoUser.badges[0].name.should.equal(randomBadge.badge_name);
            should.not.exist(mongoUser.password);
            should.not.exist(mongoUser.pass);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.tokens);
            should.not.exist(mongoUser.salt);
            should.not.exist(mongoUser.pass_verify_token);
            should.not.exist(mongoUser.ga_id);
            should.not.exist(mongoUser.verify_token);
            should.not.exist(mongoUser.pass_verify_timestamp);
        });

        it('should retrieve user by user_id', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let createdUser = await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/me')
                .send({user_id: createdUser._id})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;

            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(user.email);
            should.not.exist(mongoUser.password);
            should.not.exist(mongoUser.pass);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.tokens);
            should.not.exist(mongoUser.salt);
            should.not.exist(mongoUser.pass_verify_token);
            should.not.exist(mongoUser.ga_id);
            should.not.exist(mongoUser.verify_token);
            should.not.exist(mongoUser.pass_verify_timestamp);
        })

        it('if request is empty it should retrieve by email', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await mongoUserRepo.update({email: user.email}, {$set: {keyPair: null}});

            let response = await chai.request(server)
                .post('/me')
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;

            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(user.email);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.pass);
            should.not.exist(mongoUser.tokens);
            should.not.exist(mongoUser.salt);
            should.not.exist(mongoUser.pass_verify_token);
            should.not.exist(mongoUser.ga_id);
            should.not.exist(mongoUser.verify_token);
            should.not.exist(mongoUser.keyPair);
            should.not.exist(mongoUser.pass_verify_timestamp);
        })

        it('NoSQL Injection should not work', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            try {
                let response = await chai.request(server)
                    .post('/me')
                    .send({user_id: {$ne : ''}})
                    .set('Authorization', 'Bearer ' + tokenObj.token);

                throw new Error("Not expected to execute");
            } catch (error) {
                error.status.should.equal(404);
            }
        })

        it('should send confidence score to FE when me endpoint is called', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
            let fbScore = Object({ isVerified: true, score: 10 });
            let aggregateScore = Object({ isVerified: true, score: 10 });

            await mongoUserRepo.update({ 'email': user.email },
                { '$set': { 'confidenceScore': { 'facebookScore': fbScore, aggregateScore: aggregateScore} } });

            let response = await chai.request(server)
                .post('/me')
                .send({ email: user.email })
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let mongoUser = response.body.profile;

            mongoUser.name.should.equal(user.name);
            mongoUser.username.should.equal(user.username);
            mongoUser.email.should.equal(user.email);
            Number(mongoUser.confidenceScore.facebookScore.score).should.equal(Number(10));
            Number(mongoUser.confidenceScore.aggregateScore.score).should.equal(Number(10));
            mongoUser.confidenceScore.aggregateScore.isVerified.should.equal(true);
            mongoUser.confidenceScore.facebookScore.isVerified.should.equal(true);

        });

        it('should retrieve company details linked to work experience', async () => {

            await contractSettingInitializer.initialize();

            let user = testUtils.generateRandomUser();
            let tokenObj = {};
            let createdUser = await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let skillsUpdate = testUtils.generateRandomSkills(1);
            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            let createCompanyRequest = testUtils.generateRandomCompany();
            createCompanyRequest.visible_to_public = true;
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, adminTokenObj);

            let workUpdate = {
                company_name: createCompanyRequest.company_name,
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            let response = await chai.request(server)
                .post('/me')
                .send({user_id: createdUser._id})
                .set('Authorization', 'Bearer ' + tokenObj.token);

            let userFromResponse = response.body.profile;
            let work = userFromResponse.work[0];

            work.company.company_name.should.equal(createCompanyRequest.company_name);
            work.company.visible_to_public.should.equal(true);
        })
    })
});
