process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('../../companies/teamMembers/invitationWrapper');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('profile.myTeamMemberships', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /me/team_memberships', () => {


        it('it should return one invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let designationName = testUtils.generateRandomDesignation();
            let tokenObj = {};
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,userTokenObj);
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email, designationName);

            let res = await chai.request(server)
                .get('/me/team_memberships')
                .set('Authorization', 'Bearer ' + userTokenObj.token)
                .send();

            res.should.have.status(200);

            let teamMemberships = res.body;

            teamMemberships.length.should.equal(1);

            let teamMembership = teamMemberships[0];

            should.exist(teamMembership._id);
            should.exist(teamMembership.signed_by_company);
            should.exist(teamMembership.signed_by_user);
            should.not.exist(teamMembership.verification_timestamp);
            should.exist(teamMembership.creation_timestamp);
            should.exist(teamMembership.company);
            should.exist(teamMembership.company.id);
            should.exist(teamMembership.company.pretty_id);
            should.exist(teamMembership.company.company_name);
            should.exist(teamMembership.designation);
        });

        it('it should fail if user is not logged in', async () => {

            try {
                await chai.request(server)
                    .get('/me/team_memberships')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });


    })
});
