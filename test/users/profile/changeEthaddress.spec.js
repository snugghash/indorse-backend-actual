process.env.NODE_ENV = 'test';

const mongooseUserRepo = require("../../../models/services/mongo/mongoose/mongooseUserRepo");
const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const settings = require('../../../models/settings')
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
chai.use(chaiHttp);

describe('users.updateProfile', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /users/:user_id/ethaddress', () => {


        it('should set ethaddress if user has none', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObj);

            let createdUser = await mongooseUserRepo.findOneByEmail(newUser.email);

            let newEthaddress = '0x52bc44d5378309ee2abf1539bf71de1b7d7be3b5';

            let updateRequest = {
                address: newEthaddress
            };

            let res = await chai.request(server)
                .post('/users/' + createdUser._id + '/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(updateRequest);

            res.status.should.equal(200);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);

            should.exist(userFromMongo);

            userFromMongo.ethaddress.should.equal(newEthaddress);
        });

        it('should fail if ethaddress is already taken', async () => {

            let newUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};
            let tokenObjExistingUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);
            await authenticationWrapper.signupVerifyAuthenticate(existingUser, tokenObjExistingUser);

            let newUserFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);
            let existingUserFromMongo = await mongooseUserRepo.findOneByEmail(existingUser.email);

            let newEthaddress = '0xb2930b35844a230f00e51431acae96fe543a0347';

            let updateRequest = {
                address: newEthaddress
            };

            let res = await chai.request(server)
                .post('/users/' + existingUserFromMongo._id + '/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObjExistingUser.token)
                .send(updateRequest);

            res.status.should.equal(200);

            try {
                await chai.request(server)
                    .post('/users/' + newUserFromMongo._id + '/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('should fail if trying to update somebody else\'s ethaddress while not being an admin', async () => {

            let newUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};
            let tokenObjExistingUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);
            await authenticationWrapper.signupVerifyAuthenticate(existingUser, tokenObjExistingUser);

            let existingUserFromMongo = await mongooseUserRepo.findOneByEmail(existingUser.email);

            let newEthaddress = '0xb2930b35844a230f00e51431acae96fe543a0347';

            let updateRequest = {
                address: newEthaddress
            };

            try {
                await chai.request(server)
                    .post('/users/' + existingUserFromMongo._id + '/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('should update somebody eles\'s ethaddress when being admin', async () => {

            let newUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};
            let tokenObjExistingUser = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(newUser, tokenObjNewUser, 'admin');
            await authenticationWrapper.signupVerifyAuthenticate(existingUser, tokenObjExistingUser);

            let existingUserFromMongo = await mongooseUserRepo.findOneByEmail(existingUser.email);

            let newEthaddress = '0xb2930b35844a230f00e51431acae96fe543a0347';

            let updateRequest = {
                address: newEthaddress
            };

            let res = await chai.request(server)
                .post('/users/' + existingUserFromMongo._id + '/ethaddress')
                .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                .send(updateRequest);

            res.status.should.equal(200);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(existingUserFromMongo.email);

            should.exist(userFromMongo);

            userFromMongo.ethaddress.should.equal(newEthaddress);
        });

        it('should fail if not logged in', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);

            let newUserFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);

            let newEthaddress = '0xb2930b35844a230f00e51431acae96fe543a0347';

            let updateRequest = {
                address: newEthaddress
            };

            try {
                await chai.request(server)
                    .post('/users/' + newUserFromMongo._id + '/ethaddress')
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('should fail if no ethaddress in request', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);

            let newUserFromMongo = await mongooseUserRepo.findOneByEmail(newUser.email);
            let newEthaddress = '0xb2930b35844a230f00e51431acae96fe543a0347';

            let updateRequest = {
                address: newEthaddress
            };

            try {
                await chai.request(server)
                    .post('/users/' + newUserFromMongo._id + '/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send({});

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('should fail if user does not exist', async () => {

            let newUser = testUtils.generateRandomUser();

            let tokenObjNewUser = {};

            await authenticationWrapper.signupVerifyAuthenticate(newUser, tokenObjNewUser);

            let newEthaddress = '0xb2930b35844a230f00e51431acae96fe543a0347';

            let updateRequest = {
                address: newEthaddress
            };

            try {
                await chai.request(server)
                    .post('/users/5a7c0667ba3c887a7c64b0bf/ethaddress')
                    .set('Authorization', 'Bearer ' + tokenObjNewUser.token)
                    .send(updateRequest);

                throw new Error('Expecting to fail');

            } catch (error) {
                error.status.should.equal(404);
            }
        });
    })
});

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production' || settings.ENVIRONMENT === 'test_tmp';
}
