process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();
const config = require('config');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const testUtils = require('../../testUtils');

chai.use(chaiHttp);

describe('user.passwordChange', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /password/change', () => {

        it('should fail if user does not exist', async (done) => {

            let passwordChangeRequest = {
                email: 'some@email.com',
                old_password: 'sometoken',
                new_password: 'somepassword'
            };

            try {
                await chai.request(server)
                    .post('/password/change')
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should fail if user is not verified', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                email: user.email,
                old_password: user.password,
                new_password: 'somepassword'
            };

            try {
                await authenticationWrapper.justSignUpNoVerify(user);

                await chai.request(server)
                    .post('/password/change')
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should fail if user is not logged in', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                email: user.email,
                old_password: user.password,
                new_password: 'somepassword'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/change')
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should fail if old password is invalid', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                email: user.email,
                old_password: 'silly_password',
                new_password: 'somepassword'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/change')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should fail if email is missing', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                old_password: 'silly_password',
                new_password: 'somepassword'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/change')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should fail if old_password is missing', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                email: user.email,
                new_password: 'somepassword'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/change')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should fail if new_password is missing', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                email: user.email,
                old_password: 'silly_password',
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/password/change')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(passwordChangeRequest);

                done(new Error("Expected to fail"));
            } catch (error) {
                done();
            }
        });

        it('should change the password if all is correct', async (done) => {

            let user = testUtils.generateRandomUser();

            let passwordChangeRequest = {
                old_password: user.password,
                new_password: 'newPassword'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let res = await chai.request(server)
                    .post('/password/change')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(passwordChangeRequest);

                res.status.should.equal(200);

                let loginResult = await chai.request(server)
                    .post('/login')
                    .send({email: user.email, password: passwordChangeRequest.new_password});

                loginResult.status.should.equal(200);
                should.exist(loginResult.body.token);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('should change the password if user has no password', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let passwordChangeRequest = {
                old_password: "whatever",
                new_password: 'newPassword'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await mongoUserRepo.update({email: user.email}, {$unset : { pass : ""}});

                let res = await chai.request(server)
                    .post('/password/change')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(passwordChangeRequest);

                res.status.should.equal(200);

                let loginResult = await chai.request(server)
                    .post('/login')
                    .send({email: user.email, password: passwordChangeRequest.new_password});

                loginResult.status.should.equal(200);
                should.exist(loginResult.body.token);

                done();
            } catch (error) {
                done(error);
            }
        });
    })
});
