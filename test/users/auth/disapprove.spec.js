process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('users.disapprove', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /users/disapprove', () => {

        it('it should disapprove user', async (done) => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let normalUser = {
                name: 'Dude',
                username: 'd00d',
                email: 'doodsky@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);
                await authenticationWrapper.justSignUpNoVerify(normalUser);

                await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});
                let normalUserFromMongo = await mongoUserRepo.findOne({email: normalUser.email});
                await mongoUserRepo.update({email: normalUser.email}, {$set: {role: 'no_access'}});


                let res = await chai.request(server)
                    .post('/users/disapprove')
                    .set('Authorization', 'Bearer ' + token)
                    .send({approve_user_id: normalUserFromMongo._id});

                res.should.have.status(200);

                let updatedNormalUser = await mongoUserRepo.findOne({email: normalUser.email});

                updatedNormalUser.approved.should.equal(false);
                done();
            } catch (error) {
                done(error);
            }
        });

        it('should failed if no user_id in request', async (done) => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);
                await authenticationWrapper.justSignUpNoVerify(normalUser);

                await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

                await chai.request(server)
                    .post('/users/disapprove')
                    .set('Authorization', 'Bearer ' + token)
                    .send({});

                done(new Error("Expected to fail!"));
            } catch (error) {
                done();
            }
        });


        it('should fail for non-admin user', async (done) => {

            let normalUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {

                let createdUser = await authenticationWrapper.signupVerifyAuthenticate(normalUser, tokenObj);

                await chai.request(server)
                    .post('/users/disapprove')
                    .set('Authorization', 'Bearer ' + token)
                    .send({role: 'admin', user_id: createdUser.user_id});

                done(new Error("Expected to fail!"));
            } catch (error) {
                done();
            }
        })
    })
});