process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const should = chai.should()
const DB = require('../../../db')
const authenticationWrapper = require('../../../authenticationWrapper')
const config = require('config')
const sinon = require('sinon')
const lbUtils = require("../../../../models/services/social/linkedInService");
const linkedInUtils = require("../../../../models/services/social/linkedInService");

const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');

chai.use(chaiHttp)

describe('/auth/linked-in (login)', function () {
    this.timeout(config.get('test.timeout'));

    let linkedInUid = 'abcdefg'
    let name = 'firstname lastname'
    let email = 'bob@indorse.io'
    let testLinkedInParams = [
    ];

    let stubGetSocialParams;
    beforeEach((done) => {
        console.log('connecting to database');
        stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns(testLinkedInParams);
        DB.connect(done);

    });

    afterEach((done) => {
        console.log('dropping database');
        stubGetSocialParams.restore();
        DB.drop(done);
    });


    // 3. signup
    it('should login a user', async () => {

        let user_signup = {
            username : 'imtester',
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let user_login = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let stubGetAccessToken = await sinon.stub(lbUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = await sinon.stub(lbUtils, "getLinkedInSignupVariables").callsFake(function validatePass(){
            return [linkedInUid, name , email]
        });

        let res = await chai.request(server)
        .post('/auth/linked-in')
        .send(user_signup)
        res.should.have.status(200);

         res = await chai.request(server)
        .post('/auth/linked-in')
        .send(user_login)

        res.should.have.status(200);
        should.exist(res.body.token);

        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
    });

    it('should link to existing user via login flow', async () => {

        let userSignup = {
            name: 'Bob',
            username: 'tester',
            email: email,
            password: 'abcd'
        };

        let userLogin = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }
        let tokenObj = {};

        let stubGetAccessToken = await sinon.stub(lbUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = await sinon.stub(lbUtils, "getLinkedInSignupVariables").callsFake(function validatePass(){
            return [linkedInUid, name , email]
        });

        await authenticationWrapper.signupVerifyAuthenticate(userSignup, tokenObj);

        let res = await chai.request(server)
            .post('/auth/linked-in')
            .send(userLogin)
        res.should.have.status(200);

        let mongoDoc = await mongooseUserRepo.findOneByEmail(email);
        mongoDoc.email.should.equal(email);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.linkedIn_uid.should.equal(linkedInUid);

        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
    });

    it('should not login a user wrong code', async () => {

        let user_signup = {
            username : 'imtester',
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let user_login = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }
        let stubGetUserInfo ;
        let stubGetAccessToken ;

        stubGetAccessToken = await sinon.stub(lbUtils, "exchangeAuthorizationCode").returns('accessToken');
        stubGetUserInfo = await sinon.stub(lbUtils, "getLinkedInSignupVariables").callsFake(function validatePass(){
            return [linkedInUid, name , email]
        });

        let res = await chai.request(server)
        .post('/auth/linked-in')
        .send(user_signup)
        res.should.have.status(200);
        stubGetAccessToken.restore();

        try {
            res = await chai.request(server)
            .post('/auth/linked-in')
            .send(user_login)
            throw new Error("Expected to fail");
        } catch (error) {
            stubGetUserInfo.restore();
            error.status.should.equal(400);
        }
    });
});
