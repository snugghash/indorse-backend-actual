process.env.NODE_ENV = 'test_secret'
const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../../server')
    , should = chai.should()
    , DB = require('../../../db')
    , testUtils = require('../../../testUtils')
    , config = require('config')
    , request = require('request');

const mongoUserRepo = require('../../../../models/services/mongo/mongoRepository')('users');
const googleAuthenticationWrapper = require('../../../services/auth/googleAuthenticationWrapper')

const app_access_token = config.get('auth.facebook_token')
const app_token = config.get('auth.facebook_app_id')

let accessToken_a;

const userID_a = '100106320789229' // this user's email is permissioned , user_likes is permissioned, profile is not default

let accessToken_b;
const userID_b = '121689588593761' // this user's email is not permissioned

chai.use(chaiHttp)

describe('facebook utils test (functions require accessToken)', function () {

    this.timeout(config.get('test.timeout'));


    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    // automatically getting and assigning access_token
    before(async () => {
        let get_access_token_url = 'https://graph.facebook.com/' + app_token + '/accounts/test-users/?access_token=' + app_access_token

        await new Promise((resolve, reject) => {

            request.get(get_access_token_url, function (err, res, body) {
                var test_users = JSON.parse(body).data
                for (var i = 0; i < test_users.length; i++) {
                    if (test_users[i].id == userID_a) {
                        accessToken_a = test_users[i].access_token
                    }
                    if (test_users[i].id == userID_b) {
                        accessToken_b = test_users[i].access_token
                    }
                }
                resolve();

            })
        });
    });

    it('should extract facebook profile', async () => {
        let user_signup = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        }
        let testEmail = 'urklcsnmhs_1513914439@tfbnw.net';

        let res = await chai.request(server)
            .post('/signup/facebook')
            .send(user_signup);
        await testUtils.wait(config.get('test.bcTransactionTime'));
        res.should.have.status(200);
        let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});
    });


        it('should link Indorse account (that is signed up via Google) with Facebook and calculate confidence score (score should be 2.5)', async () => {

            let googleUid = 'abc';
            let testEmail = 'bob@tfbnw.net';

            let userSignup = {
                username: 'tester',
                name: 'bob',
                google: {
                    id_token: 'tokenGoogle'
                }
            };

            let userLinkingRequest = {
                facebook: {
                    accessToken: accessToken_a,
                    userID: userID_a
                }
            }


            let tokenObj = {};

            await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);
            await testUtils.wait(config.get('test.bcTransactionTime'));
            userLinkingRequest.facebook.accessToken = accessToken_a;

            let res = await chai.request(server)
                .post('/link/facebook')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(userLinkingRequest);

            res.should.have.status(200);
            let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});
            mongoDoc.name.should.equal(userSignup.name);
            mongoDoc.username.should.equal(userSignup.username);
            mongoDoc.email.should.equal(testEmail);
            Number(mongoDoc.confidenceScore.facebookScore.score).should.equal(Number(2.50)); // picture exist +1  (1/4 = 4.00)
            mongoDoc.confidenceScore.facebookScore.isVerified.should.equal(false);
        });

        it('should login and calculate confidnece score (score should be 2.5)', async () => {
            let user_login = {
                facebook: {
                    accessToken: accessToken_a,
                    userID: userID_a
                }
            }
            let user_signup = {
                username: 'tester',
                name: 'bob',
                facebook: {
                    accessToken: accessToken_a,
                    userID: userID_a
                }
            }

            let testEmail = 'urklcsnmhs_1513914439@tfbnw.net';

            let signup = await chai.request(server)
                .post('/signup/facebook')
                .send(user_signup);
            await testUtils.wait(config.get('test.bcTransactionTime'));
            let res = await chai.request(server)
                .post('/auth/facebook')
                .send(user_login);
            await testUtils.wait(config.get('test.bcTransactionTime'));


            res.should.have.status(200);
            let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});

            Number(mongoDoc.confidenceScore.facebookScore.score).should.equal(Number(2.50)); // picture exist +1  (1/4 = 4.00)
            mongoDoc.confidenceScore.facebookScore.isVerified.should.equal(false);


        });

        it('should not sign up a user as unable to retreive email', async () => {
            let user_signup = {
                username: 'tester',
                name: 'john',
                facebook: {
                    accessToken: accessToken_b,
                    userID: userID_b
                }
            }

            let res = await chai.request(server)
                .post('/signup/facebook')
                .send(user_signup);
            res.should.have.status(401);
        });


    });
