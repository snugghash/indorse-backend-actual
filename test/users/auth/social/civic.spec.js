process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const should = chai.should()
const DB = require('../../../db')
const config = require('config')
const sinon = require('sinon')
const civicUtils = require("../../../../models/services/social/civicUtils");
const authenticationWrapper = require('../../../authenticationWrapper');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');

const assert = require('assert');

chai.use(chaiHttp)

describe('civic.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should be able to sign up with Civic on Indorse platform', async () => {
        let testEmail = 'civicuser@civic.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            civic: {
                id_token: 'tokenCivic'
            }
        };

        let userLinkingCivic = {
            civic: {
                id_token: 'tokenCivic'
            }
        }

        let civicUserData = [
            {
                name: 'email',
                value: 'civicuser@civic.com'
            },
            {
                name: 'phone',
                value: '+44 223001202'
            }
        ]

        let civicEmail = 'civicuser@civic.com';
        let civicUid = 'civicuid';

        let stubCivic = sinon.stub(civicUtils, "validateAndGetUserData").callsFake(function validateData() {
            return [civicEmail, civicUid, civicUserData];
        });

        let res = await chai.request(server)
            .post('/signup/civic')
            .send(userSignup);

        res.should.have.status(200);

        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.civic_uid.should.equal(civicUid);
        mongoDoc.civicParams.length.should.equal(2);

        await chai.request(server)
            .post('/auth/civic')
            .send(userLinkingCivic)

        res.should.have.status(200);
        stubCivic.restore();
    });

    it('should fail when user attempts a sign in with unregistered account', async () => {
        let userLinkingCivic = {
            civic: {
                id_token: 'tokenCivic'
            }
        }

        let civicUserData = [
            {
                name: 'email',
                value: 'civicuser@civic.com'
            },
            {
                name: 'phone',
                value: '+44 223001202'
            }
        ]

        let civicEmail = 'civicuser@civic.com';
        let civicUid = 'civicuid'


        let stubCivic;
            stubCivic = sinon.stub(civicUtils, "validateAndGetUserData").callsFake(function validateData() {
                return [civicEmail, civicUid, civicUserData];
            });

        try {
            let signIn = await chai.request(server)
                .post('/auth/civic')
                .send(userLinkingCivic)

            throw new Error("We could sign in for an account not created in Indorse");
        } catch (error) {
            stubCivic.restore();
            error.status.should.equal(404, "Shold get 404 error code when calling endpoint")
        }
    });


    it('should link an existing account when a user attempts sign in civic email matches registered email', async (done) => {

        let testEmail = 'bob@bob.com'
        let userSignup = {
            name: 'Bob',
            username: 'tester',
            email: testEmail,
            password: 'abcd'
        };

        let userLinkingCivic = {
            civic: {
                id_token: 'tokenCivic'
            }
        }

        let civicUserData = [
            {
                name: 'email',
                value: 'bob@bob.com'
            },
            {
                name: 'phone',
                value: '+44 223001202'
            }
        ]

        let civicEmail = 'bob@bob.com';
        let civicUid = 'civicuid'


        let tokenObj = {};
        try {
            await authenticationWrapper.signupVerifyAuthenticate(userSignup, tokenObj, done);

            let stubCivic = sinon.stub(civicUtils, "validateAndGetUserData").callsFake(function validateData() {
                return [civicEmail, civicUid, civicUserData];
            });
            //attempt a login with civic
            let signIn = await chai.request(server)
                .post('/auth/civic')
                .send(userLinkingCivic)
            stubCivic.restore();
            done();


        } catch (error) {
            done(error);
        }
    });


    it('Should parse civic decoded data and return correctly', async (done) => {
        let testEmail = "user.test@gmail.com";
        let testPhoneNumber = "+1 5556187380";
        let testUserId = "c6d5795f8a059ez5ad29a33a60f8b402a172c3e0bbe50fd230ae8e0303609b42";
        let params = [{"name": "email", "value": "user.test@gmail.com"}, {"name": "phone", "value": "+1 5556187380"}]

        let userData = {
            "data": [
                {
                    "label": "contact.personal.email",
                    "value": testEmail,
                    "isValid": true,
                    "isOwner": true
                },
                {
                    "label": "contact.personal.phoneNumber",
                    "value": testPhoneNumber,
                    "isValid": true,
                    "isOwner": true
                }
            ],
            "userId": "c6d5795f8a059ez5ad29a33a60f8b402a172c3e0bbe50fd230ae8e0303609b42"
        };

        try {


            let parsedData = civicUtils.parseCivicDecodedData(userData);
            parsedData[0].should.equal(testEmail);
            parsedData[1].should.equal(testUserId);
            params.should.eql(parsedData[2]);
            done();
        } catch (error) {
            done(error);
        }
    });
});
