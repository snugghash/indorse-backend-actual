process.env.NODE_ENV = 'test';

const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../../server')
    , should = chai.should()
    , DB = require('../../../db')
    , testUtils = require('../../../testUtils')
    , config = require('config')
    , sinon = require('sinon')
    , fbUtils = require("../../../../models/services/social/facebookService");
;

const contractSettingInitializer = require('../../../smart-contracts/connections/contractSettingInitializer');
const mongoUserRepo = require('../../../../models/services/mongo/mongoRepository')('users');
const invitationWrapper = require('../../../companies/advisors/invitationWrapper');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');

let accessToken_a = 'token';
const userID_a = '119147722183867' // this user's email is permissioned

let accessToken_b = 'token';
const userID_b = '121689588593761' // this user's email is not permissioned

chai.use(chaiHttp)

describe('facebookSignup.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    let testEmail = 'bob@indorse.io'
    let mockImageUrl = false;
    let stub;
    let testFaceBookParams = [{
        name: 'likes',
        value: 100
    },
        {
            name: 'is_verfified',
            value: true
        },
        {
            name: 'verfified',
            value: true
        },
        {
            name: 'friends',
            value: 215
        }
    ]
    let stubFacebookVariable;

    before(() => {
        stub = sinon.stub(fbUtils, "validateAndGetEmail").callsFake(function validatePass() {
            return [testEmail, mockImageUrl]
        });
        stubFacebookVariable = sinon.stub(fbUtils, "getFacebookSocialParams").returns(testFaceBookParams);
    });

    after(() => {
        stub.restore();
        stubFacebookVariable.restore();
    });

    let SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);        
    });


    it('should signup a user', async () => {

        let adminUser = testUtils.generateRandomUser();

        let createCompanyRequest = testUtils.generateRandomCompany();

        let user_signup = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        };

        await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, createCompanyRequest, {}, testEmail);

        let res = await chai.request(server)
            .post('/signup/facebook')
            .send(user_signup);
        res.should.have.status(200);

        let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(user_signup.username);
        mongoDoc.facebook_uid.should.equal(userID_a);
        should.exist(mongoDoc.confidenceScore.facebookScore);
        should.exist(mongoDoc.confidenceScore.aggregateScore);
        mongoDoc.facebookParams.length.should.equal(4);

        let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(testEmail);
        companyFromMongo.advisors.length.should.equal(1);

        let advisorFromMongo = companyFromMongo.advisors[0];
        should.exist(advisorFromMongo.creation_timestamp);
        should.not.exist(advisorFromMongo.email);
        advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
    });

    it('should not signup a user (a user try to signup but the email from Facebook is already registered', async () => {

        let userSignupA = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        };

        let userSignupB = {
            username: 'differentTester',
            name: 'alice',
            facebook: {
                accessToken: accessToken_b,
                userID: userID_b
            }
        }

        let res = await chai.request(server)
            .post('/signup/facebook')
            .send(userSignupA);
        res.should.have.status(200);

        try {
            await chai.request(server)
                .post('/signup/facebook')
                .send(userSignupB);
        } catch (error) {
            error.status.should.equal(400);
        }
    });

    it('should not signup a user (a user try to signup but the facebook_uid is already registered', async () => {

        let userSignupA = {
            username: 'userFirst',
            name: 'bob',
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        }

        let userSignupB = {
            username: 'userSecond',
            name: 'alice',
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        }

        let res = await chai.request(server)
            .post('/signup/facebook')
            .send(userSignupA);
        res.should.have.status(200);

        try {
            let resB = await chai.request(server)
                .post('/signup/facebook')
                .send(userSignupB);
        } catch (error) {
            error.status.should.equal(400);
        }
    });

});
