process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const should = chai.should()
const DB = require('../../../db')
const config = require('config')
const sinon = require('sinon')
const uportUtils = require("../../../../models/services/social/uportUtils");
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const assert = require('assert');

chai.use(chaiHttp)

describe('uport.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);        
    });

    it('should be able to sign up with uport on Indorse platform', async () => {
        let uportEmail = 'uportuser@uport.com';
        let uportUid = 'uportuid';
        
        let userSignup = {
            username: 'tester',
            name: 'bob',
            email: uportEmail,
            uport: {
                "public_key": uportUid,
                "field2": "Value2"
            }
        };

        let userLinkingUport = {
            uport: {
                "public_key": uportUid,
                "field2": "Value2"
            }
        }

        let res = await chai.request(server)
            .post('/signup/uport')
            .send(userSignup);

        res.should.have.status(200);

        let mongoDoc = await mongooseUserRepo.findOneByEmail(uportEmail);
        mongoDoc.email.should.equal(uportEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.uport_uid.should.equal(uportUid);
        mongoDoc.uportParams.length.should.equal(2);


        await chai.request(server)
            .post('/auth/uport')
            .send(userLinkingUport)

        res.should.have.status(200);
    });

    it('should fail when user attempts a sign in with unregistered account', async () => {

        let uportEmail = 'uportuser@uport.com';
        let uportUid = 'uportuid';
                
        let userLinkingUport = {
            uport: {
                "public_key": uportUid,
                "field2": "Value2"
            }
        }

        let stubUport;
        stubUport = sinon.stub(uportUtils, "validateAndGetUserData").callsFake(function validateData() {
            return [uportUid, userLinkingUport.uport];
        });

        try {
            let signIn = await chai.request(server)
                .post('/auth/uport')
                .send(userLinkingUport)

            throw new Error("We could sign in for an account not created in Indorse");
        } catch (error) {
            stubUport.restore();
            error.status.should.equal(404); // not sure if this is the correct code
        }
    });

});
