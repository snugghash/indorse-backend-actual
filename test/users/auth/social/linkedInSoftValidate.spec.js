process.env.NODE_ENV = 'test';

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const should = chai.should()
const DB = require('../../../db')
const authenticationWrapper = require('../../../authenticationWrapper')
const config = require('config')
const sinon = require('sinon')
const lbUtils = require("../../../../models/services/social/linkedInService");
const linkedInUtils = require("../../../../models/services/social/linkedInService");
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const testUtils = require('../../../testUtils')
const companyCreationWrapper  =require('../../../../test/companies/companyCreationWrapper')
const contractSettingInitializer = require('../../../smart-contracts/connections/contractSettingInitializer');


chai.use(chaiHttp)

describe('/companies/:pretty_id/advisors/:advisor_id/validate/linked-in', function () {
    this.timeout(config.get('test.timeout'));

    let linkedInUid = 'abcdefg'
    let name = 'firstname lastname'
    let email = 'bob@indorse.io'
    let testLinkedInParams = [
    ];
    let linkedInProfileURL = "http://linkedin/user/abcdefg"

    let stubGetSocialParams;
    beforeEach(async (done) => {
        console.log('connecting to database');
        stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns(testLinkedInParams);
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);

    });

    afterEach((done) => {
        console.log('dropping database');
        stubGetSocialParams.restore();
        DB.drop(done);        
    });


    it('should create a new user when validated via accept link', async () => {

        //1.Create company and invite an Admin
        //2. Accept invitation of a user not on platform
        //3. User should be created in system
        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        let invitedUserEmail = "hello@hello.com"
        let tokenObj = {};

        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);

        let res = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ email: invitedUserEmail });

        res.should.have.status(200);
        let responseBody = res.body;

        should.exist(responseBody._id);
        should.exist(responseBody.creation_timestamp);
        should.not.exist(responseBody.user);

        let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        companyFromMongo.advisors.length.should.equal(1);

        let advisorFromMongo = companyFromMongo.advisors[0];
        advisorFromMongo.softConnection.status.should.equal('PENDING');

        let user_signup = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let user_login = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let stubGetAccessToken = await sinon.stub(lbUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = await sinon.stub(lbUtils, "getLinkedInSignupVariables").callsFake(function validatePass(){
            return [linkedInUid, name, email, linkedInProfileURL]
        });

        let res2 = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + responseBody._id + '/validate/linked-in')
            .send(user_signup)
        res2.should.have.status(200);
        let respTxt = JSON.parse(res2.text);
        should.exist(respTxt.token);
        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
    });



    it('should link to an existing user via email', async () => {

        //1.Create company and invite an Admin
        //2. Accept invitation of a user not on platform
        //3. User should be created in system
        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        let invitedUserEmail = "hello@hello.com"
        let tokenObj = {};


        let adminTokenObj = {};
        let inviteeTokenObj = {};
        let inviteeUser = testUtils.generateRandomUser();

        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);

        let res = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ email: invitedUserEmail });

        res.should.have.status(200);
        let responseBody = res.body;

        should.exist(responseBody._id);
        should.exist(responseBody.creation_timestamp);

        inviteeUser.email = invitedUserEmail;
        await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

        let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        companyFromMongo.advisors.length.should.equal(1);
        let advisorFromMongo = companyFromMongo.advisors[0];
        advisorFromMongo.softConnection.status.should.equal('PENDING');

        let user_signup = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let user_login = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let stubGetAccessToken = await sinon.stub(lbUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = await sinon.stub(lbUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, name, invitedUserEmail, linkedInProfileURL]
        });


        let resaccept = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + responseBody._id + '/soft-accept')
            
            
        let res2 = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + responseBody._id + '/validate/linked-in')
            .send(user_signup)
        res2.should.have.status(200);

        companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        companyFromMongo.advisors.length.should.equal(1);
        advisorFromMongo = companyFromMongo.advisors[0];
        advisorFromMongo.softConnection.status.should.equal('ACCEPTED');

        //User from mongo should have same UID for linked in
        let mongoDoc = await mongooseUserRepo.findOneByEmail(invitedUserEmail);
        mongoDoc.linkedIn_uid.should.equal(linkedInUid);
        advisorFromMongo.user_id.should.equal(mongoDoc._id.toString());


        let respTxt = JSON.parse(res2.text);
        should.exist(respTxt.token);
        stubGetAccessToken.restore();
        stubGetUserInfo.restore();

        //Get company to see all data is present
        let res3 = await chai.request(server)
            .get('/companies/' + createCompanyRequest.pretty_id)
            .send();        

        respTxt = JSON.parse(res3.text);
        should.exist(respTxt.advisors[0].softConnection);


        let res4 = await chai.request(server)
            .get('/companies/' + createCompanyRequest.pretty_id + "/advisors")
            .send();           
        respTxt = JSON.parse(res4.text);
        should.exist(respTxt[0].softConnection);


        let res5 = await chai.request(server)
            .get('/users/' + mongoDoc._id.toString() + '/advisories')
            .send();  
        
        respTxt = JSON.parse(res4.text);
        should.exist(respTxt[0].softConnection);        
                    
    });


    it('should link to an existing user via UUID', async () => {
        //1. Invite user with one email
        //2. User signs up via linked in using a different email
        //3. User accepts invitation by loggin in via linked in
        //4. Users ID should be populated on advisor object for company

        let adminUser = testUtils.generateRandomUser();
        let createCompanyRequest = testUtils.generateRandomCompany();
        let invitedUserEmail = "hello@hello.com"
        let tokenObj = {};


        let adminTokenObj = {};
        let inviteeTokenObj = {};
        let inviteeUser = testUtils.generateRandomUser();

        await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
            tokenObj, adminUser.username);

        let res = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send({ email: invitedUserEmail });

        res.should.have.status(200);
        let responseBody = res.body;

        should.exist(responseBody._id);
        should.exist(responseBody.creation_timestamp);

        let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        companyFromMongo.advisors.length.should.equal(1);
        let advisorFromMongo = companyFromMongo.advisors[0];
        advisorFromMongo.softConnection.status.should.equal('PENDING');

        let userSignup = {
            username: 'imtester',
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let userSignup2 = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let user_login = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        }

        let stubGetAccessToken = await sinon.stub(lbUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = await sinon.stub(lbUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, name, email, linkedInProfileURL]
        });

        res = await chai.request(server)
            .post('/auth/linked-in')
            .send(userSignup);

        res.should.have.status(200);

        let resaccept = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + responseBody._id + '/soft-accept')        

        let res2 = await chai.request(server)
            .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + responseBody._id + '/validate/linked-in')
            .send(userSignup2)


        companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
        companyFromMongo.advisors.length.should.equal(1);
        advisorFromMongo = companyFromMongo.advisors[0];
        advisorFromMongo.softConnection.status.should.equal('ACCEPTED');
        let mongoDoc = await mongooseUserRepo.findOneByEmail( email );
        advisorFromMongo.user_id.should.equal(mongoDoc._id.toString());

        let respTxt = JSON.parse(res2.text);
        should.exist(respTxt.token);
        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
    });    
});
