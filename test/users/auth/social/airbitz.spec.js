process.env.NODE_ENV = 'test'
const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../../server')
    , should = chai.should()
    , DB = require('../../../db')
    , config = require('config');
;
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('airbitz.spec.js', function () {
    this.timeout(config.get('test.timeout'));
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should create a new user ', async () => {

        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4=';
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg=';
        let buf_v = '28'
        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        let res = await chai.request(server)
            .post('/signup/airbitz')
            .send(userSignup);
        res.should.have.status(200);
        let users = DB.getDB().collection('users');
        let item = await users.findOne({email: userSignup.email});
        item.name.should.equal(userSignup.name);
        item.email.should.equal(userSignup.email);
        item.ethaddress.should.equal(userSignup.ethaddress);
        item.is_airbitz_user.should.equal(true);
        item.ethaddressverified.should.equal(true);

        let mongoDoc = await mongooseUserRepo.findOne({
            '$or': [{'email': userSignup.email},
                {'username': userSignup.username}]
        });
        mongoDoc.email.should.equal(userSignup.email);
        mongoDoc.username.should.equal(userSignup.username);
    })

    it('should not create a new user (fake signature) ', async () => {
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb23'; //this one is corrupted buf_r
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg=';
        let buf_v = '28'
        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };


        let tokenObj = {};

        try {
            let res = await chai.request(server)
                .post('/signup/airbitz')
                .send(userSignup);
            throw new Error("Expected to fail");

        } catch (error) {
            error.status.should.equal(400);
        }
    })

    it('should create a new user and trying to login without email verification will be rejected and return 403', async () => {
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4='
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg='
        let buf_v = '28'

        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        let userLogin = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        let res = await chai.request(server)
            .post('/signup/airbitz')
            .send(userSignup);
        res.should.have.status(200);

        try {
            let res_login = await chai.request(server)
                .post('/auth/airbitz')
                .send(userLogin)

            throw new Error("Expected to fail");
        } catch (error) {
            error.status.should.equal(403);
        }
    })

    it('should create a new user and logging in the same user with verification email verification should return 200, and return a valid token', async () => {
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4='
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg='
        let buf_v = '28'
        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };


        let userLogin = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        let res = await chai.request(server)
            .post('/signup/airbitz')
            .send(userSignup);
        res.should.have.status(200);
        mongooseUserRepo.update({email: userSignup.email.toLowerCase()},
            {
                $set: {
                    tokens: [],
                    verified: true,
                    approved: true
                }
            })

        let res_login = await chai.request(server)
            .post('/auth/airbitz')
            .send(userLogin)
        res_login.should.have.status(200);
    })

    it('should login the user and return valid token', async () => {
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4='
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg='
        let buf_v = '28'
        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            tokens: [],
            verified: true,
            approved: true,
            signature: signatureBuf
        };

        let userLogin = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        mongooseUserRepo.insert(userSignup)

        let res = await chai.request(server)
            .post('/auth/airbitz')
            .send(userLogin)
        res.should.have.status(200);

        let token = res.body.token;

        res = await chai.request(server)
            .post('/me')
            .set('Authorization', 'Bearer ' + token);

        res.should.have.status(200);
    })

    it('should not login the user when signature is wrong', async () => {
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4='
        let buf_s = 'Il3akXXp2HRij26s42a36zb5zKeCzrJGx8qepuNgFdg='
        let buf_v = '28'

        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignup = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            tokens: [],
            verified: true,
            approved: true,
            signature: signatureBuf
        };

        let userLogin = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        mongooseUserRepo.insert(userSignup)

        try {
            let res = await chai.request(server)
                .post('/auth/airbitz')
                .send(userLogin)
            throw new Error("Expected to fail");
        } catch (error) {
            error.status.should.equal(400);
        }
    })


    it('should not login the user when eth_address doesnt exist return 404 (FE will receive 404 then, redirect user to signup page)', async () => {

        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4='
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg='
        let buf_v = '28'

        let signatureBuf = {buf_r, buf_s, buf_v}
        let userLogin = {
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };


        try {
            let res = await chai.request(server)
                .post('/auth/airbitz')
                .send(userLogin)
            throw new Error("Expected to fail");
        } catch (error) {
            error.status.should.equal(404);
        }
    })


    it('two users should not be able to signup with same ethereum wallet address', async () => {
        let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4='
        let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg='
        let buf_v = '28'

        let signatureBuf = {buf_r, buf_s, buf_v}

        let userSignupA = {
            name: 'Person',
            username: 'username',
            email: 'p@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        let userSignupB = {
            name: 'Personb',
            username: 'username2',
            email: 'p2@example.com',
            ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
            signature: signatureBuf
        };

        let tokenObj = {};

        let res = await chai.request(server)
            .post('/signup/airbitz')
            .send(userSignupA);

        try {
            res = await chai.request(server)
                .post('/signup/airbitz')
                .send(userSignupB)
            throw new Error("Expected to fail");
        } catch (error) {
            error.status.should.equal(400);
        }
    })


});
