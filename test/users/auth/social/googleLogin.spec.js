process.env.NODE_ENV = 'test';

const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../../server')
    , should = chai.should()
    , DB = require('../../../db')
    , authenticationWrapper = require('../../../authenticationWrapper')
    , config = require('config')
    , request = require('request')
    , sinon = require('sinon')
    , gUtils = require("../../../../models/services/social/googleService");

const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');

// actual token - This can be only getting manually (check confluence page to find out how to get)
const IDTokenA = 'B4NSgiOnvekWiV8pg';


chai.use(chaiHttp)

describe('googleLogin.spec.js', function () {
    this.timeout(config.get('test.timeout'));
    let testEmail = 'bob@indorse.io';
    let googleUid = 'abc';
    let stub;

    before((done) => {
        stub = sinon.stub(gUtils, "validateAndGetEmail").callsFake(function validatePass() {
            return [testEmail, googleUid]
        });
        done();
    });

    after((done) => {
        stub.restore();
        done();
    });
    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should return 404 so that FE redirect user to signup page when the user is not found by google_uid', async (done) => {

        let userSignup = {
            google: {
                id_token: IDTokenA
            }
        };

        try {
            await chai.request(server)
                .post('/auth/google')
                .send(userSignup);

            done(new Error("Expected to fail"));

        } catch (error) {
            done();
        }

    });

    it('should login', async () => {
        let userLogin = {
            google: {
                id_token: IDTokenA
            }
        };
        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: IDTokenA
            }
        };

        await chai.request(server)
            .post('/signup/google')
            .send(userSignup);

        let res = await chai.request(server)
            .post('/auth/google')
            .send(userLogin);

        res.should.have.status(200);

        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
    });

    it('should link a user as exisitng email exists', async () => {

        let userSignup = {
            name: 'Bob',
            username: 'tester',
            email: testEmail,
            password: 'abcd'
        };

        let userLogin = {
            google: {
                id_token: IDTokenA
            }
        };

        let tokenObj = {};

        await authenticationWrapper.signupVerifyAuthenticate(userSignup, tokenObj);
        let res = await chai.request(server)
            .post('/auth/google')
            .send(userLogin);
        res.should.have.status(200);
        should.exist(res.body.token);

        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.google_uid.should.equal(googleUid);
    });
});
