process.env.NODE_ENV = 'test';

const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../../server')
    , should = chai.should()
    , DB = require('../../../db')
    , authenticationWrapper = require('../../../authenticationWrapper')
    , config = require('config')
    , sinon = require('sinon')
    , fbUtils = require("../../../../models/services/social/facebookService");

const mongoUserRepo = require('../../../../models/services/mongo/mongoRepository')('users');

const accessToken_a = 'real'; // this token doesn't matter as we are not using validation method
const userID_a = '119147722183867' // this user's email is permissioned

chai.use(chaiHttp)

describe('facebookLogin.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    let testEmail = 'bob@indorse.io';
    let mockImageUrl = false
    let stub;
    let testFaceBookParams = [{
        name: 'likes',
        value: 100
    },
        {
            name: 'is_verfified',
            value: true
        },
        {
            name: 'verfified',
            value: true
        },
        {
            name: 'friends',
            value: 215
        }
    ]
    let stubFacebookVariable
    before(() => {
        stub = sinon.stub(fbUtils, "validateAndGetEmail").callsFake(function validatePass(){
            return [testEmail,mockImageUrl]
        });
        stubFacebookVariable = sinon.stub(fbUtils, "getFacebookSocialParams").returns(testFaceBookParams);
    });

    after(() => {
        stub.restore();
        stubFacebookVariable.restore();
    });

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should return 404 when there is no facebook_uid and email found : returning 404 will lead FE to redirect user to Signup page', async () => {
        let userSignup = {
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        }
        try {
            let res = await chai.request(server)
                .post('/auth/facebook')
                .send(userSignup);
            throw new Error("Expected to fail");

        } catch (error) {
            error.status.should.equal(404);
        }
    });

    it('should login after signup', async () => {

        let userLogin = {
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        };

        let userSignup = {
            username: 'tester',
            name: 'bob',
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        };

        await chai.request(server)
            .post('/signup/facebook')
            .send(userSignup);

        let res = await chai.request(server)
            .post('/auth/facebook')
            .send(userLogin);

        res.should.have.status(200);

        let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});

        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        mongoDoc.facebook_uid.should.equal(userID_a);
    });

    it('should link a user as existing email exists', async () => {
        let userSignup = {
            name: 'Bob',
            username: 'tester',
            email: testEmail,
            password: 'abcd'
        };

        let userLogin = {
            facebook: {
                accessToken: accessToken_a,
                userID: userID_a
            }
        }

        let tokenObj = {};

        await authenticationWrapper.signupVerifyAuthenticate(userSignup, tokenObj);
        let res = await chai.request(server)
            .post('/auth/facebook')
            .send(userLogin);
        res.should.have.status(200);
        should.exist(res.body.token);
        let mongoDoc = await mongoUserRepo.findOne({'email': testEmail});
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
    });
});
