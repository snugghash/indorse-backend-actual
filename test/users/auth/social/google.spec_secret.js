process.env.NODE_ENV = 'test_secret'
const chai = require('chai')
    , chaiHttp = require('chai-http')
    , server = require('../../../../server')
    , should = chai.should()
    , DB = require('../../../db')
    , testUtils = require('../../../testUtils')
    , config = require('config');

// this method should test the 'validateAndGetEmail' method TODO: make facebook.spec_secrete and google.spec_secret one file

// actual token - This can be only getting manually (check confluence page to find out how to get)
const IDTokenA = 'setRealTokenHere';
// fake token
const IDTokenB = 'faketoken';

chai.use(chaiHttp)

describe('google.spec_secret.js', function () {
    this.timeout(config.get('test.timeout'));
    let test_email = 'bob@indorse.io'

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    //2.login
    // yes google uid
    it('should login', async () => {
        let userLogin = {
            google: {
                id_token: IDTokenA
            }
        }
        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: IDTokenA
            }
        }

        let signup = await chai.request(server)
            .post('/signup/google')
            .send(userSignup);
        let res = await chai.request(server)
            .post('/auth/google')
            .send(userLogin);
        console.log(res.body)
        res.should.have.status(200);
    });


    it('should not signup a user with fake token', async () => {
        let user_signup = {
            username: 'tester',
            name: 'alice',
            google: {
                id_token: IDTokenB
            }
        }

        let res = await chai.request(server)
            .post('/signup/google')
            .send(user_signup);
        res.should.have.status(401);
    });
});
