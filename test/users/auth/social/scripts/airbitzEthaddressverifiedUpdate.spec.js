process.env.NODE_ENV = 'test';

const mongoUserRepo = require('../../../../../models/services/mongo/mongoRepository')('users');
const airbitzScript = require('../../../../../models/users/auth/social/scripts/airbitzEthaddressverifiedUpdate.js')
const authenticationWrapper = require('../../../../authenticationWrapper')
const DB = require('../../../../db');
const config = require('config');
const server = require('../../../../../server')
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
chai.use(chaiHttp);

describe('airbitzEthaddressverifiedUpdate.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('update Airbitz users that currently does not have ethaddressverified field', () => {

        it('should update one airbitz user', async () => {
            let buf_r = 'pEMh8Fe8ybvuVN9G2+vzwELzfy4jACBVYBPuxN11Tb4=';
            let buf_s = 'Il3akXXp2HRij26s42h36zb5zKeCzrJGx8qepuNgFdg=';
            let buf_v = '28'
            let signatureBuf = {buf_r, buf_s, buf_v}

            let userSignup = {
                name: 'Person',
                username: 'username',
                email: 'p@example.com',
                ethaddress: '0x854208bc3441ff36b1a25d38aed477ef69c5f8ec',
                signature: signatureBuf
            };

            let res = await chai.request(server)
                .post('/signup/airbitz')
                .send(userSignup);
            res.should.have.status(200);

            let mongoUser = await mongoUserRepo.findOne({email: userSignup.email})
            mongoUser.ethaddressverified.should.equal(true)

            await mongoUserRepo.update({email: userSignup.email}, {$unset: {ethaddressverified:null}})
            mongoUser = await mongoUserRepo.findOne({email: userSignup.email})
            should.not.exist(mongoUser.ethaddressverified)

            let stats = await airbitzScript.updateAllAirbitz()
            stats.airbitzUserCount.should.equal(1)
            stats.usersProcessed.should.equal(1)
            stats.usersUpdated.should.equal(1)

            mongoUser = await mongoUserRepo.findOne({email: userSignup.email})
            mongoUser.ethaddressverified.should.equal(true)


            stats = await airbitzScript.updateAllAirbitz()
            stats.airbitzUserCount.should.equal(1)
            stats.usersProcessed.should.equal(1)
            stats.usersUpdated.should.equal(0)

        });


        it('should not update a regular user', async () => {
            let userSignup = {
                name: 'Person',
                username: 'UsErNaMe',
                email: 'p@example.com',
                password: 'password'
            };

            await authenticationWrapper.signupVerifyAuthenticate(userSignup, {})

            let stats = await airbitzScript.updateAllAirbitz()

            let mongoUser = await mongoUserRepo.findOne({email: userSignup.email})

            should.not.exist(mongoUser.ethaddressverified)
            stats.airbitzUserCount.should.equal(0)
            stats.usersProcessed.should.equal(0)
            stats.usersUpdated.should.equal(0)

        })

    })

})