process.env.NODE_ENV = 'test';
const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../../../server')
const DB = require('../../../db')
const googleAuthenticationWrapper = require('../../../services/auth/googleAuthenticationWrapper')
const config = require('config')
const sinon = require('sinon')
const fbUtils = require("../../../../models/services/social/facebookService");
const fbConfidenceScore = require("../../../../models/users/auth/social/confidenceScore/facebookConfidenceScore");
const linkedInUtils = require("../../../../models/services/social/linkedInService");

const mongoUserRepo = require('../../../../models/services/mongo/mongoRepository')('users');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');

chai.use(chaiHttp);

describe('facebookConfidenceScore.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {

        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    let email = "test@test.io";

    it('Should not fail if empty object is passed in computation', async () => {
        let testFaceBookParams = [{}]
        let fbScore = await fbConfidenceScore.calculateFacebookScore(testFaceBookParams, email);
        Number(fbScore.score).should.equal(Number(0));
        fbScore.isVerified.should.equal(false);
    });

    it('Should not fail if no parameters are passed to compute', async () => {
        let testFaceBookParams = []
        let fbScore = await fbConfidenceScore.calculateFacebookScore(testFaceBookParams, email);
        Number(fbScore.score).should.equal(Number(0));
        fbScore.isVerified.should.equal(false);
    });

    it('Should not fail if wrong parameters are passed to compute', async () => {
        let testFaceBookParams = 100;
        let fbScore = await fbConfidenceScore.calculateFacebookScore(testFaceBookParams, email);
        Number(fbScore.score).should.equal(Number(0));
        fbScore.isVerified.should.equal(false);
    });


    it('Expected score 5 (is_verified+0, verified+0, friends+1, therefore, 1/2 = 50%)', async () => {
        let testFaceBookParams = [
            {
                name: 'is_verified',
                value: true
            },
            {
                name: 'verified',
                value: true
            },
            {
                name: 'friends',
                value: 215
            }
        ]
        let fbScore = await fbConfidenceScore.calculateFacebookScore(testFaceBookParams, email);
        Number(fbScore.score).should.equal(Number(5.0));
        fbScore.isVerified.should.equal(true);
    });

    //Linking of the platforms test cases - Full score 10
    it('should compute and store confidence score on linking facebook account to indorse account', async () => {
        let googleUid = 'abc';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let userLinkingRequest = {
            facebook: {
                accessToken: 'token',
                userID: 'userID'
            }
        };


        let testFaceBookParams = [
            {
                name: 'is_verified',
                value: true
            },
            {
                name: 'verified',
                value: true
            },
            {
                name: 'friends',
                value: 215
            },
            {
                name: 'picture',
                value: false // is_silloutte = false means user has profile picture , Therefore, if false, user will score +1
            }
        ]


        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let stubFacebook = sinon.stub(fbUtils, "validateAccessTokenAuth").callsFake(function validatePass(){});
        let socialVarsStub = sinon.stub(fbUtils, "getFacebookSocialParams").returns(testFaceBookParams);

        let res = await chai.request(server)
            .post('/link/facebook')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(userLinkingRequest);
        stubFacebook.restore();
        socialVarsStub.restore();

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        Number(mongoDoc.confidenceScore.facebookScore.score).should.equal(Number(10));
        Number(mongoDoc.confidenceScore.aggregateScore.score).should.equal(Number(5));
        mongoDoc.confidenceScore.facebookScore.isVerified.should.equal(true);
    });


    it('should link facebook followed by linkedin profiles and not overwrite the confidence score',async () => {

        let googleUid = 'abc';
        let linkedInUid = 'def';
        let testEmail = 'bob@gmail.com';

        let userSignup = {
            username: 'tester',
            name: 'bob',
            google: {
                id_token: 'tokenGoogle'
            }
        };

        let fbUserLinkingRequest = {
            facebook: {
                accessToken: 'token',
                userID: 'userID'
            }
        };

        let linkedInuserLinkingRequest = {
            linked_in: {
                code: 'code',
                state: 'state',
                redirect_uri: 'http://localhost:4000/linked-in-callback.html'
            }
        };

        let testFaceBookParams = [
        {
            name: 'is_verified',
            value: true
        },
        {
            name: 'verified',
            value: true
        },
        {
            name: 'friends',
            value: 215
        },
        {
            name: 'picture',
            value: false // is_silloutte = false means user has profile picture , Therefore, if false, user will score +1
        }
        ]


        let tokenObj = {};

        await googleAuthenticationWrapper.googleAuthenticate(userSignup, testEmail, googleUid, tokenObj);

        let stubFacebook = sinon.stub(fbUtils, "validateAccessTokenAuth").callsFake(function validatePass(){});
        let socialVarsStub = sinon.stub(fbUtils, "getFacebookSocialParams").returns(testFaceBookParams);

        let res = await chai.request(server)
            .post('/link/facebook')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(fbUserLinkingRequest);

        res.should.have.status(200);
        let mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        mongoDoc.username.should.equal(userSignup.username);
        Number(mongoDoc.confidenceScore.facebookScore.score).should.equal(Number(10));
        Number(mongoDoc.confidenceScore.aggregateScore.score).should.equal(Number(5));
        mongoDoc.confidenceScore.facebookScore.isVerified.should.equal(true);

        let testLinkedInParams = [
            {
                name: 'num-connections',
                value: 300
            },
            {
                name: 'num-connections-capped',
                value: true
            },
            {
                name: 'picture-url',
                value: true
            }
        ]

        let stubGetAccessToken = sinon.stub(linkedInUtils, "exchangeAuthorizationCode").returns('accessToken');
        let stubGetUserInfo = sinon.stub(linkedInUtils, "getLinkedInSignupVariables").callsFake(function validatePass() {
            return [linkedInUid, userSignup, testEmail]
        });
        let stubGetSocialParams = sinon.stub(linkedInUtils, "getLinkedInSocialParams").returns(testLinkedInParams);

        res = await chai.request(server)
            .post('/link/linked-in')
            .set('Authorization', 'Bearer ' + tokenObj.token)
            .send(linkedInuserLinkingRequest);

        res.should.have.status(200);
        mongoDoc = await mongooseUserRepo.findOneByEmail(testEmail);
        mongoDoc.email.should.equal(testEmail);
        Number(mongoDoc.confidenceScore.linkedInScore.score).should.equal(Number(7.5));
        Number(mongoDoc.confidenceScore.aggregateScore.score).should.equal(Number(8.75));

        stubGetAccessToken.restore();
        stubGetUserInfo.restore();
        stubGetSocialParams.restore();
        stubFacebook.restore();
        socialVarsStub.restore();
    });
});