process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();

chai.use(chaiHttp);

describe('users.resendVerification', function () {

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /resendverification', () => {

        it('should fail if user does not exist', async (done) => {

            try {
                await chai.request(server)
                    .post('/resendverification')
                    .send({email: 'some@email.com'});

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(400);
                done();
            }
        });

        it('should fail if email is missing', async (done) => {

            try {
                await chai.request(server)
                    .post('/resendverification')
                    .send({email: ''});

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(422);
                done();
            }
        });

        it('should fail for verified user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, {});

                await chai.request(server)
                    .post('/resendverification')
                    .send({email: user.email});

                done(new Error("Expected to fail"));
            } catch (error) {
                error.response.status.should.equal(400);
                done();
            }
        });

        it('should succeed for unverified user', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.justSignUpNoVerify(user);

                let res = await chai.request(server)
                    .post('/resendverification')
                    .send({email: user.email});

                res.status.should.equal(200);

                done();
            } catch (error) {
                done(error);
            }
        })

    })
});