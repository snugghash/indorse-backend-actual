process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('../../companies/advisors/invitationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('user.changeETHAddress', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /users/:user_id/ethaddress', () => {

        


        it('it should return an error is the user has already used the ETH address for an active connection', async (done) => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};
            let userTokenObj = {};

            try {

                await authenticationWrapper.signupVerifyAuthenticate(existingUser,userTokenObj);
                await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

                await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

                await userSimulator.createUser(SC);

                let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

                let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);
                let existingUserFromMongo = await mongooseUserRepo.findOneByEmail(existingUser.email);
                

                await resolverLoop.clearAllTransactionQueues();

                await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                    ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS,adminUserFromMongo._id.toString());


                await resolverLoop.processTransactions();

                companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

                await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);


                await resolverLoop.processTransactions();


                let address_request = {
                    address : existingUserFromMongo.ethaddress
                }

                let res = await chai.request(server)
                    .post('/users/' + existingUserFromMongo._id + '/ethaddress')
                    .set('Authorization', 'Bearer ' + userTokenObj.token)
                    .send(address_request);

                done(new Error("Expected to fail"));

                
                done();
            } catch (error) {
                error.response.status.should.equal(400);
                done();
            }
        });
        
    })
});