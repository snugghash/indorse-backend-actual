process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const server = require('../../../server');
const should = chai.should();

chai.use(chaiHttp);

describe('users.login', function () {

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /login', () => {




        it('it should login when password and email match', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let loginRequest = {
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let res = await chai.request(server)
                    .post('/login')
                    .send(loginRequest);


                should.exist(res.body.token);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('it should login when password and email match, but email in login is in upper case', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let loginRequest = {
                email: 'PERSON@ANOTHER.COM',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let res = await chai.request(server)
                    .post('/login')
                    .send(loginRequest);


                should.exist(res.body.token);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('it should login when password and email match, but email in login is in lower case while user registered with upper case', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'PERSON@ANOTHER.COM',
                password: 'password'
            };

            let loginRequest = {
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let res = await chai.request(server)
                    .post('/login')
                    .send(loginRequest);


                should.exist(res.body.token);

                done();
            } catch (error) {
                done(error);
            }
        });

        it('it should login when password and email match and both registered and logging in with upper case email', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'PERSON@ANOTHER.COM',
                password: 'password'
            };

            let loginRequest = {
                email: 'person@another.com',
                password: 'password'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let res = await chai.request(server)
                    .post('/login')
                    .send(loginRequest);


                should.exist(res.body.token);

                done();
            } catch (error) {
                done(error);
            }
        });


        it('it should not login when password doesn\'t match', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let loginRequest = {
                email: 'person@another.com',
                password: 'password2'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                await chai.request(server)
                    .post('/login')
                    .send(loginRequest);

                done(new Error("Expected to fail!"));
            } catch (error) {
                done();
            }
        });

        it('it should not login when user is not verified', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let loginRequest = {
                email: 'person@another.com',
                password: 'password2'
            };

            try {
                await authenticationWrapper.justSignUpNoVerify(user);

                await chai.request(server)
                    .post('/login')
                    .send(loginRequest);

                done(new Error("Expected to fail!"));
            } catch (error) {
                done();
            }
        })
    })
});