process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('users.updateWork', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /updatework', () => {

        it('if there is no work item it should insert new asset into bigchain, create work item and update the user', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(1);

            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};


            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.work.length.should.equal(1);
            userFromMongo.work[0].title.should.equal(workUpdate.title);
            userFromMongo.work[0].location.should.equal(workUpdate.location);
            userFromMongo.work[0].start_date.year.should.equal(workUpdate.start_date.year);
            userFromMongo.work[0].start_date.month.should.equal(workUpdate.start_date.month);
            userFromMongo.work[0].end_date.year.should.equal(workUpdate.end_date.year);
            userFromMongo.work[0].end_date.month.should.equal(workUpdate.end_date.month);
            userFromMongo.work[0].currently_working.should.equal(workUpdate.currently_working);
            userFromMongo.work[0].activities.should.equal(workUpdate.activities);

            should.exist(userFromMongo.work[0].company_id);
        });

        it('if there is item_key in request it should only update user entry', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(3);

            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            let workUpdate2 = {
                company_name: 'test',
                title: 'Software Dev2',
                location: 'Singapore2',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee2",
                company_id: '1234'
            };

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

           await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            let item_key = userFromMongo.work[0].item_key;
            workUpdate2.item_key = item_key;
            workUpdate2.company_id = userFromMongo.work[0].company_id;

            response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate2);

            response.should.have.status(200);

            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.work.length.should.equal(1);
            userFromMongo.work[0].title.should.equal(workUpdate2.title);
            userFromMongo.work[0].location.should.equal(workUpdate2.location);
            userFromMongo.work[0].start_date.year.should.equal(workUpdate2.start_date.year);
            userFromMongo.work[0].start_date.month.should.equal(workUpdate2.start_date.month);
            userFromMongo.work[0].end_date.year.should.equal(workUpdate2.end_date.year);
            userFromMongo.work[0].end_date.month.should.equal(workUpdate2.end_date.month);
            userFromMongo.work[0].currently_working.should.equal(workUpdate2.currently_working);
            userFromMongo.work[0].activities.should.equal(workUpdate2.activities);

            should.exist(userFromMongo.work[0].company_id);
        });

        it('should create a new company if there is item_key in request and the company name or id do not match', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(3);

            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            let workUpdate2 = {
                company_name: 'test2',
                title: 'Software Dev2',
                location: 'Singapore2',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee2",
                company_id: '1234'
            };

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            let item_key = userFromMongo.work[0].item_key;
            workUpdate2.item_key = item_key;

            response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate2);

            response.should.have.status(200);
            let work = response.body.work;
            work.item_key.should.equal(item_key);
            work.company.company_name.should.equal(workUpdate2.company_name);
        });


        it('If company_name exists it should not insert a new one - case insensitive should also work', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(3);

            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            let workUpdate2 = {
                company_name: 'TEST',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
            };

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            response.should.have.status(200);

            response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate2);

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.work.length.should.equal(2);
            userFromMongo.work[1].title.should.equal(workUpdate2.title);
            userFromMongo.work[1].location.should.equal(workUpdate2.location);
            userFromMongo.work[1].start_date.year.should.equal(workUpdate2.start_date.year);
            userFromMongo.work[1].start_date.month.should.equal(workUpdate2.start_date.month);
            userFromMongo.work[1].end_date.year.should.equal(workUpdate2.end_date.year);
            userFromMongo.work[1].end_date.month.should.equal(workUpdate2.end_date.month);
            userFromMongo.work[1].currently_working.should.equal(workUpdate2.currently_working);
            userFromMongo.work[1].activities.should.equal(workUpdate2.activities);

            should.exist(userFromMongo.work[0].company_id);

            userFromMongo.work[0].company_id.should.equal(userFromMongo.work[1].company_id);

            let companyCount = await mongoCompanyRepo.count();

            companyCount.should.equal(1);
        });


        it('if there is work item it should not insert new asset into bigchain, create work item and update the user', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(3);

            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            workUpdate.item_key = testUtils.generateRandomString();

            await mongoCompanyRepo.insert(workUpdate);

            delete workUpdate.item_key;
            delete workUpdate._id;

            let response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.work[0].location.should.equal(workUpdate.location);

            should.exist(userFromMongo.work[0].company_id);

            let companyNameFromMongo = await mongoCompanyRepo.findOne({company_name: workUpdate.company_name});

            should.not.exist(companyNameFromMongo.keyPair);
        });

        it('it should be able to update work twice', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(1);
            let skillsUpdate2 = testUtils.generateRandomSkills(1);

            let skillsUpdateAll = {
                skills: [skillsUpdate.skills[0], skillsUpdate2.skills[0]]
            }
            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate.skills,
            };

            let workUpdate2 = {
                company_name: 'test2',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee",
                skills: skillsUpdate2.skills,
            };

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdateAll.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.work[0].location.should.equal(workUpdate.location);

            should.exist(userFromMongo.work[0].company_id);

            let companyNameFromMongo = await mongoCompanyRepo.findOne({company_name: workUpdate.company_name});

            response = await chai.request(server)
                .post('/updatework')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(workUpdate2);

            response.should.have.status(200);
            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.work.length.should.equal(2);

            userFromMongo.work[1].location.should.equal(workUpdate2.location);

            should.exist(userFromMongo.work[1].company_id);

            companyNameFromMongo = await mongoCompanyRepo.findOne({company_name: workUpdate2.company_name});

            companyNameFromMongo.company_name.should.equal(workUpdate2.company_name);
        });

        it('it should fail for unauthenticated user', async () => {

            let workUpdate = {
                company_name: 'test',
                title: 'Software Dev',
                location: 'Singapore',
                start_date: {"year": 2014, "month": 4},
                end_date: {"year": 2017, "month": 5},
                currently_working: true,
                activities: "making coffee"
            };

            try {
                await chai.request(server)
                    .post('/updatework')
                    .send(workUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    });
});
