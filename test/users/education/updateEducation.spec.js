process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoSchoolRepo = require('../../../models/services/mongo/mongoRepository')('school_names');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
chai.use(chaiHttp);

describe('users.updateEducation', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /updateeducation', () => {

        it('if there is no education item it should insert new asset into bigchain, create education item and update the user', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(1);

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.education.length.should.equal(1);
            userFromMongo.education[0].school_name.should.equal(educationUpdate.school_name);
            userFromMongo.education[0].degree.should.equal(educationUpdate.degree);
            userFromMongo.education[0].field_of_study.should.equal(educationUpdate.field_of_study);
            userFromMongo.education[0].grade.should.equal(educationUpdate.grade);
            userFromMongo.education[0].activities.should.equal(educationUpdate.activities);
            userFromMongo.education[0].start_date.year.should.equal(educationUpdate.start_date.year);
            userFromMongo.education[0].end_date.year.should.equal(educationUpdate.end_date.year);
            userFromMongo.education[0].description.should.equal(educationUpdate.description);

            should.exist(userFromMongo.education[0].school_id);

            let schoolNameFromMongo = await mongoSchoolRepo.findOne({school_name: educationUpdate.school_name});
        });

        it('if there is education item it should not insert new asset into bigchain or create education item - just update the user', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(1);

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            educationUpdate.item_key = testUtils.generateRandomString();

            await mongoSchoolRepo.insert(educationUpdate);

            delete educationUpdate.item_key;
            delete educationUpdate._id;

            let response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.education[0].school_name.should.equal(educationUpdate.school_name);
            userFromMongo.education[0].degree.should.equal(educationUpdate.degree);
            userFromMongo.education[0].field_of_study.should.equal(educationUpdate.field_of_study);
            userFromMongo.education[0].grade.should.equal(educationUpdate.grade);
            userFromMongo.education[0].activities.should.equal(educationUpdate.activities);
            userFromMongo.education[0].start_date.year.should.equal(educationUpdate.start_date.year);
            userFromMongo.education[0].end_date.year.should.equal(educationUpdate.end_date.year);
            userFromMongo.education[0].description.should.equal(educationUpdate.description);

            should.exist(userFromMongo.education[0].school_id);

            let schoolNameFromMongo = await mongoSchoolRepo.findOne({school_name: educationUpdate.school_name});

            should.not.exist(schoolNameFromMongo.keyPair);
        });

        it('if there is item_key in request it only update the user entry', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(3);

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let educationUpdate2 = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                school_id: testUtils.generateRandomString(),
                skills: skillsUpdate.skills,
            };

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await mongoSchoolRepo.insert(educationUpdate);

            delete educationUpdate._id;

            let response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate);

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            let item_key = userFromMongo.education[0].item_key;

            educationUpdate2.item_key = item_key;

            response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate2);

            response.should.have.status(200);

            userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.education[0].school_name.should.equal(educationUpdate2.school_name);
            userFromMongo.education[0].degree.should.equal(educationUpdate2.degree);
            userFromMongo.education[0].field_of_study.should.equal(educationUpdate2.field_of_study);
            userFromMongo.education[0].grade.should.equal(educationUpdate2.grade);
            userFromMongo.education[0].activities.should.equal(educationUpdate2.activities);
            userFromMongo.education[0].start_date.year.should.equal(educationUpdate2.start_date.year);
            userFromMongo.education[0].end_date.year.should.equal(educationUpdate2.end_date.year);
            userFromMongo.education[0].description.should.equal(educationUpdate2.description);
            userFromMongo.education[0].school_id.should.equal(educationUpdate2.school_id);
        });

        it('it should be able to update education twice', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(3);

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let educationUpdate2 = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.education[0].school_name.should.equal(educationUpdate.school_name);

            should.exist(userFromMongo.education[0].school_id);

            response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate2);

            response.should.have.status(200);
            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.education.length.should.equal(2);

            userFromMongo.education[1].school_name.should.equal(educationUpdate2.school_name);

            should.exist(userFromMongo.education[1].school_id);

            schoolNameFromMongo = await mongoSchoolRepo.findOne({school_name: educationUpdate2.school_name});
        });

        it('it should be able to update the same education item', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(1);
            let skillsUpdate2 = testUtils.generateRandomSkills(1);
            let skillsUpdateAll = {
                skills: [skillsUpdate.skills[0], skillsUpdate2.skills[0]],
            }

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let educationUpdate2 = {
                school_name: educationUpdate.school_name,
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "hard work",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate2.skills,
            };

            let user = testUtils.generateRandomUser()

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdateAll.skills);

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.education[0].school_name.should.equal(educationUpdate.school_name);
            educationUpdate2.item_key = userFromMongo.education[0].item_key;
            educationUpdate2.school_id = userFromMongo.education[0].school_id;

            should.exist(userFromMongo.education[0].school_id);

            let schoolNameFromMongo = await mongoSchoolRepo.findOne({school_name: educationUpdate.school_name});

            response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate2);

            response.should.have.status(200);
            userFromMongo = await mongoUserRepo.findOne({email: user.email});

            userFromMongo.education.length.should.equal(1);

            userFromMongo.education[0].school_id.should.equal(educationUpdate2.school_id);
        })

        it('it should work if user has no bigchain profile', async () => {

            let skillsUpdate = testUtils.generateRandomSkills(1);

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text',
                skills: skillsUpdate.skills,
            };

            let user = testUtils.generateRandomUser()

            let tokenObj = {};

            await testUtils.addSkillsToMongoCollection(skillsUpdate.skills);

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            await mongoUserRepo.update({email: user.email}, {$set: {keyPair: null}});

            educationUpdate.item_key = testUtils.generateRandomString();

            await mongoSchoolRepo.insert(educationUpdate);

            delete educationUpdate.item_key;

            delete educationUpdate._id;

            let response = await chai.request(server)
                .post('/updateeducation')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(educationUpdate);

            response.should.have.status(200);
            let userFromMongo = await mongoUserRepo.findOne({email: user.email});
            userFromMongo.education[0].school_name.should.equal(educationUpdate.school_name);
            userFromMongo.education[0].degree.should.equal(educationUpdate.degree);
            userFromMongo.education[0].field_of_study.should.equal(educationUpdate.field_of_study);
            userFromMongo.education[0].grade.should.equal(educationUpdate.grade);
            userFromMongo.education[0].activities.should.equal(educationUpdate.activities);
            userFromMongo.education[0].start_date.year.should.equal(educationUpdate.start_date.year);
            userFromMongo.education[0].end_date.year.should.equal(educationUpdate.end_date.year);
            userFromMongo.education[0].description.should.equal(educationUpdate.description);

            should.exist(userFromMongo.education[0].school_id);

            let schoolNameFromMongo = await mongoSchoolRepo.findOne({school_name: educationUpdate.school_name});
        });

        it('it should fail for unauthenticated user', async () => {

            let educationUpdate = {
                school_name: testUtils.generateRandomString(),
                degree: "useless degree",
                field_of_study: "wasting time",
                grade: "rekt",
                activities: "party",
                start_date: {
                    year: 2012
                },
                end_date: {
                    year: 2013
                },
                description: 'funny text'
            };


            try {
                await chai.request(server)
                    .post('/updateeducation')
                    .send(educationUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

    });
});


