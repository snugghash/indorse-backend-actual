process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoSchoolRepo = require('../../../models/services/mongo/mongoRepository')('school_names');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
chai.use(chaiHttp);

describe('users.findSchool', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    after((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('POST /findschool', () => {

        it('should return 0 school if 0 school matches in db', async () => {

            let res = await chai.request(server)
                .post('/findschool')
                .send({school_name: 'test'});

            res.body.schools.length.should.be.equal(0);
        });

        it('should return 1 school if 1 school matches in db', async () => {

            let schoolInDb = testUtils.generateRandomSchool();

            await mongoSchoolRepo.insert(schoolInDb);

            let res = await chai.request(server)
                .post('/findschool')
                .send({school_name: schoolInDb.school_name});

            res.body.schools.length.should.be.equal(1);
            res.body.schools[0].school_name.should.be.equal(schoolInDb.school_name);
        });
    });
});
