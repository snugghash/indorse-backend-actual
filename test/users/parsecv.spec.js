process.env.NODE_ENV = 'test';

const mongoUserRepo = require("../../models/services/mongo/mongoRepository")('users');
const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const settings = require('../../models/settings');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const should = chai.should();
const cvData = require('../data.json');
const config = require('config');
chai.use(chaiHttp);

describe('users.parseCV', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    it('should parse CV, update database and return success 200 - sample 5', async (done) => {

        let data_cv = cvData.cv_sample_5; // this is the data from clientside
        let user = {
            name: 'Another Person2',
            username: 'username2',
            email: 'person2@another.com',
            password: 'password',
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/parse-cv')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({cv: data_cv});

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(user.email);

            // testUtils.compareUserRecords(userFromMongo, userFromBc);
            should.exist(userFromMongo.last_linkedin_pdf_import_at);

            done();
        }
        catch (error) {
            done(error);
        }
    });

    it('should parse CV, update database and return success 200 - sample1', async (done) => {

        let data_cv = cvData.cv_sample_1; // this is the data from clientside
        let user = {
            name: 'Another Person2',
            username: 'username2',
            email: 'person@another.com',
            password: 'password',
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/parse-cv')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({cv: data_cv});

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(user.email);
            // testUtils.compareUserRecords(userFromMongo, userFromBc);
            should.exist(userFromMongo.last_linkedin_pdf_import_at);

            done();
        }
        catch (error) {
            done(error);
        }
    });

    it('should parse CV, update database and return success 200 - sample2', async (done) => {

        let data_cv = cvData.cv_june_jae; // This cv pdf was obtained on 20th June
        let user = {
            name: 'Another Person2',
            username: 'username2',
            email: 'person@another.com',
            password: 'password',
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/parse-cv')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({cv: data_cv});

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(user.email);
            // testUtils.compareUserRecords(userFromMongo, userFromBc);
            should.exist(userFromMongo.last_linkedin_pdf_import_at);

            done();
        }
        catch (error) {
            done(error);
        }
    });


    it('should parse CV, update database and return success 200 - sample 3', async (done) => {

        let data_cv = cvData.cv_sample_3; // this is the data from clientside
        let user = {
            name: 'Another Person2',
            username: 'username2',
            email: 'person@another.com',
            password: 'password',
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/parse-cv')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({cv: data_cv});

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(user.email);
            should.exist(userFromMongo.last_linkedin_pdf_import_at);

            done();
        }
        catch (error) {
            done(error);
        }
    });

    it('should parse CV, update database and return success 200 - sample 4', async (done) => {

        let data_cv = cvData.cv_sample_4; // this is the data from clientside
        let user = {
            name: 'Another Person',
            username: 'username',
            email: 'person@another.com',
            password: 'password',
        };

        let tokenObj = {};

        try {
            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let response = await chai.request(server)
                .post('/parse-cv')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({cv: data_cv});

            response.should.have.status(200);

            let userFromMongo = await mongoUserRepo.findOne({email: user.email});

            should.exist(userFromMongo);
            userFromMongo.email.should.equal(user.email);
            // testUtils.compareUserRecords(userFromMongo, userFromBc);
            should.exist(userFromMongo.last_linkedin_pdf_import_at);

            done();
        }
        catch (error) {
            done(error);
        }
    });


});
