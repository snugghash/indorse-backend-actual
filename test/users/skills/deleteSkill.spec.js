process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../../models/services/mongo/mongoose/mongooseSkillRepo');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.deleteSkill', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('DELETE /users/:id/skills/:skill_id', () => {


        it('should delete skill', async (done) => {

            let randomUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let skillInDb = testUtils.generateRandomUserSkill();

            try {
                let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(randomUser, tokenObj, 'full_access');
                await mongooseSkillRepo.insert(skillInDb);

                await chai.request(server)
                    .post('/users/' + createdUser._id + '/skills')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({name: skillInDb.name, level: 'intermediate'});

                let updatedUser = await mongooseUserRepo.findOne({email: randomUser.email});

                await chai.request(server)
                    .delete('/users/' + createdUser._id + '/skills/' + updatedUser.skills[0]._id)
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                updatedUser = await mongooseUserRepo.findOne({email: randomUser.email});

                updatedUser.skills.length.should.equal(0);

                done();
            } catch (error) {
                done(error);
            }


        })
    })
});