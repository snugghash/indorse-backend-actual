process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongoUserRepo = require('../../../models/services/mongo/mongoRepository')('users');
const mongoSkillsRepo = require('../../../models/services/mongo/mongoRepository')('skills');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.getCategories', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /skills/categories', () => {

        it('it should fetch 1 category', async (done) => {

            let adminUser = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password'
            };

            let skillInDb = {
                name: 'test',
                category : 'test'
            };

            let tokenObj = {};

            try {
                await authenticationWrapper.signupVerifyAuthenticate(adminUser, tokenObj);

                await mongoUserRepo.update({email: adminUser.email}, {$set: {role: 'admin'}});

                await mongoSkillsRepo.insert(skillInDb);

                let res = await chai.request(server)
                    .get('/skills/categories')
                    .send();

                res.should.have.status(200);

                res.body.length.should.be.equal(1);

                done();
            } catch (error) {
                done(error);
            }
        });
    })
});