process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const server = require('../../server');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('this is a template', function () {

    beforeEach((done) => {
        console.log('connecting to database')
        DB.connect(done)
    })

    afterEach((done) => {
        console.log('dropping database')
        DB.drop((done));
    })

    describe('something', () => {

        it('something', async (done) => {

            let user = {
                name: 'Another Person',
                username: 'username',
                email: 'person@another.com',
                password: 'password',
            };

            let tokenObj = {};

            try {
                let createdUser = await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

                let res = await chai.request(server)
                    .post('/somewhere')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({put: 'your request here'});
                done();
            } catch (error) {
                done(error);
            }
        })

    })
});

