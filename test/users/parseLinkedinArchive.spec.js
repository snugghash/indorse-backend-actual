process.env.NODE_ENV = 'test';

const mongoUserRepo = require("../../models/services/mongo/mongoRepository")('users');
const mongooseImportedContactsRepo = require("../../models/services/mongo/mongoose/mongooseImportedContactsRepo");
const mongooseConnectionsRepo = require("../../models/services/mongo/mongoose/mongooseConnectionsRepo");
const chai = require('chai');
const server = require('../../server');
const testUtils = require('../testUtils');
const chaiHttp = require('chai-http');
const settings = require('../../models/settings');
const authenticationWrapper = require('../authenticationWrapper');
const request = require('supertest');
const DB = require('../db');
const expect = chai.expect;
const should = chai.should();
const cvData = require('../data.json');
const config = require('config');
const fs = require('fs');
const FormData = require('form-data');
const axios = require('axios');

chai.use(chaiHttp);

describe('users.parseLinkedinArchive', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should parse Prasanna\'s archive', async function () {
        let tokenObj = {};

        let user = testUtils.generateRandomUser();

        // await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
        //
        // let o = {
        //     query: `mutation ($file: Upload!) {
        //         uploadLinkedinArchive (file: $file)
        //       }`,
        //     variables: {
        //         file: null
        //     }
        // };
        // let map = {
        //     '0': ['variables.file']
        // };
        // let fd = new FormData();
        // fd.append('operations', JSON.stringify(o));
        // fd.append('map', JSON.stringify(map));
        // fd.append(0, fs.createReadStream(`./test/data/data.zip`), 'data.zip');
        //
        // await axios.post('/graphql', fd, {
        //     headers: {
        //         ...fd.getHeaders()
        //     }
        // });
        //
        // let updatedUser = await mongoUserRepo.findOne({email: user.email});
        //
        // updatedUser.education.length.should.equal(1);
        // updatedUser.work.length.should.equal(13);
        //
        // let importedContacts = await mongooseImportedContactsRepo.findAll({user_id: updatedUser._id});
        // importedContacts.length.should.equal(5);
        //
        // let connections = await mongooseConnectionsRepo.findAll({user_id: updatedUser._id});
        // connections.length.should.equal(2);
    });
});