process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('skills.updateInterests', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /updateskills', () => {


        it('it should fail for unauthenticated user', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            let randomInterests = [testUtils.generateRandomString(),testUtils.generateRandomString()];

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            await chai.request(server)
                .post('/users/' + userFromMongo._id.toString() + '/interests')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({interests : randomInterests});

            userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            let userInterests = userFromMongo.interests;

            userInterests.length.should.equal(randomInterests.length);

            userInterests[0].should.equal(randomInterests[0]);
            userInterests[1].should.equal(randomInterests[1]);
        });

        it('it should fail fail when trying to update someone elses interests', async () => {

            let user = testUtils.generateRandomUser();
            let user2 = testUtils.generateRandomUser();

            let tokenObj = {};
            let tokenObj2 = {};

            let randomInterests = [testUtils.generateRandomString(),testUtils.generateRandomString()];

            await authenticationWrapper.signupVerifyAuthenticate(user, tokenObj);
            await authenticationWrapper.signupVerifyAuthenticate(user2, tokenObj2);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(user.email);

            try {
                await chai.request(server)
                    .post('/users/' + userFromMongo._id.toString() + '/interests')
                    .set('Authorization', 'Bearer ' + tokenObj2.token)
                    .send({interests : randomInterests});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});