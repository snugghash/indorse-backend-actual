process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http')
const companyCreationWrapper = require('./../companyCreationWrapper');
const DB = require('../../db');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('companies.socialLinks.replace', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('PUT /companies/:pretty_id/social_links/:type', () => {

        it('it should replace a social link given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let linkReplacementRequest = testUtils.generateRandomSocialLinkReplacementRequest();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .put('/companies/' + createCompanyRequest.pretty_id + '/social_links/' + createCompanyRequest.social_links[0].type)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(linkReplacementRequest);

            res.status.should.equal(200);

            let updatedCompany = await mongoCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            let updatedLink = updatedCompany.social_links[0];

            updatedLink.url.should.equal(linkReplacementRequest.url);
        });

        it('it should replace a social link with no admin given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let linkReplacementRequest = testUtils.generateRandomSocialLinkReplacementRequest();

            let tokenObj = {};

            delete createCompanyRequest.admin;

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .put('/companies/' + createCompanyRequest.pretty_id + '/social_links/' + createCompanyRequest.social_links[0].type)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(linkReplacementRequest);

            res.status.should.equal(200);

            let updatedCompany = await mongoCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            let updatedLink = updatedCompany.social_links[0];

            updatedLink.url.should.equal(linkReplacementRequest.url);
        });

        it('it should fail if link does not exists', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let linkReplacementRequest = testUtils.generateRandomSocialLinkReplacementRequest();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            try {
                await chai.request(server)
                    .put('/companies/' + createCompanyRequest.pretty_id + '/social_links/test')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(linkReplacementRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if company does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let linkReplacementRequest = testUtils.generateRandomSocialLinkReplacementRequest();

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            try {
                await chai.request(server)
                    .put('/companies/test/social_links/test')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(linkReplacementRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let linkReplacementRequest = testUtils.generateRandomSocialLinkReplacementRequest();

            try {
                await chai.request(server)
                    .put('/companies/test/social_links/test')
                    .send(linkReplacementRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is no admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let linkReplacementRequest = testUtils.generateRandomSocialLinkReplacementRequest();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            try {
                await chai.request(server)
                    .put('/companies/test/social_links/test')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(linkReplacementRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

    })
});