process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http')
const companyCreationWrapper = require('./../companyCreationWrapper');
const DB = require('../../db');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const authenticationWrapper = require('../../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('companies.socialLinks.add', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies/:pretty_id/social_links', () => {

        it('it should create a social link given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let linkCreationRequest = testUtils.generateRandomSocialLink();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/social_links')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(linkCreationRequest);

            res.status.should.equal(200);

            let updatedCompany = await mongoCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            updatedCompany.social_links.length.should.equal(2);

            let insertedLink = updatedCompany.social_links[1];

            insertedLink.should.deep.equal(linkCreationRequest);
        });

        it('it should create a social link for company with no admin given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let linkCreationRequest = testUtils.generateRandomSocialLink();

            let tokenObj = {};

            delete createCompanyRequest.admin;

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/social_links')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(linkCreationRequest);

            res.status.should.equal(200);

            let updatedCompany = await mongoCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            updatedCompany.social_links.length.should.equal(2);

            let insertedLink = updatedCompany.social_links[1];

            insertedLink.should.deep.equal(linkCreationRequest);
        });

        it('it should fail if link already exists', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            try{
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/social_links')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest.social_links[0]);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if company does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let linkCreationRequest = testUtils.generateRandomSocialLink();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            try {
                await chai.request(server)
                    .post('/companies/test/social_links')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(linkCreationRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let linkCreationRequest = testUtils.generateRandomSocialLink();

            try {
                await chai.request(server)
                    .post('/companies/test/social_links')
                    .send(linkCreationRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is no admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let linkCreationRequest = testUtils.generateRandomSocialLink();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            try {
                await chai.request(server)
                    .post('/companies/test/social_links')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(linkCreationRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

    })
});