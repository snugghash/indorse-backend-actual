process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const mongoDesignationsRepo = require('../../../models/services/mongo/mongoRepository')('designations');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../../authenticationWrapper');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('designations.findDesignations', function () {
    this.timeout(config.get('test.timeout'));

    beforeEach((done) => {
        console.log('connecting to database');
        DB.connect(done)
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop((done));
    });

    describe('GET /designations', () => {

        it('should return 0 designations if 0 designations matches in db', async () => {

            let res = await chai.request(server)
                .get('/designations')
                .query({designation: 'test'})
                .send();

            res.body.designations.length.should.be.equal(0);
        })

        it('should return 1 designation if 1 designation matches in db', async () => {

            let designationInDb = {
                designation: 'test'
            };

            await mongoDesignationsRepo.insert(designationInDb);

            let res = await chai.request(server)
                .get('/designations')
                .query({designation: designationInDb.designation})
                .send();

            res.body.designations.length.should.be.equal(1);
            res.body.designations[0].should.be.equal(designationInDb.designation);
        });

        it('should return 2 designations if 2 designations matches in db', async () => {
            let designationInDbAlpha = {
                designation: 'test'
            };

            let designationInDbBeta = {
                designation: 'test2'
            };

            await mongoDesignationsRepo.insert(designationInDbAlpha);
            await mongoDesignationsRepo.insert(designationInDbBeta);

            let res = await chai.request(server)
                .get('/designations')
                .query({designation: designationInDbAlpha.designation})
                .send();

            res.body.designations.length.should.be.equal(2);
        })

    });
});
