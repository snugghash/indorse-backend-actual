process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('companies.softRejectInvitation', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies/:pretty_id/advisors/:advisor_id/soft-reject', () => {

        it('should be able to reject an invitation for a user who does not exist on platform', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            res.should.have.status(200);
            let responseBody = res.body;
            should.exist(responseBody._id);
            let advisorId = responseBody._id

            let res2 = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('REJECTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);
        });

        it('should be able to reject and vaidate an invitation if already logged in and have linked to linkedin', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            await mongooseUserRepo.update({email: invitedUser.email}, {$set: {linkedIn_uid: '123865'}});

            let res2 = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')
                .set('Authorization', 'Bearer ' + userTokenObj.token)

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('REJECTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);
            should.exist(advisorFromMongo.softConnection.statusValidatedByUserAt);
        });

        it('should be able to reject but not vaidate an invitation if already logged in but not linked to linkedin', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')
                .set('Authorization', 'Bearer ' + userTokenObj.token)

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('REJECTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);
            should.not.exist(advisorFromMongo.softConnection.statusValidatedByUserAt);
        });

        it('should be able to vaidate a previously rejected invitation once they log in and have linked to linkedin', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('ACCEPTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);
            should.not.exist(advisorFromMongo.softConnection.statusValidatedByUserAt);

            await mongooseUserRepo.update({email: invitedUser.email}, {$set: {linkedIn_uid: '123865'}});

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')
                .set('Authorization', 'Bearer ' + userTokenObj.token)

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('REJECTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);
            should.exist(advisorFromMongo.softConnection.statusValidatedByUserAt);
        });

        it('should be able to reject an invitation that was previously rejected but not validated', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('ACCEPTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject');

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.status.should.equal('REJECTED');
            should.exist(advisorFromMongo.softConnection.statusUpdatedAt);
        });

        it('should fail to change an invitation that has been validated', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            await mongooseUserRepo.update({email: invitedUser.email}, {$set: {linkedIn_uid: '123865'}});

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')
                .set('Authorization', 'Bearer ' + userTokenObj.token)

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')
                    .set('Authorization', 'Bearer ' + userTokenObj.token)

                throw new Error("Expected to fail!");
            } catch(error) {
                error.status.should.equal(400);
            }
        });

        it('should fail if company does not exist', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            try {
                await chai.request(server)
                    .post('/companies/i_dont_exist/advisors/' + advisorId + '/soft-reject')
                    .set('Authorization', 'Bearer ' + userTokenObj.token)

                throw new Error("Expected to fail");
            } catch(error) {
                error.status.should.equal(400);
            }
        });

        it('should fail if no invitation is found', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/i_dont_exist/soft-reject')
                    .set('Authorization', 'Bearer ' + userTokenObj.token)

                throw new Error("Expected to fail");
            } catch(error) {
                error.status.should.equal(400);
            }
        });

        it('should fail to reject an invitation for a another user who is on the platform', async () => {
            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let userTokenObj = {};
            let anotherUser = testUtils.generateRandomUser();

            let anotherEmail = testUtils.generateRandomEmail();

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, userTokenObj);
            await authenticationWrapper.signupVerifyAuthenticate(anotherUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send({ email: anotherUser.email });

            let advisorId = res.body._id;

            await mongooseUserRepo.update({email: invitedUser.email}, {$set: {linkedIn_uid: '123865'}});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-reject')
                    .set('Authorization', 'Bearer ' + userTokenObj.token);

                throw new Error('Expected to fail');
            } catch(error) {
                error.status.should.equal(403);
            }
        });
    })
});