process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const resolverLoop = require('../../../smart-contracts/services/cron/resolverLoop');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('companies.getUserAdvisors', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /users/:user_id/advisories', () => {


        it('it should return one existing user', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(existingUser.email);

            let res = await chai.request(server)
                .get('/users/' + userFromMongo._id + '/advisories')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.exist(existingAdvisor.company);
            should.not.exist(existingAdvisor.email);
            existingAdvisor.signed_by_company.should.equal(false);
            existingAdvisor.signed_by_user.should.equal(false);
        });



    })
});