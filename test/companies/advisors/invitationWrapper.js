process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
const companyCreationWrapper = require('../companyCreationWrapper');

exports.creteCompanyAdminAndInviteUserByEmail = async function creteCompanyAdminAndInviteUserByEmail(user, company, tokenObj, inviteeEmail) {
    await companyCreationWrapper.authenticateAndCreateCompany(user, company, tokenObj);

    let res = await chai.request(server)
        .post('/companies/' + company.pretty_id + '/advisor')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({email : inviteeEmail});

    res.should.have.status(200);

    return tokenObj;
};

exports.justInviteUser = async function justInviteUser(company, tokenObj, inviteeEmail) {

    let res = await chai.request(server)
        .post('/companies/' + company.pretty_id + '/advisor')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({email : inviteeEmail});

    res.should.have.status(200);

    return tokenObj;
};

exports.softInviteAdvisor = async function softInviteAdvisor(company, tokenObj, inviteeEmail) {
    let res = await chai.request(server)
        .post('/companies/' + company.pretty_id  + '/advisor/soft-invite')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({ email: inviteeEmail });        

    res.should.have.status(200);

    return tokenObj;
};
