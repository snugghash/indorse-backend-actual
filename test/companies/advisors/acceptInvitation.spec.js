process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseTxQueueRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');


chai.use(chaiHttp);

describe('companies.acceptInvitation', function () {
    this.timeout(config.get('test.timeout'));

    let SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies/:pretty_id/advisors/:advisor_id/accept', () => {

        it('it should accept user invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];
            res = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            advisor = updatedCompany.advisors.find(a => a._id.toString() === advisor._id);

            should.exist(advisor.accepted_timestamp);
        });

        it('it should fail if trying to accept somebody else\'s invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];
            try {
                res = await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if invitation is already rejected', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});


            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            await mongooseCompanyRepo.setRejectedTimestamp(randomCompany.pretty_id, advisor._id, 666);

            try {
                await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if advisor invitation does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, testUtils.generateRandomEmail());

            try {
                await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/advisors/some-id/accept')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if advisor company does not exist', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};


            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            try {
                await chai.request(server)
                    .post('/companies/some-id/advisors/some-id/accept')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            try {
                await chai.request(server)
                    .post('/companies/some-id/advisors/some-id/accept')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('accepting invitation should return status 0 if user has no ethaddress', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(0);
        });

        it('accepting invitation should return status 1 if user has no entity. After the user creates entity and connection the status should be 5', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(1);

            await resolverLoop.processTransactions();

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await userSimulator.createUser(SC);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);


            result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(5);
        });

        it('accepting invitation should return status 3 if there is no connection between company and user entity', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];
            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(3);

            //Trigger a duplicate transaction for acccept connection , it should not fail
            let result2 = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result2.should.have.status(200);

            let mongoQueue = await mongooseTxQueueRepo.count({'tx_metadata.function_name': 'addConnection'})
            mongoQueue.should.equal(1);
        });

        it('accepting invitation should return status 4 if there is a connection between company and user entity, but not from user to company', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(4);


            res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);
        });

        it('accepting invitation should return status 5 if there is a bidirectional connection between company and user entity', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await userSimulator.createUser(SC, invitedUserFromMongo._id.toString());

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);

            let txHash = "0xc150c53c64ce525cfe67d175d7fec74e85b953ae7481440e178db9a85e744cd7";

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send({tx_hash : txHash});

            result.body.statusCode.should.equal(5);


            res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            advisor = retrievedAdvisors[0];

            advisor.user_tx_hash.should.equal(txHash);
            should.exist(advisor.user_tx_hash);
            should.exist(advisor.verification_timestamp);
        });

    })
});