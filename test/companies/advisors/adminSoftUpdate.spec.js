process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('companies.adminSoftUpdate', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies/:pretty_id/advisors/:advisor_id/admin-update', () => {

        it('should approved an accepted advisor', async () => {

            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            res.should.have.status(200);
            let responseBody = res.body;
            should.exist(responseBody._id);
            let advisorId = responseBody._id

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            res.should.have.status(200);
            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({status: 'approved'})

            res.should.have.status(200);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.companyStatus.should.equal('APPROVED');
            should.exist(advisorFromMongo.softConnection.companyStatusUpdatedAt);

            let adminFromMongo = await mongooseUserRepo.findOneByUsername(adminUser.username);
            advisorFromMongo.softConnection.companyStatusUpdatedBy.id.should.equal(adminFromMongo._id.toString());
        });

        it('should disaproove an accepted advisor', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            res.should.have.status(200);
            let responseBody = res.body;
            should.exist(responseBody._id);
            let advisorId = responseBody._id

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            res.should.have.status(200);
            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({status: 'disapproved'})

            res.should.have.status(200);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.companyStatus.should.equal('DISAPPROVED');
            should.exist(advisorFromMongo.softConnection.companyStatusUpdatedAt);

            let adminFromMongo = await mongooseUserRepo.findOneByUsername(adminUser.username);
            advisorFromMongo.softConnection.companyStatusUpdatedBy.id.should.equal(adminFromMongo._id.toString());
        });

        it('should disaproove an approved advisor', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            res.should.have.status(200);
            let advisorId = res.body._id

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            res.should.have.status(200);
            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({status: 'approved'})

            res.should.have.status(200);
            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({status: 'disapproved'})

            res.should.have.status(200);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let advisorFromMongo = companyFromMongo.advisors[0];
            advisorFromMongo.softConnection.companyStatus.should.equal('DISAPPROVED');
            should.exist(advisorFromMongo.softConnection.companyStatusUpdatedAt);
        });

        it('should fail if company does not exist', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            await mongooseCompanyRepo.deleteOne({email: createCompanyRequest.email});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({status: 'approved'})
            } catch(error) {
                error.status.should.equal(400)
            }
        });

        it('should fail if advisor does not exist', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            await mongooseCompanyRepo.update({email: createCompanyRequest.email}, {$set: {'advisors.0._id': '5b3f75e546283c37ccb101cc'}});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({status: 'approved'})
            } catch(error) {
                error.status.should.equal(400)
            }
        });

        it('should fail if advisor has not been invited', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            await mongooseCompanyRepo.update({email: createCompanyRequest.email}, {$set: {'advisors.0.softConnection': null}});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({status: 'approved'})
            } catch(error) {
                error.status.should.equal(400);
            }
        });

        it('should fail if advisor has not accepted invitation', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({status: 'approved'})
            } catch(error) {
                error.status.should.equal(400);
            }
        });

        it('should fail if not logged in', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            res.should.have.status(200);
            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({status: 'approved'})

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                    .send({status: 'approved'})
            } catch(error) {
                error.status.should.equal(403);
            }
        });

        it('should fail if not admin', async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUser.email });

            let advisorId = res.body._id;

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/soft-accept')

            res.should.have.status(200);
            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({status: 'approved'})

            let anotherRegularUser = testUtils.generateRandomUser();
            let regularUserToken = {};
            await authenticationWrapper.signupVerifyAuthenticate(anotherRegularUser, regularUserToken);

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' + advisorId + '/admin-update')
                    .set('Authorization', 'Bearer ' + regularUserToken.token)
                    .send({status: 'approved'})
            } catch(error) {
                error.status.should.equal(403);
            }
        });
        
    })
});