process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const companyToUserChecker = require('../../../models/services/ethereum/connections/proxy/companyToUserChecker');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');

chai.use(chaiHttp);

describe('companies.inviteAdvisor', function () {
    this.timeout(config.get('test.timeout'));

    let SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
        
    });

    describe('/companies/:pretty_id/advisor', () => {

        it('it should invite existing user by email, company should be getting an entity if it doesn\'t have one', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            await resolverLoop.processTransactions();

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);
            should.exist(companyFromMongo.entity_ethaddress);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
        });

        it('it should return no users if no advisor array exists', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, randomCompany,
                tokenObj, adminUser.username);

            await mongoCompanyRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {advisors : ''}});

            let res = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);
        });

        it('it should invite existing user by email - if company has an entity and user has no entity no connection should be created', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);


            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            let pendingTxCount = await mongooseTransactionQueueRepo.countAllWithStatus('QUEUED');

            pendingTxCount.should.equal(0);

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);
            should.exist(companyFromMongo.entity_ethaddress);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            should.not.exist(advisorFromMongo.company_tx_hash);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());

            let connectionExists = await companyToUserChecker.checkIfConnectionExists(invitedUserFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            connectionExists.should.equal(false);
        });

        it('it should invite existing user by email - if company has an entity and user has an entity a connection should be created', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await userSimulator.createUser(SC);

            await mongooseUserRepo.update({email: invitedUser.email}, {ethaddress: userSimulator.senderAddress});

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);


            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            res.should.have.status(200);

            let pendingTxCount = await mongooseTransactionQueueRepo.countAllWithStatus('QUEUED');

            pendingTxCount.should.equal(1);

            await resolverLoop.processTransactions();

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);
            should.exist(companyFromMongo.entity_ethaddress);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            should.exist(advisorFromMongo.company_tx_hash);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());

            let connectionExists = await companyToUserChecker.checkIfConnectionExists(invitedUserFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            connectionExists.should.equal(true);
        });

        it('it should invite existing user by email - if company has no entity and user has an entity an entity and  connection should be created', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await resolverLoop.processTransactions();

            await mongooseCompanyRepo.update({pretty_id : createCompanyRequest.pretty_id}, {$unset : {entity_ethaddress : undefined}});

            await userSimulator.createUser(SC);

            await resolverLoop.clearAllTransactionQueues();

            await mongooseUserRepo.update({email: invitedUser.email}, {ethaddress: userSimulator.senderAddress});

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            res.should.have.status(200);

            let pendingTxCount = await mongooseTransactionQueueRepo.countAllWithStatus('QUEUED');

            pendingTxCount.should.equal(1);

            await resolverLoop.processTransactions();

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);
            should.exist(companyFromMongo.entity_ethaddress);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            should.exist(advisorFromMongo.company_tx_hash);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());

            let connectionExists = await companyToUserChecker.checkIfConnectionExists(invitedUserFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            connectionExists.should.equal(true);
        });


        it('it should invite existing user by username', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({username: invitedUser.username});

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
        });

        it('it should invite existing user by user_id', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({user_id: invitedUserFromMongo._id.toString()});

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            companyFromMongo.advisors.length.should.equal(1);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
        });

        it('it should fail for duplicate invitation of existing user by email', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({email: invitedUser.email});

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail for duplicate invitation of non-existing user by email', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({email: invitedUser.email});

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail for duplicate invitation by username', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({username: invitedUser.username});

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({username: invitedUser.username});

                throw new Error("Expected to fail!");
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('iit should fail for duplicate invitation  by user_id', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({user_id: invitedUserFromMongo._id.toString()});

            try {
                await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({user_id: invitedUserFromMongo._id.toString()});
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should invite non-existing user by email', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUserEmail = testUtils.generateRandomEmail();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUserEmail});

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.not.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            companyFromMongo.advisors.length.should.equal(1);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            advisorFromMongo.email.should.equal(invitedUserEmail);
        });

        it('after inviting user by email it should link him with user_id after he signs up', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            res.should.have.status(200);

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            should.not.exist(advisorFromMongo.email);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
        });

        it('it should fail if company does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});
            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, roles.ADMIN.name);

            try {
                let res = await chai.request(server)
                    .post('/companies/someid/advisor')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({email: invitedUser.email});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user does not exist and email is not provided', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUserEmail = testUtils.generateRandomEmail();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                    tokenObj, adminUser.username);

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({username: invitedUserEmail});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if no valid query fields are present', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                    tokenObj, adminUser.username);

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send({});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });


        it('it should fail if user is not superadmin', async () => {

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUserEmail = testUtils.generateRandomEmail();

            let nonAminTokenObj = {};
            let requestingUser = testUtils.generateRandomUser();
            await authenticationWrapper.signupVerifyAuthenticate(requestingUser, nonAminTokenObj);

            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .set('Authorization', 'Bearer ' + nonAminTokenObj.token)
                    .send({email: invitedUserEmail});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUserEmail = testUtils.generateRandomEmail();

            let adminUser = testUtils.generateRandomUser();
            let adminTokenObj = {};
            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                adminTokenObj, adminUser.username);

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                    .send({email: invitedUserEmail});

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should invite existing user by email', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};
            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, inviteeTokenObj);

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});

            res.should.have.status(200);

            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);

            let advisorFromMongo = companyFromMongo.advisors[0];

            should.exist(advisorFromMongo.creation_timestamp);
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());

            await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisors/' +
                    advisorFromMongo._id.toString() + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();
        });
    })
});