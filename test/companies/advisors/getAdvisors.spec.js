process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('companies.getAdvisors', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /companies/:pretty_id/advisosr', () => {

        it('it should return one unverified user', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,testUtils.generateRandomEmail());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let unverifiedAdvisor = retrievedAdvisors[0];

            should.not.exist(unverifiedAdvisor.email);
            should.not.exist(unverifiedAdvisor.verification_timestamp);
            should.exist(unverifiedAdvisor.creation_timestamp);
            should.exist(unverifiedAdvisor._id);
            unverifiedAdvisor.status.should.equal('unverified');
        });


        it('it should return two unverified users if there are two', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,testUtils.generateRandomEmail());
            await invitationWrapper.justInviteUser(randomCompany,tokenObj,testUtils.generateRandomEmail());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(2);

            for(let unverifiedAdvisor of retrievedAdvisors) {
                should.not.exist(unverifiedAdvisor.email);
                should.not.exist(unverifiedAdvisor.verification_timestamp);
                should.exist(unverifiedAdvisor.creation_timestamp);
                should.exist(unverifiedAdvisor._id);
                unverifiedAdvisor.status.should.equal('unverified');
            }
        });

        it('it should return one existing user', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.not.exist(existingAdvisor.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user.id);
            should.exist(existingAdvisor.status);
        });

        it('it should return no users if no advisor array exists', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await mongooseUserRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {advisors : undefined}});

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser,randomCompany,tokenObj);

            await mongoCompanyRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {advisors : ''}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(0);
        });


        it('it should return one existing and verified user if bidirectional connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS,adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.not.exist(existingAdvisor.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user.id);
            existingAdvisor.status.should.equal('verified');
        });

        it('it should return one existing and unverified user if if only company to user connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.not.exist(existingAdvisor.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user.id);
            existingAdvisor.status.should.equal('unverified');
        });

        it('it should return one existing and unverified user if if only user to company connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});


            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.not.exist(existingAdvisor.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user.id);
            existingAdvisor.status.should.equal('unverified');
        });


        it('it should return two existing users', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser1 = testUtils.generateRandomUser();
            let existingUser2 = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser1,{});
            await authenticationWrapper.signupVerifyAuthenticate(existingUser2,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser1.email);
            await invitationWrapper.justInviteUser(randomCompany,tokenObj,existingUser2.email);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(2);

            for(let existingAdvisor of retrievedAdvisors) {
                should.not.exist(existingAdvisor.email);
                should.not.exist(existingAdvisor.verification_timestamp);
                should.exist(existingAdvisor.creation_timestamp);
                should.exist(existingAdvisor._id);
                should.exist(existingAdvisor.user.username);
                should.exist(existingAdvisor.user.name);
                should.exist(existingAdvisor.user.id);
                should.exist(existingAdvisor.status);
            }
        });

        it('it should fail if company does not exist', async () => {

            try {
                await chai.request(server)
                    .get('/companies/some-id/advisors')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });
    })
});