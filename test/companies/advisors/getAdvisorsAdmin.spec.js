process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('companies.getAdvisorsAdmin', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /companies/:pretty_id/advisors/admin', () => {

        it('it should return one unverified user', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,testUtils.generateRandomEmail());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let unverifiedAdvisor = retrievedAdvisors[0];

            should.exist(unverifiedAdvisor.email);
            should.not.exist(unverifiedAdvisor.verification_timestamp);
            should.not.exist(unverifiedAdvisor.user);
            should.exist(unverifiedAdvisor.creation_timestamp);
            should.exist(unverifiedAdvisor._id);
            should.exist(unverifiedAdvisor.signed_by_company);
            should.exist(unverifiedAdvisor.signed_by_user);
        });


        it('it should return two unverified users if there are two', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,testUtils.generateRandomEmail());
            await invitationWrapper.justInviteUser(randomCompany,tokenObj,testUtils.generateRandomEmail());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(2);

            for(let unverifiedAdvisor of retrievedAdvisors) {
                should.exist(unverifiedAdvisor.email);
                should.not.exist(unverifiedAdvisor.verification_timestamp);
                should.not.exist(unverifiedAdvisor.user);
                should.exist(unverifiedAdvisor.creation_timestamp);
                should.exist(unverifiedAdvisor._id);
                should.exist(unverifiedAdvisor.signed_by_company);
                should.exist(unverifiedAdvisor.signed_by_user);
            }
        });

        it('it should return one existing user', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user._id);
            should.exist(existingAdvisor.user.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.not.exist(existingAdvisor.email);
            existingAdvisor.signed_by_company.should.equal(false);
            existingAdvisor.signed_by_user.should.equal(false);
        });

        it('it should return one existing user - signed_by_company should be true if company to user connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);


            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user._id);
            should.exist(existingAdvisor.user.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.not.exist(existingAdvisor.email);
            existingAdvisor.signed_by_company.should.equal(true);
            existingAdvisor.signed_by_user.should.equal(false);
        });

        it('it should return one existing user - signed_by_user should be true if user to company connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user._id);
            should.exist(existingAdvisor.user.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.not.exist(existingAdvisor.email);
            existingAdvisor.signed_by_company.should.equal(false);
            existingAdvisor.signed_by_user.should.equal(true);
        });

        it('it should return no users if no advisor array exists', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await mongooseUserRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {advisors : undefined}});

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser,randomCompany,tokenObj);

            await mongoCompanyRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {advisors : ''}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(0);
        });

        it('it should return one existing user - both signed_by_user and signed_by_company should be true if a bidirectional connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

                await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
                await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email);

                await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

                await userSimulator.createUser(SC);

                let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

                let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

                await resolverLoop.clearAllTransactionQueues();

                await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                    ConnectionType.IS_ADVISOR_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

                await resolverLoop.processTransactions();

                companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_ADVISOR_OF);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let existingAdvisor = retrievedAdvisors[0];

            should.exist(existingAdvisor.user);
            should.exist(existingAdvisor.user.username);
            should.exist(existingAdvisor.user.name);
            should.exist(existingAdvisor.user._id);
            should.exist(existingAdvisor.user.email);
            should.not.exist(existingAdvisor.verification_timestamp);
            should.exist(existingAdvisor.creation_timestamp);
            should.exist(existingAdvisor._id);
            should.not.exist(existingAdvisor.email);
            existingAdvisor.signed_by_company.should.equal(true);
            existingAdvisor.signed_by_user.should.equal(true);
        });

        it('it should return two existing users with ETH invitation, and two soft invited users', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser1 = testUtils.generateRandomUser();
            let existingUser2 = testUtils.generateRandomUser();
            let existingUser3 = testUtils.generateRandomUser();
            let newUser4 = "hello@hello.com"
            // One soft invited user on the platform and one not on the platform

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser1,{});
            await authenticationWrapper.signupVerifyAuthenticate(existingUser2,{});
            await authenticationWrapper.signupVerifyAuthenticate(existingUser3, {});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser1.email);
            await invitationWrapper.justInviteUser(randomCompany,tokenObj,existingUser2.email);
            await invitationWrapper.softInviteAdvisor(randomCompany, tokenObj, existingUser3.email);
            await invitationWrapper.softInviteAdvisor(randomCompany, tokenObj, newUser4);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors/admin')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            console.log("Retrived advisors : " + JSON.stringify(retrievedAdvisors));

            retrievedAdvisors.length.should.equal(4);

            //Check attributes common to all 3
            for(let existingAdvisor of retrievedAdvisors) {
                if (existingAdvisor.email == existingUser1.email || 
                    existingAdvisor.email == existingUser2.email ||
                    existingAdvisor.email == existingUser3.email){

                        should.exist(existingAdvisor.user);
                        should.exist(existingAdvisor.user.username);
                        should.exist(existingAdvisor.user.name);
                        should.exist(existingAdvisor.user._id);
                        should.exist(existingAdvisor.user.email);
                        should.not.exist(existingAdvisor.verification_timestamp);
                        should.exist(existingAdvisor.creation_timestamp);
                        should.exist(existingAdvisor._id);
                        should.not.exist(existingAdvisor.email);
                        should.exist(existingAdvisor.signed_by_company);
                        should.exist(existingAdvisor.signed_by_user);
                }

                //Check attributes common to only soft invites
                if (existingAdvisor.email == existingUser3.email ||
                    existingAdvisor.email == newUser4){
                        should.exist(existingAdvisor.softConnection.status);
                        should.exist(existingAdvisor.softConnection.statusUpdatedAt);
                    }
                
                //Check attributes common to only soft invited users not on platform
                if ( existingAdvisor.email == newUser4) {
                    should.exist(existingAdvisor.email);                    
                }                    
            }
        });

        it('it should fail if user is not an admin', async () => {

            let user = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(user,tokenObj);

            try {
                await chai.request(server)
                    .get('/companies/some-id/advisors/admin')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let user = testUtils.generateRandomUser();

            try {
                await chai.request(server)
                    .get('/companies/some-id/advisors/admin')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});