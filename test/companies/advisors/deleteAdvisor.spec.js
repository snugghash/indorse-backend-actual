process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const companyToUserChecker = require('../../../models/services/ethereum/connections/proxy/companyToUserChecker');
const userToCompanyChecker = require('../../../models/services/ethereum/connections/proxy/userToCompanyChecker');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');

chai.use(chaiHttp);

describe('companies.deleteAdvisor', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('DELETE /companies/:pretty_id/advisors/:advisor_id', () => {

        it('it should delete advisor and connection when advisor requests it with tx_hash', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);
            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await resolverLoop.processTransactions();

            let companyToUserConnectionExists = await companyToUserChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            companyToUserConnectionExists.should.equal(true);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            res = await chai.request(server)
                .delete('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id)
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send({tx_hash : '0x44182a9664bd72b5fdf0a0b1d76b83a4e18ca6341c6ea4941634fb2186a37962'});

            res.should.have.status(200);

            await resolverLoop.processTransactions();

            let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            advisor = updatedCompany.advisors.find(a => a._id.toString() === advisor._id);

            updatedCompany.revoked_advisors.length.should.equal(1);

            let revokedAdvisor = updatedCompany.revoked_advisors[0];

            should.exist(revokedAdvisor.revoked_tx_hash);

            should.not.exist(advisor);

            companyToUserConnectionExists = await companyToUserChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            companyToUserConnectionExists.should.equal(false);
        });

        it('it should delete advisor and connection when advisor requests it without tx_hash', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);
            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await resolverLoop.processTransactions();

            let companyToUserConnectionExists = await companyToUserChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            companyToUserConnectionExists.should.equal(true);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);



            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            res = await chai.request(server)
                .delete('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id)
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            await resolverLoop.processTransactions();

            let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            advisor = updatedCompany.advisors.find(a => a._id.toString() === advisor._id);

            updatedCompany.revoked_advisors.length.should.equal(1);

            let revokedAdvisor = updatedCompany.revoked_advisors[0];

            should.not.exist(revokedAdvisor.revoked_tx_hash);

            should.not.exist(advisor);

            companyToUserConnectionExists = await companyToUserChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            companyToUserConnectionExists.should.equal(false);
        });

        it('it should fail delete advisor and connection when advisor requests it but user to company connection still exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);
            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);

            await resolverLoop.processTransactions();

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let companyToUserConnectionExists = await companyToUserChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            companyToUserConnectionExists.should.equal(true);

            await userSimulator.addConnection(SC, companyFromMongo.entity_ethaddress, ConnectionType.IS_ADVISOR_OF);

            let userToCompanyConnection = await userToCompanyChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            userToCompanyConnection.should.equal(true);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            try {
                await chai.request(server)
                    .delete('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id)
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should not fail delete advisor and connection when admin requests it but user to company connection still exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);
            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);

            await resolverLoop.processTransactions();

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let companyToUserConnectionExists = await companyToUserChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            companyToUserConnectionExists.should.equal(true);

            await userSimulator.addConnection(SC, companyFromMongo.entity_ethaddress, ConnectionType.IS_ADVISOR_OF);

            let userToCompanyConnection = await userToCompanyChecker.checkIfConnectionExists(userFromMongo._id.toString(),
                companyFromMongo._id.toString(), ConnectionType.IS_ADVISOR_OF);

            userToCompanyConnection.should.equal(true);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            await chai.request(server)
                .delete('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id)
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send();

            res.should.have.status(200);
        });

        it('it should delete advisor when admin requests it', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            res = await chai.request(server)
                .delete('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id)
                .set('Authorization', 'Bearer ' + adminTokenObj.token)
                .send();

            res.should.have.status(200);

            let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            advisor = updatedCompany.advisors.find(a => a._id.toString() === advisor._id);

            should.not.exist(advisor);
        });

        it('it should fail if neither advisor nor admin tries to delete', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();
            let randomUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};
            let randomUserTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);
            await authenticationWrapper.signupVerifyAuthenticate(randomUser, randomUserTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/advisors')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedAdvisors = res.body;

            retrievedAdvisors.length.should.equal(1);

            let advisor = retrievedAdvisors[0];

            try {
                await chai.request(server)
                    .delete('/companies/' + randomCompany.pretty_id + '/advisors/' + advisor._id)
                    .set('Authorization', 'Bearer ' + randomUserTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });


        it('it should fail if advisor invitation does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, testUtils.generateRandomEmail());

            try {
                await chai.request(server)
                    .delete('/companies/' + randomCompany.pretty_id + '/advisors/some-id')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if advisor company does not exist', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            try {
                await chai.request(server)
                    .delete('/companies/some-id/advisors/some-id/accept')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            try {
                await chai.request(server)
                    .delete('/companies/some-id/advisors/some-id')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});