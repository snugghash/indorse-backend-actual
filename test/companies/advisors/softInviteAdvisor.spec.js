process.env.NODE_ENV = 'test';
const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('companies.inviteAdvisor', function () {
    this.timeout(config.get('test.timeout'));

    let SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);        
    });

    describe('/companies/:pretty_id/advisor', () => {

        it('it should invite existing user by email', async () => {

            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUser = testUtils.generateRandomUser();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedUser.email});            

            res.should.have.status(200);
            let responseBody = res.body;            

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(invitedUser.email);

            companyFromMongo.advisors.length.should.equal(1);        

            let advisorFromMongo = companyFromMongo.advisors[0];            
            advisorFromMongo.user_id.should.equal(invitedUserFromMongo._id.toString());
            advisorFromMongo.softConnection.status.should.equal('PENDING');
        });


        it("should invite non existing user on platform", async () => {
            let adminUser = testUtils.generateRandomUser();
            let createCompanyRequest = testUtils.generateRandomCompany();
            let invitedUserEmail = "hello@hello.com"
            let tokenObj = {};
            
            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor/soft-invite')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({ email: invitedUserEmail });

            res.should.have.status(200);
            let responseBody = res.body;

            should.exist(responseBody._id);
            should.exist(responseBody.creation_timestamp);
            should.not.exist(responseBody.user);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            companyFromMongo.advisors.length.should.equal(1);

            let advisorFromMongo = companyFromMongo.advisors[0];            
            advisorFromMongo.softConnection.status.should.equal('PENDING');
        })

        it("should fail to invite existing advisor to a company which was softinvited earlier", async () => {

        })


        it("should fail to invite existing advisor to a company which was invited via eth address earlier", async () => {

        })
    })
 
});