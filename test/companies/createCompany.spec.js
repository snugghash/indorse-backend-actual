process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoCompanyRepo = require('../../models/services/mongo/mongoRepository')('company_names');
const mongooseCompanyRepo = require('../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authenticationWrapper = require('../authenticationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const resolverLoop = require('../smart-contracts/cron/resolverLoopWrapper');
const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
chai.use(chaiHttp);

describe('companies.createCompany', function () {
    this.timeout(config.get('test.timeout'));
   
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies', () => {

        it('it should create a company given valid request + create company entity', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};
            let randomBoolean = testUtils.generateRandomBoolean();

            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            createCompanyRequest.admin = createdUser.username;
            createCompanyRequest.visible_to_public = randomBoolean;
            let res = await chai.request(server)
                .post('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createCompanyRequest);

            res.should.have.status(200);

            await resolverLoop.processTransactions();

            await testUtils.wait(config.get('test.bcTransactionTime'));

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            companyFromMongo.created_by_admin.should.equal(true);
            companyFromMongo.last_updated_by.should.equal(adminUser.username);
            should.exist(companyFromMongo.entity_ethaddress);

            companyFromMongo.pretty_id.should.equal(createCompanyRequest.pretty_id);
            companyFromMongo.company_name.should.equal(createCompanyRequest.company_name);
            companyFromMongo.description.should.equal(createCompanyRequest.description);
            companyFromMongo.admin_username.should.equal(createCompanyRequest.admin);
            expect(companyFromMongo.additional_data).to.deep.equal(createCompanyRequest.additional_data);
            companyFromMongo.admin.should.equal(createdUser._id);
            companyFromMongo.social_links.length.should.equal(1);
            companyFromMongo.social_links[0].type.should.equal(createCompanyRequest.social_links[0].type);
            companyFromMongo.social_links[0].url.should.equal(createCompanyRequest.social_links[0].url);
            companyFromMongo.visible_to_public.should.equal(randomBoolean);
            let fullCompanyFromMongo = await mongoCompanyRepo.findOne({pretty_id: createCompanyRequest.pretty_id});

            should.exist(fullCompanyFromMongo.last_updated_timestamp);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            companyFromMongo.email.should.equal(createCompanyRequest.email);
        });

        it('it should create 5 companies given valid request + create company entity', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequests = [testUtils.generateRandomCompany(), testUtils.generateRandomCompany()
                , testUtils.generateRandomCompany(), testUtils.generateRandomCompany(), testUtils.generateRandomCompany()];

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            for (let company of createCompanyRequests) {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(company);
            }

            await testUtils.wait(config.get('test.bcTransactionTime'));

            for (let company of createCompanyRequests) {
                await resolverLoop.processTransactions();
                let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(company.pretty_id);

                should.exist(companyFromMongo.entity_ethaddress);
            }
        });


        it('it should create a company given valid request with no admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            createCompanyRequest.admin = createdUser._id;

            delete createCompanyRequest.admin;

            let res = await chai.request(server)
                .post('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createCompanyRequest);

            res.should.have.status(200);

            await testUtils.wait(config.get('test.bcTransactionTime'));

            let companyFromMongo = res.body;

            companyFromMongo.created_by_admin.should.equal(true);
            companyFromMongo.last_updated_by.should.equal(adminUser.username);

            companyFromMongo.pretty_id.should.equal(createCompanyRequest.pretty_id);
            companyFromMongo.company_name.should.equal(createCompanyRequest.company_name);
            companyFromMongo.description.should.equal(createCompanyRequest.description);
            expect(companyFromMongo.additional_data).to.deep.equal(createCompanyRequest.additional_data);
            should.not.exist(companyFromMongo.admin);
            companyFromMongo.social_links.length.should.equal(1);
            companyFromMongo.social_links[0].type.should.equal(createCompanyRequest.social_links[0].type);
            companyFromMongo.social_links[0].url.should.equal(createCompanyRequest.social_links[0].url);
            companyFromMongo.visible_to_public.should.equal(false);


            let fullCompanyFromMongo = await mongoCompanyRepo.findOne({pretty_id: createCompanyRequest.pretty_id});
            should.exist(fullCompanyFromMongo.last_updated_timestamp);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            companyFromMongo.email.should.equal(createCompanyRequest.email);
        });

        it('it should fail if pretty_id already in use', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createCompanyRequest.admin;

            await chai.request(server)
                .post('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createCompanyRequest);

            delete createCompanyRequest.email;

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if email already in use', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createCompanyRequest.admin;

            await chai.request(server)
                .post('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createCompanyRequest);

            createCompanyRequest.pretty_id = 'some-other-id';

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });


        it('it should fail if company_name is missing', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createCompanyRequest.company_name;

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
            error.status.should.equal(422);
            }
        });

        it('it should fail if pretty_id is missing', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createCompanyRequest.pretty_id;

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail if admin user does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            createCompanyRequest.admin = 'aaaaaaaaaaaaaaaaaaaaaaaa';

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if logo_data is missing', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createCompanyRequest.logo_data;

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(422);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            await authenticationWrapper.justSignUpNoVerify(normalUser);

            try {
                await chai.request(server)
                    .post('/companies')
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            let createdUser = await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            createCompanyRequest.admin = createdUser._id;

            try {
                await chai.request(server)
                    .post('/companies')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});