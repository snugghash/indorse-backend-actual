process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('companies.getUserTeamMembers', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /users/:user_id/team_memberships', () => {


        it('it should return one existing user', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();
            let designationName = testUtils.generateRandomDesignation();
            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,
                existingUser.email, designationName);

            let userFromMongo = await mongooseUserRepo.findOneByEmail(existingUser.email);

            let res = await chai.request(server)
                .get('/users/' + userFromMongo._id + '/team_memberships')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let existingTeamMember = retrievedTeamMembers[0];

            should.not.exist(existingTeamMember.verification_timestamp);
            should.exist(existingTeamMember.creation_timestamp);
            should.exist(existingTeamMember._id);
            should.exist(existingTeamMember.company);
            should.not.exist(existingTeamMember.email);
            existingTeamMember.signed_by_company.should.equal(false);
            existingTeamMember.signed_by_user.should.equal(false);
            existingTeamMember.designation.should.equal(designationName);
        });
    })
});