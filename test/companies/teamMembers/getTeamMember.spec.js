process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('companies.getTeamMember', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('GET /companies/:pretty_id/team_members/admin', () => {

        it('it should return one unverified user', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();
            let designationName = testUtils.generateRandomDesignation()
            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,
                testUtils.generateRandomEmail(), designationName);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let teamMember = companyFromMongo.team_members[0];

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_member/' + teamMember._id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let existingTeamMember = res.body;

            existingTeamMember.signed_by_company.should.equal(false);
            existingTeamMember.signed_by_user.should.equal(false);
            existingTeamMember.designation.should.equal(designationName);
        });

        it('it should return one existing user ', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email, testUtils.generateRandomDesignation());

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let teamMember = companyFromMongo.team_members[0];

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_member/' + teamMember._id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.should.have.status(200);

            let existingTeamMember = res.body;

            existingTeamMember.signed_by_company.should.equal(false);
            existingTeamMember.signed_by_user.should.equal(false);
        });

        it('it should return no users when company has no teamMembers array', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser, randomCompany, tokenObj);

            await mongoCompanyRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {teamMembers : ''}});

            try {
                await chai.request(server)
                    .get('/companies/' + randomCompany.pretty_id + '/team_member/some-id')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            try {
                await chai.request(server)
                    .get('/companies/some-id/team_members/admin')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });


    })
});
