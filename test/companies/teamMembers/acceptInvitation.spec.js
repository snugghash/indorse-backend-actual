
process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('companies.acceptTeamMemberInvitation', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies/:pretty_id/team_members/:teamMember_id/accept', () => {

        it('it should accept user invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            res = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            teamMember = updatedCompany.team_members.find(a => a._id.toString() === teamMember._id);

            should.exist(teamMember.accepted_timestamp);
        });

        it('it should fail if trying to accept somebody else\'s invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            try {
                res = await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if invitation is already rejected', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});


            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            await mongooseCompanyRepo.teamMember.setRejectedTimestamp(randomCompany.pretty_id, teamMember._id, 666);

            try {
                await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if teamMember invitation does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                    testUtils.generateRandomEmail(), testUtils.generateRandomDesignation());

            try {
                await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/team_members/some-id/accept')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if teamMember company does not exist', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            try {
                await chai.request(server)
                    .post('/companies/some-id/team_members/some-id/accept')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {$set: {ethaddress: '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            try {
                await chai.request(server)
                    .post('/companies/some-id/team_members/some-id/accept')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('accepting invitation should return status 0 if user has no ethaddress', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(0);
        });

        it('accepting invitation should return status 1 if user has no entity. After the user creates entity and connection the status should be 5', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(1);

            await resolverLoop.processTransactions();

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);
            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await userSimulator.createUser(SC);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_TEAM_MEMBER_OF);

            result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(5);
        });

        it('accepting invitation should return status 3 if there is no connection between company and user entity', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);


            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(3);
        });

        it('accepting invitation should return status 4 if there is a connection between company and user entity, but not from user to company', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_TEAM_MEMBER_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            result.body.statusCode.should.equal(4);


            res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            teamMember = retrievedTeamMembers[0];
        });

        it('accepting invitation should return status 5 if there is a bidirectional connection between company and user entity', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser, inviteeTokenObj);

            await mongooseUserRepo.update({email: inviteeUser.email}, {ethaddress: userSimulator.senderAddress});

            let invitedUserFromMongo = await mongooseUserRepo.findOneByEmail(inviteeUser.email);

            await userSimulator.createUser(SC, invitedUserFromMongo._id.toString());

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_TEAM_MEMBER_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_TEAM_MEMBER_OF);

            let txHash = "0xc150c53c64ce525cfe67d175d7fec74e85b953ae7481440e178db9a85e744cd7";

            await resolverLoop.processTransactions();

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            let result = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/accept')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send({tx_hash : txHash});

            result.body.statusCode.should.equal(5);


            res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            teamMember = retrievedTeamMembers[0];

            teamMember.user_tx_hash.should.equal(txHash);
            should.exist(teamMember.user_tx_hash);
            should.exist(teamMember.verification_timestamp);
        });

    })
});
