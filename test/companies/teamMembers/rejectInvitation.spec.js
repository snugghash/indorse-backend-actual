process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');

chai.use(chaiHttp);

describe('companies.rejectInvitation', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });


    describe('POST /companies/:pretty_id/team_members/:teamMember_id/reject', () => {

        it('it should reject user invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser,inviteeTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            res = await chai.request(server)
                .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/reject')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            teamMember = updatedCompany.team_members.find(a => a._id.toString() === teamMember._id);

            should.exist(teamMember.rejected_timestamp);
        });

        it('it should fail if trying to reject somebody else\'s invitation', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser,inviteeTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            try {
                res = await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/reject')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if invitation is already rejected', async () => {

            let adminUser = testUtils.generateRandomUser();
            let inviteeUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};
            let inviteeTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,adminTokenObj,
                inviteeUser.email, testUtils.generateRandomDesignation());
            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser,inviteeTokenObj);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let teamMember = retrievedTeamMembers[0];

            await mongooseCompanyRepo.teamMember.setRejectedTimestamp(randomCompany.pretty_id, teamMember._id, 666);

            try {
                await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/team_members/' + teamMember._id + '/reject')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });

        it('it should fail if teamMember invitation does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let adminTokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,adminTokenObj,
                testUtils.generateRandomEmail(), testUtils.generateRandomDesignation());

            try {
                await chai.request(server)
                    .post('/companies/' + randomCompany.pretty_id + '/team_members/some-id/accept')
                    .set('Authorization', 'Bearer ' + adminTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if teamMember company does not exist', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser,inviteeTokenObj);
            await mongooseUserRepo.update({email : inviteeUser.email}, {$set :{ethaddress : '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            try {
                await chai.request(server)
                    .post('/companies/some-id/team_members/some-id/accept')
                    .set('Authorization', 'Bearer ' + inviteeTokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let inviteeUser = testUtils.generateRandomUser();

            let inviteeTokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(inviteeUser,inviteeTokenObj);

            await mongooseUserRepo.update({email : inviteeUser.email}, {$set :{ethaddress : '0xb2930b35844a230f00e51431acae96fe543a0347'}});

            try {
                await chai.request(server)
                    .post('/companies/some-id/team_members/some-id/accept')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});
