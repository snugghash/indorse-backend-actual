process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseDesignationRepo = require('../../../models/services/mongo/mongoose/mongooseDesignationRepo')

exports.creteCompanyAdminAndInviteUserByEmail = async function creteCompanyAdminAndInviteUserByEmail(user, company, tokenObj, inviteeEmail, inviteeDesignation) {
    await companyCreationWrapper.authenticateAndCreateCompany(user, company, tokenObj);

    await addDesignation(inviteeDesignation);

    let res = await chai.request(server)
        .post('/companies/' + company.pretty_id + '/team_member')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({email : inviteeEmail, designation:inviteeDesignation});

    res.should.have.status(200);

    return tokenObj;
};

exports.justInviteUser = async function justInviteUser(company, tokenObj, inviteeEmail, inviteeDesignation) {
    await addDesignation(inviteeDesignation);

    let res = await chai.request(server)
        .post('/companies/' + company.pretty_id + '/team_member')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send({email : inviteeEmail, designation:inviteeDesignation});

    res.should.have.status(200);

    return tokenObj;
};

async function addDesignation(designationName) {
    let existingDesignation = await mongooseDesignationRepo.findOne({designation: designationName});

    if(!existingDesignation) {
        await mongooseDesignationRepo.insert({designation: designationName});
    }
}