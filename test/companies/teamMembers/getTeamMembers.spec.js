process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('./invitationWrapper');
const companyCreationWrapper = require('../companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongoCompanyRepo = require('../../../models/services/mongo/mongoRepository')('company_names');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../smart-contracts/cron/resolverLoopWrapper');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('companies.getTeamMembers', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /companies/:pretty_id/team_member', () => {

        it('it should return one unverified user', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();
            let designationName = testUtils.generateRandomDesignation();

            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,testUtils.generateRandomEmail(), designationName);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let unverifiedTeamMember = retrievedTeamMembers[0];

            should.not.exist(unverifiedTeamMember.email);
            should.not.exist(unverifiedTeamMember.verification_timestamp);
            should.exist(unverifiedTeamMember.creation_timestamp);
            should.exist(unverifiedTeamMember._id);
            should.exist(unverifiedTeamMember.designation)

            unverifiedTeamMember.status.should.equal('unverified');
        });


        it('it should return two unverified users if there are two', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,
                testUtils.generateRandomEmail(), testUtils.generateRandomDesignation());
            await invitationWrapper.justInviteUser(randomCompany,tokenObj,testUtils.generateRandomEmail(),testUtils.generateRandomDesignation());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(2);

            for(let unverifiedTeamMember of retrievedTeamMembers) {
                should.not.exist(unverifiedTeamMember.email);
                should.not.exist(unverifiedTeamMember.verification_timestamp);
                should.exist(unverifiedTeamMember.creation_timestamp);
                should.exist(unverifiedTeamMember._id);
                unverifiedTeamMember.status.should.equal('unverified');
            }
        });

        it('it should return one existing user', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email, testUtils.generateRandomDesignation());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let existingTeamMember = retrievedTeamMembers[0];

            should.not.exist(existingTeamMember.email);
            should.not.exist(existingTeamMember.verification_timestamp);
            should.exist(existingTeamMember.creation_timestamp);
            should.exist(existingTeamMember._id);
            should.exist(existingTeamMember.user);
            should.exist(existingTeamMember.user.username);
            should.exist(existingTeamMember.user.name);
            should.exist(existingTeamMember.user.id);
            should.exist(existingTeamMember.status);
        });

        it('it should return no users if no teamMember array exists', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await mongooseUserRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {teamMembers : undefined}});

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser,randomCompany,tokenObj);

            await mongoCompanyRepo.update({pretty_id : randomCompany.pretty_id}, {$unset : {teamMembers : ''}});

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(0);
        });


        it('it should return one existing and verified user if bidirectional connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email,testUtils.generateRandomDesignation());

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_TEAM_MEMBER_OF, Direction.FORWARDS,adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_TEAM_MEMBER_OF);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let existingTeamMember = retrievedTeamMembers[0];

            should.not.exist(existingTeamMember.email);
            should.not.exist(existingTeamMember.verification_timestamp);
            should.exist(existingTeamMember.creation_timestamp);
            should.exist(existingTeamMember._id);
            should.exist(existingTeamMember.user);
            should.exist(existingTeamMember.user.username);
            should.exist(existingTeamMember.user.name);
            should.exist(existingTeamMember.user.id);
            existingTeamMember.status.should.equal('verified');
        });

        it('it should return one existing and unverified user if if only company to user connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email,testUtils.generateRandomDesignation());

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});

            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            await resolverLoop.clearAllTransactionQueues();

            await createVirtualEntityAndConnectionTxExecutor.execute(companyFromMongo._id.toString(), userSimulator.senderAddress,
                ConnectionType.IS_TEAM_MEMBER_OF, Direction.FORWARDS, adminUserFromMongo._id.toString());

            await resolverLoop.processTransactions();

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let existingTeamMember = retrievedTeamMembers[0];

            should.not.exist(existingTeamMember.email);
            should.not.exist(existingTeamMember.verification_timestamp);
            should.exist(existingTeamMember.creation_timestamp);
            should.exist(existingTeamMember._id);
            should.exist(existingTeamMember.user);
            should.exist(existingTeamMember.user.username);
            should.exist(existingTeamMember.user.name);
            should.exist(existingTeamMember.user.id);
            existingTeamMember.status.should.equal('unverified');
        });

        it('it should return one existing and unverified user if if only user to company connection exists', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser.email,testUtils.generateRandomDesignation());

            await mongooseUserRepo.update({email: existingUser.email}, {ethaddress: userSimulator.senderAddress});


            await userSimulator.createUser(SC);

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            let adminUserFromMongo = await mongooseUserRepo.findOneByEmail(adminUser.email);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(randomCompany.pretty_id);

            await userSimulator.addConnection(SC,companyFromMongo.entity_ethaddress,ConnectionType.IS_TEAM_MEMBER_OF);

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(1);

            let existingTeamMember = retrievedTeamMembers[0];

            should.not.exist(existingTeamMember.email);
            should.not.exist(existingTeamMember.verification_timestamp);
            should.exist(existingTeamMember.creation_timestamp);
            should.exist(existingTeamMember._id);
            should.exist(existingTeamMember.user);
            should.exist(existingTeamMember.user.username);
            should.exist(existingTeamMember.user.name);
            should.exist(existingTeamMember.user.id);
            existingTeamMember.status.should.equal('unverified');
        });


        it('it should return two existing users', async () => {

            let adminUser = testUtils.generateRandomUser();
            let existingUser1 = testUtils.generateRandomUser();
            let existingUser2 = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(existingUser1,{});
            await authenticationWrapper.signupVerifyAuthenticate(existingUser2,{});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser,randomCompany,tokenObj,existingUser1.email,testUtils.generateRandomDesignation());
            await invitationWrapper.justInviteUser(randomCompany,tokenObj,existingUser2.email,testUtils.generateRandomDesignation());

            let res = await chai.request(server)
                .get('/companies/' + randomCompany.pretty_id + '/team_members')
                .send();

            res.should.have.status(200);

            let retrievedTeamMembers = res.body;

            retrievedTeamMembers.length.should.equal(2);

            for(let existingTeamMember of retrievedTeamMembers) {
                should.not.exist(existingTeamMember.email);
                should.not.exist(existingTeamMember.verification_timestamp);
                should.exist(existingTeamMember.creation_timestamp);
                should.exist(existingTeamMember._id);
                should.exist(existingTeamMember.user.username);
                should.exist(existingTeamMember.user.name);
                should.exist(existingTeamMember.user.id);
                should.exist(existingTeamMember.status);
            }
        });

        it('it should fail if company does not exist', async () => {

            try {
                await chai.request(server)
                    .get('/companies/some-id/team_members')
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });
    })
});
