process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoCompanyRepo = require('../../models/services/mongo/mongoRepository')('company_names');
const mongooseCompanyRepo = require('../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authenticationWrapper = require('../authenticationWrapper');
const companyCreationWrapper = require('./companyCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('companies.updateCompany', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('PATCH /companies/:pretty_id', () => {

        it('it should update company fields when request is correct', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();
            let isVisibleTopublic = testUtils.generateRandomBoolean();
            let tokenObj = {};

            let createdAdmin = await companyCreationWrapper.authenticateAndCreateCompany(adminUser,
                createCompanyRequest, tokenObj);

            companyUpdate.admin = adminUser.username;
            companyUpdate.visible_to_public = isVisibleTopublic;

            let res = await chai.request(server)
                .patch('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(companyUpdate);

            res.status.should.equal(200);

            let companyFromMongo = res.body;

            companyFromMongo.created_by_admin.should.equal(true);
            companyFromMongo.last_updated_by.should.equal(adminUser.username);

            companyFromMongo.pretty_id.should.equal(createCompanyRequest.pretty_id);
            companyFromMongo.company_name.should.equal(companyUpdate.company_name);
            companyFromMongo.description.should.equal(companyUpdate.description);
            companyFromMongo.admin.should.equal(createdAdmin._id);
            companyFromMongo.admin_username.should.equal(createdAdmin.username);
            companyFromMongo.visible_to_public.should.equal(isVisibleTopublic);
            expect(companyFromMongo.additional_data).to.deep.equal(companyUpdate.additional_data);

            let fullCompanyFromMongo = await mongoCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            should.exist(fullCompanyFromMongo.last_updated_timestamp);
            should.exist(fullCompanyFromMongo.admin);
            should.exist(fullCompanyFromMongo.admin_username);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            companyFromMongo.email.should.equal(companyUpdate.email);
        });

        it('it should update company field visible_to_public to false, when it is already flagged as false', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();
            let isVisibleTopublic = true;
            let tokenObj = {};
            let companyUpdateRequestVisibleToFalse = {visible_to_public:false};
            let createdAdmin = await companyCreationWrapper.authenticateAndCreateCompany(adminUser,
                createCompanyRequest, tokenObj);

            companyUpdate.admin = adminUser.username;
            companyUpdate.visible_to_public = isVisibleTopublic;

            let res = await chai.request(server)
                .patch('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(companyUpdate);

            res.status.should.equal(200);

            let companyFromMongo = res.body;

            companyUpdate.visible_to_public = false;
            res = await chai.request(server)
                .patch('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(companyUpdateRequestVisibleToFalse);

            let updatedCompanyFromMongo = res.body;
            updatedCompanyFromMongo.visible_to_public.should.equal(false);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            companyFromMongo.email.should.equal(companyUpdate.email);
        });

        it('it should update company fields with no admin when request is correct from any admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();

            let tokenObj = {};

            delete createCompanyRequest.admin;

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .patch('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(companyUpdate);

            res.status.should.equal(200);

            let companyFromMongo = res.body;

            companyFromMongo.created_by_admin.should.equal(true);
            companyFromMongo.last_updated_by.should.equal(adminUser.username);

            companyFromMongo.pretty_id.should.equal(createCompanyRequest.pretty_id);
            companyFromMongo.company_name.should.equal(companyUpdate.company_name);
            companyFromMongo.description.should.equal(companyUpdate.description);
            expect(companyFromMongo.additional_data).to.deep.equal(companyUpdate.additional_data);

            let fullCompanyFromMongo = await mongoCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            should.exist(fullCompanyFromMongo.last_updated_timestamp);
            should.not.exist(fullCompanyFromMongo.admin);
            should.not.exist(fullCompanyFromMongo.admin_username);

            companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);
            companyFromMongo.email.should.equal(companyUpdate.email);
        });

        it('it should return 404 for non-existing company', async () => {

            let adminUser = testUtils.generateRandomUser();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            try {
                await chai.request(server)
                    .patch('/companies/blah')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(companyUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });


        it('it should fail if user is not logged in', async () => {

            let normalUser = testUtils.generateRandomUser();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();

            await authenticationWrapper.justSignUpNoVerify(normalUser);

            try {
                await chai.request(server)
                    .patch('/companies/blag')
                    .send(companyUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is no admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            try {
                await chai.request(server)
                    .patch('/companies/blag')
                    .send(companyUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if email already in use', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();
            let createCompanyRequest2 = testUtils.generateRandomCompany();
            let companyUpdate = testUtils.generateRandomCompanyUpdate();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            delete createCompanyRequest.admin;

            await chai.request(server)
                .post('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createCompanyRequest);

            await chai.request(server)
                .post('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(createCompanyRequest2);

            companyUpdate.email = createCompanyRequest2.email;

            try {
                await chai.request(server)
                    .patch('/companies/' + createCompanyRequest.pretty_id)
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(companyUpdate);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });
    })
});