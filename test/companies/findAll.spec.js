process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const mongoCompanyRepo = require('../../models/services/mongo/mongoose/mongooseCompanyRepo')    
const authenticationWrapper = require('../authenticationWrapper');
const companyCreationWrapper = require('./companyCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');

chai.use(chaiHttp);

describe('companies.findAll', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
        //done();
    });


    describe('GET /companies', () => {

        it('When no search param it should retrieve all created companies', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let companyCount = 10;

            for (let index = 0; index < companyCount; index++) {
                let randomCompany = testUtils.generateRandomCompany();
                randomCompany.admin = createdAdmin.username;
                await companyCreationWrapper.createCompany(randomCompany, tokenObj);
            }

            let res = await chai.request(server)
                .get('/companies')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(companyCount);
            res.body.totalCompanies.should.equal(companyCount);
            should.not.exist(res.body.matchingCompanies);
        });

        it('Pagination should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let tokenObj = {};

            let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            let companyCount = 10;

            for (let index = 0; index < companyCount; index++) {
                let randomCompany = testUtils.generateRandomCompany();
                randomCompany.admin = createdAdmin.username;
                await companyCreationWrapper.createCompany(randomCompany, tokenObj);
            }

            let res = await chai.request(server)
                .get('/companies?pageNumber=' + 1 + '&pageSize=2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(2);

            res = await chai.request(server)
                .get('/companies?pageNumber=' + companyCount + '&pageSize=2')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(0);

            res = await chai.request(server)
                .get('/companies?pageNumber=' + 2 + '&pageSize=9')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(1);
        });

        it('Search should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            randomCompany.admin = createdAdmin.username;

            await companyCreationWrapper.createCompany(randomCompany, tokenObj);

            let res = await chai.request(server)
                .get('/companies?search=' + randomCompany.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(1);
            res.body.totalCompanies.should.equal(1);
            res.body.matchingCompanies.should.equal(1);
        });

        it('Sort should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            randomCompany.admin = createdAdmin.username;

            await companyCreationWrapper.createCompany(randomCompany, tokenObj);

            let res = await chai.request(server)
                .get('/companies?sort=pretty_id')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(1);
        });

        it('Reverse sort should work', async () => {

            let adminUser = testUtils.generateRandomUser();

            let randomCompany = testUtils.generateRandomCompany();

            let tokenObj = {};

            let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            randomCompany.admin = createdAdmin.username;

            await companyCreationWrapper.createCompany(randomCompany, tokenObj);

            let res = await chai.request(server)
                .get('/companies?sort=-pretty_id')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.body.companies.length.should.equal(1);
        });


        it('Should return company aggregated data as requested ', async () => {

            let adminUser = testUtils.generateRandomUser();
            let randomCompany1 = testUtils.generateRandomCompany();
            randomCompany1.company_name = "Test1"
            let randomCompany2 = testUtils.generateRandomCompany();
            randomCompany2.company_name = "Test2"

            let tokenObj = {};

            let createdAdmin = await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');
            randomCompany1.admin = createdAdmin.username;

            await companyCreationWrapper.createCompany(randomCompany1, tokenObj);
            await companyCreationWrapper.createCompany(randomCompany2, tokenObj);

            let signatureData = {
                "advisors":
                [
                        {
                        "company_tx_hash": "abc",
                        "user_tx_hash": "def"
                        }

                ],
                "team_members":
                [
                        {
                            "user_id": "dummy1"
                        },
                        {
                            "company_tx_hash": "hij",
                            "user_tx_hash": "klm"
                        },
                        {
                            "user_id": "dummy2"
                        }
                ]
            }

            await mongoCompanyRepo.update({ company_name: 'Test1' }, signatureData);
            let res = await chai.request(server)
                .get('/companies-validation')
                .send();

            let resultObj = JSON.parse(res.text);
            resultObj.companies.length.should.equal(2);

            for(let companyResult of resultObj.companies){
                if(companyResult.name ==='Test1'){
                    companyResult.total_team_members.should.equal(3);
                    companyResult.total_advisors.should.equal(1);
                    companyResult.verified_team_members.should.equal(1);
                    companyResult.verified_advisors.should.equal(1);
                } else if (companyResult.name === 'Test2'){
                    companyResult.total_team_members.should.equal(0);
                    companyResult.total_advisors.should.equal(0);
                    companyResult.verified_team_members.should.equal(0);
                    companyResult.verified_advisors.should.equal(0);
                } else {
                    throw new Errror("Opps found a new company in result cursor")
                }
            }
        });
    })
});
