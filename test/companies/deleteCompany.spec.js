process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http')
const companyCreationWrapper = require('./companyCreationWrapper');
const DB = require('../db');
const mongoooseCompanyRepo = require('../../models/services/mongo/mongoose/mongooseCompanyRepo');
const authenticationWrapper = require('../authenticationWrapper');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
chai.use(chaiHttp);

describe('companies.deleteCompany', function () {

    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('DELETE /companies/:pretty_id', () => {

        it('it should delete a company given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .delete('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.status.should.equal(200);

            let deletedCompany = await mongoooseCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            should.not.exist(deletedCompany);
        });

        it('it should delete a company with no admin given valid request from any admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            delete createCompanyRequest.admin;

            await companyCreationWrapper.authenticateAndCreateCompanyWithoutAdmin(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .delete('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            res.status.should.equal(200);

            let deletedCompany = await mongoooseCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});

            should.not.exist(deletedCompany);
        });

        it('it should fail if company does not exist', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

            try {
                await chai.request(server)
                    .delete('/companies/test')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });

        it('it should fail if user is not logged in', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            await authenticationWrapper.justSignUpNoVerify(normalUser);

            try {
                await chai.request(server)
                    .delete('/companies/' + createCompanyRequest.pretty_id)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not an admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticateWithRole(normalUser, tokenObj, 'full_access');

            try {
                await chai.request(server)
                    .delete('/companies/' + createCompanyRequest.pretty_id)
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send(createCompanyRequest);

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });

        it('it should fail if user is not the company admin', async () => {

            let adminUser = testUtils.generateRandomUser();

            let secondAdminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            let secondAdmin =  await authenticationWrapper.signupVerifyAuthenticateWithRole(secondAdminUser, {}, 'admin');

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, secondAdmin.username);

            try {
                let res = await chai.request(server)
                    .delete('/companies/' + createCompanyRequest.pretty_id)
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});