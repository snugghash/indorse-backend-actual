process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../authenticationWrapper');
const companyCreationWrapper = require('./companyCreationWrapper');
const mongooseCompanyRepo = require('../../models/services/mongo/mongoose/mongooseCompanyRepo');
const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
const resolverLoop = require('../smart-contracts/cron/resolverLoopWrapper');
const mongooseTxQueueRepo = require('../../models/services/mongo/mongoose/mongooseTransactionQueueRepo');
const mongooseTransactionRepo = require('../../models/services/mongo/mongoose/mongooseTransactionRepo');

chai.use(chaiHttp);

describe('companies.createEntity', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('POST /companies/:pretty_id/entity', () => {

        it('it should create entity for company if it doesnt have one', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await resolverLoop.processTransactions();

            await mongooseCompanyRepo.update({pretty_id : createCompanyRequest.pretty_id}, {$unset : {entity_ethaddress : undefined}});
            await resolverLoop.clearAllTransactionQueues();

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/entity')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();

            await resolverLoop.processTransactions();

            let companyFromMongo = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

            should.exist(companyFromMongo.entity_ethaddress);
        });

        it('it should fail if company has an entity ethaddress', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await resolverLoop.processTransactions();

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/entity')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(400);
            }
        });


        it('it should not allow duplicate transactions ', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let invitedUser = testUtils.generateRandomUser();

            let tokenObj = {};

            await authenticationWrapper.signupVerifyAuthenticate(invitedUser, {});

            await companyCreationWrapper.authenticateAndCreateCompanyWithAdmin(adminUser, createCompanyRequest,
                tokenObj, adminUser.username);

            await resolverLoop.processTransactions();

            let companyDoc = await mongooseCompanyRepo.findOne({pretty_id : createCompanyRequest.pretty_id});
            let companyId = companyDoc._id.toString();
            await mongooseCompanyRepo.update({pretty_id : createCompanyRequest.pretty_id}, {$unset : {entity_ethaddress : undefined}});
            await mongooseTxQueueRepo.deleteOne({'tx_metadata.callback_data.company_id': companyId})
            await mongooseTransactionRepo.deleteOne({'tx_metadata.callback_data.company_id': companyId})

            let res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/entity')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send();
            let txCount = await mongooseTxQueueRepo.count({status: 'QUEUED', 'tx_metadata.contract': 'Connections', 'tx_metadata.function_name': 'createVirtualEntity'})
            txCount.should.equal(1, "Only one transaction should be in the queue")

            try {
                await chai.request(server)
                    .post('/companies/' + createCompanyRequest.pretty_id + '/entity')
                    .set('Authorization', 'Bearer ' + tokenObj.token)
                    .send();
            } catch (error) {
                error.status.should.equal(400)
            }
            txCount = await mongooseTxQueueRepo.count({status: 'QUEUED', 'tx_metadata.contract': 'Connections', 'tx_metadata.function_name': 'createVirtualEntity'})
            txCount.should.equal(1, "Only one transaction should be in the queue")

        });

        it('it should fail if user is not admin', async () => {

            let normalUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};


            try {
                await companyCreationWrapper.authenticateAndCreateCompanyWithRoleWithoutAdmin(normalUser, createCompanyRequest,
                    tokenObj, "profile_access");

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(403);
            }
        });
    })
});