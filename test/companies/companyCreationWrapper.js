process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http');
const authenticationWrapper = require('../authenticationWrapper');
const resolverLoopWrapper = require('../smart-contracts/cron/resolverLoopWrapper');
chai.use(chaiHttp);

exports.authenticateAndCreateCompany = async function authenticateAndCreateCompany(adminUser, company, tokenObj) {
    let createdUser =  await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

    company.admin = createdUser.username;

    await createCompany(company,tokenObj);

    return createdUser;
};

exports.authenticateAndCreateCompanyWithAdmin = async function authenticateAndCreateCompanyWithAdmin(adminUser, company, tokenObj, admin) {
    await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

    company.admin = admin;

    await createCompany(company,tokenObj);
};

exports.authenticateAndCreateCompanyWithRole = async function authenticateAndCreateCompanyWithAdmin(user, company, tokenObj, admin, role) {
    await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, role);

    company.admin = admin;

    await createCompany(company,tokenObj);
};

exports.authenticateAndCreateCompanyWithRoleWithoutAdmin = async function authenticateAndCreateCompanyWithRoleWithoutAdmin(user, company, tokenObj, role) {
    await authenticationWrapper.signupVerifyAuthenticateWithRole(user, tokenObj, role);

    await createCompany(company,tokenObj);
};

exports.authenticateAndCreateCompanyWithoutAdmin = async function authenticateAndCreateCompanyWithAdmin(adminUser, company, tokenObj) {
    await authenticationWrapper.signupVerifyAuthenticateWithRole(adminUser, tokenObj, 'admin');

    await createCompany(company,tokenObj);
};

async function createCompany(company, tokenObj) {    
    await chai.request(server)
        .post('/companies')
        .set('Authorization', 'Bearer ' + tokenObj.token)
        .send(company);

    await resolverLoopWrapper.processTransactions();    
}

exports.createCompany = createCompany;