process.env.NODE_ENV = 'test';
const chai = require('chai');
const server = require('../../server');
const chaiHttp = require('chai-http')
const companyCreationWrapper = require('./companyCreationWrapper');
const DB = require('../db');
const expect = chai.expect;
const testUtils = require('../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../authenticationWrapper');
const mongooseDesignationRepo = require('../../models/services/mongo/mongoose/mongooseDesignationRepo');

chai.use(chaiHttp);

describe('companies.getCompany', function () {
    this.timeout(config.get('test.timeout'));

    const contractSettingInitializer = require('../smart-contracts/connections/contractSettingInitializer');
    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /companies/:pretty_id', () => {

        /*
            Following fields should exist in advisor / team_member data
                img_url
                photo_ipfs
                username
                name
                ethAddress
                bio

         */
        it('it should retrieve a company with an array of advisor and team users array', async () => {

            let adminUser = testUtils.generateRandomUser();
            let invitedAdvisorAndTeamMember = testUtils.generateRandomUser();
            let invitedTeamMember = testUtils.generateRandomUser();
            let invitedAdvisorNotSignedup = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();
            let designationName = testUtils.generateRandomDesignation();
            let ethAddressAdvisor = '0x8af51b3415e58c55c2c11663cabc7ef8bad518ee';
            let tokenObj = {};
            let tokenObjAdvisor = {};
            let invitedAdvisorEthAddressUpdate = {
                bio: 'about myself Im a tester',
                name: invitedAdvisorAndTeamMember.name,
                ethaddress: ethAddressAdvisor
            };

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);
            await authenticationWrapper.signupVerifyAuthenticate(invitedAdvisorAndTeamMember, tokenObjAdvisor);
            let res = await chai.request(server)
                .post('/updateprofile')
                .set('Authorization', 'Bearer ' + tokenObjAdvisor.token)
                .send(invitedAdvisorEthAddressUpdate);

            await authenticationWrapper.signupVerifyAuthenticate(invitedTeamMember, {});
            await mongooseDesignationRepo.insert({designation: designationName});

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedAdvisorAndTeamMember.email});

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/team_member')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedTeamMember.email, designation: designationName});

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/team_member')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedAdvisorAndTeamMember.email, designation: designationName});

            res = await chai.request(server)
                .post('/companies/' + createCompanyRequest.pretty_id + '/advisor')
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send({email: invitedAdvisorNotSignedup.email});

            res = await chai.request(server)
                .get('/companies/' + createCompanyRequest.pretty_id)
                .send();
            let retrievedCompany = res.body;

            expect(createCompanyRequest.pretty_id).to.equal(retrievedCompany.pretty_id);

            expect(invitedAdvisorAndTeamMember.username).to.equal(retrievedCompany.advisors[0].user.username);
            expect(invitedAdvisorAndTeamMember.name).to.equal(retrievedCompany.advisors[0].user.name);
            expect(ethAddressAdvisor).to.equal(retrievedCompany.advisors[0].user.ethaddress);
            expect(invitedAdvisorAndTeamMember.username).to.equal(retrievedCompany.team_members[1].user.username);
            expect(invitedAdvisorAndTeamMember.name).to.equal(retrievedCompany.team_members[1].user.name);
            expect(ethAddressAdvisor).to.equal(retrievedCompany.team_members[1].user.ethaddress);
            expect(invitedTeamMember.username).to.equal(retrievedCompany.team_members[0].user.username);
            expect(invitedTeamMember.name).to.equal(retrievedCompany.team_members[0].user.name);
            expect(invitedAdvisorNotSignedup.email).to.equal(retrievedCompany.advisors[1].email);
        });

        it('it should retrieve a company given valid request', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .get('/companies/' + createCompanyRequest.pretty_id)
                .send();

            let retrievedCompany = res.body;

            should.exist(retrievedCompany.visible_to_public);
            should.not.exist(retrievedCompany.keyPair);
            expect(createCompanyRequest.pretty_id).to.equal(retrievedCompany.pretty_id);
        });

        it('it should retrieve a company with has_admin_email true if admin email is present', async () => {
            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .get('/companies/' + createCompanyRequest.pretty_id)
                .send();

            let retrievedCompany = res.body;

            should.exist(retrievedCompany.has_admin_email)
            expect(retrievedCompany.has_admin_email).to.equal(true);
            should.not.exist(retrievedCompany.keyPair);
            expect(createCompanyRequest.pretty_id).to.equal(retrievedCompany.pretty_id);
        });

        it('it should retrieve a company with has_admin_email false if admin email is not present', async () => {
            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();
            delete createCompanyRequest.email;

            let tokenObj = {};

            await companyCreationWrapper.authenticateAndCreateCompany(adminUser, createCompanyRequest, tokenObj);

            let res = await chai.request(server)
                .get('/companies/' + createCompanyRequest.pretty_id)
                .send();

            let retrievedCompany = res.body;

            should.exist(retrievedCompany.has_admin_email)
            expect(retrievedCompany.has_admin_email).to.equal(false);
            should.not.exist(retrievedCompany.keyPair);
            expect(createCompanyRequest.pretty_id).to.equal(retrievedCompany.pretty_id);
        });

        it('it should update company fields and should the response should return visible_to_public as true', async () => {

            let adminUser = testUtils.generateRandomUser();

            let createCompanyRequest = testUtils.generateRandomCompany();

            let companyUpdate = testUtils.generateRandomCompanyUpdate();
            let isVisibleTopublic = true;
            let tokenObj = {};

            let createdAdmin = await companyCreationWrapper.authenticateAndCreateCompany(adminUser,
                createCompanyRequest, tokenObj);

            companyUpdate.admin = adminUser.username;
            companyUpdate.visible_to_public = isVisibleTopublic;

            let res = await chai.request(server)
                .patch('/companies/' + createCompanyRequest.pretty_id)
                .set('Authorization', 'Bearer ' + tokenObj.token)
                .send(companyUpdate);

            res.status.should.equal(200);

            res = await chai.request(server)
                .get('/companies/' + createCompanyRequest.pretty_id)
                .send();

            let retrievedCompany = res.body;
            retrievedCompany.visible_to_public.should.equal(true);
        });

        it('it should return 404 for non-existing company', async () => {

            let createCompanyRequest = testUtils.generateRandomCompany();

            try {
                let res = await chai.request(server)
                    .get('/companies/' + createCompanyRequest.pretty_id)
                    .send();

                throw new Error('Expected to fail!');
            } catch (error) {
                error.status.should.equal(404);
            }
        });
    })
});