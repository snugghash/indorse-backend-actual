const senderAddress = '0xd1610215aad8be56377f2af05168799ce8cc8afc';
const EthereumTx = require('ethereumjs-tx');
const mockPK = Buffer.from('de3a2af7faa3a66a9f1e70bd85cfd673eb3fd7bc58af93ca974f81ca288a7609', 'hex');
const scHelper = require('../../../smart-contracts/services/scHelper');

exports.senderAddress = senderAddress;

exports.casteVote = async function casteVote(SC,claimId, type, hash, url, tokenHash) {
    let web3 = await scHelper.getWeb3Instance();
    let data = SC.castVote.getData(claimId,type,hash,url,tokenHash);
    let rawTx = {
        nonce: web3.eth.getTransactionCount(senderAddress),
        gasPrice: web3.toHex('50e9'),
        gasLimit: web3.toHex('3800000'),
        to: SC.address,
        from: senderAddress,
        value: '0x00',
        data: data
    };
    let signedTx = sign(rawTx);
    let txHash = await web3.eth.sendRawTransaction(signedTx);
    return txHash;
};


function sign(txParams) {
    const tx = new EthereumTx(txParams);
    tx.sign(mockPK);
    return '0x' + tx.serialize().toString('hex');
}