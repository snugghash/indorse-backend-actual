const web3 = require('../initializers/testWeb3Provider');
const connectionsDeploymentService = require('./connectionsDeployer');
const userSimulator = require('./userSimulator');
const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const mongooseContractsRepo = require('../../../models/services/mongo/mongoose/mongooseContractsRepo');
const settings = require('../../../models/settings')

exports.initialize = async function initialize() {

    let connectionsSC = await connectionsDeploymentService.deployConnections();
    web3.eth.defaultAccount = web3.eth.accounts[0];
    let senderAddress = settings.CONNECTIONS_MOCK_ADDRESS;

    if (web3.eth.getBalance(senderAddress) < 1000000000000000000) {
        await web3.eth.sendTransaction({from: web3.eth.accounts[0], to: senderAddress, value: 1000000000000000000})
    }

    if (web3.eth.getBalance(userSimulator.senderAddress) < 1000000000000000000) {
        await web3.eth.sendTransaction({from: web3.eth.accounts[0], to: userSimulator.senderAddress, value: 1000000000000000000})
    }

    await mongooseEthNwRepo.insert({
        network: 'LOCAL',
        http_provider: 'http://localhost:8545',
        max_gas_price: 1,
        max_inspection_count : 20
    });

    await mongooseContractsRepo.insert({
        contract_name: 'Connections',
        caller_address: senderAddress,
        contract_address: connectionsSC.address,
        network: 'LOCAL'
    });

    return connectionsSC;
};

