process.env.NODE_ENV = 'test';

const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseEthNetworkRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const chai = require('chai');
const DB = require('../../db');
const testUtils = require('../../testUtils');
const config = require('config');
const contractSettingInitializer = require('../connections/contractSettingInitializer');
const invitationWrapper  = require("../../companies/advisors/invitationWrapper");


describe('Check web3 related configuration', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);       
    });

    describe('Check if gas price is set correctly', () => {
        it('should use the appropriate gas price in transaction sent out', async () => {
            let adminUser = testUtils.generateRandomUser();            
            let adminUser2 = testUtils.generateRandomUser();    
            let adminUser3 = testUtils.generateRandomUser();         
            let inviteeUser = testUtils.generateRandomUser();
            let inviteeUser2 = testUtils.generateRandomUser();
            let inviteeUser3 = testUtils.generateRandomUser();
            let randomCompany = testUtils.generateRandomCompany();
            let randomCompany2 = testUtils.generateRandomCompany();
            let randomCompany3 = testUtils.generateRandomCompany();
            let adminTokenObj = {};
            let adminTokenObj2 = {};
            let adminTokenObj3 = {};

            await contractSettingInitializer.initialize();

            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser, randomCompany, adminTokenObj, inviteeUser.email);
            let company = await mongooseCompanyRepo.findOneByEmail(randomCompany.email);
            let mongoResult = await mongooseTransactionRepo.findOne({ 'tx_metadata.callback_data.company_id': String(company._id)});
            mongoResult.tx_raw.gasPrice.should.equal('0x3b9aca00');

            await mongooseEthNetworkRepo.update({ network: 'LOCAL' }, {$set : { max_gas_price : 20}});
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser2, randomCompany2, adminTokenObj2, inviteeUser2.email);
            let company2 = await mongooseCompanyRepo.findOneByEmail(randomCompany2.email);
            let mongoResult2 = await mongooseTransactionRepo.findOne({ 'tx_metadata.callback_data.company_id': String(company2._id) });
            mongoResult2.tx_raw.gasPrice.should.equal('0x4a817c800');     

            await mongooseEthNetworkRepo.update({ network: 'LOCAL' }, { $set: { max_gas_price: 100 } });
            await invitationWrapper.creteCompanyAdminAndInviteUserByEmail(adminUser3, randomCompany3, adminTokenObj3, inviteeUser3.email);
            let company3 = await mongooseCompanyRepo.findOneByEmail(randomCompany3.email);
            let mongoResult3 = await mongooseTransactionRepo.findOne({ 'tx_metadata.callback_data.company_id': String(company3._id) });
            mongoResult3.tx_raw.gasPrice.should.equal('0x9502f9000');
        });
    });

});
