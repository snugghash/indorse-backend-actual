const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
// let initialized = false;

module.exports.initialize = async function initialize() {
    if (process.env.NODE_ENV === 'integration') {
        await mongooseEthNwRepo.insert({
            network: 'LOCAL',
            http_provider: 'http://ganache:8545',
            max_gas_price: 1000000000000,
            max_inspection_count: 20
        });
    }
    else {
        await mongooseEthNwRepo.insert({
            network: 'LOCAL',
            http_provider: 'http://localhost:8545',
            max_gas_price: 1000000000000,
            max_inspection_count: 20
        });
    }
}