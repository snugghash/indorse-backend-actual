process.env.NODE_ENV = 'test';
const chai = require('chai');
const DB = require('../../db');
const expect = chai.expect;
const should = chai.should();
const testUtils = require('../../testUtils');
const config = require('config');
const resolver = require('../../../smart-contracts/services/cron/resolverLoop');
const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const contractSettingInitializer = require('../connections/contractSettingInitializer');

describe('resolver.spec.js', function () {
    this.timeout(config.get('test.timeout'));

    before((done) => {
        console.log('connecting to database');
        DB.connect(done);        
    });

    after((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    it('should update status to SUSPENDED for transactions not found', async () => {
        await contractSettingInitializer.initialize();

        let hash = '0xc97032602c7323f603cafd8ecc4cce763b824fd461b31c2a566341f5e07ff42d';
        await mongooseTransactionRepo.insert({
            tx_hash: hash,
            tx_nonce: '0x01',
            initiating_user_id : "ble",
            tx_metadata: {
                contract: "Connections",
                function_name: 'createVirtualEntity',
                function_args: [],
                callback_data: {
                    company_id: '3413'
                }
            },
            status: 'PENDING',
            inspected_count: 18
        });

        await resolver.processTransactions();
        let result = await mongooseTransactionRepo.findOneByTxHash(hash);
        result.inspected_count.should.equal(19);
        result.status.should.equal('PENDING');

        await resolver.processTransactions();
        await resolver.processTransactions();
        let result2 = await mongooseTransactionRepo.findOneByTxHash(hash);
        result2.inspected_count.should.equal(20);
        result2.status.should.equal('SUSPENDED');
    });

});