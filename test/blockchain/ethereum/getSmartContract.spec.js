process.env.NODE_ENV = 'test';

const chai = require('chai');
const server = require('../../../server');
const chaiHttp = require('chai-http');
const DB = require('../../db');
const roles = require('../../../models/services/auth/roles');
const expect = chai.expect;
const testUtils = require('../../testUtils');
const should = chai.should();
const config = require('config');
const authenticationWrapper = require('../../authenticationWrapper');
const invitationWrapper = require('../../companies/advisors/invitationWrapper');
const companyCreationWrapper = require('../../companies/companyCreationWrapper');
const mongooseCompanyRepo = require('../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const contractSettingInitializer = require('../../smart-contracts/connections/contractSettingInitializer');
const createVirtualEntityTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../../../models/services/ethereum/connections/txExecutors/createVirtualEntityAndConnectionTxExecutor');
const userSimulator = require('../../smart-contracts/connections/userSimulator');
const resolverLoop = require('../../../smart-contracts/services/cron/resolverLoop');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const Direction = require('../../../smart-contracts/services/connections/models/direction');

chai.use(chaiHttp);

describe('blockchain.ethereum.getContract', function () {
    this.timeout(config.get('test.timeout'));

    var SC;

    beforeEach(async (done) => {
        console.log('connecting to database');
        SC = await contractSettingInitializer.initialize();
        DB.connect(done);
    });

    afterEach((done) => {
        console.log('dropping database');
        DB.drop(done);
    });

    describe('GET /ethereum/:network/contracts/:contract_name', () => {

        it('it should return smart contract', async () => {
            let res = await chai.request(server)
                .get('/ethereum/LOCAL/contracts/Connections')
                .send();

            let contract = res.body;

            should.exist(contract.abi);
        });



    })
});