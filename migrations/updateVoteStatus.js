const mongooseSkillRepo = require('../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseUserRepo = require('../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseClaimsRepo = require('../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseVoteRepo = require('../models/services/mongo/mongoose/mongooseVoteRepo');
const mongoUserRepo = require('../models/services/mongo/mongoRepository')('users');
const claimsProxy = require('../models/services/ethereum/claims/proxy/claimsProxy');

async function migrate() {

    console.log("Migrating...");

    try {

        let votingRoundCursor = await mongooseVotingRoundRepo.findAllWithCursor({status: 'finished'});

        for (let votingRound = await votingRoundCursor.next(); votingRound != null; votingRound = await votingRoundCursor.next()) {

            let votes = await mongooseVoteRepo.findAll({
                $and: [
                    {voted_at : {$gt : 0}},
                    {tx_status: {$ne: 'SUCCESS'}},
                    {voting_round_id: votingRound._id}]
            });

            for(let vote of votes) {
                if(vote.sc_vote_exists) {
                    console.log("SC VOTE EXISTS FOR THIS VOTE!")
                } else {
                    let voter = await mongooseUserRepo.findOne({_id : vote.voter_id});
                    let claim = await mongooseClaimsRepo.findOne({_id : vote.claim_id});
                    let voteFromSc = await claimsProxy.getVote(claim.sc_claim_id, voter.ethaddress);
                    if (voteFromSc.exists) {
                        console.log("WOOHOO!")
                    } else {
                        console.log("FUCK...")
                    }
                }
            }
        }
    } catch (e) {
        console.log("Error " + e)
    }
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();