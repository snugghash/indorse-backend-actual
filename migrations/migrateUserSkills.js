process.env.NODE_ENV = 'script';

const mongooseSkillRepo = require('../models/services/mongo/mongoose/mongooseSkillRepo');
const mongooseUserRepo = require('../models/services/mongo/mongoose/mongooseUserRepo');
const mongoUserRepo = require('../models/services/mongo/mongoRepository')('users');

async function migrate() {

    console.log("Migrating...");

    try {

        let usersCursor = await mongooseUserRepo.findAllWithCursor({skills: {$exists: true, $not: {$size: 0}}});

        for (let user = await usersCursor.next(); user != null; user = await usersCursor.next()) {
            console.log("Updating " + user.username);

            let newSkills = [];

            let userNoSchema = await mongoUserRepo.findOne({email: user.email});

            for (let skillIndex = 0; skillIndex < user.skills.length; skillIndex++) {


                let skill = userNoSchema.skills[skillIndex].toString();

                console.log("Looking for" + skill);

                let skillObj = await mongooseSkillRepo.findOne({name: skill});

                console.log("Found" + JSON.stringify(skillObj));

                if (skillObj) {
                    newSkills.push({skill: skillObj, level : "beginner"});
                }
            }

            console.log(JSON.stringify(newSkills));

            await mongooseUserRepo.update({email: user.email}, {$unset: {skills: ''}});
            await mongooseUserRepo.update({email: user.email}, {$push: {skills: {$each: newSkills}}})
        }
    } catch (e) {
        console.log("Error " + e)
    }
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();