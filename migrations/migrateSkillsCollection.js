process.env.NODE_ENV = 'script';

const csv = require("csvtojson");
const mongooseSkillRepo = require('../models/services/mongo/mongoose/mongooseSkillRepo');

async function migrate() {

    console.log("Migrating...");
    await csv()
        .fromFile('integration_2.skills.csv')
        .then(async function (skillsObj) {
            try {
                for (let skillIndex = 0; skillIndex < skillsObj.length; skillIndex++) {
                    let skillRecord = skillsObj[skillIndex];

                    console.log("Inserting" + JSON.stringify(skillRecord));

                    await mongooseSkillRepo.insert(skillRecord);
                }
            } catch (error) {
                console.log(error);
            }

        });
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(error);
    }
})();