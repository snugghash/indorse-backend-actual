process.env.NODE_ENV = 'script';

const mongoUserRepo = require('../models/services/mongo/mongoRepository')('bot_users');
const fs = require('fs');

async function migrate() {

    function wait(ms) {
        return new Promise(resolve => setTimeout(resolve, ms))
    }

    console.log("Migrating...");


    await wait(1000);

    try {

        let usersCursor = await mongoUserRepo.findAllWithCursor({});

        let json = [];

        for (let user = await usersCursor.next(); await usersCursor.hasNext() === true; user = await usersCursor.next()) {
            console.log("Parsing " + user.email);
            json.push(JSON.stringify({
                timestamp: new Date(parseInt(user._id.toString().substring(0, 8), 16) * 1000),
                telegram_id: user.telegram_id,
                email: user.email,
                chat_id: user.chat_id,
                verified: user.verified,
                ethaddress: user.ethaddress
            }));
        }

        await fs.writeFile('myjsonfile.json', json, 'utf8', function(){});
    } catch (e) {
        console.log("Error " + e)
    }
}

(async () => {
    try {
        await migrate();
    } catch (e) {
        console.log(e);
    }
})();