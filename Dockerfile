FROM node:8

COPY . /indorse-backend-actual
WORKDIR /indorse-backend-actual
RUN npm i npm@latest -g
RUN npm install && npm install github:jaeseokan94/resume-pdf-to-json
RUN chmod +x ecs-start.sh
EXPOSE 3000