const errorUtils = require('../services/error/errorUtils');

exports.getCurrentWizardStep = function getCurrentWizardStep(wizard, user) {
    errorUtils.falsyGuard(arguments);

    let currentStep, wizardFinished;

    if (!user.wizards) {
        currentStep = 1;
        wizardFinished = false;
    } else {
        let currentUserStep = user.wizards.find((step => step.name === wizard.name));

        if (!currentUserStep) {
            currentStep = 1;
            wizardFinished = false;
        } else {
            currentStep = currentUserStep.current_step;
            let maxStep = wizard.steps.length;
            wizardFinished = (currentStep > maxStep);
            if("isFinished" in currentUserStep)
            {
                wizardFinished = currentUserStep.isFinished;
            }
        }
    }

    return {
        currentStep: currentStep,
        wizardFinished: wizardFinished
    };
};

/*
    This function must be called before mark user's wizard as finished.
    Input -> user object
    Output -> Object( completedSkills: Boolean, completedInterests: Boolean, completedSocialLink: Boolean )

 */
exports.getWizardStatusBeforeComplete = function getWizardStatusBeforeComplete(user){
    let wizardStatusObj = {};
    wizardStatusObj.completedInterests = false;
    wizardStatusObj.completedSkills = false;
    wizardStatusObj.completedSocialLink = false;

    if(user.interests && user.interests.length >= 1){
        wizardStatusObj.completedInterests = true;
    }
    if(user.skills && user.skills.length >= 1){
        wizardStatusObj.completedSkills = true;
    }
    if(isSocialLinked(user)){
        wizardStatusObj.completedSocialLink = true;
    }
    return wizardStatusObj;
}

function isSocialLinked(user){
    let socialAccountsLinked = 0;
    if (user.linkedIn_uid ) socialAccountsLinked++;
    if (user.google_uid) socialAccountsLinked++;
    if (user.civic_uid) socialAccountsLinked++;
    if (user.uport_uid) socialAccountsLinked++;
    if (user.is_airbitz_user) socialAccountsLinked++;
    if (user.github_uid) socialAccountsLinked++;
    
    // If password exists, check if one+ uid exist.
    if(user.pass){
        if(socialAccountsLinked >= 1){
            return true;
        }
    }else{ // If password not exist, check if two + uid exist
        if(socialAccountsLinked >= 2){
            return true;
        }
    }


    return false;
}