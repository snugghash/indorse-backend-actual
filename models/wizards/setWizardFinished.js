const mongooseWizardsRepo = require('../services/mongo/mongoose/mongooseWizardsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const wizardStepService = require('./wizardStepService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.post('/wizards/:wizard_name/:user_id/finished',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getWizardStep));
};

async function getWizardStep(req, res) {
    let wizard_name = safeObjects.sanitize(req.params.wizard_name);
    let user_id = safeObjects.sanitize(req.params.user_id);

    if(req.user_id !== user_id) {
        errorUtils.throwError("You cannot finish somebody else's wizard!", 403);
    }

    let wizard = await mongooseWizardsRepo.findOneByName(wizard_name);

    if (!wizard) {
        errorUtils.throwError("Wizard not found", 404);
    }

    let user = await mongooseUserRepo.findOneById(user_id);

    if (!user) {
        errorUtils.throwError("User not found", 404);
    }

    let response = {};

    let userStepState = wizardStepService.getCurrentWizardStep(wizard, user);
    let step_number = userStepState.currentStep;
    let currentWizardStep = wizard.steps[step_number-1];

    if (currentWizardStep.canFinish) {
        await mongooseUserRepo.setCurrentWizardFinished(user_id, wizard_name);
        let wizardStatus = wizardStepService.getWizardStatusBeforeComplete(user);
        amplitudeTracker.publishData('wizard_completed', {wizard_status: wizardStatus}, user._id);
        response.wizard_finished = true;
    } else {
        response.next_step = currentWizardStep;
    }

    res.status(200).send(response);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['wizard_name', 'user_id'],
    properties: {
        wizard_name: {
            type: 'string',
            minLength: 1
        },
        user_id: {
            type: 'string',
            minLength: 1
        }
    }
};

