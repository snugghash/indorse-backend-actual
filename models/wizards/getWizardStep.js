const mongooseWizardsRepo = require('../services/mongo/mongoose/mongooseWizardsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const wizardStepService = require('./wizardStepService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.get('/wizards/:wizard_name/:user_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getWizardStep));
};

async function getWizardStep(req, res) {
    let wizard_name = safeObjects.sanitize(req.params.wizard_name);
    let user_id = safeObjects.sanitize(req.params.user_id);

    if(req.user_id !== user_id) {
        errorUtils.throwError("You cannot get somebody else's wizard!", 403);
    }

    let wizard = await mongooseWizardsRepo.findOneByName(wizard_name);

    if (!wizard) {
        errorUtils.throwError("Wizard not found", 404);
    }

    let user = await mongooseUserRepo.findOneById(user_id);

    if (!user) {
        errorUtils.throwError("User not found", 404);
    }

    let userStepState = wizardStepService.getCurrentWizardStep(wizard, user);

    let response = {
        wizard_finished: userStepState.wizardFinished
    };

    if(!userStepState.finished) {
        response.current_step = wizard.steps[userStepState.currentStep-1];
    }

    res.status(200).send(response);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['wizard_name', 'user_id'],
    properties: {
        wizard_name: {
            type: 'string',
            minLength: 1
        },
        user_id: {
            type: 'string',
            minLength: 1
        }
    }
};

