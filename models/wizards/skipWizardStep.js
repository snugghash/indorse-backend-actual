const mongooseWizardsRepo = require('../services/mongo/mongoose/mongooseWizardsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const routeUtils = require('../services/common/routeUtils');
const wizardStepService = require('./wizardStepService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.post('/wizards/signUp/:user_id/skip/:step_number',
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(skipWizardStep));
};

async function skipWizardStep(req, res) {
    let user_id = safeObjects.sanitize(req.params.user_id);
    let step_number = Number(safeObjects.sanitize(req.params.step_number));

    if (req.user_id !== user_id) {
        errorUtils.throwError("You cannot complete somebody else's wizard!", 403);
    }

    let wizard = await mongooseWizardsRepo.findOneByName('signUp');

    if (!wizard) {
        errorUtils.throwError("Wizard not found!", 404);
    }

    if (step_number > wizard.steps.length) {
        errorUtils.throwError("This wizard doesn't have that many steps!", 400);
    }

    let user = await mongooseUserRepo.findOneById(user_id);

    if (!user) {
        errorUtils.throwError("User not found", 404);
    }

    let response = {};

    let currentWizardStep = wizard.steps[step_number-1];
    if (currentWizardStep.canSkip) {
        if (step_number === 4) {
            let socialAccountsLinked = 0;

            if (user.isGoogleLinked) socialAccountsLinked++;
            if (user.isFacebookLinked) socialAccountsLinked++;
            if (user.isAirbitzLinked) socialAccountsLinked++;
            if (user.isUportLinked) socialAccountsLinked++;
            if (user.isLinkedInLinked) socialAccountsLinked++;
            if (user.linkedIn_uid) socialAccountsLinked++;
            if (user.google_uid) socialAccountsLinked++;
            if (user.github_uid) socialAccountsLinked++;

            if (user.interests && user.interests.length >= 1 && socialAccountsLinked >= 1 && user.skills && user.skills.length >= 1) {
                await mongooseUserRepo.update({email: user.email}, {$addToSet: {badges: "profile_complete"}});
                await mongooseUserRepo.setCurrentWizardStep(user_id, 'signUp', step_number+1);
                response.next_step = wizard.steps[step_number];
            } else {
                await mongooseUserRepo.setCurrentWizardFinished(user_id, "signUp");
                response.wizard_finished = true;
                let wizardStatus = wizardStepService.getWizardStatusBeforeComplete(user);
                amplitudeTracker.publishData('wizard_completed', {wizard_status: wizardStatus}, user._id);
            }
        } else {
            await mongooseUserRepo.setCurrentWizardStep(user_id, 'signUp', step_number+1);
            response.next_step = wizard.steps[step_number];
        }
    } else {
        response.next_step = currentWizardStep;
        response.currentStep = wizard.steps[step_number-1];
    }

    res.status(200).send(response);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id', 'step_number'],
    properties: {
        user_id: {
            type: 'string',
            minLength: 1
        },
        step_number: {
            type: 'string',
            minLength: 1
        }
    }
};