const mongooseClaimsRepo = require('../mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../mongo/mongoose/mongooseSkillRepo');
const errorUtils = require('../error/errorUtils');
const mongooseVotingRoundRepo = require('../mongo/mongoose/mongooseVotingRoundRepo');
const randomValidatorService = require('../../claims/randomValidatorService');
const claimsEmailService = require('../common/claimsEmailService');
const amplitudeTracker = require('../tracking/amplitudeTracker');
const slackService = require('../common/slackService');

exports.createClaim = async function createClam(createClaimRequest, requestingUser) {

    let existingClaimsSameLevel = await mongooseClaimsRepo.findAll({
        $and: [
            {title: createClaimRequest.title},
            {level: createClaimRequest.level},
            {ownerid: requestingUser._id}]
    });

    for (let i = 0; i < existingClaimsSameLevel.length; i++) {
        let existingClaimSameLevel = existingClaimsSameLevel[i];
        if (existingClaimSameLevel.final_status) {
            amplitudeTracker.publishData('claim_creation_failed_claim_already_obtained', createClaimRequest, requestingUser._id);
            errorUtils.throwError("A claim for that skill and level has already been obtained!", 400)
        } else if (!existingClaimSameLevel.approved && !existingClaimSameLevel.disapproved && !existingClaimSameLevel.sc_claim_id) {
            amplitudeTracker.publishData('claim_creation_failed_claim_pending_approval', createClaimRequest, requestingUser._id);
            errorUtils.throwError("A claim for that skill and level is already pending approval!", 400)
        }
    }

    let existingClaimsAnyLevel = await mongooseClaimsRepo.findAll({$and: [{title: createClaimRequest.title}, {ownerid: requestingUser._id}]});

    for (let i = 0; i < existingClaimsAnyLevel.length; i++) {
        let existingClaimAnyLevel = existingClaimsAnyLevel[i];

        if (!existingClaimAnyLevel.approved && !existingClaimAnyLevel.disapproved && !existingClaimAnyLevel.sc_claim_id) {
            amplitudeTracker.publishData('claim_creation_failed_claim_pending_approval', createClaimRequest, requestingUser._id);
            errorUtils.throwError("A claim for that skill and level is already pending approval!", 400)
        }

        let existingVotingRound = await mongooseVotingRoundRepo.findOne({
            $and: [
                {claim_id: existingClaimAnyLevel._id},
                {status: 'in_progress'}]
        });
        if (existingVotingRound) {
            amplitudeTracker.publishData('claim_creation_failed_claim_already_pending', createClaimRequest, requestingUser._id);
            errorUtils.throwError("Pending claim with this title already exists!", 400)
        }
    }

    //Just checking if there is enough validators
    await randomValidatorService.getValidators(createClaimRequest.title, requestingUser._id);

    createClaimRequest.state = 'new';
    createClaimRequest.visible = true;
    createClaimRequest.is_sc_claim = true;
    createClaimRequest.ownerid = requestingUser._id;

    let createdClaimId = await mongooseClaimsRepo.insert(createClaimRequest);

    createClaimRequest._id = createdClaimId;
    createClaimRequest.ownerid = requestingUser._id;
    createClaimRequest.claim_id = createdClaimId;

    amplitudeTracker.publishData('claim_creation_succesful', createClaimRequest, requestingUser._id);

    claimsEmailService.sendClaimAwaitingApproval(createdClaimId);

    let userHasSkill;

    if (requestingUser.skills) {
        userHasSkill = requestingUser.skills.find((s) => s.skill && s.skill.name === createClaimRequest.title.toLowerCase());
    }

    let validationToAdd = {
        type: "claim",
        level: createClaimRequest.level,
        id: createdClaimId,
        validated: false
    };

    if (userHasSkill) {
        await mongooseUserRepo.addValidationToSkill(requestingUser.email, userHasSkill.skill.name, validationToAdd);
    } else {
        let skill = await mongooseSkillRepo.findOne({name: createClaimRequest.title.toLowerCase()});

        await mongooseUserRepo.addSkill(requestingUser.email, {
            skill: skill,
            level: createClaimRequest.level,
            validations: [validationToAdd]
        });
    }

    slackService.reportClaim(createClaimRequest, requestingUser.img_url, requestingUser.username);
};
