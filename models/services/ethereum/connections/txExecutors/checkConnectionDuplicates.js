const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const errorUtils = require('../../../error/errorUtils');
const logger = require('../../../common/logger').getLogger();

module.exports.checkDuplicates = async function checkDuplicates(tx_metadata, initiatingUserId) {
    if (tx_metadata.function_name === 'addConnection') {
        let companyId = tx_metadata.callback_data.company_id;
        let userID = tx_metadata.callback_data.user_id;
        let type = tx_metadata.callback_data.type;

        let txQueueCount = await mongooseTransactionQueueRepo.count({
                'tx_metadata.callback_data.company_id': companyId,
                'tx_metadata.callback_data.user_id': userID,
                'tx_metadata.callback_data.type': type,
                'tx_metadata.function_name': 'addConnection',
                'tx_metadata.function_args': tx_metadata.function_args,
                status: 'QUEUED'
            }
        );
        if (txQueueCount > 0) {
            logger.error("A duplicate transaction was detected : " + JSON.stringify(tx_metadata))
            return true;
        }
        return false;
    } else if (tx_metadata.function_name === 'createVirtualEntity' || tx_metadata.function_name === 'createVirtualEntityAndConnection') {
        let companyId = tx_metadata.callback_data.company_id;
        let txQueueCount = await mongooseTransactionQueueRepo.count(
            {
                'tx_metadata.callback_data.company_id': companyId,
                'tx_metadata.function_name': {$in: ['createVirtualEntityAndConnection', 'createVirtualEntity']},
                status: 'QUEUED'
            }
        );
        if (txQueueCount > 0) {
            errorUtils.throwError("Company creation on blockchain is in progress...please try again in a few minutes", 400)
        }

        let txCount = await mongooseTransactionRepo.count({
                'tx_metadata.callback_data.company_id': companyId,
                'tx_metadata.function_name': { $in: ['createVirtualEntityAndConnection', 'createVirtualEntity'] },
                status: { $in: ['PENDING','SUCCESSFUL']}
            }
        )

        if (txCount > 0) {
            errorUtils.throwError("Company creation on blockchain is in progress...please try again in a few minutes", 400)
        }

        return false;
    } else if (tx_metadata.function_name === 'removeConnection') {
        return false;
    } else {
        errorUtils.throwError("Duplicate guard function name was not found, please check the tx settings", 400)
    }
}