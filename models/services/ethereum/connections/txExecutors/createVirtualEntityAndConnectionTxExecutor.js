const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const connectionsContract = require('../../../../../smart-contracts/services/connections/connectionsContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');
const settings = require('../../../../settings')

exports.execute = async function execute(companyId, userEntityAddress, connectionType, direction, initiatingUserId, metaData = undefined) {
    errorUtils.falsyGuard(arguments);

    let tx_metadata = {
        contract: "Connections",
        function_name: 'createVirtualEntityAndConnection',
        function_args: [userEntityAddress, connectionType.value, direction.value],
        callback_data: {
            company_id: companyId
        },
    };

    if (metaData && metaData.user_id && metaData.type) {
        tx_metadata.callback_data.user_id = metaData.user_id;
        tx_metadata.callback_data.type = metaData.type;
    }

    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, initiatingUserId);

    if (!dupTx) {
        let result = await connectionsContract.createVirtualEntityAndConnection(userEntityAddress, connectionType.value, direction.value);
        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: initiatingUserId,
            tx_metadata: tx_metadata,
            status: 'QUEUED'
        });
        return result;
    }

};