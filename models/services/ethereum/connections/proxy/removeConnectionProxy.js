const mongooseUserRepo = require('../../../mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../mongo/mongoose/mongooseCompanyRepo');
const connectionsFacade = require('../../../../../smart-contracts/services/connections/connectionsContract');
const errorUtils = require('../../../error/errorUtils');
const removeConnectionTxExecutor = require('../txExecutors/removeConnectionTxExecutor');

exports.removeConnection = async function removeConnection(user_id, company_id, connectionType, initiatingUserId) {
    let company = await mongooseCompanyRepo.findOneById(company_id);

    if (!company) {
        errorUtils.throwError("Company not found!", 500);
    }

    if(!company.entity_ethaddress) {
        return;
    }

    let user = await mongooseUserRepo.findOneById(user_id);

    if (!user.ethaddress) {
        return;
    }

    let companyToUserConnection = await connectionsFacade.getConnection(company.entity_ethaddress, user.ethaddress, connectionType.value);

    if (companyToUserConnection.connectionActive) {
        await removeConnectionTxExecutor.execute(company.entity_ethaddress, user.ethaddress, connectionType, initiatingUserId)
    }
};