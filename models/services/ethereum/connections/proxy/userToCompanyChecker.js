const mongooseUserRepo = require('../../../mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../mongo/mongoose/mongooseCompanyRepo');
const connectionsFacade = require('../../../../../smart-contracts/services/connections/connectionsContract');
const errorUtils = require('../../../error/errorUtils');
const timestampService = require('../../../common/timestampService');
const Direction = require('../../../../../smart-contracts/services/connections/models/direction');

exports.checkIfConnectionExists = async function checkIfConnectionExists(user_id, company_id, connectionType) {
    let company = await mongooseCompanyRepo.findOneById(company_id);

    if(!company) {
        errorUtils.throwError("Company not found!", 500);
    }

    if(!company.entity_ethaddress) {
        return false;
    }

    let user = await mongooseUserRepo.findOneById(user_id);

    if(!user) {
        errorUtils.throwError("User not found!", 500);
    }

    if(!user.ethaddress) {
       return false;
    }

    let connection = await connectionsFacade.getConnection(user.ethaddress,company.entity_ethaddress, connectionType.value);

    let currentTimestamp = timestampService.createTimestamp();

    let direction = connection.direction.toNumber();
    let expiration = connection.expiration.toNumber();

    return connection.entityActive && connection.connectionEntityActive && connection.connectionActive
        && (expiration === 0 || expiration > currentTimestamp) && direction === Direction.BACKWARDS.value;
};