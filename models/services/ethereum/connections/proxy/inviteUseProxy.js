const mongooseUserRepo = require('../../../mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../mongo/mongoose/mongooseCompanyRepo');
const connectionsFacade = require('../../../../../smart-contracts/services/connections/connectionsContract');
const errorUtils = require('../../../error/errorUtils');
const createVirtualEntityTxExecutor = require('../txExecutors/createVirtualEntityTxExecutor');
const createVirtualEntityAndConnectionTxExecutor = require('../txExecutors/createVirtualEntityAndConnectionTxExecutor');
const addConnectionTxExecutor = require('../txExecutors/addConnectionTxExecutor');
const Direction = require('../../../../../smart-contracts/services/connections/models/direction');

exports.inviteUser = async function inviteUser(user_id, company_id, connectionType, initiatingUserId, metaData) {
    let company = await mongooseCompanyRepo.findOneById(company_id);

    if (!company) {
        errorUtils.throwError("Company not found!", 500);
    }

    let userEntity, user;

    if (user_id) {

        user = await mongooseUserRepo.findOneById(user_id);

        if (user.ethaddress) {
            userEntity = await connectionsFacade.getEntity(user.ethaddress);
        }
    }

    let userHasEntity = user_id && user.ethaddress && userEntity && userEntity.active;

    if (userHasEntity) {

        if (company.entity_ethaddress) {
            let connectionCompanyToUser = await connectionsFacade.getConnection(company.entity_ethaddress, user.ethaddress,
                connectionType.value);

            if (connectionCompanyToUser.connectionActive) {
                errorUtils.throwError("Connection already exists!", 400);
            }        

            await addConnectionTxExecutor.execute(company_id, company.entity_ethaddress, user.ethaddress,connectionType, Direction.FORWARDS, initiatingUserId,metaData);            

        } else {
            let result = await createVirtualEntityAndConnectionTxExecutor.execute(company_id, user.ethaddress, connectionType,
                Direction.FORWARDS,initiatingUserId,metaData);            
        }
    } else {
        if (!company.entity_ethaddress) {
            await createVirtualEntityTxExecutor.execute(company_id, initiatingUserId);
        }
    }
};