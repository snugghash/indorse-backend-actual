const mongooseUserRepo = require('../../../mongo/mongoose/mongooseUserRepo');
const mongooseCompanyRepo = require('../../../mongo/mongoose/mongooseCompanyRepo');
const ConnectionType = require('../../../../../smart-contracts/services/connections/models/connectionType');
const connectionsFacade = require('../../../../../smart-contracts/services/connections/connectionsContract');
const errorUtils = require('../../../error/errorUtils');
const addConnectionTxExecutor = require('../txExecutors/addConnectionTxExecutor');
const Direction = require('../../../../../smart-contracts/services/connections/models/direction');

/**
 *
 * @param user_id
 * @param company_id
 * @param connectionType
 * @returns {Promise.<void>}
 */
exports.acceptInvitation = async function acceptInvitation(user_id, company_id, connectionType, initiatingUserId,metaData=undefined) {
    if (connectionType !== ConnectionType.IS_TEAM_MEMBER_OF && connectionType !== ConnectionType.IS_ADVISOR_OF) {
        errorUtils.throwError("Connection type is not supported", 500);
    }

    let company = await mongooseCompanyRepo.findOneById(company_id);

    if (!company) {
        errorUtils.throwError("Company not found!", 500);
    }


    let user = await mongooseUserRepo.findOneById(user_id);

    if (!user.ethaddress) {
        return {
            statusCode: 0,
            message: "User has no Ethaddress"
        }
    }

    if (!company.entity_ethaddress) {
        errorUtils.throwError("Company has no entity ethaddress!", 500);
    }

    let companyEntity = await connectionsFacade.getEntity(company.entity_ethaddress);

    if (!companyEntity.active) {
        return {
            statusCode: 2,
            message: "Company has no entity or it is inactive"
        }
    }

    let companyToUserConnection = await connectionsFacade.getConnection(company.entity_ethaddress, user.ethaddress, connectionType.value);

    let userEntity = await connectionsFacade.getEntity(user.ethaddress);

    if (!companyToUserConnection.connectionActive) {
        await addConnectionTxExecutor.execute(company_id, company.entity_ethaddress, user.ethaddress, connectionType, Direction.FORWARDS, initiatingUserId,metaData);

        if (!userEntity.active) {

            return {
                statusCode: 1,
                message: "User has no entity or it is inactive"
            }
        } else {

            return {
                statusCode: 3,
                message: "Company is not connected to user or the connection is inactive"
            }
        }

    }

    let userToCompanyConnection = await connectionsFacade.getConnection(user.ethaddress, company.entity_ethaddress, connectionType.value);

    if (!userToCompanyConnection.connectionActive) {
        if (!userEntity.active) {

            return {
                statusCode: 1,
                message: "User has no entity or it is inactive"
            }
        } else {

            return {
                statusCode: 4,
                message: "User is not connected to company or the connection is inactive"
            }
        }
    } else {

        return {
            statusCode: 5,
            message: "Invitation was successfully accepted"
        }
    }
};