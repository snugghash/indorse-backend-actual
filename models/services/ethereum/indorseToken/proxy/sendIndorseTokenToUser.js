const mongooseUserRepo = require('../../../mongo/mongoose/mongooseUserRepo');
const indorseTokenFacade = require('../../../../../smart-contracts/services/indorseToken/indorseTokenContract');
const errorUtils = require('../../../error/errorUtils');
const transferExecutor = require('../txExecutors/transferExecutor');

exports.sendIndorseTokensToUserForClaim = async function sendIndorseTokensToUserForClaim(user_id, value, initiatingUserId, claim_id, metaData=undefined) {
    let userRecipient = await mongooseUserRepo.findOneById(user_id);

    if (!userRecipient) {
        errorUtils.throwError("User to send to not found!", 500);
    }

    if (!userRecipient.ethaddress) {
        return {
            statusCode: 'TODO',
            message: "User has no Ethaddress"
        }
    }

    // TODO: check that the claim has not already been paid...
    metaData.claim_id = claim_id;

    // TODO: check that the sending address IND pool has enough balance...???

    // TODO: check that the value is not above a limit...???

    let result = await transferExecutor.execute(user_id, userRecipient.ethaddress, initiatingUserId, metaData);

    // TODO: check the result

    return {
        statusCode: 'TODO',
        message: "Indorse Tokens where successfully sent"
    }

}