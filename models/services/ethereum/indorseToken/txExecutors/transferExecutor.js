const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const indorseTokenFacade = require('../../../../../smart-contracts/services/indorseToken/indorseTokenContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');

exports.execute = async function execute(userId, userAddress, value, initiatingUserId, metaData=undefined) {
    errorUtils.falsyGuard(arguments);

    let tx_metadata = {
        contract: "IndorseToken",
        function_name: 'transfer',
        function_args: [userAddress, value],
        callback_data: {
            user_id: userId.toString()
        },
    };

    if (metaData && metaData.claim_id){
        tx_metadata.callback_data.claim_id = metaData.claim_id;
    }

    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, initiatingUserId);

    if (!dupTx) {
        let result = await indorseTokenFacade.transfer(userAddress, value);

        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: initiatingUserId,
            tx_metadata: tx_metadata,
            status : 'QUEUED'
        });

        return result;
    }
};