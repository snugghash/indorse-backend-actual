const Web3 = new require('web3');
const web3 = new Web3();
const ethereumjsUtil = require('ethereumjs-util')
const errorUtils = require('../../services/error/errorUtils');

module.exports.verifySignature = function verifySignature(signature, address, msgString) {
    address = address.toLowerCase()
    let msg = new Buffer(msgString)

    const prefix = new Buffer("\x19Ethereum Signed Message:\n");
    const prefixedMsg = ethereumjsUtil.sha3(
        Buffer.concat([prefix, new Buffer(String(msg.length)), msg])
    );

    let res = ethereumjsUtil.fromRpcSig(signature)
    let pub = ethereumjsUtil.ecrecover(prefixedMsg, res.v, res.r, res.s);
    let addrBuf = ethereumjsUtil.pubToAddress(pub);
    let addr = ethereumjsUtil.bufferToHex(addrBuf)

    if (addr.toLowerCase() !== address) {
        // Testrpc and some other clients do not add prefix to message

        let hashedMsg = ethereumjsUtil.sha3(new Buffer(msgString))
        pub  = ethereumjsUtil.ecrecover(hashedMsg, res.v, res.r, res.s);
        addrBuf = ethereumjsUtil.pubToAddress(pub);
        addr = ethereumjsUtil.bufferToHex(addrBuf)

        if (addr.toLowerCase() !== address) {
            errorUtils.throwError('The Etherem address does not match the signature', 400);
        }
    }
}
