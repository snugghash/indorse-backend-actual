const mongooseTransactionRepo = require('../../../mongo/mongoose/mongooseTransactionRepo');
const mongooseTransactionQueueRepo = require('../../../mongo/mongoose/mongooseTransactionQueueRepo');
const claimsContract = require('../../../../../smart-contracts/services/claims/claimsContract');
const duplicationGuardService = require('../../duplicationGuardService');
const errorUtils = require('../../../error/errorUtils');
const settings = require('../../../../settings')

exports.execute = async function execute(voters, deadline, claimantAddress,  metadata) {
    errorUtils.falsyGuard(arguments);

    let tx_metadata = {
        contract: "NaiveClaims",
        function_name: 'createClaim',
        function_args: [voters, deadline, claimantAddress],
        callback_data: {
            user_id: metadata.userId,
            mongoClaimID: metadata.mongoClaimID
        },
    };

    let dupTx = await duplicationGuardService.checkForDuplicates(tx_metadata, metadata.userId);   

    if(!dupTx){
        let result = await claimsContract.createClaim(voters, deadline, claimantAddress);
        await mongooseTransactionQueueRepo.insert({
            tx_raw: result,
            initiating_user_id: metadata.userId,
            tx_metadata: tx_metadata,
            status: 'QUEUED'
        });
        return result;
    }
};