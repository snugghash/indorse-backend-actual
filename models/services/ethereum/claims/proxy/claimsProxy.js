const errorUtils = require('../../../error/errorUtils');
const createClaimTxExecutor = require('../txExecutors/createClaimTxExecutor');
const claimsFacade = require('../../../../../smart-contracts/services/claims/claimsContract')
const mongoContractRepo = require('../../../../../models/services/mongo/mongoose/mongooseContractsRepo')
const settings = require('../../../../../models/settings')

exports.createClaim = async function createClaim(voters, deadline, claimantAddress,userId,mongoClaimID) {
    let metadata =  {
        userId: userId,
        mongoClaimID: mongoClaimID
    };

    if(!claimantAddress) {
        claimantAddress = '0x0000000000000000000000000000000000000000';
    }

    console.log("Claim deadline is : " + deadline );
    await createClaimTxExecutor.execute(voters, deadline, claimantAddress,metadata); 
};


exports.getClaimTally = async function getClaimTally(claimID) {
    console.log("Claim tally timestamp is " + Math.floor(Date.now() / 1000));
    let contract = await mongoContractRepo.findOneByNameAndNetwork("NaiveClaims", settings.ETH_NETWORK);
    let status = await claimsFacade.calculateTally(contract.contract_address, claimID);
    return status;
};

exports.getVote = async function getVote(claimId, voter) {
    let status = await claimsFacade.getVote(claimId, voter);

    return {
        pType : status[0].toNumber(),
        hash : status[1],
        url : status[2],
        exists : status[3],
        tokenHash : status[4]
    };
};
