const config = require('config');
const request = require('request');
const errorUtils = require('../error/errorUtils');
const accessTokenApp = config.get('auth.facebook_token')
const logger = require("../common/logger").getLogger();

exports.validateAndGetEmail = async function validateAndGetEmail(inspectToken) {

    const [isTokenValid, resp] = await Promise.all([
        validateAccessToken(inspectToken),
        getUserEmailAndProfile(inspectToken)
    ]);

    if (isTokenValid === false || resp === false) {
        logger.debug("isTokenValid = " + isTokenValid + " email and profile " + JSON.stringify(resp));
        errorUtils.throwError("Facebook authentication failed", 401);
    }
    return [resp.email,resp.imageUrl];
};

exports.validateAccessTokenAuth = async function validateAccessTokenAuth(inspectToken) {

    const isTokenValid = await validateAccessToken(inspectToken);
    if (isTokenValid === false) {
        errorUtils.throwError("Facebook authentication failed", 401);
    }
};

/**
 * @param inspectToken (access token given by FrontEnd)
 * @returns
 */
async function validateAccessToken(inspectToken) {
    let url = 'https://graph.facebook.com/debug_token?input_token=' + inspectToken + '&access_token=' + accessTokenApp
    return await new Promise((resolve, reject) => {
        request.get(url, function (error, response, body) {
            if (error) {
                reject(error);
            }
            let content = JSON.parse(body);
            if (response.statusCode === 200 && content.data.is_valid === true) {
                resolve(true);
            } else {
                resolve(false);
            }
        })
    });
}

/**
 *
 * @param accessToken
 * @returns {Promise.<*>}
 */
async function getUserEmailFromFacebook(accessToken) {
    let getEmail = 'https://graph.facebook.com/me?fields=email&access_token=' + accessToken;
    return await new Promise((resolve, reject) => {
        request.get(getEmail, function (error, response, body) {
            if (error) {
                reject(error);
            }
            let emailResp = JSON.parse(body).email;

            if (!error && emailResp) {
                resolve(emailResp.toLowerCase());
            } else {
                resolve(false);
            }
        })
    });

}

async function getUserEmailAndProfile(accessToken){
    let getProfilePicture = 'https://graph.facebook.com/me?fields=email,picture.width(300).height(300)&access_token=' + accessToken;
    return await new Promise((resolve, reject) => {
        request.get(getProfilePicture, function (error, response, body) {
            if (error) {
                reject(error);
            }
            let profileResp = JSON.parse(body).picture;
            let emailResp = JSON.parse(body).email;

            if (!error && emailResp && profileResp) {
                let isDefaultImage = profileResp.data.is_silhouette;
                if(isDefaultImage === true){
                    resolve({email:emailResp.toLowerCase(),imageUrl:false});
                }else{
                    let imageUrl = profileResp.data.url;
                    resolve({email:emailResp.toLowerCase(),imageUrl:imageUrl});
                }
            } else {
                resolve(false);
            }
        })
    });
}


/*
* Getting following varaibles  age_range , is_verified, friends, picture, verifeid
* TODO: likes
*/

exports.getFacebookSocialParams = async function getFacebookSocialParams(inspectToken) {
    let url = 'https://graph.facebook.com/me?fields=is_verified,verified,friends,picture&access_token=' + inspectToken
    const response = await new Promise((resolve, reject) => {

        request.get(url, function (error, response, body) {
            if (error) {
                reject(error);
            }
            let resp = JSON.parse(body);
            logger.debug('Facebook Params obtained : ' + JSON.stringify(resp));

            if (!error && resp) {
                if(!resp.error){
                    resolve(resp);
                }else{
                    reject();
                }
            } else {
                reject();
            }
        })
    });

    let friend = response.friends;
    let picture = response.picture;
    let isVerified = response.is_verified;
    let verified = response.verified;



    let facebookParams = [];
    // TODO: implementing user_likes
    if (typeof friend !== "undefined") {
        let numberOfFriends = friend.summary.total_count;
        if(typeof numberOfFriends !== "undefined"){
            facebookParams.push({
                name: 'friends',
                value: numberOfFriends
            });
        }

    }

    if (typeof picture !== "undefined") {
        let isPicture = picture.data.is_silhouette;
        if(typeof isPicture !== "undefined"){
            facebookParams.push({
                name: 'picture',
                value: isPicture
            });
        }
    }


    if(typeof isVerified !== "undefined"){
        facebookParams.push({
            name: 'is_verified',
            value: isVerified
        })
    }

    if(verified !== "undefined"){
        facebookParams.push({
            name: 'verified',
            value: verified
        })
    }


    logger.debug('Facebook Params for confidence score: ' + JSON.stringify(facebookParams));
    return facebookParams;

}
