const config = require('config');
const errorUtils = require('../error/errorUtils');
const GoogleAuth = require('google-auth-library');
const CLIENT_ID = config.get('auth.google_token');

/**
 *
 * @param idToken
 * @returns {Promise.<[null,null]>}
 */
exports.validateAndGetEmail = async function validateAndGetEmail(idToken) {
    const response = await validateAccessToken(idToken);

    if (!response) {
        errorUtils.throwError('Google authentication failed', 400);
    }

    let googleUUID = response.sub;
    let gmailAddress = response.email;
    return [gmailAddress, googleUUID];
};

exports.getGoogleSocialParams = async function getGoogleSocialParams(idToken) {
    const response = await validateAccessToken(idToken);

    if (!response) {
        errorUtils.throwError('Google authentication failed', 400);
    }

    let googleUUID = response.sub;
    let gmailAddress = response.email;
    let isEmailVerified = response.email_verified;
    return [gmailAddress, googleUUID, isEmailVerified];
};


/**
 * @param idToken (access token given by FrontEnd)
 * @returns response from Google server
 */
async function validateAccessToken(idToken) {

    let auth = new GoogleAuth;
    let client = new auth.OAuth2(CLIENT_ID, '', '');

    return await new Promise((resolve, reject) => {
        client.verifyIdToken(
            idToken,
            CLIENT_ID,
            function (error, login) {
                if (error) {
                    reject(error);
                } else {
                    resolve(login.getPayload());
                }
            });
    });
}
