const config = require('config');
const errorUtils = require('../error/errorUtils');
const linkedInAuth = require('google-auth-library');
const clientID = config.get('auth.linkedInClientID');
const clientSecret = config.get('auth.linkedInClinetSecret')
const request = require('request');
const logger = require("../common/logger").getLogger();

/**
 *
 * @param idToken
 * @returns {Promise.<[null,null]>}
 */
exports.exchangeAuthorizationCode = async function exchangeAuthorizationCode(code, redirectUri) {

    let url = 'https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code=' + code +
        '&redirect_uri=' + redirectUri + '&client_id=' + clientID + '&client_secret=' + clientSecret

    const accessToken = await new Promise((resolve, reject) => {
        request.post(url, function (error, response, body) {
            if (error) {
                reject(error);
            }
            let content = JSON.parse(body);
            if (content.error) {
                resolve();
            }

            resolve(content.access_token);

        })
    });
    if (!accessToken) {
        errorUtils.throwError('linkedIn authentication failed', 400);
    }

    return accessToken;
};

exports.getLinkedInSignupVariables = async function getLinkedInSignupVariables(accessToken) {

    let url = 'https://api.linkedin.com/v1/people/~:(first-name,last-name,id,picture-url,public-profile-url,email-address)?format=json'

    let request_parameters = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        },
        uri: url,
        method: 'GET'
    };

    const response = await new Promise((resolve, reject) => {
        request(request_parameters, function (error, response, body) {
            if (error) {
                reject(error);
            }

            let content = JSON.parse(body);
            resolve(content)

        })
    });

    console.log("LISignup Variables are :" + JSON.stringify(response) );

    if (!response) {
        errorUtils.throwError('linkedIn authentication failed', 400);
    }
    let email = response.emailAddress;
    let name = response.firstName + ' ' + response.lastName;
    let linkedInUid = response.id;
    let  publicProfileUrl = response.publicProfileUrl;



    if (!email || !name || !linkedInUid) {
        errorUtils.throwError('LinkedIn authentication failed', 400);
    }

    return [linkedInUid, name, email, publicProfileUrl];
}

exports.getLinkedInSocialParams = async function getLinkedInSocialParams(accessToken) {
    let url = 'https://api.linkedin.com/v1/people/~:(id,headline,public-profile-url,first-name,last-name,location,num-connections,num-connections-capped,summary,positions,picture-url)?format=json'

    let request_parameters = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        },
        uri: url,
        method: 'GET'
    };

    const response = await new Promise((resolve, reject) => {
        request(request_parameters, function (error, response, body) {
            if (error) {
                reject(error);
            }

            let content = JSON.parse(body);
            console.log('LinkedIn Params = ' + JSON.stringify(content));

            if (typeof content.errorCode !== 'undefined') {
                reject();
            }
            resolve(content)

        })
    });

    console.log('LinkedIn Params frm resp= ' + JSON.stringify(response));

    let linkedInParams = [];

    if (typeof response.location !== undefined && typeof response.location.name !== undefined) {
        linkedInParams.push({
            name: 'location',
            value: true
        })
    }

    if (typeof response.headline !== undefined) {
        linkedInParams.push({
            name: 'headline',
            value: true
        })
    }

    if (typeof response.numConnections !== undefined) {
        linkedInParams.push({
            name: 'num-connections',
            value: response.numConnections
        })
    }

    if (typeof response.numConnectionsCapped !== undefined) {
        linkedInParams.push({
            name: 'num-connections-capped',
            value: response.numConnectionsCapped
        })
    }

    if (typeof response.summary !== undefined) {
        linkedInParams.push({
            name: 'summary',
            value: true
        })
    }

    if (typeof response.positions !== undefined && typeof response.positions._total !== undefined) {
        linkedInParams.push({
            name: 'positions',
            value: true
        })
    }

    if (typeof response.pictureUrl !== undefined) {
        linkedInParams.push({
            name: 'picture-url',
            value: true
        })
    }
    return linkedInParams;
}
