const config = require('config');
const errorUtils = require('../error/errorUtils');
const linkedInAuth = require('google-auth-library');
const clientID = config.get('auth.githubClientID');
const clientSecret = config.get('auth.githubClientSecret');
const request = require('request');
const logger = require("../common/logger").getLogger();
const mongooseUserRepo = require("../mongo/mongoose/mongooseUserRepo");
const mongooseUserGithubRepo = require("../mongo/mongoose/mongooseUserGithubRepo");

exports.getGithubUserAndRepoData = async function getGithubUserAndRepoData(accessToken) {
    let email = await getEmailFromGithub(accessToken);
    let query = `query { ` +
        `viewer { ` +
        `  login ` +
        ` id ` +
        ` bio ` +
        `name ` +
        `company ` +
        `createdAt ` +
        `avatarUrl ` +
        `isBountyHunter ` +
        `isHireable ` +
        `isEmployee ` +
        `websiteUrl ` +
        `  repositories (first:100){ ` +
        `    edges{` +
        `     node { ` +
        `name ` +
        `projectsUrl ` +
        `description ` +
        `isArchived ` +
        `isPrivate ` +
        `forkCount ` +
        `createdAt ` +
        `updatedAt `+
        `owner {id} ` +
        `stargazers(){ totalCount } ` +
        `    owner { ` +
        `     login` +
        `  } ` +
        ` languages(first: 7){ ` +
        `  nodes{ ` +
        `   name` +
        `}}}}` +
        `}` +
        `}` +
        `}`;
    let queryToSend = `{"query" : "${query}" } `;

    const data = await new Promise((resolve, reject) => {
        request.post({
            headers: {
                'content-type': 'application/json',
                'Authorization': 'bearer ' + accessToken,
                'User-Agent': 'Indorse'
            },
            url: 'https://api.github.com/graphql',
            body: queryToSend
        }, function (error, response, body) {
            if (error) {
                logger.debug('Github API responded with error ' + error);
                reject(new Error("Github authentication failed"));
            }
            let resp = JSON.parse(body).data;
            if (!resp) {
                logger.debug('Github API responded with error ' + body);
                reject(new Error("Github authentication failed"));
            }
            resolve(resp);
        });
    })

    data.viewer.email = email;

    return data.viewer;

}

/*
    Input: Object obtained from Github API AND user object
    Output: Void
 */
exports.updateGithubDataOfUser = async function updateGithubDataOfUser(gitDataObj, user){
    let githubUid = gitDataObj.id;
    let githubUidFromUser = user.github_uid;

    if(githubUid !==  githubUidFromUser){
        errorUtils.throwError('Github UID mismatch', 500);
    }

    let userGithubData = await mongooseUserGithubRepo.findOneByGithubUid(githubUid);

    if(!userGithubData){
        // Create a new one
        let id = await mongooseUserGithubRepo.insert(gitDataObj);
    }else{
        await mongooseUserGithubRepo.update({id:githubUid}, gitDataObj);
    }
}




/**
 *
 * @param idToken
 * @returns email (promise object)
 */

async function getEmailFromGithub(access_token) {

    let url = 'https://api.github.com/user/emails?access_token=' + access_token;

    const email = await new Promise((resolve, reject) => {

        request.get({url,headers:{'Accept':'application/json','User-Agent': 'telepras'}},function (error, response, body) {
            if (error) {
                reject(error);
            }
            body = body.substring(1, body.length-1);
            let emails = body.split(",{");

            if(body[0] == "{" && body[body.length-1] == "}")
            {
                let email = JSON.parse(emails[0]);
                resolve(email.email);
            }
            else
            {
                reject(new Error("Github authentication failed"));
            }
            

        })
    });

    if (!email) {
        errorUtils.throwError('Github authentication failed', 400);
    }

    //profile.email = email;

    return email;
};

exports.exchangeAuthorizationCode = async function exchangeAuthorizationCode(code, state, redirect_uri) {


    let url = 'https://github.com/login/oauth/access_token?client_id=' + clientID + '&redirect_uri=' +  redirect_uri +'&client_secret=' + clientSecret  + '&code=' + code + '&state=' + state;


    const accessToken = await new Promise((resolve, reject) => {

        request.post({url,headers:{'Accept':'application/json','User-Agent': 'telepras'}},function (error, response, body) {
            if (error) {
                reject(error);
            }
            let content = JSON.parse(body);
            if (content.error) {
                resolve();
            }
            resolve(content.access_token);

        })
    });
    if (!accessToken) {
        errorUtils.throwError('Github authentication failed', 400);
    }

    return accessToken;
};
