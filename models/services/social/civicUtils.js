const civicSip = require('civic-sip-api')
const config = require('config');
const _ = require('lodash');
const logger = require('../common/logger').getLogger();


exports.parseCivicDecodedData = function parseCivicDecodedData(decodedData) {
    let userEmail;
    let userUID;
    let userData = [];

    for (let field of decodedData.data) {
        if (field.label === 'contact.personal.email') {
            userEmail = field.value;
            userData.push(
                {
                    name : 'email',
                    value : field.value
                }
            );
            logger.debug("Populating email from civic JWT " + userEmail);
            continue;
          
        }
        if (field.label === 'contact.personal.phoneNumber') {
            userData.push(
                {
                    name: 'phone',
                    value: field.value
                }
            );            
            logger.debug("Populating phone from civic JWT " + field.value);
            continue;            
        }       
    }
    if (decodedData.userId){
        userUID = decodedData.userId;
        logger.debug("Populating userID from civic JWT " + userUID);
    }

    return [userEmail, userUID, userData];
}


exports.validateAndGetUserData = async function validateAndGetUserData(jwtToken) {

    const civicClient = civicSip.newClient({
        appId: config.get('civic.appid'),
        prvKey: config.get('civic.privatekey'),
        appSecret: config.get('civic.appsecret')
    });

    let decodedData = await civicClient.exchangeCode(jwtToken);
    let userData = this.parseCivicDecodedData(decodedData);
    logger.debug("Returning civic object " + JSON.stringify(userData)); 
    return userData;
}