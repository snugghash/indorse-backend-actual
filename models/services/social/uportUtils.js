const civicSip = require('civic-sip-api')
const config = require('config');
var _ = require('lodash');
const logger = require("../common/logger").getLogger();

/*
Sample data

@context(pin): "http://schema.org"
@type(pin): "Person"
publicKey(pin): "0x040ad8dc6c15d547c6814c333770f1d956666b7066addb4b938fc89f71e5eccb729f9b5192321347e09fb265c459b9464ae65361779dbeb9e9f37b3ea0e7545278"
publicEncKey(pin): "WJtg8T80nFLN4pWg1JNkm/88qHbL4D32ktcM7ZIddn8="
name(pin): "Mark Le"
phone(pin): "+6594465904"
country(pin): "SG"
pushToken(pin): "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NksifQ.eyJpc3MiOiIyb2paeExYNkFUdW5WVkNXUTRCS1RTVWVMcjkzejR0UGdKbiIsImlhdCI6MTUyMjgyODE5MSwiYXVkIjoiMm9oUE50bnZQb0dhSnh2Wm1wV0Y1bnRHYXE2R25SMnRSYWYiLCJ0eXBlIjoibm90aWZpY2F0aW9ucyIsInZhbHVlIjoiYXJuOmF3czpzbnM6dXMtd2VzdC0yOjExMzE5NjIxNjU1ODplbmRwb2ludC9BUE5TL3VQb3J0LzU5ODg2NTY2LTA1YWItMzQwZi1hZTQ2LTM0ZTIxYzljZjdmOCIsImV4cCI6MTUyNDEyNDE5MX0.OWkZBJbFL0bBEUUMDWUYb6Kg4YTs6ALKjJ2VIgEGGudRXuKaCYTCmDA4MwwipPPR8eK8mYOgVZOkCNaQ_pQBSQ"
address(pin): "2ojZxLX6ATunVVCWQ4BKTSUeLr93z4tPgJn"
networkAddress(pin): "2ojZxLX6ATunVVCWQ4BKTSUeLr93z4tPgJn"

*/

//TODO : Update once more information is received.
exports.parseUportDecodedData = function parseUportDecodedData(decodedData){
    let userUID;
    let userData =[];
    
    if(decodedData["public_key"]){
        userUID = decodedData["public_key"];
    }

    for (var key in decodedData) {
        userData.push({
            name : key,
            value: decodedData[key]
        })        
    }
    return [userUID,userData];
}


exports.validateAndGetUserData = async function validateAndGetUserData(decodedData){
    let userData = this.parseUportDecodedData(decodedData);
    logger.debug("Returning uport object " + JSON.stringify(userData)); 
    return userData;
}