exports.throwError = throwError;

exports.falsyGuard = function falsyGuard(...args) {
    for(let arg of args) {
        if(!arg) {
            throwError("Invalid function argument",500);
        }
    }
};

function throwError(message, code) {
    let error = new Error(message);
    if (code) {
        error.code = code;
    }
    throw error;
}