const mongo = require('mongodb');
const MongoClient = mongo.MongoClient
const settings = require('../../settings');

let db;

exports.getDB = async function getDB() {
    if (!db) {
        db = await MongoClient.connect(settings.DATABASE_CONNECTION_STRING);
    }

    return db;
};