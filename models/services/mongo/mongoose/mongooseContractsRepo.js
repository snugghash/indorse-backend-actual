let mongoose = require('./mongooseDB');
let contractsSchema = require('./schemas/contracts').schema;
let Contracts = mongoose.model('contracts', contractsSchema);

module.exports.insert = async function insert(contractData) {
    let contract = new Contracts(contractData);
    await contract.save();
    return contract._doc._id.toString();
};

module.exports.findOneByNameAndNetwork = async function findOneByNameAndNetwork(contract,network) {
    return await Contracts.findOne({ contract_name: contract , network : network}).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Contracts.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Contracts.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Contracts.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Contracts.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Contracts.find(selector).remove();
};