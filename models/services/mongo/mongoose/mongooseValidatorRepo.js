let mongoose = require('./mongooseDB');
let validatorSchema = require('./schemas/validator').schema;
let Validator = mongoose.model('validators', validatorSchema);

module.exports.insert = async function insert(validatorData) {
    let validator = new Validator(validatorData);
    await validator.save();
    return validator._doc._id.toString();
};

module.exports.getRandomSampleBySkill = async function getRandomSampleBySkill(sampleSize, skill,claimantUserId) {
    return await Validator
        .aggregate({$match : {
            $and:
                [
                    {skills: { $elemMatch: { $eq : skill}}},
                    { user_id: { $ne: claimantUserId } }
                ]
            }}).sample(sampleSize);
};

module.exports.findOneById = async function findOneById(id) {
    return await Validator.findById(id).lean();
};

module.exports.findByClaimID = async function findByClaimID(claimId) {
    return await Validator.find({claim_id : claimId}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Validator.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Validator.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Validator.findOneAndUpdate(selector, updateObj);
};

module.exports.remove = async function remove(selector) {
    await Validator.remove(selector);
};