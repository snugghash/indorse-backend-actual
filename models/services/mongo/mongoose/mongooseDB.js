const mongoose = require('mongoose');
const settings = require('../../../settings');

mongoose.connect(settings.DATABASE_CONNECTION_STRING, {useMongoClient: true});
mongoose.Promise = global.Promise;

module.exports = mongoose;