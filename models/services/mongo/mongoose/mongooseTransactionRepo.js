let mongoose = require('./mongooseDB');
let transactionSchema = require('./schemas/transaction');
let Transaction = mongoose.model('transactions', transactionSchema);

module.exports.insert = async function insert(txData) {
    let transaction = new Transaction(txData);
    await transaction.save();
    return transaction._doc._id.toString();
};

module.exports.findOneByTxHash = async function findOneById(txHash) {
    return await Transaction.findOne({tx_hash: txHash}).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Transaction.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Transaction.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Transaction.find(selector).lean();
};

module.exports.findAllWithStatus = async function findAllWithStatus(status) {
    return await Transaction.find({status : status}).cursor();
};

module.exports.countAllWithStatus = async function countAllWithStatus(status) {
    return await Transaction.find({status : status}).count();
};

module.exports.count = async function count(selector) {
    return await Transaction.find(selector).count();
};

module.exports.update = async function update(selector, updateObj) {
    await Transaction.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Transaction.find(selector).remove();
};

module.exports.deleteAll = async function deleteAll() {
    await Transaction.remove({});
};

