//This collection tracks all the jobs a user has applied for and their status
let mongoose = require('./mongooseDB');
let userJobsSchema = require('./schemas/userJob').schema;
let UserJob = mongoose.model('userJobs', userJobsSchema);

module.exports.insert = async function insert(jobsData) {
    let userJob = new UserJob(jobsData);
    await userJob.save();
    return userJob._doc._id.toString();
};


module.exports.findAllWithCursorByUserId = async function findAllWithCursorByUserId(id) {
    return await UserJob.find({user : ObjectID(id)}).cursor();
};

module.exports.findAllWithCursorByJobId = async function findAllWithCursorByJobId(id) {
    return await UserJob.find({job : ObjectID(id)}).cursor();
};

module.exports.findAllByJobId = async function findAllByJobId(id) {
    return await UserJob.find({job : ObjectID(id)});
};

module.exports.findOne = async function findOne(selector) {
    return await UserJob.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await UserJob.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await UserJob.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await UserJob.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await UserJob.find(selector).cursor();
};