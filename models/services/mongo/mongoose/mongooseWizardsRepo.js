let mongoose = require('./mongooseDB');
let wizardSchema = require('./schemas/wizard');

let Wizard = mongoose.model('wizards', wizardSchema);

module.exports.insert = async function insert(wizardData) {
    let wizard = new Wizard(wizardData);

    await wizard.save();

    return wizard._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Wizard.findById(id).lean();
};

module.exports.findOneByName = async function findOneByName(name) {
    return await Wizard.findOne({name : name}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Wizard.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Wizard.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Wizard.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Wizard.find(selector).remove();
};