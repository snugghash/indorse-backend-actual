let mongoose = require('./mongooseDB');
let timestampService = require('../../common/timestampService');
let claimsSchema = require('./schemas/claims').schema;
let Claim = mongoose.model('claims', claimsSchema);

module.exports.insert = async function insert(claimsData) {
    let claim = new Claim(claimsData);
    await claim.save();
    return claim._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Claim.findById(id).lean();
};

module.exports.findByOwnerId = async function findByOwnerId(ownerID) {
    return await Claim.find({ownerid: ownerID.toString()}).lean();
};

module.exports.findOneByTitleAndOwnerId = async function findOneByTitleAndOwnerId(title, ownerID) {
    return await Claim.findOne({$and: [{title: title},{ownerid: ownerID}]}).lean();
};

module.exports.findOneByTitleLevelAndOwnerId = async function findOneByTitleLevelAndOwnerId(title, level, ownerID) {
    return await Claim.findOne({$and: [{title: title}, {level : level},{ownerid: ownerID}]}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Claim.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Claim.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Claim.findOneAndUpdate(selector, updateObj);
};

module.exports.updateClaimID = async function updateClaimID(mongoClaimID, scClaimId) {
    await Claim.update({_id: mongoClaimID}, {sc_claim_id: scClaimId});
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await Claim.findOne(selector).remove();
};

module.exports.approveClaim = async function approveClaim(mongoClaimID) {
    return await Claim.update({_id: mongoClaimID}, {approved: timestampService.createTimestamp()});
};

module.exports.disapproveClaim = async function disapproveClaim(mongoClaimID) {
    return await Claim.update({_id: mongoClaimID}, {disapproved: timestampService.createTimestamp()});
};