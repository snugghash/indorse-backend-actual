let mongoose = require('./mongooseDB');
let ethNetworkSchema = require('./schemas/ethNetwork');
let EthNetwork = mongoose.model('ethnetworks', ethNetworkSchema);

module.exports.insert = async function insert(nwData) {
    let ethNetwork = new EthNetwork(nwData);
    await ethNetwork.save();
    return ethNetwork._doc._id.toString();
};

module.exports.findOneByNetwork = async function findOneByNetwork(nwName) {
    return await EthNetwork.findOne({ network: nwName});
};

module.exports.findOneById = async function findOneById(id) {
    return await EthNetwork.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await EthNetwork.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await EthNetwork.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await EthNetwork.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await EthNetwork.find(selector).remove();
};