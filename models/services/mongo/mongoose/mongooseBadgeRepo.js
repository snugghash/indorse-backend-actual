let mongoose = require('./mongooseDB');
let badgeSchema = require('./schemas/badge').schema;

let Badge = mongoose.model('badges', badgeSchema);

module.exports.insert = async function insert(badgeData) {
    let badge = new Badge(badgeData);
    await badge.save();
    return badge._doc._id.toString();
};

module.exports.findOne = async function findOne(selector) {
    return await Badge.findOne(selector).lean();
};

module.exports.findOneByPrettyId = async function findOneByPrettyId(pretty_id) {
    return await Badge.findOne({id: pretty_id}).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Badge.findById(id).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Badge.find(selector).lean();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await Badge.find(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await Badge.findOneAndUpdate(selector, updateObj);
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
    await Badge.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Badge.find(selector).remove();
};