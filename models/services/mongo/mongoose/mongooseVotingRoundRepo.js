let mongoose = require('./mongooseDB');
let votingRoundsSchema = require('./schemas/votingRound').schema;
let VotingRound = mongoose.model('votingrounds', votingRoundsSchema);

module.exports.insert = async function insert(votingRoundData) {
    let votingRound= new VotingRound(votingRoundData);
    await votingRound.save();
    return votingRound._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await VotingRound.findById(id).lean();
};

module.exports.findOneByClaimID = async function findOneByClaimID(claimId) {
    return await VotingRound.findOne({claim_id : claimId}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await VotingRound.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await VotingRound.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await VotingRound.findOneAndUpdate(selector, updateObj);
};

module.exports.updateClaimID = async function updateClaimID(mongoClaimID, scClaimId) {
    await VotingRound.update({_id: mongoClaimID}, {sc_claim_id: scClaimId});
};

module.exports.deleteOne = async function deleteOne(selector) {
    return await VotingRound.findOne(selector).remove();
};

module.exports.findAllWithCursor = async function findAllWithCursor(selector) {
    return await VotingRound.find(selector).cursor();
};

module.exports.deleteMany = async function deleteMany(selector) {
    await VotingRound.deleteMany(selector);
};