let mongoose = require('./mongooseDB');
let languageSchema = require('./schemas/language');

let Language = mongoose.model('languages', languageSchema);

module.exports.insert = async function insert(languageData) {
    let school = new Language(languageData);

    await school.save();

    return school._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Language.findById(id).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await Language.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Language.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await Language.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Language.find(selector).remove();
};