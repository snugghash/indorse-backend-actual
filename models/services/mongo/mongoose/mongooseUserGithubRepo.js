let mongoose = require('./mongooseDB');
let userGithubSchema = require('./schemas/userGithub').schema;
let UserGithub = mongoose.model('usergithubs', userGithubSchema);

module.exports.insert = async function insert(contractData) {
    let userGithubData = new UserGithub(contractData);
    await userGithubData.save();
    return userGithubData._doc._id.toString();
};

module.exports.findOneByGithubUid = async function findOneByGithubUid(githubUid) {
    return await UserGithub.findOne({id: githubUid}).lean();
};

module.exports.findOneByUsername = async function findOneByUsername(username) {
    return await UserGithub.findOne({username: username}).lean();
};

module.exports.findOne = async function findOne(selector) {
    return await UserGithub.findOne(selector).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await UserGithub.find(selector).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await UserGithub.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await UserGithub.find(selector).remove();
};