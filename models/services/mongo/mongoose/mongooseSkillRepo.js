let mongoose = require('./mongooseDB');
let skillSchema = require('./schemas/skill');

let Skill = mongoose.model('skills', skillSchema);

module.exports.insert = async function insert(skillData) {
    let skill = new Skill(skillData);

    await skill.save();

    return skill._doc._id.toString();
};

module.exports.findOneById = async function findOneById(id) {
    return await Skill.findById(id).lean();
};

module.exports.findDistinctCategories = async function findDistinctCategories() {
    return await Skill.find().distinct('category');
};

module.exports.findOne = async function findOne(selector) {
    return await Skill.findOne(selector).lean();
};
    
module.exports.findAll = async function findAll(selector) {
    return await Skill.find(selector).lean();
};

module.exports.findAllWithProjection = async function findAllWithProjection(conditions, projection) {
    return await Skill.find(conditions).select(projection).lean();
}

module.exports.findAllWithLimit = async function findAll(selector, limit) {
    return await Skill.find(selector).limit(limit).lean();
}

module.exports.update = async function update(selector, updateObj) {
    await Skill.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Skill.find(selector).remove();
};