let mongoose = require('./mongooseDB');
let mandrillConfig = require('./schemas/mandrillConfig').schema;

let MandrillConfigs = mongoose.model('mandrillconfigs', mandrillConfig);

module.exports.insert = async function insert(templateData) {
    let mandrillCfg = new MandrillConfigs(templateData);
    await mandrillCfg.save();
    return mandrillCfg._doc._id.toString();
};

module.exports.findOneByAction  = async function findOneByAction(action) {
    return await MandrillConfigs.findOne({action:action}).lean();
};

module.exports.update = async function update(selector, updateObj) {
    await MandrillConfigs.findOneAndUpdate(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await MandrillConfigs.find(selector).remove();
};

module.exports.findAll = async function findAll(selector) {
    return await MandrillConfigs.find(selector).lean();
};