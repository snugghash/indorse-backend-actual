const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const mandrillConfig = new Schema({
    template_name: {
        type: String,
        required: true
    },
    subject :{
        type : String,
        required : true  
    },
    from_email :{
        type: String,
        required: true
    },
    from_name : {
        type: String,
        required : true
    }, 
    bcc_email: {
        type: String,
    }    
});

module.exports.schema = mandrillConfig;
