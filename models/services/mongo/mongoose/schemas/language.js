const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const languages = new Schema({

    language: {
        type: String,
        minlength: 1,
        required: true
    },

    item_key: {
        type: String,
        minlength: 1,
        required: true
    }
});

module.exports = languages;
