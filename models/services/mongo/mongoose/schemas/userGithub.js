const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const usergithubs = new Schema({
    id: {
        type: String,
        required: true
    }, // this is github_uid
    email: {
        type: String,
    },
    username: {
        type: String,
    }, // This is a Username from Indorse
    login: {
        type: String,
    }, // This is a Username from Github
    bio: {
        type: String,
    },
    name: {
        type: String,
    },
    company: {
        type: String,
    },
    createdAt: {
        type: String,
    },
    avatarUrl: {
        type: String,
    },
    websiteUrl: {
        type: String,
    },
    isBountyHunter: {
        type: Boolean,
    },
    isHireable: {
        type: Boolean,
    },
    isEmployee: {
        type: Boolean,
    },
    repositories: {
        edges: [new Schema({
            node: {
                login: {
                    type: String,
                },
                isPrivate: {
                  type: String,
                },
                updatedAt: {
                    type: String,
                },
                createdAt: {
                    type: String,
                },
                name: {
                    type: String,
                },
                projectsUrl: {
                    type: String,
                },
                description: {
                    type: String,
                },
                isArchived: {
                    type: Boolean,
                },
                forkCount: {
                    type: Number,
                },
                owner: {
                    id: {
                        type: String, // github_uid of owner of this repo
                    }
                },
                stargazers: [new Schema({
                    totalCount: {
                        type: Number,
                    },
                })],
                languages: {
                    nodes: [new Schema({
                        name: {
                            type: String, // This array will have a maximum number of 7 names of most used programming languages. a name in index[0] is the one used the most. (descending order)
                        }
                    })],
                }

            },

        })],
    },


});

module.exports.schema = usergithubs;
