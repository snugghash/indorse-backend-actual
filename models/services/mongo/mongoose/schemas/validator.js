const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const validator = new Schema({
    user_id: {
        type: String,
        required: true
    },
    skills : [String]
});

module.exports.schema = validator;