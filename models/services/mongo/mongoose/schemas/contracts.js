const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

let fn_metadata_schema = {
    function_name : {
        type : String,
        required : true
    },
    gas : {
        type :Number,
        required : true
    }
}

//TODO : Move to common file?
function validateConnectionsFnMetadata(data){
    let function_names = ['createVirtualEntity', 'createVirtualEntityAndConnection',
        'editEntity', 'transferEntityOwnerPush', 'transferEntityOwnerPull',
        'editConnection', 'removeConnection'];

        if (function_names.indexOf(data.function_name) == -1)
            return false;
}

function validateMetadata(data){
    if(data.name == "Connections"){
        validateConnectionsFnMetadata(data);
    }
}

//TODO : Versioning / Synchro for nonce if possible
const contracts = new Schema({
    contract_name: {
        type: String,
        required: true      
    },
    network :{
        type : String,
        required: true
    },
    contract_address : {
        type: String,
        required : true,
        match: /^(0x)?[0-9aA-fF]{40}$/
    },
    version :{
        type: Number
    },
    caller_address: {
        type: String,
        required: true,
        match: /^(0x)?[0-9aA-fF]{40}$/
    },
    caller_nonce : {
        type: Number
    },
    function_metadata: {
        type: [fn_metadata_schema],
        validate: validateMetadata
    },
    abi : [Object]
});

module.exports.schema = contracts;