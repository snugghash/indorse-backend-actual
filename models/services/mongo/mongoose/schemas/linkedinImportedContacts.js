const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const importedContact = new Schema({

    user_id: {
        type: String,
        minlength: 1,
        required: true
    },

    first_name : {
        type: String
    },

    last_name : {
        type: String
    },

    companies : {
        type: String
    },

    title : {
        type: String
    },

    email_address : {
        type: String
    },

    phone_numbers : {
        type: String
    },

    created_at : {
        type: String
    },

    instant_message_handles : {
        type: String
    },

    addresses : {
        type: String
    },

    sites : {
        type: String
    }
});

module.exports.schema = importedContact;
