const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const SAFE_USER_FIELDS = ['name', 'bio', 'ethaddress', 'ethaddressverified', 'username', 'email', 'user_id', 'work', 'education', 'is_pass', '_id'
    , `img_url`, `photo_ipfs`, `score_count`, `role`, `verified`, `approved`, 'isCivicLinked', 'last_linkedin_pdf_import_at', 'is_airbitz_user', 'language'
    , 'isFacebookLinked', 'isGoogleLinked', 'isLinkedInLinked', 'isAirbitzLinked', 'confidenceScore','isUportLinked', 'skills', 'badges', 'wizards', 'interests',
    'timestamp', 'professions', 'pending_badges', 'terms_privacy', 'data_request', 'social_links','linkedInProfileURL','github_uid'];


const users = new Schema({

    name: {
        type: String,
        minlength: 1,
        maxlength: 64
    },
    civicParams: [],

    civic_uid: {
        type: String
    },

    uport_uid: {
        type: String
    },

    isCivicLinked: {
        type: Boolean
    },

    bio: {
        type: String
    },

    username: {
        type: String
    },

    role: {
        type: String,
        enum: ['admin', 'full_access', 'profile_access', 'no_access']
    },

    email: {
        type: String,
        lowercase: true
    },

    ethaddress: {
        type: String,
        match: /^(0x)?[0-9aA-fF]{40}$/
    },

    ethaddressverified: {
        type: Boolean,
    },

    is_pass: {
        type: Boolean
    },

    verified: {
        type: Boolean
    },

    approved: {
        type: Boolean
    },

    keyPair: {

        type: {

            privateKey: {
                type: String,
                required: true,
            },

            publicKey: {
                type: String,
                required: true,
            },
        }
    },

    score_count: {
        type: Number
    },

    confidenceScore: {
        type: Object //TODO add field schema
    },

    //TODO add item schema
    linkedInParams: [],

    //TODO add item schema
    facebookParams: [],

    //TODO : May be not an array
    uportParams: [],

    social_links : [],

    img_url: {
        type: String
    },

    photo_ipfs: {
        type: String
    },

    last_linkedin_pdf_import_at: {
        type: String
    },

    pdf_aws: {
        type: String
    },

    is_airbitz_user: {
        type: Boolean
    },

    facebook_uid: {
        type: String
    },

    linkedIn_uid: {
        type: String
    },

    google_uid: {
        type: String
    },

    github_uid : {
      type: String
    },

    isFacebookLinked: {
        type: Boolean
    },

    isGoogleLinked: {
        type: Boolean
    },

    isAirbitzLinked: {
        type: Boolean
    },

    isLinkedInLinked: {
        type: Boolean
    },
    
    linkedInProfileURL: {
        type: String
    },

    isUportLinked: {
        type: Boolean
    },

    pass: {
        type: String
    },

    pass_verify_token: {
        type: String
    },

    pass_verify_timestamp: {
        type: Number
    },

    timestamp: {
        type: Number
    },

    salt: {
        type: String
    },

    tokens: [String],

    verify_token: {
        type: String
    },

    ga_id: {
        type: String
    },

    skills: [new Schema({
        skill: {
            name: {
                type: String,
                required: true
            },
            category: {
                type: String,
                required: true
            },
            _id: {
                type: String,
                required: true
            }
        },
        level: {
            type: String,
            enum: ['beginner', 'intermediate', 'expert']
        },
        validations: [new Schema({
            type: {
                type: String,
                required: true
            },
            level: {
                type: String,
                enum: ['beginner', 'intermediate', 'expert']
            },
            id: {
                type: String,
                required : true
            },
            validated : {
                type: Boolean,
                required: true
            }
        })],
    } )],

    badges: [String],

    pending_badges: [String],

    professions : [String],

    education: [new Schema({

        skills: [String],

        school_name: {
            type: String,
            required: true
        },

        item_key: {
            type: String,
            required: true
        },

        school_id: {
            type: String,
            minLength: 1
        },

        degree: {
            type: String
        },

        field_of_study: {
            type: String,
            minLength: 1
        },

        grade: {
            type: String,
            minLength: 1
        },

        activities: {
            type: String,
            minLength: 1
        },

        description: {
            type: String,
            minLength: 1
        },

        start_date: {
            year: {
                type: Number
            },
            month: {
                type: Number
            }
        },

        end_date: {
            year: {
                type: Number
            },
            month: {
                type: Number
            }
        },
    }, {_id: false})],

    work: [new Schema({

        skills: [String],

        item_key: {
            type: String,
            required: true
        },

        company_id: {
            type: String,
            minLength: 1
        },

        title: {
            type: String,
            minLength: 1
        },

        currently_working: {
            type: Boolean
        },

        location: {
            type: String,
            minLength: 1
        },

        activities: {
            type: String,
            minLength: 1
        },

        description: {
            type: String,
            minLength: 1
        },

        start_date: {
            year: {
                type: Number
            },
            month: {
                type: Number
            }
        },

        end_date: {
            year: {
                type: Number
            },
            month: {
                type: Number
            }
        },
    }, {_id: false})],

    language: [new Schema({

        language: {
            type: String,
            required: true
        },

        item_key: {
            type: String,
            required: true
        },

        language_id: {
            type: String,
            required: true
        },

        proficiency: {
            type: String,
            required: true,
            enum: ['elementary', 'limited_working', 'general_professional', 'advanced_professional', 'native']
        }

    }, {_id: false})],

    wizards: [new Schema({

        name: {
            type: String,
            required: true
        },

        current_step: {
            type: Number,
            required: true
        },

        isFinished : {
            type: Boolean
        }

    }, {_id: false})],

    interests: [String],

    terms_privacy: [new Schema({

        tc_version: {
            type: String,
            required: true
        },

        privacy_version: {
            type: String,
            required: true
        },

        timestamp: {
            type: Number,
            required: true
        }

    }, {_id: false})],

    interests: [String],

    terms_privacy: [new Schema({

        tc_version: {
            type: String,
            required: true
        },

        privacy_version: {
            type: String,
            required: true
        },

        timestamp: {
            type: Number,
            required: true
        }

    }, {_id: false})],

    data_request: [new Schema({

        request_type: {
            type: String,
            required: true
        },

        timestamp: {
            type: Number,
            required: true
        }

    }, {_id: false})],

}, {
    usePushEach: true
},{runSettersOnQuery: true});


module.exports.toSafeObject = function toSafeObject(user) {
    let safe_user = {};

    SAFE_USER_FIELDS.forEach((fieldName) => {
        if (user[fieldName]) {
            safe_user[fieldName] = user[fieldName];
        }
    });

    return safe_user;
};

module.exports.schema = users;

