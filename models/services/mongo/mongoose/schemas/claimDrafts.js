const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claims = new Schema({
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    proof: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    token: {
        type: String,
        required: true
    },
    finalized: {
        type: Boolean
    },
    source : {
        type: String
    }
});

module.exports.schema = claims;
