const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const connection = new Schema({

    user_id: {
        type: String,
        minlength: 1,
        required: true
    },

    first_name : {
        type: String
    },

    last_name : {
        type: String
    },

    company : {
        type: String
    },

    email_address : {
        type: String
    },

    position : {
        type: String
    },

    connected_on : {
        type: String
    }
});

module.exports.schema = connection;
