const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

var metadata_schema = {
    contract: {
        type: String,
        required  : true
    },
    function_name: {
        type: String,
        required: true,
    },
    function_args: {
        type :  [],
        required : true
    },
    callback_data : {
        type:Object,
        required: false
    }
}


//TODO : May be move out to a different file
function validateConnectionsTxMetadata(data){
    var function_names = ['createVirtualEntity','createVirtualEntityAndConnection',
                          'editEntity', 'transferEntityOwnerPush', 'transferEntityOwnerPull',
                          'editConnection','removeConnection','addConnection'];

    if (function_names.indexOf(data.function_name) == -1)
        return false;

    //TODO : Add custom call back data if required based on function names
      
}

function validateTxMetaData(data){
    if(data.contract === "Connections"){
        return validateConnectionsTxMetadata(data);
    }
}

const transactionqueue = new Schema({
    tx_raw : {
        type: Object
    },    

    initiating_user_id : {
        type : String,
        required : true
    },

    tx_metadata: { 
        type: metadata_schema , 
        validate : validateTxMetaData
    },

    status: {
        type: String,
        required: true,
        enum: ['QUEUED', 'SUBMITTED']
    },

    submission_id : {
        type : String
    }


});

module.exports = transactionqueue;