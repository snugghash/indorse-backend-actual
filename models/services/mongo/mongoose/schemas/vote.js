const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const vote = new Schema({
    claim_id: {
        type: String,
        required: true
    },
    voter_id: {
        type: String,
        required: true
    },
    voting_round_id: {
        type: String,
        required: true
    },
    endorsed : {
        type : Boolean
    },
    voted_at : {
        type: Number
    },
    /**
     * Token is received on vote submission to the server, must be hashed and sent to SC
     */
    voting_token : {
        type: String
    },
    tx_hash: {
        type: String
    },
    tx_status: {
        type: String
    },
    tx_timestamp: {
        type: Number
    },
    sc_vote_exists: {
        type: Boolean
    },
    paid: {
        type: Boolean
    }
});

module.exports.schema = vote;
