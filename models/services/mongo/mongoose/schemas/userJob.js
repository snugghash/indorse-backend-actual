const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const userJobs = new Schema({
    user :{
        type:String,
        required : true
    },

    job :{
        type: String,
        required: true
    },
    
    appliedAt: {
        type: Number,
        required: true
    }   
})
module.exports.schema = userJobs;
