const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const wizards = new Schema({

    name: {
        type: String,
        minlength: 1,
        required: true
    },

    title: {
        type: String,
        minlength: 1
    },

    description: {
        type: String,
        minlength: 1
    },

    steps: [new Schema(
        {
            number: {
                type: Number,
                minlength: 1
            },
            title: {
                type: String,
                minlength: 1
            },
            description: {
                type: String,
                minlength: 1
            },
            component: {
                type: String,
                minlength: 1,
            },
            canFinish: {
                type: Boolean,
            },
            canSkip: {
                type: Boolean,
            },
            includeInProgress: {
                type: Boolean,
            }
        },
        {_id: false})]
});

module.exports = wizards;
