const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const skills = new Schema({

    name: {
        type: String,
        required: true
    },
    category : {
        type: String,
        required: true
    },
    validation: new Schema({
            chatbot : {
                type: Boolean                
            },
            aip : {
                type :Boolean
            }
        },{_id:false})    
});

module.exports = skills;
