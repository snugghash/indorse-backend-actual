const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const votingRound = new Schema({
    claim_id: {
        type: String,
        required: true
    },
    end_registration: {
        type: Number,
        required: true
    },
    end_voting: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },  
    voters_notified : {
        type : Boolean,
        required : true
    },
    notification_25_sent: {
        type : Boolean
    },
    notification_50_sent: {
        type : Boolean
    },
    notification_75_sent: {
        type : Boolean
    }
});

module.exports.schema = votingRound;