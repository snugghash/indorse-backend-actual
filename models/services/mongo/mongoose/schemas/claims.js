const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const claims = new Schema({
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    proof: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    level: {
        type: String
    },
    visible: {
        type: Boolean
    },
    approved: {
        type: Number
    },
    disapproved: {
        type: Number
    },
    ownerid: {
        type: String,
        required: true
    },
    endorse_count: {
        type: Number
    },
    flag_count: {
        type: Number
    },
    final_status: {
        type: Boolean
    },
    sc_claim_id :{
        type: Number    
    },
    claim_expiry :{
        type: Number
    },
    is_sc_claim :{
        type: Boolean
    },
    source : {
        type: String
    }
});

module.exports.schema = claims;