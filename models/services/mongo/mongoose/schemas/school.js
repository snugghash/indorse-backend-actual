const mongoose = require('../mongooseDB');
const Schema = mongoose.Schema;

const school_names = new Schema({

    school_name: {
        type: String,
        minlength: 1
    },

    item_key: {
        type: String
    }
});

module.exports = school_names;
