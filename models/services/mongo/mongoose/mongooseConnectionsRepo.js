let mongoose = require('./mongooseDB');
let connectionSchema = require('./schemas/connections').schema;
let Connection = mongoose.model('userconnections', connectionSchema);

module.exports.insert = async function insert(doc) {
    let connection = new Connection(doc);
    await connection.save();
    return connection._doc._id.toString();
};

module.exports.insertMany = async function insertMany(docs) {
    return await Connection.insertMany(docs);
};

module.exports.findOne = async function findOne(selector) {
    return await Connection.findOne(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Connection.findById(id).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Connection.find(selector).lean();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await Connection.find(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await Connection.findOneAndUpdate(selector, updateObj);
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
    await Connection.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Connection.find(selector).remove();
};

module.exports.deleteMany = async function deleteMany(selector) {
    await Connection.deleteMany(selector);
};