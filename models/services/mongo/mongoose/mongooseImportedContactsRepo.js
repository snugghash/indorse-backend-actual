let mongoose = require('./mongooseDB');
let contactSchema = require('./schemas/linkedinImportedContacts').schema;
let Contact = mongoose.model('importedcontacts', contactSchema);

module.exports.insert = async function insert(doc) {
    let badge = new Contact(doc);
    await badge.save();
    return badge._doc._id.toString();
};

module.exports.insertMany = async function insertMany(docs) {
    return await Contact.insertMany(docs);
};

module.exports.findOne = async function findOne(selector) {
    return await Contact.findOne(selector).lean();
};

module.exports.findOneById = async function findOneById(id) {
    return await Contact.findById(id).lean();
};

module.exports.findAll = async function findAll(selector) {
    return await Contact.find(selector).lean();
};

module.exports.findAllEntities = async function findAllEntities(selector) {
    return await Contact.find(selector);
};

module.exports.update = async function update(selector, updateObj) {
    await Contact.findOneAndUpdate(selector, updateObj);
};

module.exports.updateAll = async function updateAll(selector, updateObj) {
    await Contact.update(selector, updateObj);
};

module.exports.deleteOne = async function deleteOne(selector) {
    await Contact.find(selector).remove();
};

module.exports.deleteMany = async function deleteMany(selector) {
    await Contact.deleteMany(selector);
};
