const mongooseUserRepo = require("./mongoose/mongooseUserRepo");
const cryptoUtils = require('../common/cryptoUtils');
const randtoken = require('rand-token');
const settings = require('../../settings');

exports.insert = async function insert(user) {

    let passwordData = cryptoUtils.saltHashPassword(user.password);
    user.pass = passwordData.passwordHash;
    user.salt = passwordData.salt;

    user.terms_privacy = [];
    let terms_object = {};
    terms_object.tc_version = settings.TERMS_VERSION;
    terms_object.privacy_version = settings.PRIVACY_VERSION;
    terms_object.timestamp = Math.floor(Date.now() / 1000);

    user.terms_privacy.push(terms_object);

    delete user.password;

    return await persistUser(user);
};

exports.insertSocial = async function insertSocial(user, authType) {

    if (authType !== 'airbitz') {
        user.tokens = [];
        user.verified = true;
        user.role = 'profile_access';
        user.approved = true;
    }

    user.terms_privacy = [];
    let terms_object = {};
    terms_object.tc_version = settings.TERMS_VERSION;
    terms_object.privacy_version = settings.PRIVACY_VERSION;
    terms_object.timestamp = Math.floor(Date.now() / 1000);

    user.terms_privacy.push(terms_object);

    return await persistUser(user);
};

async function persistUser(user) {
    user.verify_token = randtoken.generate(16);
    user.ga_id = randtoken.generate(16);
    user.timestamp = Math.floor(Date.now() / 1000);

    return await mongooseUserRepo.insert(user);
}

exports.update = async function update(selector, updateObj) {
    await mongooseUserRepo.update(selector, updateObj);
};

