/*
    Reference: https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
    This function will go through an input String and will insert \ for special characters, so that we the String becomes regex-able.
    Please refer to the above Stackoverflow docs before testing this function.
    Input: String
    Output: Object {$regex: new RegExp(String)}
 */
exports.createSafeRegexObject = function escapeRegExp(str) {
    if(!str){
        return {$regex: new RegExp(str, "i")};
    }
    return {$regex: new RegExp(str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), "i")};
}