const settings = require('../../settings');
const config = require('config');

let s3Bucket;
let s3ArchiveBucket;

if (uploadsAreEnabled()) {
    const AWS = require('aws-sdk');
    s3Bucket = new AWS.S3({
            params: {Bucket: config.get('aws.s3Bucket')},
            region: settings.AWS_REGION
    });
    s3ArchiveBucket = new AWS.S3({
        params: {Bucket: config.get('aws.s3LinkedinArchiveBucket')},
        region: settings.AWS_REGION
    });
}

/**
 *
 * @param bufferedData
 * @returns {Promise.<void>}
 */
exports.uploadFile = async function uploadFile(key, bufferedData) {
    if (uploadsAreEnabled()) {

        let data = {
            Key: key,
            Body: bufferedData,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg'
        };

        await new Promise((resolve, reject) => {
            s3Bucket.putObject(data, function (error, result) {
                if (!error) {
                    resolve(result);
                } else {
                    console.log("S3 ERROR:" + JSON.stringify(error));
                    reject(error);
                }
            })
        });
    }
};

exports.uploadLinkedinArchive = async function uploadLinkedinArchive(data, key) {
    let base64data = new Buffer(data, 'binary');

    let params = {
        Key: key,
        Body: base64data,
        ContentEncoding: 'base64',
        ContentType: 'application/zip',
        ACL: 'authenticated-read'
    };

    if (uploadsAreEnabled()) {
        await new Promise((resolve, reject) => {
            s3ArchiveBucket.putObject(params, function (error, result) {
                if (!error) {
                    resolve(result);
                } else {
                    console.log("S3 ERROR:" + JSON.stringify(error));
                    reject(error);
                }
            })
        });
    }
};

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'ecs-integration'
        || settings.ENVIRONMENT === 'integration';
}
