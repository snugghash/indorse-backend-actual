const s3Service = require('./s3Service');
const ipfsService = require('./ipfsService');
const config = require('config');
const settings = require('../../settings');

exports.uploadImage = async function uploadImage(prefix, imageData, key) {

    let bufferedData = new Buffer(imageData.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    let imageUpdateResult =  await uploadImageS3AndIPFS(prefix,bufferedData,key);
    return imageUpdateResult;
};

exports.uploadImageBuffer = async function uploadImageBuffer(prefix, bufferedData, key) {

    let imageUpdateResult =  await uploadImageS3AndIPFS(prefix,bufferedData,key);
    return imageUpdateResult;
};

async function uploadImageS3AndIPFS(prefix, bufferedData, key){
    await s3Service.uploadFile(prefix + key, bufferedData);
    let hash = await ipfsService.uploadFile(bufferedData);

    return {
        s3: config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + prefix + key + "?t=" + Math.floor(Date.now() / 1000),
        ipfs: hash
    };
}

exports.uploadsAreEnabled = function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'integration';
};