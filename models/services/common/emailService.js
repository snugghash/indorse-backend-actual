const settings = require('../../settings.js');
const config = require('config');
const jade = require('jade');
const fs = require('fs');
const mongooseMandrillConfig = require('../mongo/mongoose/mongooseMandrillConfig')
var mandrill = require('mandrill-api/mandrill');
var logger = require('./../common/logger').getLogger();


function isEmailEnabled(){
    if (settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production' || settings.ENVIRONMENT === 'integration'){
        return true;
    }else {
        return false;
    }
}   


exports.isEmailEnabled = isEmailEnabled;

async function getTemplateAndMessage(action,email,name){
    let mandrillTemplate = await mongooseMandrillConfig.findOneByAction(action);    

    if(!mandrillTemplate){
        logger.error("Template not found for action " + action );
        throw new Error('Unable to find template for action : ' + action);
    }

    let toValue = {};
    toValue.email = email;
    if(name){
        toValue.name  = name;
    }

    let to = []
    to.push(toValue);

    let msg = {};

    if(mandrillTemplate.bcc_email){
        msg.bcc_address = mandrillTemplate.bcc_email;       
    }    

    msg.subject = mandrillTemplate.subject;
    msg.from_email = mandrillTemplate.from_email;
    msg.from_name = mandrillTemplate.from_name;
    msg.merge_language = "mailchimp";
    msg.to = to;
    return {templateName : mandrillTemplate.template_name, message: msg}
}

exports.getTemplateAndMessage = getTemplateAndMessage;

async function sendEmail(templateName,message,action) {
    console.log("Calling sendEmail : ");
    console.log("template Name : " + JSON.stringify(templateName));
    console.log("Message : "+ JSON.stringify(message))
    let templateContent = [];
    let async = true;
    let ip_pool = "Main Pool";
    let mandrill_client = new mandrill.Mandrill(settings.MANDRILL_API_KEY);

    mandrill_client.messages.sendTemplate({"template_name": templateName, "template_content": templateContent, "message": message, "async": async, "ip_pool": ip_pool}, function(result) {           
        if(result[0].status!=='sent'){
            logger.error('Status is not sent for email to : \nEmail: ' + JSON.stringify(message.to) + "\nAction : " + action +
            '\nMandrill ID : ' + result[0]._id + 'Reason : ' + result[0].reject_reason); 
        }     
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        logger.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);       
        logger.error('\nUnable to send email to \nEmail: ' + JSON.stringify(message.to) + "\nAction : " + action);
        
    });

}

exports.sendEmail = sendEmail;



//Verified
exports.sendWelcomeEmail = async function sendWelcomeEmail(name, email, username) {
    if(!isEmailEnabled())
        return;
    let action = 'WELCOME_EMAIL'
    let data  = await getTemplateAndMessage(action,email,name);
    let username_link = "https://indorse.io/" + username;
    let ref_link  = '<a href="' + username_link + '">' + username_link + '</a>'    

    data.message.global_merge_vars = [
            {
                "name": "INDORSE_FIRSTNAME",
                "content": name
            },
            {
                "name" : "INDORSE_USERNAME",
                "content" : ref_link
            }         
        ]
    await sendEmail(data.templateName,data.message,action);
}

//verified
exports.sendVerificationEmail = async function sendVerificationEmail(name, email, verifyToken, source) {
    if(!isEmailEnabled())
        return;

    let action = 'VERIFICATION_LINK_EMAIL'
    let data  = await getTemplateAndMessage(action,email,name);
    //TODO : Check if this needs to be based on server & https is needed?
    let verify_link_text =  config.get('server.hostname') + "verify-email?email=" + email + "&token=" + verifyToken;

    if(source) {
        verify_link_text += "&source=" + source;
    }

    let verify_link_href  = '<a href="' + verify_link_text + '">' + "Verification link" + '</a>'

    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "INDORSE_VERIFICATION_LINK",
            "content" : verify_link_href
        },
        {
            "name": "INDORSE_VERIFICATION_LINK_TEXT",
            "content" : verify_link_text
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

//Verified
exports.sendPasswordResetEmail = async function sendVerificationEmail(name, email, passVerifyToken) {
    if(!isEmailEnabled())
        return;

    let action = 'PASSWORD_RESET_EMAIL';
    let data  = await getTemplateAndMessage(action,email,name);
    let passwd_link = config.get('server.hostname') + "password/reset?email=" + email + "&pass_token=" + passVerifyToken;
    let passwd_link_href  = '<a href="' + passwd_link + '">' + "Password reset link" + '</a>'   

    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "PASSWORD_RESET_LINK",
            "content" : passwd_link_href
        },
        {
            "name": "INDORSE_PASSWORD_RESET_TEXT",
            "content": passwd_link
        }
    ]
    await sendEmail(data.templateName,data.message,action);    
};

exports.sendUserApprovedEmail = async function sendUserApprovedEmail(name, email) {
    if(!isEmailEnabled())
        return;


    let data  = await getTemplateAndMessage('USER_APPROVED_EMAIL',email,name);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        }
    ]    
    await sendEmail(data.templateName,data.message);    
};

//Verified
exports.sendAdvisorInvitationEmail = async function sendAdvisorInvitationEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;
    //TODO : Check server.hostname or use indorse.io
    let data  = await getTemplateAndMessage('INVITE_ADVISOR_IN_INDORSE',email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }       
    ]    
    await sendEmail(data.templateName,data.message);    
};
//Verified
exports.sendNonExistentAdvisorInvitationEmail = async function sendNonExistentAdvisorInvitationEmail(companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let data  = await getTemplateAndMessage('INVITE_ADVISOR_NOT_IN_INDORSE',email);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }       
    ]
    await sendEmail(data.templateName,data.message);
};

///Verified
exports.sendAdvisorRemovedByUserInfoToCompany = async function sendAdvisorRemovedByUserInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let data  = await getTemplateAndMessage('ADVISOR_REVOKED_NOTIFY_TO_ADMIN',email);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
        
    ]
    await sendEmail(data.templateName,data.message);
};
//Verified
exports.sendAdvisorRemovedByAdminInfoToCompany = async function sendAdvisorRemovedByAdminInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REMOVED_BY_ADMIN_NOTIFY_ADMIN'
    let data  = await getTemplateAndMessage(action,email,name);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
        
    ]
    await sendEmail(data.templateName,data.message,action);
};

//Verified
exports.sendAdvisorRemovedInfoToAdvisor = async function sendAdvisorRemovedInfoToAdvisor(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REMOVED_BY_ADMIN_NOTIFY_ADVISOR'
    let data = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'

    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": company_link_href
        }      
    ]
    await sendEmail(data.templateName, data.message, action);
};
//verified
exports.sendYouRemovedInfoToAdvisor = async function sendYouRemovedInfoToAdvisor(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REVOKED_NOTIFY_ADVISOR'
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "INDORSE_COMPANY_NAME",
            "content" : company_link_href
        }
        
    ]
    await sendEmail(data.templateName,data.message,action);
};
//Verified
exports.sendAdvisorAdvisorAcceptedInvitationToCompanyEmail = async function sendAdvisorAdvisorAcceptedInvitationEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action ='ADVISOR_ACCEPTED_REQ_NOTIFY_ADMIN'
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

exports.sendAdvisorAdvisorApprovedEmail = async function sendAdvisorAdvisorApprovedEmail(name, companyName, companyPrettyId, email) {
    if(!isEmailEnabled())
        return;

    let action ='ADVISOR_APPROVED_REQ_NOTIFY_ADVISOR'
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href = '<a href="' + company_link + '">' + companyName + '</a>'

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": company_link_href
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

exports.sendAdvisorAdvisorDisapprovedEmail = async function sendAdvisorAdvisorDisapprovedEmail(name, companyName, companyPrettyId, email) {
    if(!isEmailEnabled())
        return;

    let action ='ADVISOR_DISAPPROVED_REQ_NOTIFY_ADVISOR'
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href = '<a href="' + company_link + '">' + companyName + '</a>'

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": company_link_href
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

//Verified
exports.sendYouConfirmedAdvisorConnectionInfoToUserEmail = async function sendYouConfirmedAdvisorConnectionInfoToUserEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_ACCEPTED_REQ_NOTIFY_ADVISOR'
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }        
    ]
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendAdvisorAdvisorRejectedInvitationToCompanyEmail = async function sendAdvisorAdvisorRejectedInvitationToCompanyEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REJECTED_REQ_NOTIFY_ADMIN'
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendAdvisorAdvisorRejectedInvitationToUserEmail = async function sendAdvisorAdvisorRejectedInvitationToCompanyEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'ADVISOR_REJECTED_REQ_NOTIFY_ADVISOR';
    let data = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        } 
    ]
    await sendEmail(data.templateName, data.message, action);
};

//verified
exports.sendTeamMemberInvitationEmail = async function sendTeamMemberInvitationEmail(name, companyName, email, companyPrettyId, designation) {
    if(!isEmailEnabled())
        return;

    let action = 'INVITE_TEAM_MEMBER_IN_INDORSE';
    let data = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'

    let designationAndArticle = getDesignationWithArticle(designation);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name" : "INDORSE_COMPANY",
            "content" : company_link_href
        },
        {
            "name" : "INDORSE_DESIGNATION",
            "content" : designationAndArticle
        }       
        
    ]
    await sendEmail(data.templateName, data.message, action);
};
//verified
exports.sendNonExistentTeamMemberInvitationEmail = async function sendNonExistentTeamMemberInvitationEmail(companyName, email, companyPrettyId, designation) {
    if(!isEmailEnabled())
        return;


    let action = 'INVITE_TEAM_MEMBER_NOT_IN_INDORSE'
    let data  = await getTemplateAndMessage(action,email);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'    

    let designationAndArticle = getDesignationWithArticle(designation);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_DESIGNATION",
            "content": designationAndArticle
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }       
    ]
    await sendEmail(data.templateName,data.message,action);
};

function getDesignationWithArticle(designation) {
    let article
    let firstLetter = designation.toLowerCase().charAt(0)
    if (firstLetter === 'a' ||
        firstLetter === 'e' ||
        firstLetter === 'i' ||
        firstLetter === 'o' ||
        firstLetter === 'u') {
        article = 'an';
    } else {
        article = 'a';
    }
    return article + ' ' + designation;
}
//Verified
exports.sendTeamMemberRemovedByUserInfoToCompany = async function sendTeamMemberRemovedByUserInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_REVOKED_NOTIFY_TO_ADMIN'
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};
//verified
exports.sendTeamMemberRemovedByAdminInfoToCompany = async function sendTeamMemberRemovedByAdminInfoToCompany(name, companyName, email) {
    if(!isEmailEnabled())
        return;


    let action = 'TEAM_MEMBER_REMOVED_BY_ADMIN_NOTIFY_ADMIN';
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};
//verified
exports.sendTeamMemberRemovedInfoToTeamMember = async function sendTeamMemberRemovedInfoToTeamMember(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_REMOVED_BY_ADMIN_NOTIFY_TEAM_MEMBER'        
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'   

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }     
    ]
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendTeamMemberTeamMemberAcceptedInvitationToCompanyEmail = async function sendTeamMemberTeamMemberAcceptedInvitationEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_ACCEPTED_REQ_NOTIFY_ADMIN'
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendTeamMemberTeamMemberConfirmedConnectionInfoToUserEmail = async function sendTeamMemberTeamMemberAcceptedInvitationEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_ACCEPTED_REQ_NOTIFY_TEAM_MEMBER'
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);

};

//verified
exports.sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail = async function sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail(name, companyName, email) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEMBER_REJECTED_REQ_NOTIFY_ADMIN'
    let data  = await getTemplateAndMessage(action,email,name);
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};

//verified
exports.sendTeamMemberTeamMemberRejectedInvitationToUserEmail = async function sendTeamMemberTeamMemberRejectedInvitationToCompanyEmail(name, companyName, email, companyPrettyId) {
    if(!isEmailEnabled())
        return;

    let action = 'TEAM_MEM_REJECTED_REQ_NOTIFY_TEAM_MEMBER'
    let data  = await getTemplateAndMessage(action,email,name);
    let company_link = "https://indorse.io/c/" + companyPrettyId;
    let company_link_href  = '<a href="' + company_link + '">' + companyName + '</a>'


    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_COMPANY",
            "content": company_link_href
        }
    ]
    await sendEmail(data.templateName,data.message,action);
};


exports.sendEtherCheckEmail = async function sendEtherCheckEmail(address, environment) {
    if(!isEmailEnabled())
        return;

    let action = 'LOW_ETH_BALANCE'
    let data  = await getTemplateAndMessage(action,settings.ETHEREUM_TECH_SUPPORT_EMAIL,"Admin");
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADDRESS",
            "content": address
        },
        {
            "name": "INDORSE_CURRENCY",
            "content":"Ether"
        }                
    ]
    await sendEmail(data.templateName,data.message,action);      
};


exports.sendINDCheckEmail = async function sendINDCheckEmail(address, environment) {
    if(!isEmailEnabled())
        return;

    let action ='LOW_IND_BALANCE'
    let data  = await getTemplateAndMessage(action,settings.ETHEREUM_TECH_SUPPORT_EMAIL,"Admin");
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADDRESS",
            "content": address
        },
        {
            "name": "INDORSE_CURRENCY",
            "content":"IND"
        }      
    ]
    await sendEmail(data.templateName,data.message,action);      
};  
                                                //softAdvisorInvitationEmail(company.company_name, pretty_id, advisorID, userName, inviteAdvisorRequest.email)
exports.softAdvisorInvitationEmail = async function softAdvisorInvitationEmail(companyName, companyPrettyId, advisorId,userName,email) {
    if (!isEmailEnabled())
        return;

    let action = 'SOFT_INVITE_ADVISOR'
    if (!userName) {
        userName = "User";
    }
    
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "companies/" + companyPrettyId + "/advisors/" + advisorId;

    if(!userName){
        userName = "User";
    }

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "INDORSE_COMPANY_ADVISOR_SOFT_SHOW_URL",
            "content": company_link
        },
        {
            "name": "INDORSE_COMPANY_ADVISOR_SOFT_REJECT_LINK",
            "content": '<a href="' + company_link + '">here</a>'
        }
    ]
    await sendEmail(data.templateName, data.message, action);
};


exports.softAdvisorAcceptNotifyAdmin = async function softAdvisorAcceptNotifyAdmin(userName,companyName, companyPrettyId,email) {
    if (!isEmailEnabled())
        return;
    
    let action = 'SOFT_ACCEPT_ADVISOR_NOTIFY_ADMIN'
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "admin/companies/" + companyPrettyId + "/edit/";
    let dashboard_link_href = '<a href="' + company_link + '">' + 'Dashboard' + '</a>'

    //Add company name to the email
    data.message.subject += ' ' + companyName;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADV_EMAIL",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "INDORSE_DASHBOARD_LINK",
            "content": dashboard_link_href
        }
    ]
    await sendEmail(data.templateName, data.message, action);
};

//TODO : Refactor
exports.softAdvisorValidatedNotifyAdmin = async function softAdvisorValidatedNotifyAdmin(userName, companyName, companyPrettyId,
    profileUrl, email,status) {

    if (!isEmailEnabled())
        return;

    let action = 'SOFT_VALIDATE_ADVISOR_NOTIFY_ADMIN'
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "admin/companies/" + companyPrettyId + "/edit/";
    let dashboard_link_href = '<a href="' + company_link + '">' + 'Dashboard' + '</a>'
    let profile_link_href = '<a href="' + profileUrl + '">' + 'here' + '</a>'
    let indorse_flag = "rejected";

    if(status === "ACCEPTED"){
        indorse_flag = "accepted";
    }

    //Add company name to the email
    data.message.subject += ' ' + companyName;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADV_EMAIL",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "LINKED_IN_PROFILE",
            "content": profile_link_href
        },        
        {
            "name": "INDORSE_DASHBOARD_LINK",
            "content": dashboard_link_href
        },
        {
            "name": "INDORSE_ACCEPT_FLAG",
            "content": indorse_flag            
        }
    ]
    await sendEmail(data.templateName, data.message, action);
};

//TODO : Refactor
exports.softAdvisorRejectNotifyAdmin = async function softAdvisorRejectNotifyAdmin(userName, companyName, companyPrettyId, email) {
    if (!isEmailEnabled())
        return;


    let action = 'SOFT_REJECT_ADVISOR_NOTIFY_ADMIN'
    let data = await getTemplateAndMessage(action, email, userName);
    let company_link = config.get('server.hostname') + "admin/companies/" + companyPrettyId + "/edit/";
    let dashboard_link_href = '<a href="' + company_link + '">' + 'Dashboard' + '</a>'

    //Add company name to the email
    data.message.subject += ' ' + companyName;

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_ADV_EMAIL",
            "content": userName
        },
        {
            "name": "INDORSE_COMPANY_NAME",
            "content": companyName
        },
        {
            "name": "INDORSE_DASHBOARD_LINK",
            "content": dashboard_link_href
        }
    ]
    await sendEmail(data.templateName, data.message, action);
};

exports.notifyJobApplication = async function notifyJobApplication(name,indorseUserName,userEmail,jobTitle,jobLocation, email) {
    if (!isEmailEnabled())
        return;

    let action = 'NEW_JOB_APPLICATION_NOTIFY_COMPANY'
    let data = await getTemplateAndMessage(action, email);

    let username_link = config.get('server.hostname') + indorseUserName;
    
    data.message.subject = name + " applied for your " + jobTitle + "(" + jobLocation + ")"

    data.message.headers ={}
    data.message.headers["Reply-To"] = userEmail


    data.message.global_merge_vars = [
        {
            "name": "INDORSE_JOB_TITLE",
            "content": jobTitle
        },
        {
            "name": "INDORSE_USERNAME",
            "content": name
        },
        {
            "name": "INDORSE_USER_PROFILE",
            "content": username_link
        },
        {
            "name": "INDORSE_JOB_LOCATION",
            "content": jobLocation
        }
    ]
    await sendEmail(data.templateName, data.message, action);
};