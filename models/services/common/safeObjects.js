const sanitize = require('mongo-sanitize');
const xssSanitize = require('sanitizer');

const SAFE_USER_FIELDS = ['name', 'bio', 'ethaddress', 'user_id', 'username', 'email', 'user_id', 'work', 'education', 'is_pass', '_id'
    , `img_url`, `photo_ipfs`, `score_count`, `role`, `verified`, `approved`, 'last_linkedin_pdf_import_at', 'is_airbitz_user', 'language'
    , 'isFacebookLinked', 'isGoogleLinked', 'isLinkedInLinked', 'isAirbitzLinked', 'isCivicLinked', 'confidenceScore', 'skills', 'skill_name' ,
    'social_links','linkedInProfileURL'];


const SAFE_JWT_FIELDS = ['name', 'ethaddress','_id', 'user_id', 'username', 'email', 'user_id', 'role', 'linkedIn_uid','linkedInParams','githubUid','profileUrl', 'accessToken'];

const LOWERCASE_REQUEST_FIELDS = ['pretty_id ', 'email', 'username', 'skill_name'];

function extractFields(obj, fieldNames) {
    let extractedFields = {};

    fieldNames.forEach((fieldName) => {
        let fieldValue = obj[fieldName];
        if(typeof fieldValue === 'number') {
            extractedFields[fieldName] = fieldValue;
        } else if (typeof fieldValue === 'boolean') {
            extractedFields[fieldName] = fieldValue;
        } else if (fieldValue) {
            extractedFields[fieldName] = recursivelySanitize(fieldValue);
        }
    });

    return extractedFields;
}

exports.safeObjectParse = function safeObjectParse(obj, fieldNames) {
    return extractFields(obj, fieldNames);
};

/**
 * Safely extracts body fields into a new object.
 * Converts all case insensitive fields to lower case.
 * @param req
 * @para fieldNames array of strings
 */
exports.safeReqestBodyParser = function safeReqestBodyParser(req, fieldNames) {
    return safelyParseObject(req.body, fieldNames);
};

/**
 * Safely extracts body fields into a new object.
 * Converts all case insensitive fields to lower case.
 * @param req
 * @para fieldNames array of strings
 */
exports.safeReqestQueryParser = function safeReqestQueryParser(req, fieldNames) {
    return safelyParseObject(req.query, fieldNames);
};

exports.safeUserObject = function safeUserObject(user) {
    return extractFields(user, SAFE_USER_FIELDS);
};

exports.safeJWTObject = function safeJWTObject(user) {
    return extractFields(user, SAFE_JWT_FIELDS);
};

/**
 * Safely extracts params fields into a new object.
 * Converts all case insensitive fields to lower case.
 * @param req
 * @para fieldNames array of strings
 */
exports.safeReqestParamsParser = function safeReqestParamsParser(req, fieldNames) {

    return safelyParseObject(req.params, fieldNames);
};

function safelyParseObject(object, fieldNames) {
    let safeRequest = extractFields(object, fieldNames);

    LOWERCASE_REQUEST_FIELDS.forEach((fieldName) => {
        if (safeRequest[fieldName]) {
            safeRequest[fieldName] = safeRequest[fieldName].toLowerCase();
        }
    });

    return safeRequest;
}

function recursivelySanitize(field) {
    if (field instanceof Object) {
        sanitize(field);
        for (let key in field) {
            field[key] = recursivelySanitize(field[key]);
        }
        return field;
    } else {
        if (typeof field === 'string' || field instanceof String) {
            return xssSanitize.unescapeEntities(xssSanitize.sanitize(field));
        } else {
            return field;
        }
    }
}

function sanitizeMultiple(...fields) {
    for(let field in fields) {
        recursivelySanitize(field);
    }
}
exports.sanitize = recursivelySanitize;

exports.sanitizeMultiple = sanitizeMultiple;