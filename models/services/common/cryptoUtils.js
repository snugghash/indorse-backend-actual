let _self = this;
const crypto = require('crypto');
const randtoken = require('rand-token');
const config = require('config');
const jwt = require('jsonwebtoken');
const safeObjects = require('./safeObjects');
const logger = require('./logger').getLogger();

exports.genRandomString = function genRandomString(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length);
    /** return required number of characters */
};

exports.sha512 = function sha512(password, salt) {
    let hash = crypto.createHmac('sha512', salt);
    /** Hashing algorithm sha512 */
    hash.update(password);
    let value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

exports.sha256 = function sha256(password, salt) {
    let hash = crypto.createHmac('sha256', salt);
    /** Hashing algorithm sha256 */
    hash.update(password);
    let value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

exports.saltHashPassword = function saltHashPassword(userpassword) {
    let salt = _self.genRandomString(16);
    /** Gives us salt of length 16 */
    return _self.sha512(userpassword, salt);
};

exports.generateItemKey = function generateItemKey() {
    return randtoken.generate(16) + Math.floor(Date.now() / 1000).toString();
};

exports.generateJWT = function generateJWT(user) {
    return jwt.sign(safeObjects.safeJWTObject(user), config.get('JWT.jwtSecret'), {
        expiresIn: 60 * 60 * 24 * 31 // expires in 31 days
    });
};

exports.generateGithubJWTTmpUser = function generateGithubJWTTmpUser(githubUid, name, email, profileUrl, accessToken) {
    let user = {email: email, githubUid: githubUid, name: name, profileUrl : profileUrl, accessToken: accessToken};
    return jwt.sign(safeObjects.safeJWTObject(user), config.get('JWT.jwtSecret'), {
        expiresIn: config.get('JWT.signupTokenTime') // expires in 30 minutes
    });
};

exports.generateJWTTmpUser = function generateJWTTmpUser(linkedInUid, email, linkedInParams) {
    let user = {email: email, linkedIn_uid: linkedInUid, linkedInParams: linkedInParams};
    return jwt.sign(safeObjects.safeJWTObject(user), config.get('JWT.jwtSecret'), {
        expiresIn: config.get('JWT.signupTokenTime') // expires in 30 minutes
    });
};

exports.decodeGithubSignupToken = function decodeGithubSignupToken(signupToken) {
    let email, githubUid, profileUrl, name, accessToken;
    return new Promise((resolve, reject) => {
        jwt.verify(signupToken, config.get('JWT.jwtSecret'), function (err, decoded) {
            if (err) {
                logger.debug('Github JWT error' + JSON.stringify(err));
                reject(new Error("Github Signup token is not valid"));
            }
            else {
                if (decoded && decoded.email && decoded.githubUid && decoded.profileUrl && decoded.name && decoded.accessToken) {
                    email = decoded.email.toLowerCase();
                    githubUid = decoded.githubUid;
                    profileUrl = decoded.profileUrl;
                    name = decoded.name;
                    accessToken = decoded.accessToken;
                    resolve([email,githubUid,profileUrl,accessToken]);
                }else{
                    logger.debug('Github decoded token error : githubUid ' + githubUid + ' email ' + email + ' profileUrl ' + profileUrl + ' name ' + name );
                    reject(new Error("Github Signup token is missing with fields"));
                }
            }
        });
    } )

};