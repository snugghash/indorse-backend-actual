const winston = require('winston');


function getLoggingLevel(){
    if ((process.env.NODE_ENV === 'test') || (process.env.NODE_ENV === 'staging') || process.env.NODE_ENV === 'integration')
        return 'debug'
    else
        return 'info';
}


exports.getLogger = function getLogger(label) {
    return new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                level: getLoggingLevel(),
                stringify: true, 
                timestamp: true,
                colorize: true,
                json: true 
            })
        ]
    })
};

exports.getBigchainLogger = function getBigchainLogger() {
    return new (winston.Logger)({
        transports: [
            new (winston.transports.File)({
                filename: 'bigchain.log',
                level: 'debug',
                prettyPrint: true,
                json: false
            })
        ]
    })
};

exports.getScriptLogger = function getScriptLogger() {
    return new (winston.Logger)({
        transports: [
            new (winston.transports.File)({
                filename: 'script.log',
                level: 'debug',
                prettyPrint: true,
                json: false
            })
        ]
    })
};
