exports.createTimestamp = function createTimestamp() {
    return Math.floor(Date.now() / 1000);
};