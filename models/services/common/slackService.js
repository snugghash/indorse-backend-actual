const Slack = require('slack-node');
const settings = require('../../settings');
let slackBugs;
let slackClaims;

if (process.env.NODE_ENV === 'production') {
    const SLACK_BUGS_WEBHOOK = settings.SLACK_BUGS_WEBHOOK;
    const SLACK_CLAIMS_WEBHOOK = settings.SLACK_CLAIMS_WEBHOOK;
    webhookUri = "__uri___";
    slackBugs = new Slack();
    slackClaims = new Slack();
    slackBugs.setWebhook(SLACK_BUGS_WEBHOOK);
    slackClaims.setWebhook(SLACK_CLAIMS_WEBHOOK);
}

exports.reportBug = function reportBug(bug) {

    if (process.env.NODE_ENV === 'production') {
        try {
            slackBugs.webhook({
                channel: "#i_backend_bugs",
                text: "BUG ON PRODUCTION\n url: " + bug.url + "\nmethod: " + bug.method + "\nRequestId: "
                + bug.requestID + "\nStack: " + bug.stack,
            }, function(err, response) {
                if(err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to report slack bug!", e)
        }
    }
};

exports.reportClaim = function reportClaim(claim, img_url, username) {

    if (process.env.NODE_ENV === 'production') {
        try {
            slackClaims.webhook({
                channel: "#i_claims",
                title: "Claim awaits approval",
                attachments: [
                    {
                        "title": username,
                        "title_link": "https://indorse.io/users/" + claim.ownerid,
                        "image_url": "https://" + img_url,
                        "color": "#111FA5"
                    },
                    {
                        "title": "Title",
                        "text": claim.title,
                        "color": "#764FA5"
                    },
                    {
                        "title": "Description",
                        "text": claim.desc,
                        "color": "#01FF45"
                    },
                    {
                        "title": "Level",
                        "text": claim.level,
                        "color": "#01FFFF"
                    },
                    {
                        "text": "Choose action",
                        "fallback": "You are unable to act",
                        "color": "#000000",
                        "attachment_type": "default",
                        actions: [
                            {
                                "type": "button",
                                "text": "Decide",
                                "url": "https://indorse.io/admin/claims?search=" + claim.claim_id.toString() + "&page=1"
                            },
                            {
                                "type": "button",
                                "text": "Proof",
                                "url": claim.proof
                            }
                        ]
                    }]
            }, function(err, response) {
                if(err) {
                    console.log("SLACK ERR:" + err);
                } else {
                    console.log(response);
                }
            });
        } catch (e) {
            console.log("Unable to send claim notification to slack!", e)
        }
    }
};


