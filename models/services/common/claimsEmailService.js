const settings = require('../../settings.js');
const emailService = require('../../../models/services/common/emailService')
const config = require('config');

exports.sendVoteInvitationEmail = async function sendVoteInvitationEmail(name, claimId, email) {
    if (!emailService.isEmailEnabled())
        return;

    let data = await emailService.getTemplateAndMessage('INVITATION_TO_VOTE', email, name);
    let claim_link = config.get('server.hostname') + "claims/" + claimId;
    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>'
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_CLAIM_LINK",
            "content": claim_link_href
        }
    ]
    await emailService.sendEmail(data.templateName, data.message);
};


exports.sendClaimTalliedEmail = async function sendClaimTalliedEmail(name, claimId, email) {
    if (!emailService.isEmailEnabled())
        return;

    let data = await emailService.getTemplateAndMessage('CLAIM_TALLIED', email, name);
    let claim_link = config.get('server.hostname') + "claims/" + claimId;
    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>'
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_CLAIM_LINK",
            "content": claim_link_href
        }
    ]
    await emailService.sendEmail(data.templateName, data.message);
};


exports.sendClaimApprovedEmail = async function sendClaimApprovedEmail(name, claimId, email) {
    if (!emailService.isEmailEnabled())
        return;

    let data = await emailService.getTemplateAndMessage('CLAIM_APPROVED', email, name);
    let claim_link = config.get('server.hostname') + "claims/" + claimId;
    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>'
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "INDORSE_CLAIM_LINK",
            "content": claim_link_href
        }
    ]
    await emailService.sendEmail(data.templateName, data.message);
};

exports.sendClaimDisapprovedEmail = async function sendClaimDisapprovedEmail(name, claimId, email) {
    if (!emailService.isEmailEnabled())
        return;

    let data = await emailService.getTemplateAndMessage('CLAIM_REJECTED', email, name);
    //populate dynamic content
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        }
    ]
    await emailService.sendEmail(data.templateName, data.message);
};


exports.sendVoteTxFailedEmail = async function sendVoteTxFailedEmail(name, claimId, email, txHash) {
    if (!emailService.isEmailEnabled())
        return;

    let data = await emailService.getTemplateAndMessage('VOTE_TX_FAILED', email, name);
    let claim_link = config.get('server.hostname') + "claims/" + claimId;
    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>'
    let etherscan_link_href = "<a href='https://etherscan.io/tx/" + txHash + "'>etherscan</a>"

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "ETHSCAN_VOTE_TX",
            "content": etherscan_link_href
        },
        {
            "name": "INDORSE_CLAIM_LINK",
            "content": claim_link_href
        }
    ]
    await emailService.sendEmail(data.templateName, data.message);
};


exports.sendVoteTxTimedOutEmail = async function sendVoteTxTimedOutEmail(name, claimId, email, txHash) {
    if (!emailService.isEmailEnabled())
        return;

    let data = await emailService.getTemplateAndMessage('VOTE_TX_TIMEOUT', email, name);
    let claim_link = config.get('server.hostname') + "claims/" + claimId;
    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>'
    let etherscan_link_href = "<a href='https://etherscan.io/tx/" + txHash + "'>etherscan</a>"
    data.message.global_merge_vars = [
        {
            "name": "INDORSE_FIRSTNAME",
            "content": name
        },
        {
            "name": "ETHSCAN_VOTE_TX",
            "content": etherscan_link_href
        },
        {
            "name": "INDORSE_CLAIM_LINK",
            "content": claim_link_href
        }
    ]
    await emailService.sendEmail(data.templateName, data.message);
};

exports.sendClaimAwaitingApproval = async function sendClaimAwaitingApproval(claimId) {
    if (!emailService.isEmailEnabled())
        return;

    //TODO : Find a better solution , mandrill does not like multiple args packed.   
    let to_array = ["ivo.zielinski@gmail.com", "telepras@gmail.com", "kedar@blimp.co.in", "gaurang@attores.com"]
    for (var i = 0; i < to_array.length; i++) {
        let to = to_array[i];
        let data = await emailService.getTemplateAndMessage('ADMIN_CLAIM_APPROVAL', to, 'Admin');
        let claim_link_href = '<a href="' + config.get('server.hostname') + "admin/claims?search=" + claimId + '&page=1">here</a>';
        data.message.global_merge_vars = [
            {
                "name": "INDORSE_CLAIM_LINK",
                "content": claim_link_href
            }
        ]
        await emailService.sendEmail(data.templateName, data.message);
    }
}

exports.sendVoteReminder = async function sendClaimAwaitingApproval(claimId, email, percentage, deadline) {
    if (!emailService.isEmailEnabled())
        return;

    let to = email;
    let data = await emailService.getTemplateAndMessage('VOTING_DEADLINE_REMINDER', to);
    let claim_link = config.get('server.hostname') + "claims/" + claimId;
    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>';

    data.message.global_merge_vars = [
        {
            name : "DEADLINE_DATE",
            content : deadline
        },
        {
            name : "PERCENTAGE_PASSED",
            content : percentage
        },
        {
            "name": "INDORSE_CLAIM_LINK",
            "content": claim_link_href
        }
    ];

    await emailService.sendEmail(data.templateName, data.message);
};

exports.sendClaimDraftLinkEmail = async function sendClaimDraftLinkEmail(token, email, source) {
    if (!emailService.isEmailEnabled())
        return;

    let to = email;
    let data = await emailService.getTemplateAndMessage('CLAIM_SUBMISSION_CONFIRMATION', to);
    let claim_link = config.get('server.hostname') + "claims/verify?token=" + token;

    if(source) {
        claim_link += "&source=" + source;
    }

    let claim_link_href = '<a href="' + claim_link + '">' + "link" + '</a>';

    data.message.global_merge_vars = [
        {
            "name": "INDORSE_CLAIM_REGISTRATION_LINK",
            "content": claim_link_href
        }
    ];
    console.log(data);
    await emailService.sendEmail(data.templateName, data.message);
};
