const Amplitude = require('amplitude')
const settings = require('../../settings');
const User = require('../mongo/mongoose/schemas/user');
let amplitude;

if (settings.ENVIRONMENT !== 'test') {
    amplitude = new Amplitude(settings.AMPLITUDE_API_TOKEN);
}
/*
    if user_id is null, please pass as null (falsy), and send device_id in the fourth param. Device_id could be obtined from request.
    !! USER ID must always be the mongo user ID of a user who made the request.
 */
exports.publishData = function publishData(event_type, event_properties, user_id, device_id) {

    let amplitudeData = {
        event_type: event_type,
        event_properties: event_properties,
    };

    console.log('Sending data to Amplitude' + JSON.stringify(amplitudeData));

    if (settings.ENVIRONMENT !== 'test') {
        if(user_id){
            amplitudeData.user_id = user_id;
        }
        if(device_id){
            amplitude.deviceId = device_id;
        }
        amplitude.track(amplitudeData);
    }
};

exports.identify = function identify(user, ip) {

    let userObj = User.toSafeObject(user);
    userObj.ip = ip;

    let amplitudeData = {
        user_id: user._id.toString(),
        user_properties: userObj
    };

    if (settings.ENVIRONMENT !== 'test') {
        amplitude.identify(amplitudeData);
    }
};

exports.publishSuccessResponse = function publishSuccessResponse(req, res, next) {
    if (!req.user_id) {
        req.user_id = 'unidentified user';
    }
    let pureUrl = extractPureUrl(req.url, req.params);

    let amplitudeData = {
        event_type: pureUrl,
        user_id: req.user_id,
        method: req.method,
        event_properties: {result: 'success'},
    };

    console.log('Sending data to Amplitude' + JSON.stringify(amplitudeData));

    if (settings.ENVIRONMENT !== 'test') {
        amplitude.track(amplitudeData);
    }

}

// This function is deprecated
function extractPureUrl(rawUrl, urlParams){
    let pureUrl = rawUrl;
    for (param in urlParams) {
        let pattern = new RegExp('/' + urlParams[param]);
        rawUrl = rawUrl.replace(pattern, "");
    }
    return rawUrl;
}