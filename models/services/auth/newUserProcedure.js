const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');

exports.startLinkingProcedures = async function startLinkingProcedures(user) {
    if(!user._id) {
        user = await mongooseUserRepo.findOneByEmail(user.email);
    }

    await linkUserWithAdvisors(user);
    await linkUserWithTeamMembers(user);
};


//When user email is same as user object
async function linkUserWithAdvisors(user,email=undefined) {
    if (!email){
        email = user.email;
    }
    let companies = await mongooseCompanyRepo.findAllEntities({ 'advisors.email': email});
    for(let company of companies){
        if(company.advisors && company.advisors.length){            
            for (let advisor of company.advisors){
                if (advisor.email === email){
                    advisor.user_id = user._id;
                    advisor.email = undefined;
                    await company.save();
                }
            }
        }            
    }
}

async function linkUserWithTeamMembers(user) {
    let companies = await mongooseCompanyRepo.findAllEntities({'team_members.email': user.email});
    for (let company of companies) {
        if (company.team_members && company.team_members.length) {
            for (let team_member of company.team_members) {
                if (team_member.email === user.email) {
                    team_member.user_id = user._id;
                    team_member.email = undefined;
                    await company.save();
                }
            }
        }
    }
}


module.exports.linkUserWithAdvisors = linkUserWithAdvisors;