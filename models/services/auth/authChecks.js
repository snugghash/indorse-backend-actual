const mongooseUserRepo = require('../mongo/mongoose/mongooseUserRepo');
const errorUtils = require('../error/errorUtils');

/**
 *
 * @param role name
 * @returns {requiresRole}
 */
exports.roleCheck = function roleCheck(role) {

    return async function requiresRole(req, res, next) {
        let user = await mongooseUserRepo.findOneByEmail(req.email);

        if (user.role !== role.name && user.role !== 'admin') {
            errorUtils.throwError("Insufficient role, requires:" + role.toString(), 403);
        }

        next();
    }
};

/**
 *
 * @returns {loginCheck}
 */
exports.loginCheck = function loginCheck() {

    return async function loginCheck(req, res, next) {

        if (!req.login) {
            errorUtils.throwError("Must be logged in", 403);
        }

        next();
    }
};

/**
 *
 * @param permissions array of objects {role,permission}
 * @returns {requiresPermissions}
 */
exports.permissionCheck = function permissionCheck(permissions) {

    return async function requiresPermissions(req, res, next) {

        for (let index = 0; index < permissions.length; index++) {
            if(!req.permissions[permissions[index].role][permissions[index].operation]) {
                errorUtils.throwError("Insufficient permissions, requires:" + permissions[index].role + '.' + permissions[index].operation, 403);
            }
        }

        next();
    }
};

