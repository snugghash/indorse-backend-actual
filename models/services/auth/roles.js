module.exports.ADMIN = {
    name: "admin",
    permissions: {
        login: {
            read: true,
            write: true
        },

        profile: {
            read: true,
            write: true
        },

        claim: {
            read: true,
            write: true
        },

        vote: {
            read: true,
            write: true
        },

        admin: {
            read: true,
            write: true
        }
    }
};

module.exports.FULL_ACCESS = {
    name: "full_access",
    permissions: {
        login: {
            read: true,
            write: true
        },

        profile: {
            read: true,
            write: true
        },

        claim: {
            read: true,
            write: true
        },

        vote: {
            read: true,
            write: true
        },

        admin: {
            read: false,
            write: false
        }
    }
};

module.exports.PROFILE_ACCESS = {
    name: "profile_access",
    permissions: {
        login: {
            read: true,
            write: true
        },

        profile: {
            read: true,
            write: true
        },

        claim: {
            read: false,
            write: false
        },

        vote: {
            read: false,
            write: false
        },

        admin: {
            read: false,
            write: false
        }
    }
};

module.exports.NO_ACCESS = {
    name: "no_access",
    permissions: {
        login: {
            read: true,
            write: true
        },

        profile: {
            read: true,
            write: false
        },

        claim: {
            read: false,
            write: false
        },

        vote: {
            read: false,
            write: false
        },

        admin: {
            read: false,
            write: false
        }
    }
};

module.exports.DEFAULT = {
    name: "no_access",
    permissions: {
        login: {
            read: true,
            write: true
        },

        profile: {
            read: false,
            write: false
        },

        claim: {
            read: false,
            write: false
        },

        vote: {
            read: false,
            write: false
        },

        admin: {
            read: false,
            write: false
        }
    }
};