const mongo = require('mongodb');
const config = require('config');
const settings = require('../../settings.js');

const jwt = require('jsonwebtoken');
const sanitize = require('mongo-sanitize');

const roles = require('./roles');

const MongoClient = mongo.MongoClient
var db;

MongoClient.connect(settings.DATABASE_CONNECTION_STRING, function (err, database) {
    if (err) return console.log(err);
    db = database;
});

module.exports = function (req, res, next) {
    if (req && req.headers) {
        if(req.headers['x-amp-device-id']){
            req.device_id = req.headers['x-amp-device-id'];
        }
        if(req.headers['x-amp-session-id']){
            req.session_id = req.headers['x-amp-session-id'];
        }
    }
    if ('token' in req && req['token'] !== '') {
        var decoded = jwt.decode(req['token'], {complete: true});
        if (decoded && 'payload' in decoded && 'email' in decoded['payload']) {
            email = decoded['payload']['email'].toLowerCase();
            //TODO email should not be here if token is invalid!
            let username = decoded['payload']['username'];
            req.email = email;
            if (username) {
                req.username = username.toLowerCase();
            }
        }
        else {
            req.login = false;
            next();
            return;
        }
        token = sanitize(req['token']);
        req.token = token;
        req.permissions = {login: {}, profile: {}, claim: {}, vote: {}, admin: {}};
        db.collection('users', function (err, collection) {
            collection.findOne({
                'email': email.toLowerCase(),
                'approved': true,
                'tokens': {'$in': [token]}
            }, function (err, item) {
                if (item) {
                    //console.log('Token not found for the user')
                    jwt.verify(token, config.get('JWT.jwtSecret'), function (err, decoded) {
                        if (err) {
                            console.log('JWT verification failed')
                            req.login = false;
                            next();
                        }
                        else {
                            req.user_id = item._id.toString();
                            req.name = item.name.toString();
                            req.login = true;
                            if ('role' in item) {
                                if (item['role'] === "admin") {
                                    req.permissions = roles.ADMIN.permissions;
                                }
                                else if (item['role'] === "full_access") {
                                    req.permissions = roles.FULL_ACCESS.permissions;
                                }
                                else if (item['role'] === "profile_access") {
                                    req.permissions = roles.PROFILE_ACCESS.permissions;
                                }
                                else if (item['role'] === "no_access") {
                                    req.permissions = roles.NO_ACCESS.permissions;
                                }
                            }
                            else {
                                req.permissions = roles.DEFAULT.permissions;
                            }
                            next();
                        }
                    })
                }
                else {
                    req.login = false;
                    next();
                }
            })
        })
    }
    else {
        req.login = false;
        next();
    }
}
