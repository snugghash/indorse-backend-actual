const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../services/common/safeObjects');
const claimsEmailService = require('../services/common/claimsEmailService');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.post('/claims/:claim_id/disapprove',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(approveClaim),
    );
};

async function approveClaim(req, res) {
    let claim_id = safeObjects.sanitize(req.params.claim_id);

    let claim = await mongooseClaimsRepo.findOneById(claim_id);

    if (!claim) {
        errorUtils.throwError("Claim not found!", 400)
    }

    if (claim.approved || claim.disapproved) {
        errorUtils.throwError("Claim already classified!", 400)
    }

    let claimantUserId = claim.ownerid;

    let claimant = await mongooseUserRepo.findOneById(claimantUserId);

    let claimEvent = {};

    claimEvent._id = claim_id;
    claimEvent.ownerid = claimantUserId;
    claimEvent.claim_id = claim_id;

    await mongooseClaimsRepo.disapproveClaim(claim_id);

    amplitudeTracker.publishData('claim_disapproved', claimEvent, claimantUserId);

    claimsEmailService.sendClaimDisapprovedEmail(claimant.name,claim_id,claimant.email);

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['claim_id'],
    properties: {
        claim_id: {
            type: 'string',
            minLength: 1
        }
    }
};

