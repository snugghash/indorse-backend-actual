const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseSkillRepo = require('../services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const roles = require('../services/auth/roles');
const randomValidatorService = require('./randomValidatorService');
const routeUtils = require('../services/common/routeUtils');
const claimsEmailService = require('../services/common/claimsEmailService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const slackService = require('../services/common/slackService');

const validator = new Validator({allErrors: true});
const validate = validator.validate;

const REQUEST_FIELD_LIST = ['title', 'desc', 'proof', 'level'];

exports.register = function register(app) {
    app.post(
        '/claims',
        validate({body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(createClaim),
    );
};


async function createClaim(req, res) {
    let createClaimRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    createClaimRequest.feature_category = 'skill_validation';
    amplitudeTracker.publishData('claim_creation_initialized', createClaimRequest, req.user_id);
    delete createClaimRequest.feature_category;
    let requestingUserId = req.user_id;

    let requestingUser = await mongooseUserRepo.findOneById(requestingUserId);

    let existingClaimsSameLevel = await mongooseClaimsRepo.findAll({
        $and: [
            {title: createClaimRequest.title},
            {level: createClaimRequest.level},
            {ownerid: requestingUserId}]
    });

    for (let i = 0; i < existingClaimsSameLevel.length; i++) {
        let existingClaimSameLevel = existingClaimsSameLevel[i];
        if (existingClaimSameLevel.final_status) {
            amplitudeTracker.publishData('claim_creation_failed_claim_already_obtained', createClaimRequest, req.user_id);
            errorUtils.throwError("A claim for that skill and level has already been obtained!", 400)
        } else if (!existingClaimSameLevel.approved && !existingClaimSameLevel.disapproved  && !existingClaimSameLevel.sc_claim_id) {
            amplitudeTracker.publishData('claim_creation_failed_claim_pending_approval', createClaimRequest, req.user_id);
            errorUtils.throwError("A claim for that skill and level is already pending approval!", 400)
        }
    }

    let existingClaimsAnyLevel = await mongooseClaimsRepo.findAll({$and: [{title: createClaimRequest.title}, {ownerid: requestingUserId}]});

    for (let i = 0; i < existingClaimsAnyLevel.length; i++) {
        let existingClaimAnyLevel = existingClaimsAnyLevel[i];

        if (!existingClaimAnyLevel.approved && !existingClaimAnyLevel.disapproved && !existingClaimAnyLevel.sc_claim_id) {
            amplitudeTracker.publishData('claim_creation_failed_claim_pending_approval', createClaimRequest, req.user_id);
            errorUtils.throwError("A claim for that skill and level is already pending approval!", 400)
        }

        let existingVotingRound = await mongooseVotingRoundRepo.findOne({
            $and: [
                {claim_id: existingClaimAnyLevel._id},
                {status: 'in_progress'}]
        });
        if (existingVotingRound) {
            amplitudeTracker.publishData('claim_creation_failed_claim_already_pending', createClaimRequest, req.user_id);
            errorUtils.throwError("Pending claim with this title already exists!", 400)
        }
    }

    //Just checking if there is enough validators
    await randomValidatorService.getValidators(createClaimRequest.title,req.user_id);

    createClaimRequest.state = 'new';
    createClaimRequest.visible = true;
    createClaimRequest.is_sc_claim = true;
    createClaimRequest.ownerid = req.user_id;

    let createdClaimId = await mongooseClaimsRepo.insert(createClaimRequest);

    createClaimRequest._id = createdClaimId;
    createClaimRequest.ownerid = req.user_id;
    createClaimRequest.claim_id = createdClaimId;

    amplitudeTracker.publishData('claim_creation_succesful', createClaimRequest, req.user_id);

    claimsEmailService.sendClaimAwaitingApproval(createdClaimId);

    let userHasSkill;

    if(requestingUser.skills) {
        userHasSkill = requestingUser.skills.find((s) => s.skill && s.skill.name === createClaimRequest.title.toLowerCase());
    }

    let validationToAdd = {
        type: "claim",
        level: createClaimRequest.level,
        id: createdClaimId,
        validated : false
    };

    if(userHasSkill) {
        await mongooseUserRepo.addValidationToSkill(req.email, userHasSkill.skill.name, validationToAdd);
    } else {
        let skill = await mongooseSkillRepo.findOne({name : createClaimRequest.title.toLowerCase()});

        await mongooseUserRepo.addSkill(req.email, {
            skill : skill,
            level : createClaimRequest.level,
            validations : [validationToAdd]
        });
    }

    slackService.reportClaim(createClaimRequest, requestingUser.img_url, requestingUser.username);

    res.status(200).send({claim : createClaimRequest});
}


const BODY_SCHEMA = {
    type: 'object',
    required: ['title', 'desc', 'proof'],
    properties: {
        title: {
            type: 'string',
            allowedValues: ['Javascript', 'Java', 'Solidity']
        },
        desc: {
            type: 'string',
            minLength: 1
        },
        proof: {
            type: 'string',
            allowedValues: 1
        },
        level : {
            type: 'string',
            enum: ['beginner', 'intermediate', 'expert']
        }
    },
    additionalProperties: false
};
