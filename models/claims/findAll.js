const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongoClaimsRepo = require("../services/mongo/mongoRepository")('claims');
const mongoVotesRepo = require("../services/mongo/mongoRepository")('votes');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const stringUtils = require('../services/blob/stringUtils');

var mongo = require('mongodb');

exports.register = function register(app) {
    app.get('/claimsall',
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to list claims!', 403);
    }

    let searchParam = null;
    let sortParam = "title";
    let sortOrder = 1;
    let fields;

    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.sort) {
        sortParam = req.query.sort;
        if (sortParam[0] == "-") {
            sortOrder = -1;
            sortParam = sortParam.substring(1);
        }
    }

    if (req.query.fields) {
        fields = safeObjects.sanitize(req.query.fields).split(',');
    }

    let sortObject = {};
    sortObject[sortParam] = sortOrder;
    let pageNo = safeObjects.sanitize(req.query.pageNo);
    let perPage = safeObjects.sanitize(req.query.perPage);

    if (!pageNo) {
        pageNo = 1;
    }
    if (!perPage) {
        perPage = 10;
    }

    let skip = (parseInt(pageNo) - 1) * parseInt(perPage);
    let limit = parseInt(perPage);

    if (searchParam) {
        let o_id;

        if(searchParam.length === 24) {
            o_id = new mongo.ObjectID(searchParam);
        } else {
            o_id = searchParam;
        }
        let claimsCursor = await mongoClaimsRepo.findAllWithCursor({
            '$or': [{'title': stringUtils.createSafeRegexObject(searchParam)},
                {'desc': stringUtils.createSafeRegexObject(searchParam)},
                {'_id': o_id},
                {'proof': stringUtils.createSafeRegexObject(searchParam)}]
        }, {'state': 0, 'visible': 0, 'tokens': 0});

        let sortedClaims = await claimsCursor.sort(sortObject).toArray();

        let matchingClaims = sortedClaims.length;
        sortedClaims = sortedClaims.slice(skip, skip + limit);

        let claimsCount = await mongoClaimsRepo.count();
        //let totalVerifiedUsers =  await mongoUserRepo.countVerifiedUsers();
        for(let key of Object.keys(sortedClaims))
        {
            claim = sortedClaims[key];
            sortedClaims[key]['votes'] = await getVotes(claim['_id']);
            if('ownerid' in claim && claim['ownerid'] != undefined)
            {
                let owner = await getOwner(claim['ownerid']);
                sortedClaims[key]['ownername'] = owner['name'];
            }
            if(!('final_status' in claim))
            {
                console.log("Final status not present");
                sortedClaims[key]['final_status'] = "ongoing";
            }
            sortedClaims[key]['createdAt'] = (claim['_id'].getTimestamp().getTime()) / 1000;
        }

        let returnObj = {
            success: true,
            totalClaims: claimsCount,
            matchingClaims: matchingClaims,
            claims: sortedClaims
        };

        res.status(200).send(returnObj);

    } else {

        let claimsCursor = await mongoClaimsRepo.findAllWithCursor({},{'state': 0, 'visible': 0, 'tokens': 0});

        let sortedClaims = await claimsCursor.sort(sortObject).toArray();

        let matchingClaims = sortedClaims.length;
        sortedClaims = sortedClaims.slice(skip, skip + limit);

        let claimsCount = await mongoClaimsRepo.count();
        for(let key of Object.keys(sortedClaims))
        {
       
            claim = sortedClaims[key];
            sortedClaims[key]['votes'] = await getVotes(claim['_id']);
            if('ownerid' in claim && claim['ownerid'] != undefined)
            {
                let owner = await getOwner(claim['ownerid']);
                sortedClaims[key]['ownername'] = owner['name'];
            }
            if(!('final_status' in claim))
            {
                console.log("Final status not present");
                sortedClaims[key]['final_status'] = "ongoing";
            }
            sortedClaims[key]['createdAt'] = (claim['_id'].getTimestamp().getTime()) / 1000;
        }

        let returnObj = {
            success: true,
            totalClaims: claimsCount,
            matchingClaims: matchingClaims,
            claims: sortedClaims
        };

        res.status(200).send(returnObj);
    }
};

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}

async function getOwner(userId)
{
    let user = await mongooseUserRepo.findOneById(userId);
    return user;
}

async function getVotes(claim_id) {

    let votes_invited = await mongoVotesRepo.findAll({'claim_id' : claim_id.toString()});   
    let invited_count = votes_invited.length;
    let registered_count = 0;
    let indorsed_count = 0;
    let flagged_count = 0;
    votes_invited.forEach(function(vote)
    {
        if('registered' in vote && vote['registered'] == true)
        {
            registered_count++;
        }
        if('endorsed' in vote && vote['endorsed'] == true)
        {
            indorsed_count++;
        }
        if('endorsed' in vote && vote['endorsed'] == false)
        {
            flagged_count++;
        }

    })
    let vote_details = {};
    vote_details['invited'] = invited_count;
    vote_details['registered'] = registered_count;
    vote_details['indorsed'] = indorsed_count;
    vote_details['flagged'] = flagged_count;
    return vote_details;
}
