const mongooseValidatorRepo = require('../services/mongo/mongoose/mongooseValidatorRepo');
const errorUtils = require('../services/error/errorUtils');
const settings = require('../settings');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

exports.getValidators = async function getValidators(skill, requestingUserId) {

    let validatorCount = Number(settings.CLAIM_VALIDATOR_COUNT);

    let randomValidators = await mongooseValidatorRepo.getRandomSampleBySkill(validatorCount, skill,
        requestingUserId);

    if (randomValidators.length < validatorCount) {
        amplitudeTracker.publishData('claim_creation_failed_not_enough_validators', {skill : skill}, requestingUserId);
        errorUtils.throwError("Not enough validators for this skill in the database", 400);
    }

    return randomValidators;
};