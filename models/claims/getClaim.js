const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const getVoteService = require('../votes/getVoteService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.get('/claims/:claim_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getClaim));
};

async function getClaim(req, res) {

    let claim_id = safeObjects.sanitize(req.params.claim_id);

    let claim = await mongooseClaimsRepo.findOneById(claim_id);

    if (!claim) {
        errorUtils.throwError("Claim not found", 404);
    }

    amplitudeTracker.publishData('claim_accessed', {claim_id: claim_id}, req.user_id);

    let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id.toString());

    let returnObj = {
        claim: claim
    };

    if (votingRound)  {
        let votes = await mongooseVoteRepo.findByVotingRoundId(votingRound._id.toString());
        returnObj.votes = await Promise.all(votes.map((vote) => extractVote(vote)));

        returnObj.votinground = votingRound;
    }

    res.status(200).send(returnObj);
}

async function extractVote(voteDoc) {
    return await getVoteService.getVote(voteDoc._id.toString());
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['claim_id'],
    properties: {
        claim_id: {
            type: 'string',
            minLength: 1
        }
    }
};

