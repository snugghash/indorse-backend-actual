const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const routeUtils = require('../services/common/routeUtils');
const validate = require('../services/common/validate');
const safeObjects = require('../services/common/safeObjects');

const REQUEST_FIELD_LIST = ['userId'];

exports.register = function register(app) {
    app.get('/users/:user_id/claims',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getUserClaims));
};

async function getUserClaims(req, res) {
    let user_id = safeObjects.sanitize(req.params.user_id);

    let claims = await mongooseClaimsRepo.findByOwnerId(user_id);

    let claimsToReturn = [];

    for (let claimIndex = 0; claimIndex < claims.length; claimIndex++) {
        let claim = claims[claimIndex];
        let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id.toString());
        claimsToReturn.push(
            {
                claim : claim,
                votinground: votingRound
            }
        );
    }

    res.status(200).send({claims : claimsToReturn});
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            pattern: "^(0x)?[0-9aA-fF]{24}$"
        }
    }
};