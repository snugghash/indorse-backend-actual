const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const safeObjects = require('../services/common/safeObjects');
const claimsEmailService = require('../services/common/claimsEmailService');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const claimsProxy = require('../services/ethereum/claims/proxy/claimsProxy');
const roles = require('../services/auth/roles');
const settings = require('../settings');
const randomValidatorService = require('./randomValidatorService');
const routeUtils = require('../services/common/routeUtils');
const timestampService = require('../services/common/timestampService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.post(
        '/claims/:claim_id/approve',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(approveClaim),
    );
};


async function approveClaim(req, res) {
    let claim_id = safeObjects.sanitize(req.params.claim_id);

    let claim = await mongooseClaimsRepo.findOneById(claim_id);

    if (!claim) {
        errorUtils.throwError("Claim not found!", 400)
    }

    if (claim.approved || claim.disapproved) {
        errorUtils.throwError("Claim already classified!", 400)
    }

    let claimantUserId = claim.ownerid;

    let randomValidators = await randomValidatorService.getValidators(claim.title, claimantUserId);

    let claimant = await mongooseUserRepo.findOneById(claimantUserId);

    let now = timestampService.createTimestamp();
    let votingDeadline = now + Number(settings.CLAIM_VOTE_PERIOD);

    let votingRound = {
        claim_id: claim_id,
        end_registration: now,
        end_voting: votingDeadline,
        status: 'in_progress',
        voters_notified: false
    };

    let createdVotingRoundId = await mongooseVotingRoundRepo.insert(votingRound);

    let voterAddresses = [];

    for (let validatorIndex = 0; validatorIndex < randomValidators.length; validatorIndex++) {
        let validator = randomValidators[validatorIndex];

        let newVote = {
            claim_id: claim_id,
            voter_id: validator.user_id.toString(),
            voting_round_id: createdVotingRoundId
        };

        await mongooseVoteRepo.insert(newVote);

        let user = await mongooseUserRepo.findOneById(validator.user_id.toString());

        voterAddresses.push(user.ethaddress);
    }

    await claimsProxy.createClaim(voterAddresses, votingDeadline, claimant.ethaddress, claimantUserId,
        claim_id.toString());

    await mongooseClaimsRepo.approveClaim(claim_id);

    let claimEvent = {};

    claimEvent._id = claim_id;
    claimEvent.ownerid = claimantUserId;
    claimEvent.claim_id = claim_id;


    amplitudeTracker.publishData('claim_approved', claimEvent, claimantUserId);

    claimsEmailService.sendClaimApprovedEmail(claimant.name,claim_id,claimant.email);

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['claim_id'],
    properties: {
        claim_id: {
            type: 'string',
            minLength: 1
        }
    }
};

