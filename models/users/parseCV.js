const logger = require("../services/common/logger").getLogger("parseCV.js");
const config = require('config');
const userRepo = require('../services/mongo/userRepo');
const cryptoUtils = require('../services/common/cryptoUtils');
const fs = require('fs');
const resumePdfToJson = require('resume-pdf-to-json');
const moment = require('moment');
const safeObjects = require('../services/common/safeObjects');
const settings = require('../settings');
const mongooseCompanyRepo = require("../services/mongo/mongoose/mongooseCompanyRepo");
const mongoUserRepo = require('../services/mongo/mongoRepository')('users');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const validate = require('../services/common/validate');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const REQUESTED_FIELD_LIST = ['cv'];

exports.register = function register(app) {
    app.post('/parse-cv',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(parseCV));
};

async function parseCV(req, res) {

    let parseCVRequest = safeObjects.safeReqestBodyParser(req, REQUESTED_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    amplitudeTracker.publishData('parse-cv',{feature_category: 'profile'},req.user_id);

    let cv_data = parseCVRequest.cv;
    let buff = new Buffer(cv_data.replace(/^data:application\/\w+;base64,/, ""), 'base64');
    let pathRoot = process.cwd();
    let path = pathRoot + '/data/cv_tmp/' + req.email + Math.floor(Date.now()).toString() + '_tmp.pdf' // create a temporary pdf file named email_tmp.pdf


    let userToUpdate;
    let user_id;
    let aws_link;
    try {
        userToUpdate = await mongoUserRepo.findOne({email: req.email});
        user_id = userToUpdate._id;
        let aws_link ='';

        if (uploadsAreEnabled()) {
            aws_link = await updateImage(buff, user_id);
        }

    } catch (error) {
        logger.error('aws upload failed ' + error.stack);
    }


    try {
        fs.writeFileSync(path, buff);
        let cv = await resumePdfToJson(path);
        fs.unlinkSync(path);
        let [workEntries, educationEntries] = await extractDetails(cv);
        await userRepo.update({
            email: req.email
        }, {
            $push: {
                work: {$each: workEntries},
                last_linkedin_pdf_import_at: Math.floor(Date.now() / 1000).toString(),
                pdf_aws: aws_link
            }
        })
    } catch (error) {
        fs.unlinkSync(path);
        errorUtils.throwError(error, 500);
    }

    res.status(200).send({
        success: true,
        message: 'pass'
    });
};

async function extractDetails(cv) {
    let cv_summary, cv_education, cv_experience, cv_vol_experience;
    //assigning experience fields
    let workEntries = [];
    let educationEntries = [];

    // extract and assign fields from JSON
    for (let i = 0; i < cv.length; i++) {
        let context = cv[i];
        let header = context.head[0];
        if (header === 'Summary') {
            cv_summary = context;
        }
        if (header === 'Experience') {
            cv_experience = context;
        }
        if (header === 'Volunteer Experience') {
            cv_experience = context;
        }
        if (header === 'Skills & Expertise') {
            cv_vol_experience = context;
        }
        if (header === 'Education') {
            cv_education = context;
        }
    }// end of for loop

    if (cv_experience) {
        for (let i = 0; i < cv_experience.sections.length; i++) {
            let work_info = cv_experience.sections[i];
            let work_description = work_info.text;
            let work_title_splits = work_info.head[0].split(' at ', 2);
            let work_period = work_info.head[1].split(' - ', 2); // from - to
            
            let company_name, title;
            if (typeof work_title_splits[1] !== 'undefined') {
                company_name = work_title_splits[1];
                title = work_title_splits[0]
            } else {
                company_name = ''; //case when copmany name doesn't exist in linkedin pdf
                title = work_title_splits[0]
            }

            let workEntry = {};
            workEntry['company_name'] = company_name;
            workEntry['title'] = title;
            workEntry['description'] = work_description;
            if (work_description === '') {
                workEntry['description'] = ''
            }
            workEntry['start_date'] = convertStringDate(work_period[0]); 
            workEntry['end_date'] = convertStringDate(work_period[1]);
            let item_key = cryptoUtils.generateItemKey();
            workEntry['company_id'] = await getCopmanyID(company_name); //temporary id -> this need to be further confirmed by the user, then it will have proper company_id
            workEntry['item_key'] = item_key;
            workEntries.push(workEntry)
        } // end of experience assigning for loop
    }

    //assigning eductation information
    if (false) { // skip education parse TODO: modfiy the package to handle education
        let educationEntry = {};
        educationEntry['description'] = cv_education.text;
        let item_key = cryptoUtils.generateItemKey();
        educationEntry['school_id'] = 'linkedIn_' + cryptoUtils.generateItemKey();//temporary id
        educationEntry['item_key'] = item_key;
        educationEntries.push(educationEntry)
    } // end of education assigning

    return [workEntries, educationEntries];

}

function convertStringDate(dateString) {
    let formatYear = ['YYYY'];
    let date = dateString.split(' ');
    let isYear = moment(date[1],"YYYY").isValid();
    let safeYear;
    if(isYear){
        safeYear = date[1];
    }
    let safeMonth = moment().month(date[0]).format("M");
    return {month: safeMonth, year: safeYear};
}

async function getCopmanyID(company_name) {
    let company_id;
    try {
        let company = await mongooseCompanyRepo.findOne({'company_name': company_name});

        if (company) {
            company_id = company['_id'].toString();
        } else {
            let companyNameItem = {
                company_name: company_name
            };

            company_id = await mongooseCompanyRepo.insert(companyNameItem);
        }
    } catch (error) {
        logger.error(error.stack);
        res.status(500).send({
            success: false,
            message: error.message
        });
        return;
    }

    return company_id;
}

async function updateImage(bufferedData, user_id) {

    const AWS = require('aws-sdk');
    AWS.config.loadFromPath('./config/aws.json');
    s3 = new AWS.S3();
    s3Bucket = new AWS.S3({params: {Bucket: config.get('aws.s3Bucket')}});

    key = user_id.toString()
    let data = {
        Key: key,
        Body: bufferedData,
        ContentEncoding: 'base64',
        ContentType: 'application/pdf'
    };
    logger.debug('/parse-cv storing pdf on S3');
    let result = await s3Bucket.putObject(data);

    return config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + user_id + "?t=" + Math.floor(Date.now() / 1000);
}

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'integration';
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            cv: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['cv'],
        additionalProperties: false
    };
}
