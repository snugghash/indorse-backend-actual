const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['item_key'];

exports.register = function register(app) {
    app.post('/deletelanguage',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(deleteLanguage));
};

async function deleteLanguage(req, res) {
    let deleteLanguageRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    await userRepo.update({email: req.email}, {$pull: {language: {item_key: deleteLanguageRequest.item_key}}});

    res.status(200).send({
        success: true,
        message: 'Deleted successfully'
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            item_key: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['item_key'],
        additionalProperties: false
    };
}