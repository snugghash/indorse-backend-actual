const mongooseLanguageRepo = require('../../services/mongo/mongoose/mongooseLanguageRepo');
const replicationUserRepo = require('../../services/mongo/userRepo');
const cryptoUtils = require('../../services/common/cryptoUtils');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['language', 'proficiency', 'item_key', 'language_id'];

exports.register = function register(app) {
    app.post('/updatelanguage',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(updateLanguage));
};

async function updateLanguage(req, res) {
    let languageRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    if (languageRequest.item_key) {
        await replicationUserRepo.update(
            {'language.item_key': languageRequest.item_key},
            {$set: {'language.$': languageRequest}})
    } else {
        let languageFromMongo = await mongooseLanguageRepo.findOne({
            language:
                {$regex: '^' + languageRequest.language + '$', $options: 'i'}
        });

        if (languageFromMongo) {
            languageRequest.language_id = languageFromMongo._id.toString();
        } else {
            let languageItem = {
                language: languageRequest.language,
                item_key: cryptoUtils.generateItemKey()
            };

            languageRequest.language_id = await mongooseLanguageRepo.insert(languageItem);
        }

        languageRequest.item_key = cryptoUtils.generateItemKey();
        await replicationUserRepo.update({email: req.email}, {$push: {language: languageRequest}});
    }

    res.status(200).send({
        success: true,
        language: languageRequest,
        message: 'Updated successfully'
    });

};

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            language: {
                type: 'string',
                minLength: 1
            },
            item_key: {
                type: 'string',
                minLength: 1
            },
            language_id: {
                type: 'string',
                minLength: 1
            },
            proficiency: {
                type: 'string',
                enum: ['elementary', 'limited_working', 'general_professional', 'advanced_professional', 'native']
            },
        },
        required: ['language', 'proficiency'],
        additionalProperties: false
    };
}