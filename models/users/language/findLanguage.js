const mongooseLanguageRepo = require('../../services/mongo/mongoose/mongooseLanguageRepo');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');
const stringUtils = require('../../services/blob/stringUtils');

const REQUEST_FIELD_LIST = ['language'];

exports.register = function register(app) {

    app.post('/findlanguage',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(findLanguage));
};

async function findLanguage(req, res) {
    let findRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let languages = await mongooseLanguageRepo
        .findAll({language: stringUtils.createSafeRegexObject(findRequest.language)});

    res.status(200).send({
        success: true,
        languages: languages
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            language: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['language'],
        additionalProperties: false
    };
}