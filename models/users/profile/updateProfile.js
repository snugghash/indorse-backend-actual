const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const User = require('../../services/mongo/mongoose/schemas/user');
const settings = require('../../settings');
const s3Service = require('../../services/blob/s3Service');
const ipfsService = require('../../services/blob/ipfsService');
const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const config = require('config');
const Web3 = new require('web3');
const web3 = new Web3();
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');
const requestIp = require('request-ip');


const REQUEST_FIELD_LIST = ['name', 'bio', 'img_data', 'username','ethaddress', 'social_links'];

exports.register = function register(app) {
    app.post('/updateprofile',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(updateProfile));
};

function validateSocialLinks(socialLinks){
    for (val of socialLinks){
        if(val.value){
            if(val.value.length > 128) 
                errorUtils.throwError('Social links need to be less than 128 characters.', 400);            
            else 
                val.value = val.value.toLowerCase();     
       }   
    }    
    return true;
}

async function updateProfile(req, res) {
    let profileUpdateRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('Not logged in.', 403);
    }

    let userToUpdate = await mongooseUserRepo.findOne({email: req.email});

    if (!userToUpdate) {
        errorUtils.throwError('User does not exist.', 404);
    }

    if (profileUpdateRequest.ethaddress) {

        if (!web3.isAddress(profileUpdateRequest.ethaddress)) {
            errorUtils.throwError('Invalid Ethereum address format!', 422);
        }

        let userWithEthaddress = await mongooseUserRepo.findOne({
            $and: [
                {ethaddress: profileUpdateRequest.ethaddress},
                {email: {$ne: req.email}}]
        });

        if (userWithEthaddress) {
            errorUtils.throwError('This Ethereum address is already taken.', 400);
        }
    }

    if (profileUpdateRequest.img_data) {
        await updateImage(profileUpdateRequest, userToUpdate._id.toString());
    }

    if (userNameInRequest(profileUpdateRequest) && userHasNoUserName(userToUpdate)) {
        profileUpdateRequest.username = profileUpdateRequest.username;
    } else {
        delete profileUpdateRequest.username;
    }
  
    if (profileUpdateRequest.social_links){
        validateSocialLinks(profileUpdateRequest.social_links);
    }


    await userRepo.update({email: req.email}, {$set: profileUpdateRequest});

    let userToReturn = await mongooseUserRepo.findOne({email: req.email});

    let ip = requestIp.getClientIp(req);

    amplitudeTracker.publishData('profile_update',{feature_category: 'profile', profile_update_data: profileUpdateRequest}, req.user_id); // TODO: profile_update_data should only contain meaningful insight
    amplitudeTracker.identify(userToReturn,ip);

    res.status(200).send({
        success: true,
        profile: User.toSafeObject(userToReturn),
        message: 'Profile was updated successfully.'
    });
}

async function updateImage(profileUpdateRequest, user_id) {

    if (uploadsAreEnabled()) {
        let bufferedData = new Buffer(profileUpdateRequest.img_data.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        await s3Service.uploadFile(user_id, bufferedData);
        let hash = await ipfsService.uploadFile(bufferedData);

        profileUpdateRequest.img_url = config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + user_id +
            "?t=" + Math.floor(Date.now() / 1000);
        profileUpdateRequest.photo_ipfs = hash;
    }
}

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'integration';
}

function userNameInRequest(profileUpdateRequest) {
    return profileUpdateRequest.username && true;
}

function userHasNoUserName(user) {
    return !user.username || user.username === '';
}


exports.updateImage = updateImage;
exports.uploadsAreEnabled = uploadsAreEnabled;

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            bio: {
                type: 'string',
                minLength: 1
            },
            ethaddress: {
                type: 'string',
                pattern: "^(0x)?[0-9aA-fF]{40}$"
            },
            img_data: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            social_links : {
                type: 'array',
                items: {
                    type: 'object',
                }
            }
        },
        required: ['name'],
        additionalProperties: false
    };
}