const logger = require("../../services/common/logger").getBigchainLogger();
const mongoUserRepo = require("../../services/mongo/mongoRepository")('users');
const User = require('../../services/mongo/mongoose/schemas/user');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const stringUtils = require('../../services/blob/stringUtils');

exports.register = function register(app) {
    app.get('/users',
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {

    let searchParam = null;
    let sortParam = "name";
    let sortOrder = 1;
    let fields;

    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.sort) {
        sortParam = req.query.sort;
        if (sortParam[0] == "-") {
            sortOrder = -1;
            sortParam = sortParam.substring(1);
        }
    }

    if (req.query.fields) {
        fields = safeObjects.sanitize(req.query.fields).split(',');
    }

    let sortObject = {};
    sortObject[sortParam] = sortOrder;
    let pageNo = safeObjects.sanitize(req.query.pageNo);
    let perPage = safeObjects.sanitize(req.query.perPage);

    if (!pageNo) {
        pageNo = 1;
    }
    if (!perPage) {
        perPage = 10;
    }

    if (!req.login) {
        errorUtils.throwError("User is not logged in", 403);
    }

    let requestingUser = await mongoUserRepo.findOne({'email': req.email, 'role': 'admin'});

    if (!requestingUser) {
        errorUtils.throwError("User is not admin", 403);
    }

    let skip = (parseInt(pageNo) - 1) * parseInt(perPage);
    let limit = parseInt(perPage);

    if (searchParam) {

        let usersCursor = await mongoUserRepo.findAllWithCursor({
            '$or': [{'name': stringUtils.createSafeRegexObject(searchParam)},
                {'username': stringUtils.createSafeRegexObject(searchParam)},
                {'email': stringUtils.createSafeRegexObject(searchParam)},
                {'ethaddress': stringUtils.createSafeRegexObject(searchParam)}]
        }, {'salt': 0, 'pass': 0, 'tokens': 0});

        let sortedUsers = await usersCursor.sort(sortObject).skip(skip).limit(limit).toArray();

        let matchingUsers = sortedUsers.length;
        let matchingVerifiedUsers = sortedUsers.filter(function(user){return user.verified===true}).length;

        let userCount = await mongoUserRepo.count();
        let totalVerifiedUsers =  await mongoUserRepo.countVerifiedUsers();

        let usersToReturn = sortedUsers.map((user) => {
            let safeUser = safeObjects.safeUserObject(user);
            if (fields) {
                let filteredUser = {};
                fields.forEach(field => {
                    if (safeUser[field]) {
                        filteredUser[field] = safeUser[field];
                    }
                });
                return filteredUser;
            } else {
                return safeUser;
            }
        });

        let returnObj = {
            success: true,
            totalUsers: userCount,
            matchingUsers: matchingUsers,
            users: usersToReturn,
            totalVerifiedUsers: totalVerifiedUsers,
            matchingVerifiedUsers: matchingVerifiedUsers
        };

        res.status(200).send(returnObj);

    } else {

        let usersCursor = await mongoUserRepo.findAllWithCursor({'email': {'$exists': true}}, {
            //TODO why is this here?
            'salt': 0,
            'pass': 0,
            'tokens': 0
        });
        let sortedUsers = await usersCursor.sort(sortObject).skip(skip).limit(limit).toArray();
        let userCount = await mongoUserRepo.count();
        let totalVerifiedUsers = await mongoUserRepo.countVerifiedUsers();
        let matchingVerifiedUsers = totalVerifiedUsers;

        let usersWithNoCredentials = sortedUsers.map((user) => {
            return User.toSafeObject(user);
        });

        res.status(200).send({
            success: true,
            totalUsers: userCount,
            users: usersWithNoCredentials,
            totalVerifiedUsers: totalVerifiedUsers,
            matchingVerifiedUsers: matchingVerifiedUsers
        });
    }
};
