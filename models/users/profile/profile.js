const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseBadgeRepo = require('../../services/mongo/mongoose/mongooseBadgeRepo');
const User = require('../../services/mongo/mongoose/schemas/user');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const ObjectID = require('mongodb').ObjectID;
const routeUtils = require('../../services/common/routeUtils');
const addCompanyToWork = require('../work/addCompanyToWork');

const REQUEST_FIELD_LIST = ['email', 'user_id', 'username'];
const USER_NOT_FOUND_MESSAGE = "Unable to find the user";

exports.register = function register(app) {
    app.post('/me', routeUtils.asyncMiddleware(profile));
};

async function profile(req, res) {
    let profileRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (thereIsUsernameIn(profileRequest)) {
        await findAndReturnUserBasedOnCriteria(res, {username: profileRequest.username})

    } else if (userIdIsNotSetIn(profileRequest) && req.email) {
        await findAndReturnUserBasedOnCriteria(res, {email: req.email});

    }
    else if (userIdIsNotSetIn(profileRequest) && profileRequest.email) {
        await findAndReturnUserBasedOnCriteria(res, {email: profileRequest.email});

    }
    else if (thereIsValidUserIdIn(profileRequest)) {
        await findAndReturnUserBasedOnCriteria(res, {_id: new ObjectID(profileRequest.user_id)})

    } else {
        errorUtils.throwError(USER_NOT_FOUND_MESSAGE, 404);
    }
};

async function findAndReturnUserBasedOnCriteria(res, criteria) {
    let user = await mongooseUserRepo.findOne(criteria);

    if (!user) {
        errorUtils.throwError(USER_NOT_FOUND_MESSAGE, 404);
    }

    if(user.badges) {
        let badgesToReturn = [];

        for(let badgeId of user.badges) {
            let badge = await mongooseBadgeRepo.findOneByPrettyId(badgeId);

            if(badge) {
                badgesToReturn.push(badge);
            }
        }

        user.badges = badgesToReturn;
    }

    if(user.pending_badges) {
        let badgesToReturn = [];

        for(let badgeId of user.pending_badges) {
            let badge = await mongooseBadgeRepo.findOneByPrettyId(badgeId);

            if(badge) {
                badgesToReturn.push(badge);
            }
        }

        user.pending_badges = badgesToReturn;
    }

    setSocialLinkedStatus(user);

    if (user.pass) {
        user.is_pass = true;
    }

    if (user.work) {
        await setWorkCompanyDetails(user);
    }

    let userWithNoCredentials = User.toSafeObject(user);

    res.status(200).send({
        success: true,
        profile: userWithNoCredentials
    });
}

function setSocialLinkedStatus(user) {
    if (user.facebook_uid) {
        user.isFacebookLinked = true;
    }
    if (user.google_uid) {
        user.isGoogleLinked = true;
    }
    if (user.linkedIn_uid) {
        user.isLinkedInLinked = true;
    }
    if (user.is_airbitz_user) {
        user.isAirbitzLinked = true;
    }

    if(user.civic_uid){
        user.isCivicLinked = true;
    }

    if (user.uport_uid) {
        user.isUportLinked = true;
    }
}

function userIdIsNotSetIn(profileRequest) {
    return !(profileRequest.user_id);
}

function thereIsValidUserIdIn(profileRequest) {
    return profileRequest.user_id && (profileRequest.user_id.length === 12 || profileRequest.user_id.length === 24);
}

function thereIsUsernameIn(profileRequest) {
    return profileRequest.username;
}

async function setWorkCompanyDetails(user) {
    let workExperiences = user.work;

    let newWorkExperiences = []
    for (let i = 0; i < workExperiences.length; i++) {
        let work = workExperiences[i];

        await addCompanyToWork.addCompany(work)

        newWorkExperiences.push(work);
    }

    user.work = newWorkExperiences;
}