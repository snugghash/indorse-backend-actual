const mongoUserRepo = require('../../services/mongo/mongoRepository')('users');
const mongooseUserRepo = require('../../services/mongo/mongoRepository')('users');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const mongoUsernamesRepo = require('../../services/mongo/mongoRepository')('usernames');

const REQUEST_FIELD_LIST = ['username'];

exports.register = function register(app) {
    app.post('/findusername',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(findUsername));
};

/**
 * @swagger
 * definitions:
 *   FindUsernameQuery:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /findusername:
 *   post:
 *     description: Checks if username exists (success=false  if exists, true otherwise)
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/FindUsernameQuery'
 *     responses:
 *       200:
 *         description: Search successful
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Response'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function findUsername(req, res) {
    let findRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOne({'username': findRequest.username});

    if (user) {
        //TODO what is up with those success flags?
        res.status(200).send({success: false, message: 'Username already exists'});
    } else {
        let usernameIsBannded = await mongoUsernamesRepo.findOne({username: findRequest.username});

        if (usernameIsBannded) {
            res.status(200).send({success: false, message: 'Username is banned'});
        } else {
            res.status(200).send({success: true, message: "Username not found"});
        }
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            username: {
                type: 'string'
            }
        },
        required: ['username'],
        additionalProperties: false
    };
}
