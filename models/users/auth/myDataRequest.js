const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const errorUtils = require('../../services/error/errorUtils');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const settings = require('../../settings');
const cryptoUtils = require('../../services/common/cryptoUtils');
const validate = require('../../services/common/validate');

exports.register = function register(app) {
    app.post('/mydatarequest',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(myDataRequest));
};

const REQUEST_FIELD_LIST = ['password','requestType'];

async function myDataRequest(req, res) {

    let dataRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    let user = await mongooseUserRepo.findOneByEmail(req.email);

    if (!user || !passwordIsCorrect(dataRequest, user)) {
        errorUtils.throwError(USERNAME_OR_PASSWORD_INCORRECT_MESSAGE, 400);
    }

    let request_object = {};
    request_object.request_type = dataRequest.requestType;
    request_object.timestamp = Math.floor(Date.now() / 1000);

    await mongooseUserRepo.update({email: req.email}, {$push: {data_request: request_object}});

    res.status(200).send({
        success: true,
        message: 'Data request received succesfully',
    });
}

function passwordIsCorrect(dataRequest, user) {

    if(!dataRequest.password)
    {
        if(user.salt && user.pass)
        {
            return false;
        }
        else
        {
            return true;
        }
    } 
    else
    {
        if(!user.salt && !user.pass)
        {
            return false;
        }
        else
        {
            let hashingResult = cryptoUtils.sha512(dataRequest.password, user.salt);
            return hashingResult.passwordHash === user.pass;
        }
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            password: {
                type: 'string',
                minLength: 1
            },
            requestType: {
                type: 'string',
                enum: ['delete', 'getdata']
            }
        },
        required: ['requestType'],
        additionalProperties: false
    };
};

