const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const cryptoUtils = require('../../services/common/cryptoUtils');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['email', 'password'];
const USERNAME_OR_PASSWORD_INCORRECT_MESSAGE = 'Username or password is incorrect';


exports.register = function register(app) {
    app.post('/login',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(login));
};

async function login(req, res) {
    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOneByEmail(loginRequest.email);

    if (!user || !passwordIsCorrect(loginRequest, user)) {
        errorUtils.throwError(USERNAME_OR_PASSWORD_INCORRECT_MESSAGE, 400);
    }

    if (!userCanLogin(user)) {
        errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
    }

    let token = cryptoUtils.generateJWT(user);

    if (!user.tokens) {
        user.tokens = [];
    }

    user.tokens.push(token);

    await mongooseUserRepo.update({email: loginRequest.email}, {$set: {tokens: user.tokens}});

    amplitudeTracker.publishData('login', {type: 'email'}, user._id, req.device_id);

    res.status(200).send({
        success: true,
        message: 'User logged in successfully',
        token: token
    });
}

function userCanLogin(user) {
    return user.verified && user.approved && user.salt && user.pass;
}

function passwordIsCorrect(loginRequest, user) {
    try {
        let hashingResult = cryptoUtils.sha512(loginRequest.password, user.salt);
        return hashingResult.passwordHash === user.pass;
    } catch (e) {
        return false;
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                minLength: 1
            },
            password: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['email', 'password'],
        additionalProperties: false
    };
};
