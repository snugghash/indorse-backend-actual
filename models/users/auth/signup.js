const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const mongooseClaimDraftsRepo = require("../../services/mongo/mongoose/mongooseClaimDraftsRepo");
const emailService = require("../../services/common/emailService");
const replicationUserRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const createClaimService = require('../../services/claims/createClaimService');
const settings = require('../../settings');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['password', 'name', 'username', 'email', 'claimToken'];

exports.register = function register(app) {
    app.post('/signup',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(signup));
};

async function signup(req, res) {
    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if(!newUserRequest.source) {
        newUserRequest.source = "page";
    }

    let userInMongo = await mongooseUserRepo.findOne({
        $or: [
            {email: newUserRequest.email},
            {username: newUserRequest.username}]
    });

    if (userInMongo) {
        errorUtils.throwError("User with this email or username exists", 400);
    }

    newUserRequest.terms_privacy = [];
    let terms_object = {};
    terms_object.tc_version = settings.TERMS_VERSION;
    terms_object.privacy_version = settings.PRIVACY_VERSION;
    terms_object.timestamp = Date.now();

    newUserRequest.terms_privacy.push(terms_object);

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        let newUserId = await replicationUserRepo.insert(newUserRequest);

        await mongooseUserRepo.update({_id: newUserId}, {
            tokens: [],
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let newUser = await mongooseUserRepo.findOneById(newUserId);

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, newUser);


        amplitudeTracker.publishData('sign_up', {type: 'email', flow: 'claimToken'}, newUserId, req.device_id);


        res.status(200).send({
            success: true,
            message: "Signed up successfully. Please login to access your profile."
        })
    } else {
        let newUserId = await replicationUserRepo.insert(newUserRequest);

        await emailService.sendVerificationEmail(newUserRequest.name, newUserRequest.email, newUserRequest.verify_token, newUserRequest.source);

        amplitudeTracker.publishData('sign_up', {type: 'email', flow: 'signupPage'}, newUserId, req.device_id);



        res.status(200).send({
            success: true,
            message: "Verification email sent successfully"
        })
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            },
            password: {
                type: 'string',
                minLength: 1
            },
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            claimToken: {
                type: 'string',
                minLength: 1
            },
            source: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['email', 'password', 'name', 'username'],
        additionalProperties: false
    };
}

