const safeObjects = require('../../../services/common/safeObjects')
const uportUtils = require("../../../services/social/uportUtils");
const socialLogin = require("../../../services/social/socialLoginService");
const errorUtils = require('../../../services/error/errorUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');

const REQUEST_FIELD_LIST_LOGIN = ['uport'];

exports.register = function register(app) {
    app.post('/link/uport',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(uportLink));
};

async function uportLink(req, res) {   
    let userRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN); 
    if (!req.login) {
        errorUtils.throwError("Login failed", 400);
    }      
    let [userUid, userData] = await uportUtils.validateAndGetUserData(userRequest.uport);    
    await socialLogin.socialLinkTemplate(req.email, userUid, 'uport', userData, res);  
    
}

//TODO: Update schema
function getRequestSchema() {
        return {
            type: 'object',
            properties: {
                uport: {
                    type: 'object'
                }
            },
            required: ['uport'],
            additionalProperties: false
        };
}
