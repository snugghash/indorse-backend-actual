const linkedInUtils = require("../../../services/social/linkedInService");
const socialLogin = require("../../../services/social/socialLoginService");
const errorUtils = require('../../../services/error/errorUtils');
const safeObjects = require('../../../services/common/safeObjects');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');



const REQUEST_FIELD_LIST = ['linked_in'];

exports.register = function register(app) {
    app.post('/link/linked-in',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(linkedInLink));
};


async function linkedInLink(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError("User is not logged in", 400);
    }

    let accessToken = await linkedInUtils.exchangeAuthorizationCode(loginRequest.linked_in.code, loginRequest.linked_in.redirect_uri);

    let [linkedInUid, _name, _email,_profileUrl] = await linkedInUtils.getLinkedInSignupVariables(accessToken);
    let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
    let additionalData = {
        profileUrl: _profileUrl
    }
    await socialLogin.socialLinkTemplate(req.email, linkedInUid, 'linkedIn', linkedInParams, res, additionalData);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            linked_in: {
                type: 'object',
                properties: {
                    code: {
                        type: 'string',
                        minLength: 1
                    },
                    state: {
                        type: 'string',
                        minLength: 1
                    },
                    redirect_uri: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['code', 'state', 'redirect_uri'],
                additionalProperties: false
            }
        },
        required: ['linked_in'],
        additionalProperties: true
    };
}
