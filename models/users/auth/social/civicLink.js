const safeObjects = require('../../../services/common/safeObjects')
const civicUtils = require('../../../services/social/civicUtils');
const socialLogin = require('../../../services/social/socialLoginService');
const errorUtils = require('../../../services/error/errorUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST_LOGIN = ['civic'];

exports.register = function register(app) {
    app.post('/link/civic',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(civicLink));
};

async function civicLink(req, res) {    
    let userRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);
    if (!req.login) {
        errorUtils.throwError("Login failed", 400);
    }      
    let [userEmail, userUid, userData] = await civicUtils.validateAndGetUserData(userRequest.civic.id_token);
    await socialLogin.socialLinkTemplate(req.email, userUid, 'civic', userData, res);  
}

function getRequestSchema() {
        return {
            type: 'object',
            properties: {
                civic: {
                    type: 'object',
                    properties: {
                        id_token: {
                            type: 'string',
                            minLength: 1
                        }
                    },
                    required: ['id_token'],
                    additionalProperties: false
                }
            },
            required: ['civic'],
            additionalProperties: false
        };
}
