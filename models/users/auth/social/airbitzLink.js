const safeObjects = require('../../../services/common/safeObjects')
const abUtils = require("../../../services/social/airbitzUtils");
const socialLogin = require("../../../services/social/socialLoginService");
const errorUtils = require('../../../services/error/errorUtils');
const Web3 = new require('web3');
const web3 = new Web3();
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const mongooseUserRepo = require("../../../services/mongo/mongoose/mongooseUserRepo");

const REQUESTED_FIELD_LOGIN = ['ethaddress', 'signature'];

exports.register = function register(app) {
    app.post('/link/airbitz',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(airbitzLink));
};

async function airbitzLink(req, res) {
    let userRequest = safeObjects.safeReqestBodyParser(req, REQUESTED_FIELD_LOGIN);

    if (!req.login) {
        errorUtils.throwError("Login failed", 400);
    }

    if (!abUtils.validateSignature(userRequest.signature, userRequest.ethaddress)) {
        errorUtils.throwError('Login failed', 400);
    }

    if (!web3.isAddress(userRequest.ethaddress)) {
        errorUtils.throwError('Invalid Ethereum address format!', 422);
    }

    let user = await mongooseUserRepo.findOneByEmail(req.email);
    if(user.ethaddress){
        errorUtils.throwError('Sorry, we are currently not supporting Airbitz for users with an existing Ethereum address.',400);

    }

    await socialLogin.socialLinkTemplate(req.email, userRequest.ethaddress,'airbitz','',res);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            ethaddress: {
                type: 'string',
                pattern: "^(0x)?[0-9aA-fF]{40}$"
            },
            signature: {
                type: 'object',
                properties: {
                    buf_r: {
                        type: 'string',
                        minLength: 1
                    },
                    buf_s: {
                        type: 'string',
                        minLength: 1
                    },
                    buf_v: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['buf_r', 'buf_s', 'buf_v'],
                additionalProperties: false
            }
        },
        required: ['signature', 'ethaddress'],
        additionalProperties: false

    };
}
