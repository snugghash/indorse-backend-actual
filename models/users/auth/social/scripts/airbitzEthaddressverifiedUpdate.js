const mongoUserRepo = require('../../../../services/mongo/mongoRepository')('users');
const scriptLogger = require('../../../../services/common/logger').getScriptLogger();
const userReplicationRepo = require('../../../../services/mongo/userRepo')

async function waitHack(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Goes through all Airbitz users and adds ethaddressverified=true to their document if not already there
 * @returns {Promise.<void>}
 */
exports.updateAllAirbitz = async function updateAllAirbitz() {
    let usersProcessed = 0, usersUpdated = 0;

    await waitHack(1000) // This is needed as async await behaves a bit funny when you call a function at root level

    let airbitzUserQuery = {"is_airbitz_user": true};
    let airbitzUserCount = await mongoUserRepo.countWithQuery(airbitzUserQuery)

    console.log('Adding ethaddressverified=true for all users that signed up with airbitz')
    scriptLogger.debug()
    scriptLogger.debug('Airbitz user updates for ethaddressverified script started...')

    let hasFinished = (airbitzUserCount === 0 ? true : false)

    let mongocursor = await mongoUserRepo.findAllWithCursor(airbitzUserQuery)

    await mongocursor.forEach(function(user) {
        usersProcessed++;

        if (!user.ethaddressverified || user.ethaddressverified !== true) {
            usersUpdated++

            user.ethaddressverified = true
            userReplicationRepo.update({_id: user._id}, user)
        }

        console.log(usersProcessed, '/', airbitzUserCount, ' airbitz users checked with ', usersUpdated, ' users updated')
        scriptLogger.debug(usersProcessed, '/', airbitzUserCount, ' airbitz users checked with ', usersUpdated,
            ' users updated. Last username processed: ', user.username)

        if(usersProcessed === airbitzUserCount) {
            hasFinished = true
        }
    });

    while(!hasFinished) {
        await waitHack(500)
    }

    return {
        "airbitzUserCount": airbitzUserCount,
        "usersProcessed": usersProcessed,
        "usersUpdated": usersUpdated,
    }

}