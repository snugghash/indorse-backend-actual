const safeObjects = require('../../../services/common/safeObjects');
const fbUtils = require("../../../services/social/facebookService");
const completeSignup = require("../../../services/social/socialSignupService")
const cryptoUtils = require('../../../services/common/cryptoUtils');
const mongoUserRepo = require("../../../services/mongo/mongoRepository")('users');
const imageService = require("../../../services/blob/imageService");
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const fbConfidenceScore = require('./confidenceScore/facebookConfidenceScore')
const confidenceScore = require('./confidenceScore/confidenceScore')

const REQUEST_FIELD_LIST_SIGNUP = ['name', 'username', 'facebook'];

exports.register = function register(app) {
    app.post('/signup/facebook',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(facebookSignup));
};


async function facebookSignup(req, res) {
    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_SIGNUP);
    let facebookAccessToken = newUserRequest.facebook.accessToken;
    let [email, profileUrl] = await fbUtils.validateAndGetEmail(facebookAccessToken);

    newUserRequest.facebook_uid = newUserRequest.facebook.userID;
    newUserRequest.email = email;
    delete newUserRequest.facebook;

    let facebookParams = await fbUtils.getFacebookSocialParams(facebookAccessToken);
    let facebookScore = await fbConfidenceScore.calculateFacebookScore(facebookParams, newUserRequest.email);

    newUserRequest.confidenceScore  = {};
    newUserRequest.confidenceScore.facebookScore  = facebookScore;
    let aggregateScore = confidenceScore.aggregateConfidenceScore(newUserRequest);
    newUserRequest.confidenceScore.aggregateScore = aggregateScore;
    newUserRequest.facebookParams = facebookParams;    
    await completeSignup.completeSocialSignup(req, newUserRequest, 'facebook');

    let userToLogin = await mongoUserRepo.findOne({email: newUserRequest.email});

    if (profileUrl !== false) {

        let imageBuffer = await completeSignup.getProfileBufferFromUrl(profileUrl);

        if (imageBuffer) {
            if (imageService.uploadsAreEnabled()) {
                let imageUpdateResult = await imageService.uploadImageBuffer("", imageBuffer, userToLogin._id.toString());
                let imageUpdate = {};
                imageUpdate.img_url = imageUpdateResult.s3;
                imageUpdate.photo_ipfs = imageUpdateResult.ipfs;
                await mongoUserRepo.update({email: newUserRequest.email}, {$set: imageUpdate});
            }
        }
    }




    let userItem = Object.assign({}, userToLogin);
    let token = cryptoUtils.generateJWT(userItem);
    userToLogin.tokens.push(token);

    await mongoUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});
    await newUserProcedures.startLinkingProcedures(newUserRequest);
    res.status(200).send({
        success: true,
        message: "User successfully logged in",
        token: token
    })


}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            facebook: {
                type: 'object',
                properties: {
                    accessToken: {
                        type: 'string',
                        minLength: 1
                    },
                    userID: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['accessToken', 'userID'],
                additionalProperties: false
            }
        },
        required: ['facebook', 'username', 'name'],
        additionalProperties: false
    };
};
