const safeObjects = require('../../../services/common/safeObjects');
const fbUtils = require("../../../services/social/facebookService");
const socialLogin = require("../../../services/social/socialLoginService");
const errorUtils = require('../../../services/error/errorUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');


const REQUEST_FIELD_LIST_LOGIN = ['facebook'];

exports.register = function register(app) {
    app.post('/link/facebook',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(facebookLink));
};

/**
* @param req
* @param res
* @returns {Promise.<void>}
*/
async function facebookLink(req, res) {
    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);

    if(!req.login){
        errorUtils.throwError("User is not logged in", 400);
    }

    await fbUtils.validateAccessTokenAuth(loginRequest.facebook.accessToken);
    let facebookParams = await fbUtils.getFacebookSocialParams(loginRequest.facebook.accessToken);

    await socialLogin.socialLinkTemplate(req.email,loginRequest.facebook.userID,'facebook',facebookParams,res);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            facebook: {
                type: 'object',
                properties: {
                    accessToken: {
                        type: 'string',
                        minLength: 1
                    },
                    userID: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['accessToken', 'userID'],
                additionalProperties: false
            }
        },
        required: ['facebook'],
        additionalProperties: false
    };
};
