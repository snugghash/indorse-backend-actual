const mongooseUserRepo = require("../../../services/mongo/mongoose/mongooseUserRepo");
const emailService = require("../../../services/common/emailService");
const replicationUserRepo = require('../../../services/mongo/userRepo');
const safeObjects = require('../../../services/common/safeObjects');
const abUtils = require("../../../services/social/airbitzUtils");
const errorUtils = require('../../../services/error/errorUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const createClaimService = require('../../../services/claims/createClaimService');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');

const REQUESTED_FIELD_SIGNUP = ['name', 'username', 'email', 'ethaddress', 'signature', 'claimToken'];

exports.register = function register(app) {
    app.post('/signup/airbitz',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(airbitzSignup));
};

async function airbitzSignup(req, res) {

    let newUserRequest = safeObjects.safeReqestBodyParser(req, REQUESTED_FIELD_SIGNUP);

    if (!abUtils.validateSignature(newUserRequest.signature, newUserRequest.ethaddress)) {
        errorUtils.throwError('Login failed', 400);
    }

    let userInMongo = await mongooseUserRepo.findOne({
        $or: [
            {email: newUserRequest.email},
            {username: newUserRequest.username},
            {ethaddress: newUserRequest.ethaddress}]
    }); // It is expensive operation. But we should check all email , username, and ethaddress is unique

    if (userInMongo) {
        errorUtils.throwError("User with this email or username exists", 400);
    }

    delete newUserRequest.signature;

    newUserRequest.is_airbitz_user = true;
    newUserRequest.ethaddressverified = true;

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        let newUserId = await replicationUserRepo.insertSocial(newUserRequest, 'airbitz');

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        await mongooseUserRepo.update({_id: newUserId}, {
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let newUser = await mongooseUserRepo.findOneById(newUserId);

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, newUser);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);


        amplitudeTracker.publishData('sign_up', {type: 'airbitz', flow: 'claimToken'}, newUserId, req.device_id);

        res.status(200).send({
            success: true,
            message: "Successful signup and claim creation!"
        })
    } else {
        let newUserId = await replicationUserRepo.insertSocial(newUserRequest, 'airbitz');
        await emailService.sendVerificationEmail(newUserRequest.name, newUserRequest.email, newUserRequest.verify_token);

        amplitudeTracker.publishData('sign_up', {type: 'airbitz', flow: 'signupPage'}, newUserId, req.device_id);

        await newUserProcedures.startLinkingProcedures(newUserRequest);
        req.user_id = newUserId;

        res.status(200).send({
            success: true,
            message: "Verification email sent successfully"
        })
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            username: {
                type: 'string',
                minLength: 1
            },
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            },
            ethaddress: {
                type: 'string',
                pattern: "^(0x)?[0-9aA-fF]{40}$"
            },
            claimToken: {
                type: 'string'
            },
            signature: {
                type: 'object',
                properties: {
                    buf_r: {
                        type: 'string',
                        minLength: 1
                    },
                    buf_s: {
                        type: 'string',
                        minLength: 1
                    },
                    buf_v: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['buf_r', 'buf_s', 'buf_v'],
                additionalProperties: false
            }
        },
        required: ['signature', 'ethaddress', 'email', 'username', 'name'],
        additionalProperties: false

    };
}

