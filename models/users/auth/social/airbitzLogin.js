const mongooseUserRepo = require("../../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../../services/common/safeObjects');
const abUtils = require("../../../services/social/airbitzUtils");
const errorUtils = require('../../../services/error/errorUtils');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const Web3 = new require('web3');
const web3 = new Web3();
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUESTED_FIELD_LOGIN = ['ethaddress', 'signature'];

exports.register = function register(app) {
    app.post('/auth/airbitz',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(airbitzLogin));
};

async function airbitzLogin(req, res) {
    let userRequest = safeObjects.safeReqestBodyParser(req, REQUESTED_FIELD_LOGIN);

    if (!abUtils.validateSignature(userRequest.signature, userRequest.ethaddress)) {
        errorUtils.throwError('Login failed', 400);
    }

    if (!web3.isAddress(userRequest.ethaddress)) {
        errorUtils.throwError('Invalid Ethereum address format!', 422);
    }

    let user = await mongooseUserRepo.findOne({ethaddress: userRequest.ethaddress});

    if (!user) {
        errorUtils.throwError("Login failed", 404);
    }

    if (!userIsApproved(user)) {
        console.log(user._id);
        errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 403);
    }

    let token = cryptoUtils.generateJWT(user);
    user.tokens.push(token);

    await mongooseUserRepo.update({ethaddress: userRequest.ethaddress}, {$set: {tokens: user.tokens}});

    amplitudeTracker.publishData('login', {type: 'airbitz'}, user._id, req.device_id);

    res.status(200).send({
        success: true,
        message: 'User logged in successfully',
        token: token
    });
}

function userIsApproved(user) {
    return user.approved && user.verified;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            ethaddress: {
                type: 'string',
                pattern: "^(0x)?[0-9aA-fF]{40}$"
            },
            signature: {
                type: 'object',
                properties: {
                    buf_r: {
                        type: 'string',
                        minLength: 1
                    },
                    buf_s: {
                        type: 'string',
                        minLength: 1
                    },
                    buf_v: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['buf_r', 'buf_s', 'buf_v'],
                additionalProperties: false
            }
        },
        required: ['signature', 'ethaddress'],
        additionalProperties: false

    };
}