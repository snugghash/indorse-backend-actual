const mongooseUserRepo = require("../../../../services/mongo/mongoose/mongooseUserRepo");
const replicationUserRepo = require("../../../../services/mongo/userRepo");
const confidenceScore = require("./confidenceScore")
const logger = require("../../../../services/common/logger").getLogger();



var _ = require('lodash');

let version = "0.1";

let linkedInParams = [{ name: 'num-connections', threshold: 100, weight: 1 },
    { name: 'num-connections-capped', weight: 1 },
    { name: 'summary', weight: 1 },
    { name: 'picture-url', weight: 1 }
];

exports.updateLinkedInScore = async function updateLinkedInScore(inputLinkedInParams, inputEmail) {
    let score = await this.calculateLinkedInScore(inputLinkedInParams, inputEmail);
    let user = await mongooseUserRepo.findOne({email: inputEmail});

    if (user.confidenceScore) {
        user.confidenceScore.linkedInScore = score;
    } else {
        user.confidenceScore = { "linkedInScore": score };
    }
    let aggregateScore = confidenceScore.aggregateConfidenceScore(user);
    await replicationUserRepo.update({ email: inputEmail }, 
                                    { '$set': { 'confidenceScore.linkedInScore': score, 
                                                'confidenceScore.aggregateScore': aggregateScore,
                                                'linkedInParams': inputLinkedInParams }});
}

exports.calculateLinkedInScore = async function calculateLinkedInScore(params,email) {
    logger.debug("Calculating LinkedIn Score");
    let linkedInScore = Object({ isVerified: false, score: 0, version : version});
    try{
        if (params === undefined || params.length === 0) {
            return Object({ isVerified: false, score: 0 });
        }

        let totalScore = 0;
        for (let field of params) {
            let linkedInParamExtracted = _.find(linkedInParams, { name: field.name });
            if (!linkedInParamExtracted)
                continue;

            switch (field.name) {
                case "num-connections":
                    if (field.value >= linkedInParamExtracted.threshold) {
                        totalScore += linkedInParamExtracted.weight;
                        logger.debug('num-connections, adding weight :' + linkedInParamExtracted.weight);
                    }
                    break;

                case "num-connections-capped":
                    if (field.value === true) {
                        totalScore += linkedInParamExtracted.weight;
                        logger.debug('num-connections-capped, adding weight :' + linkedInParamExtracted.weight);
                    }
                    break;

                case "summary":
                    if (field.value) {
                        totalScore += linkedInParamExtracted.weight;
                        logger.debug('summary, adding weight :' + linkedInParamExtracted.weight);
                    }
                    break;

                case "picture-url":
                    if (field.value) {
                        totalScore += linkedInParamExtracted.weight;
                        logger.debug('picture-url, adding weight :' + linkedInParamExtracted.weight);
                    }
                    break;
            }
        }
        linkedInScore.score = confidenceScore.calculateAverage(totalScore, linkedInParams);
        if (Number(linkedInScore.score) >= Number(5)){
            linkedInScore.isVerified = true;
        }

    }catch(error){
        logger.debug("Unable to create linkedIn confidence score for user email :" + email + " Params: " + JSON.stringify(params));
        logger.debug(error);
    }
    return linkedInScore;
}
