const mongooseUserRepo = require("../../../../services/mongo/mongoose/mongooseUserRepo");
const replicationUserRepo = require("../../../../services/mongo/userRepo");
const confidenceScore = require("./confidenceScore")
const logger = require("../../../../services/common/logger").getLogger();
var _ = require('lodash');


let version = "0.1";

//TODO async
exports.calculateUportScore = function calculateUportScore(params, email) {    
    return 10;
}

exports.updateUportScore = async function updateUportScore(inputUportParams, inputEmail) {
    let score = this.calculateUportScore(inputUportParams, inputEmail);
    let user = await mongooseUserRepo.findOneByEmail(inputEmail);

    if (user.confidenceScore) {
        user.confidenceScore.civicScore = score;
    } else {
        user.confidenceScore = { "uportScore": score };
    }
    let aggregateScore = confidenceScore.aggregateConfidenceScore(user);
    await replicationUserRepo.update({ 'email': inputEmail },
        {
            '$set': {
                'confidenceScore.uportScore': score,
                'confidenceScore.aggregateScore': aggregateScore,
                'uportParams': inputUportParams
            }
        });
    return;
}