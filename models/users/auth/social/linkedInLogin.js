const safeObjects = require('../../../services/common/safeObjects');
const linkedInUtils = require("../../../services/social/linkedInService");
const socialLogin = require("../../../services/social/socialLoginService");
const socialSignup = require("../../../services/social/socialSignupService");
const errorUtils = require('../../../services/error/errorUtils');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const linkedInConfidenceScore = require('./confidenceScore/linkedInConfidenceScore');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const confidenceScore = require('./confidenceScore/confidenceScore')
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo')

const REQUEST_FIELD_LIST = ['username', 'linked_in'];

exports.register = function register(app) {
    app.post('/auth/linked-in',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(linkedInLogin));
};

async function linkedInLogin(req, res) {

    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let accessToken = await linkedInUtils.exchangeAuthorizationCode(loginRequest.linked_in.code , loginRequest.linked_in.redirect_uri);
    let [linkedInUid, name, email, _profileUrl] = await linkedInUtils.getLinkedInSignupVariables(accessToken);

    let authType = 'linkedIn'
    let user = await socialLogin.findUserByUid(linkedInUid, authType);

    if(user){
        //req.user_id = user._id;
        if (!socialLogin.userCanLogin(user) || (loginRequest.username)) {
            errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
        }
        let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
        await linkedInConfidenceScore.updateLinkedInScore(linkedInParams, user.email);
        await mongooseUserRepo.update({ email: user.email }, { $set: { linkedInProfileURL: _profileUrl } });
        let token = await socialLogin.setToken(user,user.email);
        amplitudeTracker.publishData('login', {type: authType}, user._id);

        res.status(200).send({
            success: true,
            message: 'User logged in successfully',
            token: token
        });
    }
    else{
        if(!email){
            errorUtils.throwError("Unable to get an email from LinkedIn", 400);
        }
        user = await socialLogin.findUserByEmail(email);
        // link
        if(user){
            //req.user_id = user._id;
            await socialLogin.linkUserByEmail(user, linkedInUid, email, authType);            
            if (!socialLogin.userCanLogin(user)) {
                errorUtils.throwError("Sorry, this user account is either not verified or not approved for access", 400);
            }
            let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
            await linkedInConfidenceScore.updateLinkedInScore(linkedInParams, user.email);
            let token = await socialLogin.setToken(user,user.email);
            amplitudeTracker.publishData('social_link', {type: authType}, user._id);
            await mongooseUserRepo.update({ email: user.email }, { $set: { linkedInProfileURL: _profileUrl } });
            res.status(200).send({
                success: true,
                message: 'User account is linked to '+ authType +' successfully',
                token: token
            });
        } // create a JWT for signup : 2 cases -> 1. with username 2. without username
        else{
            if(loginRequest.username){
                let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);
                let linkedInScore = await linkedInConfidenceScore.calculateLinkedInScore(linkedInParams, email);
                let signupRequest = {
                    linkedIn_uid : linkedInUid,
                    email : email,
                    name : name,
                    username: loginRequest.username,
                    confidenceScore: {linkedInScore : linkedInScore},
                    linkedInProfileURL: _profileUrl
                };
                let aggregateScore = confidenceScore.aggregateConfidenceScore(signupRequest);
                signupRequest.confidenceScore.aggregateScore = aggregateScore;
                signupRequest.linkedInParams = linkedInParams;

                let token = await socialSignup.completeSocialSignup(req, signupRequest, 'linkedIn');
                res.status(200).send({
                    success: true,
                    message: "User successfully logged in",
                    token: token
                });
            }else{
                let linkedInParams = await linkedInUtils.getLinkedInSocialParams(accessToken);

                let indAccessToken = await cryptoUtils.generateJWTTmpUser(linkedInUid,email,linkedInParams);
                res.status(404).send({
                    success: true,
                    message: "Redirecting to signup page",
                    name: name,
                    token: indAccessToken
                });
            }
        }
    }
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            username: {
                type: 'string',
                minLength: 1
            },
            linked_in: {
                type: 'object',
                properties: {
                    code: {
                        type: 'string',
                        minLength: 1
                    },
                    state: {
                        type: 'string',
                        minLength: 1
                    },
                    redirect_uri: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['code', 'state', 'redirect_uri'],
                additionalProperties: false
            }
        },
        required: ['linked_in'],
        additionalProperties: true
    };
}
