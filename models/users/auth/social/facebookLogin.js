const safeObjects = require('../../../services/common/safeObjects');
const fbUtils = require("../../../services/social/facebookService");
const socialLogin = require("../../../services/social/socialLoginService")
const fbConfidenceScore = require('./confidenceScore/facebookConfidenceScore')
const logger = require("../../../services/common/logger").getLogger();
const validate = require('../../../services/common/validate');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST_LOGIN = ['facebook'];

const routeUtils = require('../../../services/common/routeUtils');

exports.register = function register(app) {
    app.post('/auth/facebook',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(facebookLogin));
};

/**
 * @param req
 * @param res
 * @returns {Promise.<void>}
 */
async function facebookLogin(req, res) {
    let authType = 'facebook'
    let loginRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST_LOGIN);

    let [email,_profile] = await fbUtils.validateAndGetEmail(loginRequest.facebook.accessToken);

    let user = await socialLogin.findUserByUid(loginRequest.facebook.userID, authType);

    if (!user) {
        user = await socialLogin.findUserByEmail(email);
        await socialLogin.linkUserByEmail(user, loginRequest.facebook.userID, email, authType);
    }
    let facebookParams = await fbUtils.getFacebookSocialParams(loginRequest.facebook.accessToken);
    await fbConfidenceScore.updateFacebookScore(facebookParams, user.email);
    logger.debug("Calculating Facebook Score" + user.email + "  " + JSON.stringify(facebookParams));
    req.user_id = user._id;
    await socialLogin.socialLoginTemplate(user, res, authType);
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            facebook: {
                type: 'object',
                properties: {
                    accessToken: {
                        type: 'string',
                        minLength: 1
                    },
                    userID: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['accessToken', 'userID'],
                additionalProperties: false
            }
        },
        required: ['facebook'],
        additionalProperties: false
    };
};
