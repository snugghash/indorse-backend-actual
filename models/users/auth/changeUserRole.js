const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const ObjectID = require('mongodb').ObjectID;
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['role', 'user_id'];

exports.register = function register(app) {
    app.post('/changeuserrole',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(changeUserRole));
};

/**
 * @swagger
 * definitions:
 *   ChangeUserRoleRequest:
 *     type: object
 *     properties:
 *       role:
 *         type: string
 *       user_id:
 *         type: string
 */

/**
 * @swagger
 * /changeuserrole:
 *   post:
 *     description: Changes user role.
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/ChangeUserRoleRequest'
 *     responses:
 *       200:
 *         description: Role change successful
 *         schema:
 *           $ref: '#/definitions/Response'
 *       403:
 *         description: On insufficient permissions
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function changeUserRole(req, res) {
    let changeUserRoleRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to change role', 403);
    }

    await mongooseUserRepo.update({_id: new ObjectID(changeUserRoleRequest.user_id)},
        {$set: {role: changeUserRoleRequest.role}});

    let userProfile = await mongooseUserRepo.findOne({_id: new ObjectID(changeUserRoleRequest.user_id)});

    res.status(200).send({
        success: true,
        profile: safeObjects.safeUserObject(userProfile),
        message: 'Profile was updated successfully'
    });
}

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.write;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            role: {
                type: 'string',
                enum: ['admin', 'full_access', 'no_access', 'profile_access']
            },
            user_id: {
                type: 'string'
            }
        },
        required: ['role', 'user_id'],
        additionalProperties: false
    };
}