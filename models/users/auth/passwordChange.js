const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const cryptoUtils = require('../../services/common/cryptoUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['old_password', 'new_password'];

exports.register = function register(app) {
    app.post('/password/change',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(passwordChange));
};


/**
 * @swagger
 * definitions:
 *   PasswordChangeRequest:
 *     type: object
 *     properties:
 *       old_password:
 *         type: string
 *         required: true
 *       new_password:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /password/change:
 *   post:
 *     description: Changes user password
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PasswordChangeRequest'
 *     responses:
 *       200:
 *         description: On successful password update
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Response'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       400:
 *         description: When user is not found
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       404:
 *         description: On failed request
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function passwordChange(req, res) {
    let passwordChangeRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    let user = await mongooseUserRepo.findOneByEmail(req.email);

    if (!user) {
        errorUtils.throwError('User not found', 404);
    }

    if (!user.verified) {
        errorUtils.throwError('User not verified', 403);
    }

    if (!oldPasswordIsCorrect(passwordChangeRequest.old_password, user)) {
        errorUtils.throwError('Invalid credentials', 400);
    }

    let passwordData = cryptoUtils.saltHashPassword(passwordChangeRequest.new_password);

    await mongooseUserRepo.update({email: req.email}, {
        $set: {pass: passwordData.passwordHash, salt: passwordData.salt}
    });

    res.status(200).send({
        success: true,
        message: 'Password updated successfully'
    });
}

function oldPasswordIsCorrect(oldPassword, user) {
    if (!user.pass || !user.salt) return true;
    let hashingResult = cryptoUtils.sha512(oldPassword, user.salt);
    return hashingResult.passwordHash === user.pass || !user.pass;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            old_password: {
                type: 'string',
                minLength: 1
            },
            new_password: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['old_password', 'new_password'],
        additionalProperties: false
    };
}
