const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const emailService = require("../../services/common/emailService");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const newUserProcedures = require('../../services/auth/newUserProcedure');

const REQUEST_FIELD_LIST = ['email', 'verify_token'];

exports.register = function register(app) {

    app.post('/verify-email',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(verify));
};

async function verify(req, res) {
    let verifyRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOne({
        email: verifyRequest.email,
        verify_token: verifyRequest.verify_token
    });

    if (!user) {
        errorUtils.throwError("User with email and token not found!", 400);
    }
    req.user_id = user._id;

    await mongooseUserRepo.update({'email': verifyRequest.email}, {
        $set: {
            tokens: [],
            verified: true,
            role: 'profile_access',
            approved: true
        },
        $unset: {
            verify_token: ""
        }
    });

    await emailService.sendWelcomeEmail(user.name, user.email, user.username);

    await newUserProcedures.startLinkingProcedures(user);

    res.status(200).send({
        success: true,
        message: "User verified successfully"
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                minLength: 1
            },
            verify_token: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['email', 'verify_token'],
        additionalProperties: false
    };
}
