const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const settings = require('../../settings');

exports.register = function register(app) {
    app.post('/acceptterms', routeUtils.asyncMiddleware(logout));
};

async function logout(req, res) {

    if (!req.login) {
        errorUtils.throwError('User is not logged in', 403);
    }

    let terms_object = {};
    terms_object.tc_version = settings.TERMS_VERSION;
    terms_object.privacy_version = settings.PRIVACY_VERSION;
    terms_object.timestamp = Date.now();

    await mongooseUserRepo.update({email: req.email}, {$push: {terms_privacy: terms_object}});

    res.status(200).send({
        success: true,
        message: 'Terms accepted succesfully',
    });
}

