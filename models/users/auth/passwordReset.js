const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const cryptoUtils = require('../../services/common/cryptoUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['email', 'pass_token', 'password'];

exports.register = function register(app) {
    app.post('/password/reset',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(passwordReset));
};

/**
 * @swagger
 * definitions:
 *   PasswordResetRequest:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *         required: true
 *       pass_token:
 *         type: string
 *         required: true
 *       password:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /password/reset:
 *   post:
 *     description: Resets user password
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PasswordResetRequest'
 *     responses:
 *       200:
 *         description: On succesful password update
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Response'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       400:
 *         description: On failed request
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function passwordReset(req, res) {
    let passwordResetRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOne({
        email: passwordResetRequest.email,
        pass_verify_token: passwordResetRequest.pass_token
    });

    if (!user) {
        errorUtils.throwError('Invalid email or token', 400);
    }

    let currentTime = Math.floor(Date.now() / 1000);

    if (!user.pass_verify_timestamp || !user.pass_verify_token || !(currentTime - user.pass_verify_timestamp <= 86400)) {
        errorUtils.throwError('Sorry, there was a problem with the verification. The token authorization failed for ' +
            'this account. Please go to the forgotten password submission and request a new verification link', 400);
    }

    let passwordData = cryptoUtils.saltHashPassword(passwordResetRequest.password);

    await mongooseUserRepo.update({email: passwordResetRequest.email}, {
        $set: {pass: passwordData.passwordHash, salt: passwordData.salt},
        $unset: {pass_verify_token: "", pass_verify_timestamp: ""}
    });

    res.status(200).send({
        success: true,
        message: 'Password updated successfully',
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            },
            pass_token: {
                type: 'string',
                minLength: 1
            },
            password: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['email', 'pass_token', 'password'],
        additionalProperties: false
    };
}
