const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const emailService = require('../../services/common/emailService');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['email'];

exports.register = function register(app) {
    app.post('/resendverification',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(resendVerification));
};

/**
 * @swagger
 * definitions:
 *   ResendVerificationRequest:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /logout:
 *   post:
 *     description: Sends the verification email again
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/ResendVerificationRequest'
 *     responses:
 *       200:
 *         description: Reset email successfully sent
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Response'
 *       404:
 *         description: On invalid request
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function resendVerification(req, res) {
    let resendVerificationRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOneByEmail(resendVerificationRequest.email);

    if (!user || user.verified || !user.verify_token) {
        errorUtils.throwError('Invalid request', 400);
    }

    await emailService.sendVerificationEmail(user.name, user.email, user.verify_token);

    res.status(200).send({
        success: true,
        message: 'Verification email sent successfully'
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            }
        },
        required: ['email'],
        additionalProperties: false
    };
}
