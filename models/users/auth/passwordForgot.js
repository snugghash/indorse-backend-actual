const mongooseUserRepo = require("../../services/mongo/mongoose/mongooseUserRepo");
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const randtoken = require('rand-token');
const emailService = require('../../services/common/emailService');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');

const REQUEST_FIELD_LIST = ['email'];

exports.register = function register(app) {
    app.post('/password/forgot',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(passwordForgot));
};

/**
 * @swagger
 * definitions:
 *   PasswordForgotRequest:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *         required: true
 */

/**
 * @swagger
 * /password/forgot:
 *   post:
 *     description: Logs out the user
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/PasswordForgotRequest'
 *     responses:
 *       200:
 *         description: Reset email successfully sent
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Response'
 *       404:
 *         description: When user is not verified
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function passwordForgot(req, res) {
    let passwordForgotRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user = await mongooseUserRepo.findOneByEmail(passwordForgotRequest.email);

    if (!user) {
        errorUtils.throwError('User does not exist', 404);
    }

    if (!user.verified) {
        errorUtils.throwError('User is not verified', 400);
    }

    let pass_verify_token = randtoken.generate(16);
    let pass_verify_timestamp = Math.floor(Date.now() / 1000);

    await mongooseUserRepo.update({email: passwordForgotRequest.email},
        {$set: {pass_verify_token: pass_verify_token, pass_verify_timestamp: pass_verify_timestamp}});

    await emailService.sendPasswordResetEmail(user.name, user.email, pass_verify_token);

    res.status(200).send({
        success: true,
        message: 'Forgot password email sent successfully',
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            }
        },
        required: ['email'],
        additionalProperties: false
    };
}