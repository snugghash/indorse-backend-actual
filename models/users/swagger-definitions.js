/**
 * @swagger
 * definitions:
 *   School:
 *     type: object
 *     properties:
 *       school_name:
 *         type: string
 *       _id:
 *         type: string
 *   Company:
 *     type: object
 *     properties:
 *       company_name:
 *         type: string
 *       _id:
 *         type: string
 *   Language:
 *     type: object
 *     properties:
 *       language:
 *         type: string
 *   WorkItem:
 *     type: object
 *     properties:
 *       company_name:
 *         type: string
 *       title:
 *         type: string
 *       location:
 *         type: string
 *       item_key:
 *         type: string
 *       company_id:
 *         type: string
 *         optional: true
 *       start_date:
 *         $ref: '#/definitions/Date'
 *       end_date:
 *         $ref: '#/definitions/Date'
 *       currently_working:
 *         type: boolean
 *       activities:
 *         type: string
 *       description:
 *         type: string
 *   EducationItem:
 *     type: object
 *     properties:
 *       school_name:
 *         type: string
 *       item_key:
 *         type: string
 *       school_id:
 *         type: string
 *         optional: true
 *       degree:
 *         type: string
 *       field_of_study:
 *         type: string
 *       grade:
 *         type: string
 *       start_date:
 *         $ref: '#/definitions/Date'
 *       end_date:
 *         $ref: '#/definitions/Date'
 *       activities:
 *         type: string
 *       description:
 *         type: string
 *   LanguageItem:
 *     type: object
 *     properties:
 *       language:
 *         type: string
 *       item_key:
 *         type: string
 *         optional: true
 *       language_id:
 *         type: string
 *         optional: true
 *       proficiency:
 *         type: string
 *   Profile:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *       _id:
 *         type: string
 *       approved:
 *         type: boolean
 *       verified:
 *         type: boolean
 *       pass_verify_timestamp:
 *         type: number
 *       username:
 *         type: string
 *       is_pass:
 *         type: boolean
 *       bio:
 *         type: string
 *       ethaddress:
 *         type: string
 *       name:
 *         type: string
 *       img_url:
 *         type: string
 *       photo_ipfs:
 *         type: string
 *       last_linkedin_pdf_import_at:
 *         type: string
 *       signature:
 *         type: string
 *       is_airbitz_user:
 *         type: boolean
 *       work:
 *         type: array
 *         items:
 *           $ref: '#/definitions/WorkItem'
 *       education:
 *         type: array
 *         items:
 *           $ref: '#/definitions/EducationItem'
 *   Date:
 *     type: object
 *     properties:
 *       year:
 *         type: number
 *       month:
 *         type: number
 *   BigchainProfile:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *       ethaddress:
 *         type: string
 *       work:
 *         type: array
 *         items:
 *           $ref: '#/definitions/WorkItem'
 *       education:
 *         type: array
 *         items:
 *           $ref: '#/definitions/EducationItem'
 *   Skill:
 *     type: object
 *     properties:
 *       skill_name: string
 *   Response:
 *     type: object
 *     properties:
 *       message:
 *         type: string
 *       success:
 *         type: boolean
 *   ErrorResponse:
 *     type: object
 *     properties:
 *       message:
 *         type: string
 *       success:
 *         type: boolean
 *       requestID:
 *         type: string
 *   ValidationErrorResponse:
 *     type: object
 *     properties:
 *       message:
 *         type: string
 *       success:
 *         type: boolean
 *       requestID:
 *         type: string
 *       validations:
 *         type: object
 *   SkillQueryResponse:
 *     type: object
 *     properties:
 *       skills:
 *         type: array
 *         items: string
 */

