const mongoSkillsRepo = require("../../services/mongo/mongoRepository")('skills');
const errorUtils = require('../../services/error/errorUtils');

exports.checkThatSkillsInMongo = async function checkThatSkillsInMongo(skills) {
    if (!skills || !skills.length) return

    lowerCaseSkills = [];

    for (let i = 0; i < skills.length; i++) {
        let skill = skills[i].toLowerCase()
        let foundSkill = await mongoSkillsRepo.findOne({name: skill});
        if (!foundSkill) errorUtils.throwError('Skill is not in skills collection', 403);

        lowerCaseSkills.push(skill)
    }
    return lowerCaseSkills
}