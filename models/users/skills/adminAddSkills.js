const mongooseSkillRepo = require('../../services/mongo/mongoRepository')('skills');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');

const REQUEST_FIELD_LIST = ['name', 'category'];

exports.register = function register(app) {
    app.post('/skills',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(addSkill));
};

async function addSkill(req, res) {
    let addSkillsRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let skillInMongo = await mongooseSkillRepo.findOne({name: addSkillsRequest.name});

    if (skillInMongo) {
        errorUtils.throwError('Skill already exists in collection', 400);
    }

    await mongooseSkillRepo.insert(addSkillsRequest);

    res.status(200).send()
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            category: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['name','category'],
        additionalProperties: false
    };
}
