const mongooseSkillRepo = require('../../services/mongo/mongoose/mongooseSkillRepo')
const routeUtils = require('../../services/common/routeUtils');

exports.register = function register(app) {
    app.get('/skills/categories',
        routeUtils.asyncMiddleware(getCategories));
};

async function getCategories(req, res) {
    let categories = await mongooseSkillRepo.findDistinctCategories();

    res.status(200).send(categories)
}
