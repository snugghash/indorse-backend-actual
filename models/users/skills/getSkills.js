const mongoSkillsRepo = require('../../services/mongo/mongoRepository')('skills');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');
const errorUtils = require('../../services/error/errorUtils');
const stringUtils = require('../../services/blob/stringUtils');

const REQUEST_FIELD_LIST = ['search', 'sort'];
const REQUEST_PARAMS_LIST = ['perPage', 'pageNo'];

exports.register = function register(app) {
    app.get('/admin/skills/:pageNo/:perPage',
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(getSkills));
};

async function getSkills(req, res) {
    let getSkillQuery = safeObjects.safeReqestQueryParser(req, REQUEST_FIELD_LIST);
    let getSkillParams = safeObjects.safeReqestParamsParser(req, REQUEST_PARAMS_LIST);

    let searchParam = getSkillQuery.search ? getSkillQuery.search : '';
    let sortParam = "name";
    let sortOrder = 1;

    if (getSkillQuery.sort) {
        sortParam = getSkillQuery.sort;
        if (sortParam[0] == "-") {
            sortOrder = -1;
            sortParam = sortParam.substring(1);
        }
    }

    let sortObject = {};
    sortObject[sortParam] = sortOrder;
    let pageNo = getSkillParams.pageNo;
    let perPage = getSkillParams.perPage;

    if (!pageNo) {
        pageNo = 1;
    }
    if (!perPage) {
        perPage = 10;
    }

    let skip = (parseInt(pageNo) - 1) * parseInt(perPage);
    let limit = parseInt(perPage);

    let skillsCursor_full = await mongoSkillsRepo.findAllWithCursor({'name': {'$exists': true}});
    let totalSkills = await skillsCursor_full.toArray();
    let totalSkillsLength = totalSkills.length;
    if (searchParam) {

        let skillsCursor = await mongoSkillsRepo.findAllWithCursor({'name': stringUtils.createSafeRegexObject(searchParam)});
        let sortedSkills = await skillsCursor.sort(sortObject);

        let matchingSkills = await sortedSkills.count();
        sortedSkills = await sortedSkills.skip(skip).limit(limit).toArray();

        res.status(200).send({
            skills: sortedSkills,
            totalSkills: totalSkillsLength,
            matchingSkills: matchingSkills
        });

    } else {

        let skillsCursor = await mongoSkillsRepo.findAllWithCursor({'name': {'$exists': true}});
        let sortedSkills = await skillsCursor.sort(sortObject).skip(skip).limit(limit).toArray();

        res.status(200).send({
            skills: sortedSkills,
            totalSkills: totalSkillsLength,
            matchingSkills: totalSkillsLength
        });
    }
}