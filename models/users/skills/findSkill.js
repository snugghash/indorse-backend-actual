const mongooseSkillRepo = require('../../services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../services/common/safeObjects');
const validate = require('../../services/common/validate');
const stringUtils = require('../../services/blob/stringUtils')

const routeUtils = require('../../services/common/routeUtils');

exports.register = function register(app) {
    app.get('/skills',
        validate({query: getQuerySchema()}),
        routeUtils.asyncMiddleware(findSkill));
};

async function findSkill(req, res) {
    let name = safeObjects.sanitize(req.query.name);
    let category = safeObjects.sanitize(req.query.category);

    let andQuery = [];

    if (name) {
        andQuery.push({name: stringUtils.createSafeRegexObject(name)})
    }

    if (category) {
        andQuery.push({category: stringUtils.createSafeRegexObject(category)})
    }

    let skills;

    if (andQuery.length === 0) {
        skills = await mongooseSkillRepo.findAllWithLimit({}, 10);
    } else {
        skills = await mongooseSkillRepo.findAllWithLimit({$and: andQuery}, 10);
    }

    res.status(200).send({
        skills: skills
    });
}



function getQuerySchema() {
    return {
        type: 'object',
        properties: {
            name: {
                type: 'string',
                minLength: 1
            },
            category: {
                type: 'string',
                minLength: 1
            }
        },
        additionalProperties: false
    };
}
