const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseSkillRepo = require('../../services/mongo/mongoose/mongooseSkillRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

const REQUEST_FIELD_LIST = ['name', 'level'];

exports.register = function register(app) {
    app.post('/users/:id/skills',
        validate({body: REQUEST_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(addSkill));
};

async function addSkill(req, res) {
    let addSkillRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let id = safeObjects.sanitize(req.params.id);

    let user = await mongooseUserRepo.findOneById(id);

    if(!user) {
        errorUtils.throwError("User does not exist", 404);
    }

    if (user.username !== req.username) {
        errorUtils.throwError("Cannot update somebody else's skill", 403);
    }

    let skill = await mongooseSkillRepo.findOne({name : addSkillRequest.name});

    if (!skill) {
        errorUtils.throwError("Skill does not exist", 404);
    }
    amplitudeTracker.publishData('skill_add',{feature_category: 'profile', skill: skill},req.user_id);
    let userSkill = null;

    if (user.skills && user.skills.length > 0) {
        //
        userSkill = user.skills.find((skill) => skill.skill.name === addSkillRequest.name);
    }

    if (userSkill) {
        errorUtils.throwError("User already has that skill", 400);
    }

    await mongooseUserRepo.update({email: req.email}, {$push: {skills: {skill : skill, level : addSkillRequest.level}}});

    let updatedUser = await mongooseUserRepo.findOneByEmail(req.email);
    userSkill = updatedUser.skills.find((skill) => skill.skill.name === addSkillRequest.name && skill.level === addSkillRequest.level);

    res.status(200).send(userSkill);
}

const REQUEST_SCHEMA = {
    type: 'object',
    properties: {
        name: {
            type: 'string',
            minLength: 1
        },
        level: {
            type: 'string',
            enum: ['beginner', 'intermediate', 'expert']
        }
    },
    additionalProperties: false
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['id'],
    properties: {
        id: {
            type: 'string',
            minLength: 1
        }
    }
};
