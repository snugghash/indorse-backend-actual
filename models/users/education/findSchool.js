const mongooseSchoolRepo = require('../../services/mongo/mongoose/mongooseSchoolRepo');
const safeObjects = require('../../services/common/safeObjects');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const stringUtils = require('../../services/blob/stringUtils');

const REQUEST_FIELD_LIST = ['school_name'];

exports.register = function register(app) {
    app.post('/findschool',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(findSchool));
};

async function findSchool(req, res) {
    let findRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let schoolNames = await mongooseSchoolRepo.findAll({
        school_name: stringUtils.createSafeRegexObject(findRequest.school_id)
    });

    res.status(200).send({
        //TODO 200 means success, no need for an additional flag
        success: true,
        schools: schoolNames
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            school_name: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['school_name'],
        additionalProperties: false
    };
}