const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const userRepo = require('../../services/mongo/userRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const validate = require('../../services/common/validate');
const authChecks = require('../../services/auth/authChecks');

const REQUEST_FIELD_LIST = ['interests'];

exports.register = function register(app) {
    app.post('/users/:user_id/interests',
        validate({body: BODY_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(updateInterests));
};

async function updateInterests(req, res) {
    let profileUpdateRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let user_id = safeObjects.sanitize(req.params.user_id);

    let userToUpdate = await mongooseUserRepo.findOneById(user_id);

    if (!userToUpdate) {
        errorUtils.throwError("User does not exist!", 404);
    }

    if ((req.username !== userToUpdate.username) && !req.permissions.admin.write) {
        errorUtils.throwError("Insufficient permissions to update this user's interests", 403);
    }

    await userRepo.update({_id: user_id}, {$set: {interests: profileUpdateRequest.interests}});

    res.status(200).send();
}

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        interests: {
            type: 'array',
            items: {
                type: 'string',
                minLength: 1,
                maxLength: 50
            },
        },
    },
    required: ['interests'],
    additionalProperties: false
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            pattern: "^(0x)?[0-9aA-fF]{24}$"
        }
    }
};