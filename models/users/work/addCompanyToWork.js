const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongoose = require('mongoose');

module.exports.addCompany = async function addCompany(work) {

    let company;
    if (mongoose.Types.ObjectId.isValid(work.company_id)) {
        company = await mongooseCompanyRepo.findOneById(work.company_id);
    } else {
        company = await mongooseCompanyRepo.findOne({'company_name': work.company_name});
    }

    if(company) {
        work.company = {
            pretty_id: company.pretty_id,
            company_name: company.company_name,
            logo_s3: company.logo_s3,
            logo_ipfs: company.logo_ipfs,
            visible_to_public: company.visible_to_public
        }
    }

    delete work.company_name;
};