const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseValidatorRepo = require('../services/mongo/mongoose/mongooseValidatorRepo');
const getVoteService = require('./getVoteService');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const cryptoUtils = require('../services/common/cryptoUtils');
const routeUtils = require('../services/common/routeUtils');
const timestampService = require('../services/common/timestampService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

const REQUEST_FIELD_LIST = ['endorse'];

exports.register = function register(app) {
    app.post(
        '/votes/:vote_id',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.FULL_ACCESS)),
        routeUtils.asyncMiddleware(submitVote),
    );
};

async function submitVote(req, res) {
    let voteSubmissionRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let vote_id = safeObjects.sanitize(req.param("vote_id"));
    let vote = await getVoteService.getVote(vote_id);

    voteSubmissionRequest.vote_id = vote_id;
    voteSubmissionRequest.claim_id = vote.claim_id;

    amplitudeTracker.publishData('vote_submission_initialized', voteSubmissionRequest, req.user_id);

    if (vote.voter_id !== req.user_id) {
        amplitudeTracker.publishData('vote_submission_failed_not_users_vote', voteSubmissionRequest, req.user_id);
        errorUtils.throwError('Users can only modify votes they own!', 403);
    }

    if (vote.sc_vote_exists) {
        amplitudeTracker.publishData('vote_submission_failed_already_voted', voteSubmissionRequest, req.user_id);
        errorUtils.throwError('Vote is already registered in the smart contract!', 400);
    }

    let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(vote.claim_id);

    let now = timestampService.createTimestamp();

    if (now > votingRound.end_voting) {
        amplitudeTracker.publishData('vote_submission_failed_deadline_passed', voteSubmissionRequest, req.user_id);
        errorUtils.throwError('Voting deadline has passed!', 400);
    }

    let votingToken = await cryptoUtils.genRandomString(32);
    if (!vote.voted_at) {

        await mongooseVoteRepo.update({_id: vote_id},
            {
                $set: {
                    voted_at: now,
                    endorsed: voteSubmissionRequest.endorse,
                    voting_token: votingToken
                }
            });
    }

    amplitudeTracker.publishData('vote_submission_successful', voteSubmissionRequest, req.user_id);
    res.status(200).send({votingToken: votingToken});
}


const PARAMS_SCHEMA = {
    type: 'object',
    required: ['vote_id'],
    properties: {
        vote_id: {
            type: 'string',
            minLength: 1,
        }
    },
    additionalProperties: false
};

const BODY_SCHEMA = {
    type: 'object',
    required: ['endorse'],
    properties: {
        endorse: {
            type: 'boolean'
        }
    },
    additionalProperties: false
};