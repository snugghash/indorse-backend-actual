const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const routeUtils = require('../services/common/routeUtils');
const authChecks = require('../services/auth/authChecks');
const getVoteService = require('./getVoteService');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.get('/votes',
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getVotes));
};

async function getVotes(req, res) {
    let votes = await mongooseVoteRepo.findByVoterId(req.user_id);

    let results = await Promise.all(votes.map((vote) => voteExtractionWrapper(vote)));

    amplitudeTracker.publishData('my_votes_accessed', {}, req.user_id);

    res.status(200).send({results: results});
}

async function voteExtractionWrapper(voteDoc) {
    let vote = await getVoteService.getVote(voteDoc._id.toString());
    let claim = await mongooseClaimsRepo.findOneById(vote.claim_id);
    let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(claim._id.toString());
    return {
        claim: claim,
        vote: vote,
        votinground: votingRound
    };
}

