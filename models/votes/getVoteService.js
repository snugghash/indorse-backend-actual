const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const mongooseClaimsRepo = require('../services/mongo/mongoose/mongooseClaimsRepo');
const errorUtils = require('../services/error/errorUtils');
const web3initializer = require('../../smart-contracts/services/initializers/web3');
const claimsProxy = require('../services/ethereum/claims/proxy/claimsProxy');
const claimsEmailService = require('../services/common/claimsEmailService');
const timestampService = require('../services/common/timestampService');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const SECONDS_IN_SIX_HOURS = 21600;

module.exports = {

    getVote: async function getVote(voteId) {
        let vote = await mongooseVoteRepo.findOneById(voteId);

        if (!vote) {
            errorUtils.throwError("Vote not found!", 404);
        }

        let now = timestampService.createTimestamp();

        if (vote.tx_hash && vote.tx_status === 'PENDING') {
            let web3 = await web3initializer.getWeb3()
            let receipt = await web3.eth.getTransactionReceipt(vote.tx_hash);
            if (receipt && receipt.blockNumber) {
                let txStatus = parseInt(receipt.status, 16);
                if (txStatus === 1) {
                    vote.tx_status = 'SUCCESS';
                    await mongooseVoteRepo.update({_id: vote._id}, {$set: {tx_status: 'SUCCESS'}});
                } else {
                    vote.tx_status = 'FAILURE';
                    await mongooseVoteRepo.update({_id: vote._id}, {$set: {tx_status: 'FAILURE', sc_vote_exists: false}});
                    let voter = await mongooseUserRepo.findOneById(vote.voter_id);
                    claimsEmailService.sendVoteTxFailedEmail(voter.name, vote.claim_id, voter.email, vote.tx_hash);
                    amplitudeTracker.publishData('vote_tx_failed', {vote_id : vote._id, claim_id :  vote.claim_id},
                        vote.voter_id);
                }
            } else if (vote.tx_timestamp && (now - vote.tx_timestamp) > SECONDS_IN_SIX_HOURS) {
                vote.tx_status = 'TIMEOUT';
                await mongooseVoteRepo.update({_id: vote._id}, {$set: {tx_status: 'TIMEOUT', sc_vote_exists: false}});
                let voter = await mongooseUserRepo.findOneById(vote.voter_id);
                claimsEmailService.sendVoteTxTimedOutEmail(voter.name, vote.claim_id, voter.email, vote.tx_hash);
                amplitudeTracker.publishData('vote_tx_timeout', {vote_id : vote._id, claim_id :  vote.claim_id},
                    vote.voter_id);
            }
        }

        if (!vote.sc_vote_exists) {
            let voter = await mongooseUserRepo.findOneById(vote.voter_id);
            let claim = await mongooseClaimsRepo.findOneById(vote.claim_id);
            if (voter.ethaddress && (claim.sc_claim_id || claim.sc_claim_id === 0)) {
                let voteFromSc = await claimsProxy.getVote(claim.sc_claim_id, voter.ethaddress);
                if (voteFromSc.exists) {
                    vote.sc_vote_exists = true;
                    await mongooseVoteRepo.update({_id: vote._id}, {$set: {sc_vote_exists: true}});
                } else {
                    vote.sc_vote_exists = false;
                }
            }
        }

        return vote;
    }
};