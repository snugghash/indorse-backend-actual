const mongooseVotingRoundRepo = require('../services/mongo/mongoose/mongooseVotingRoundRepo');
const getVoteService = require('./getVoteService');
const mongooseVoteRepo = require('../services/mongo/mongoose/mongooseVoteRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const timestampService = require('../services/common/timestampService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const amplitudeTracker = require('../services/tracking/amplitudeTracker');

const validator = new Validator({allErrors: true});
const validate = validator.validate;

const REQUEST_FIELD_LIST = ['txHash'];

exports.register = function register(app) {
    app.post(
        '/votes/:vote_id/txHash',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.FULL_ACCESS)),
        routeUtils.asyncMiddleware(submitVote),
    );
};

async function submitVote(req, res) {
    let hashSubmissionRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);
    let txHash = hashSubmissionRequest.txHash.toLowerCase();

    let vote_id = safeObjects.sanitize(req.param("vote_id"));

    let vote = await getVoteService.getVote(vote_id);

    hashSubmissionRequest.vote_id = vote_id;
    hashSubmissionRequest.claim_id = vote.claim_id;

    amplitudeTracker.publishData('vote_tx_hash_submission_initialized', hashSubmissionRequest, req.user_id);

    if (vote.voter_id !== req.user_id) {
        amplitudeTracker.publishData('vote_tx_hash_submission_failed_not_users_vote', hashSubmissionRequest, req.user_id);
        errorUtils.throwError('Users can only modify votes they own!', 403);
    }

    if (!vote.voted_at) {
        amplitudeTracker.publishData('vote_tx_hash_submission_failed_not_voted_yet', hashSubmissionRequest, req.user_id);
        errorUtils.throwError('User has not yet voted on Indorse!', 400);
    }

    if (vote.sc_vote_exists) {
        amplitudeTracker.publishData('vote_tx_hash_submission_failed_sc_vote_exists', hashSubmissionRequest, req.user_id);
        errorUtils.throwError('Vote is already registered in the smart contract!', 400);
    }

    let votingRound = await mongooseVotingRoundRepo.findOneByClaimID(vote.claim_id);

    let now = timestampService.createTimestamp();

    if (now > votingRound.end_voting) {
        amplitudeTracker.publishData('vote_tx_hash_submission_failed_deadline_passed', hashSubmissionRequest, req.user_id);
        errorUtils.throwError('Voting deadline has passed!', 400);
    }
    
    await mongooseVoteRepo.update({_id: vote_id}, {$set: {tx_hash: txHash,tx_status : 'PENDING',
            tx_timestamp : timestampService.createTimestamp()}});

    let voteToReturn = await getVoteService.getVote(vote_id);

    amplitudeTracker.publishData('vote_tx_hash_submission_successful', hashSubmissionRequest, req.user_id);

    res.status(200).send(voteToReturn);
}


const PARAMS_SCHEMA = {
    type: 'object',
    required: ['vote_id'],
    properties: {
        vote_id: {
            type: 'string',
            minLength: 1,
        }
    },
    additionalProperties: false
};

const BODY_SCHEMA = {
    type: 'object',
    required: ['txHash'],
    properties: {
        txHash: {
            type: 'string',
            pattern: '^(0x)?[0-9aA-fF]{64}$'
        }
    },
    additionalProperties: false
};