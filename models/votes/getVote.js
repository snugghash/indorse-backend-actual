const safeObjects = require('../services/common/safeObjects');
const authChecks = require('../services/auth/authChecks');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const getVoteService = require('./getVoteService');
const {Validator, ValidationError} = require('express-json-validator-middleware');

const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.get(
        '/votes/:vote_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.FULL_ACCESS)),
        routeUtils.asyncMiddleware(getVote),
    );
};

async function getVote(req, res) {
    let vote_id = safeObjects.sanitize(req.param("vote_id"));

    let vote = await getVoteService.getVote(vote_id);

    res.status(200).send(vote);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['vote_id'],
    properties: {
        vote_id: {
            type: 'string',
            minLength: 1,
        }
    },
    additionalProperties: false
};