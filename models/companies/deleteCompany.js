const mongooseCompanyRepo = require("../services/mongo/mongoose/mongooseCompanyRepo");
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const companyAdminService = require('./companyAdminService');

exports.register = function register(app) {
    app.delete('/companies/:pretty_id',
        routeUtils.asyncMiddleware(deleteCompany));
};

async function deleteCompany(req, res) {
    let pretty_id = safeObjects.sanitize(req.param("pretty_id"));

    if (!pretty_id) {
        errorUtils.throwError("Invalid pretty_id", 422);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    await companyAdminService.userIsCompanyAdmin(req.email, pretty_id);

    await mongooseCompanyRepo.deleteOne({pretty_id: pretty_id});

    res.status(200).send();
}

function userIsAuthorized(req) {
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}