const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const authChecks = require('../services/auth/authChecks');
const createVirtualEntityTxExecutor = require('../services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');
const roles = require('../services/auth/roles');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');

const validator = new Validator({allErrors: true});
const validate = validator.validate;

exports.register = function register(app) {
    app.post(
        '/companies/:pretty_id/entity',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(createEntity),
    );
};

async function createEntity(req, res) {
    const pretty_id = safeObjects.sanitize(req.params.pretty_id);

    const company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    if(company.entity_ethaddress) {
        errorUtils.throwError('Company already has an entity!', 400);
    }

    await createVirtualEntityTxExecutor.execute(company._id.toString(), req.user_id);

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
        },
    },
};
