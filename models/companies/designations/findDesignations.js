const mongooseDesignationRepo = require('../../services/mongo/mongoose/mongooseDesignationRepo');
const safeObjects = require('../../services/common/safeObjects');
const validate = require('../../services/common/validate');
const stringUtils = require('../../services/blob/stringUtils');

const REQUEST_FIELD_LIST = ['designation'];

const routeUtils = require('../../services/common/routeUtils');

exports.register = function register(app) {
    app.get('/designations',
        validate({query: getRequestSchema()}),
        routeUtils.asyncMiddleware(findDesignation));
};

/**
 * @swagger
 * definitions:
 *   DesignationQuery:
 *     type: object
 *     properties:
 *       designation:
 *         type: string
 *   DesignationQueryResponse:
 *     type: object
 *     properties:
 *       success:
 *         type: boolean
 *       designations:
 *         type: array
 *         items:
 *          type: string
 */

/**
 * @swagger
 * /designations:
 *   get:
 *     description: Searches for designations based on criteria
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/DesignationQuery'
 *     responses:
 *       200:
 *         description: Search successful
 *         schema:
 *           $ref: '#/definitions/DesignationQueryResponse'
 *       422:
 *         description: On invalid request body
 *         schema:
 *           $ref: '#/definitions/ValidationErrorResponse'
 *       500:
 *         description: On unexpected error
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 */
async function findDesignation(req, res) {
    let findRequest = safeObjects.safeReqestQueryParser(req, REQUEST_FIELD_LIST);
    let designationNames = await mongooseDesignationRepo.findAll({designation: stringUtils.createSafeRegexObject(findRequest.designation)});

    let designationsStrings = []
    designationNames.forEach(function(designation) {
        designationsStrings.push(designation.designation);
    })

    res.status(200).send({
        success: true,
        designations: designationsStrings
    });
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            designation: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['designation'],
        additionalProperties: false
    };
}
