const mongocompanyRepo = require('../services/mongo/mongoRepository')('company_names');
const mongooseUserRepo = require('../services/mongo/mongoose/mongooseUserRepo');
const Company = require('../services/mongo/mongoose/schemas/company');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const mongoose = require('mongoose');

exports.register = function register(app) {
    app.get('/companies/:pretty_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getCompany));
};

async function getCompany(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let company = await mongocompanyRepo.findOne({pretty_id: pretty_id});

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let safeCompany = Company.toSafeObject(company);

    if (req.permissions && req.permissions.admin && req.permissions.admin.read && company.email) {
        safeCompany.email = company.email;
    }

    safeCompany.has_admin_email = !!company.email;

    let companyConnectedUsers = [];

    if (safeCompany.advisors) { // due to the legacy copmany collections
        for (let advisor of safeCompany.advisors) {
            if (advisor.user_id) {
                let id = mongoose.Types.ObjectId(advisor.user_id);
                companyConnectedUsers.push(id);
            }
        }
    }
    if (safeCompany.team_members) {
        for (let teamMember of safeCompany.team_members) {
            if (teamMember.user_id) {
                let id = mongoose.Types.ObjectId(teamMember.user_id);
                companyConnectedUsers.push(id);
            }
        }
    }

    let companyUsersArray = await mongooseUserRepo.findAllWithKeys(companyConnectedUsers);

    for (let userObject of companyUsersArray) {

        let userAdvisorUpdated = safeCompany.advisors.find(advisors => advisors.user_id === (userObject._id).toString());
        let userTeamUpdated = safeCompany.team_members.find(teamMembers => teamMembers.user_id === (userObject._id).toString());

        if (userAdvisorUpdated) {
            assignUserObject(userAdvisorUpdated, userObject);
        }
        if (userTeamUpdated) {
            assignUserObject(userTeamUpdated, userObject);
        }
    }

    res.status(200).send(safeCompany);
}

function assignUserObject(companyConnection, userObject) {
    companyConnection.user = {};
    companyConnection.user.id = userObject._id;
    companyConnection.user.img_url = userObject.img_url;
    companyConnection.user.photo_ipfs = userObject.photo_ipfs;
    companyConnection.user.name = userObject.name;
    companyConnection.user.username = userObject.username;
    companyConnection.user.ethaddress = userObject.ethaddress;
    companyConnection.user.bio = userObject.bio;
    delete companyConnection.user_id;
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};

