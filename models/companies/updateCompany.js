const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const Company = require('../services/mongo/mongoose/schemas/company');
const coverUpdateService = require('./coverUpdateService');
const logoUpdateService = require('./logoUpdateService');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const companyAdminService = require('./companyAdminService');

const REQUEST_FIELD_LIST = ['company_name', 'cover_data', 'logo_data', 'description', 'admin', 'social_links',
    'additional_data', 'email', 'visible_to_public'];

exports.register = function register(app) {
    app.patch('/companies/:pretty_id',
        validate({body: REQUEST_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(updateCompany));
};

async function updateCompany(req, res) {
    let updateCompanyRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    if (!pretty_id) {
        errorUtils.throwError("Invalid pretty_id", 422);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    if (updateCompanyRequest.email) {

        let emailAlreadyInUse = await mongooseCompanyRepo.findOne({
                $and: [
                    {email: updateCompanyRequest.email},
                    {pretty_id: {$ne: pretty_id}}]
            });

        if (emailAlreadyInUse) {
            errorUtils.throwError('This company email is already in use', 400);
        }
    }

    await
        companyAdminService.userIsCompanyAdmin(req.email, pretty_id);

    if (updateCompanyRequest.admin) {
        let adminID = await
            companyAdminService.getAdminId(updateCompanyRequest);

        updateCompanyRequest.admin_username = updateCompanyRequest.admin;
        updateCompanyRequest.admin = adminID;
    }

    if (updateCompanyRequest.logo_data) {
        await
            logoUpdateService.updateLogo(updateCompanyRequest, updateCompanyRequest.logo_data, pretty_id);
        delete updateCompanyRequest.logo_data;
    }

    if (updateCompanyRequest.cover_data) {
        await
            coverUpdateService.updateCover(updateCompanyRequest, updateCompanyRequest.cover_data, pretty_id);
        delete updateCompanyRequest.cover_data;
    }

    updateCompanyRequest.last_updated_by = req.username;
    updateCompanyRequest.last_updated_timestamp = Date.now();

    await mongooseCompanyRepo.update({pretty_id: pretty_id}, {$set: updateCompanyRequest});

    let companyToReturn = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    res.status(200).send(Company.toSafeObject(companyToReturn));
}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company
    if (req.body.additional_data && !(req.permissions && req.permissions.admin && req.permissions.admin.write)) {
        return false;
    }

    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

const REQUEST_SCHEMA = {
    type: 'object',
    properties: {
        logo_data: {
            type: 'string',
            minLength: 1
        },
        email: {
            type: 'string',
            pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
        },
        cover_data: {
            type: 'string',
            minLength: 1
        },
        company_name: {
            type: 'string',
            minLength: 1,
            maxLength: 255
        },
        description: {
            type: 'string',
            minLength: 1
        },
        admin: {
            type: 'string',
            minLength: 1
        },
        additional_data: {
            type: 'object'
        },
        visible_to_public:{
          type: 'boolean'
        },
        //TODO social_links is a temporary hack, in the future a separate endpoint will be used
        social_links: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    type: {
                        type: 'string',
                        minLength: 1,
                        maxLength: 64
                    },
                    url: {
                        type: 'string',
                        minLength: 1
                    }
                },
                required: ['type', 'url'],
                additionalProperties: false
            }
        }
    },
    additionalProperties: false
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};
