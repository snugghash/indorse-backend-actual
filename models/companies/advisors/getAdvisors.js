const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const advisorExtractor = require('./advisorExtractor');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');

exports.register = function register(app) {
    app.get('/companies/:pretty_id/advisors',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getAdvisors));
};

async function getAdvisors(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let advisorsToReturn = [];

    let companyAdvisors = [];

    if (company.advisors) {
        for (let advisor of company.advisors) {
            companyAdvisors.push([advisor, company._id]);
        }
    }
    advisorsToReturn = await Promise.all(companyAdvisors.map((userCompnayAdvisorPair) => extractAdvisorWrapper(userCompnayAdvisorPair[0], userCompnayAdvisorPair[1])));

    res.status(200).send(advisorsToReturn);
}

async function extractAdvisorWrapper(advisor, companyId) {
    let advisorToReturn = await advisorExtractor.extractAdvisor(advisor, companyId);
    if (advisor.user_id) {
        let user = await mongooseUserRepo.findOneById(advisor.user_id);

        advisorToReturn.user = {
            img_url: user.img_url,
            photo_ipfs: user.photo_ipfs,
            name: user.name,
            username: user.username,
            id: user._id
        }
    }
    return advisorToReturn;
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};