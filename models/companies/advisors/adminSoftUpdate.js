const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const timestampService = require('../../services/common/timestampService');
const errorUtils = require('../../services/error/errorUtils');
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const emailService = require('../../services/common/emailService');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/admin-update',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(adminSoftUpdate));
};

async function adminSoftUpdate(req, res) {

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);
    let status = req.body.status;

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 400);
    }

    let advisorToUpdate = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToUpdate) {
        errorUtils.throwError("Advisor not found", 400);
    }

    if (!advisorToUpdate.softConnection){
        errorUtils.throwError("Invitation to be advisor not found", 400);
    }

    if (advisorToUpdate.softConnection.status !== 'ACCEPTED') {
        errorUtils.throwError("This advisor has not accepted the invitation", 400);
    }

    let adminUser = await mongooseUserRepo.findOneByEmail(req.email);
    let advisorUser = await mongooseUserRepo.findOneById(advisorToUpdate.user_id);

    let updatedTimestamp = timestampService.createTimestamp();

    let connectionDataToTrack = {
        approved_or_disapproved_user_type: advisorUser.role,
        company_name: pretty_id,
        advisor_id: advisor_id,
        feature_category: 'company-connection',
        feature_sub_category: 'company-connection-enhance-ID-994'
    }

    let softConnectionUpdate = advisorToUpdate.softConnection;
    if (status === 'approved') {
        softConnectionUpdate.companyStatus = 'APPROVED';
        amplitudeTracker.publishData('advisor_connection_approved',connectionDataToTrack, adminUser._id);
        await emailService.sendAdvisorAdvisorApprovedEmail(advisorUser.name, company.company_name, pretty_id, advisorUser.email);
    } else if (status === 'disapproved') {
        softConnectionUpdate.companyStatus = 'DISAPPROVED';
        amplitudeTracker.publishData('advisor_connection_disapproved',connectionDataToTrack, adminUser._id);
        await emailService.sendAdvisorAdvisorDisapprovedEmail(advisorUser.name, company.company_name, pretty_id, advisorUser.email);
    }
    softConnectionUpdate.companyStatusUpdatedBy = {
        id: adminUser._id.toString()
    };
    softConnectionUpdate.companyStatusUpdatedAt = updatedTimestamp;

    await mongooseCompanyRepo.setCompanyAdvisorSoftConnection(pretty_id, advisor_id, softConnectionUpdate);

    res.status(200).send({softConnection: softConnectionUpdate});
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};

const BODY_SCHEMA = {
    type: 'object',
    required: ['status'],
    properties: {
        status: {
            enum: ['approved', 'disapproved']
        }
    },
    additionalProperties: false,
};