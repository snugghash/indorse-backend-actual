const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const advisorExtractor = require('./advisorExtractor');

exports.register = function register(app) {
    app.get('/users/:user_id/advisories',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getAdvisors));
};

async function getAdvisors(req, res) {
    let userId = safeObjects.sanitize(req.params.user_id);

    let user = await mongooseUserRepo.findOneById(userId);

    if (!user) {
        errorUtils.throwError("User does not exist!", 404);
    }

    let companiesInvitedTo = await mongooseCompanyRepo.findAll({'advisors.user_id': user._id});

    let advisorsToReturn = [];

    let userAdvisorArray = []; // it is an array of array. Subarray has 3 elements. [advisor, companyId, company]

    for (let company of companiesInvitedTo) {
        let userAdvisories = company.advisors.filter(advisor => advisor.user_id === user._id.toString()); //Filter?
        for (let advisor of userAdvisories) {
            userAdvisorArray.push([advisor,company._id,company]);
        }
    }

    advisorsToReturn = await Promise.all(userAdvisorArray.map((userCompnayAdvisorPair,index) =>{
        return extractAdvisorWrapper(userCompnayAdvisorPair[0],userCompnayAdvisorPair[1],userCompnayAdvisorPair[2]);
    }));

    res.status(200).send(advisorsToReturn);
}

async function extractAdvisorWrapper(advisor, companyId, company){
    let advisorToReturn = await advisorExtractor.extractAdvisor(advisor, companyId);
    advisorToReturn.company = {
        pretty_id: company.pretty_id,
        id: company._id,
        logo_s3: company.logo_s3,
        logo_ipfs: company.logo_ipfs,
        company_name: company.company_name
    };
    return advisorToReturn;
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};