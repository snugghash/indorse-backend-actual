const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const timestampService = require('../../services/common/timestampService');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const emailService = require('../../services/common/emailService');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/soft-reject',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(rejectInvitation));
};

async function rejectInvitation(req, res) {

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);  

    if (!company) {
        errorUtils.throwError("Company not found", 400);
    }

    let advisorToReject = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToReject) {
        errorUtils.throwError("Invitation not found", 400);
    }

    if (!advisorToReject.softConnection){
        errorUtils.throwError("Invitation not found", 400);
    } else {
        if (advisorToReject.softConnection.statusValidatedByUserAt) {
            errorUtils.throwError("This invitation was already validated", 400);
        }
    }

    let rejectedTimestamp = timestampService.createTimestamp();
    await mongooseCompanyRepo.setRejectedTimestamp(pretty_id, advisor_id, rejectedTimestamp);

    let softConnectionUpdate = advisorToReject.softConnection;
    softConnectionUpdate.status = 'REJECTED';
    softConnectionUpdate.statusUpdatedAt = rejectedTimestamp;
    let rejectedUsersDetails;

    if(advisorToReject.email)
        rejectedUsersDetails = advisorToReject.email;

    if(advisorToReject.user_id){
        let rejectedUser = await mongooseUserRepo.findOneById(advisorToReject.user_id);
        rejectedUsersDetails = rejectedUser.email;
    }
    let invitationDataToTrack = {
        company_name: pretty_id,
        advisor_id: advisor_id,
        feature_category: 'company-connection',
        feature_sub_category: 'company-connection-enhance-ID-994',
        is_user_already_validated: false
    }
    let advisorUserId;
    let validatedEmailSent = false;    
    if (req.login){
        let rejectingUser = await mongooseUserRepo.findOneByEmail(req.email);
        if (advisorToReject.user_id && advisorToReject.user_id !== rejectingUser._id.toString()){
            errorUtils.throwError("No permission to reject other user's invitation", 403);        
        } else {
            advisorUserId = rejectingUser._id.toString();
            invitationDataToTrack.role = rejectingUser.role;
            if (rejectingUser.linkedIn_uid) {
                softConnectionUpdate.statusValidatedByUserAt = rejectedTimestamp;
                invitationDataToTrack.is_user_already_validated = true;
                validatedEmailSent = true;
                await emailService.softAdvisorValidatedNotifyAdmin(rejectingUser.name, company.company_name,
                    pretty_id, rejectingUser.linkedInProfileURL, company.email, "REJECTED");
            }
        }

    }else{
        advisorUserId = 'unidentified';
        invitationDataToTrack.role = 'not_logged_in';
    }


    amplitudeTracker.publishData('advisor_soft_rejected_invitation',invitationDataToTrack, req.user_id, req.device_id);
    if(invitationDataToTrack.is_user_already_validated){
        amplitudeTracker.publishData('advisor_soft_rejected_validated',invitationDataToTrack, req.user_id);
    }
    await mongooseCompanyRepo.setCompanyAdvisorSoftConnection(pretty_id, advisor_id, softConnectionUpdate);
   
    if (company.email && !validatedEmailSent) {       
        await emailService.softAdvisorRejectNotifyAdmin(rejectedUsersDetails, company.company_name, pretty_id, company.email);
    }    
    res.status(200).send({softConnection:softConnectionUpdate});
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};