const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const timestampService = require('../../services/common/timestampService');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const emailService = require('../../services/common/emailService');
const amplitudeTracker = require('../../services/tracking/amplitudeTracker');

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/soft-accept',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(acceptInvitation));
};

async function acceptInvitation(req, res) {

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 400);
    }

    let advisorToAccept = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToAccept) {
        errorUtils.throwError("Advisor not found", 400);
    }

    if (!advisorToAccept.softConnection){
        errorUtils.throwError("Invitation to be advisor not found", 404);
    } else {
        if (advisorToAccept.softConnection.statusValidatedByUserAt) {
            errorUtils.throwError("This invitation was already validated", 400);
        }
    }

    let acceptedTimestamp = timestampService.createTimestamp();
    await mongooseCompanyRepo.setRejectedTimestamp(pretty_id, advisor_id, acceptedTimestamp);

    let softConnectionUpdate = advisorToAccept.softConnection;
    softConnectionUpdate.status = 'ACCEPTED';
    softConnectionUpdate.statusUpdatedAt = acceptedTimestamp;
    let acceptedUsersDetails;

    if(advisorToAccept.email) {
        acceptedUsersDetails = advisorToAccept.email;
    } else if(advisorToAccept.user_id){
        let acceptedUser = await mongooseUserRepo.findOneById(advisorToAccept.user_id);
        acceptedUsersDetails = acceptedUser.email;
    }
    let invitationDataToTrack = {
        company_name: pretty_id,
        advisor_id: advisor_id,
        feature_category: 'company-connection',
        feature_sub_category: 'company-connection-enhance-ID-994',
        is_user_already_validated: false
    }
    let advisorUserId;
    let validatedEmailSent= false;
    if (req.login){
        let acceptedUser = await mongooseUserRepo.findOneByEmail(req.email);
        if (advisorToAccept.user_id && advisorToAccept.user_id !== acceptedUser._id.toString()) {
            errorUtils.throwError("No permission to reject other user's invitation", 403);
        } else {
            advisorUserId = acceptedUser._id.toString();
            invitationDataToTrack.role = acceptedUser.role;
            if (acceptedUser.linkedIn_uid) {
                softConnectionUpdate.statusValidatedByUserAt = acceptedTimestamp;
                invitationDataToTrack.is_user_already_validated = true;
                validatedEmailSent = true;
                await emailService.softAdvisorValidatedNotifyAdmin(acceptedUser.name, company.company_name, 
                    pretty_id, acceptedUser.linkedInProfileURL, company.email,"ACCEPTED")
            }
        }

    }else{
        advisorUserId = 'unidentified';
        invitationDataToTrack.role = 'not_logged_in';
    }


    amplitudeTracker.publishData('advisor_soft_accepted_invitation',invitationDataToTrack, req.user_id, req.device_id);

    if(invitationDataToTrack.is_user_already_validated){
        amplitudeTracker.publishData('advisor_soft_accepted_validated',invitationDataToTrack, req.user_id);
    }
    await mongooseCompanyRepo.setCompanyAdvisorSoftConnection(pretty_id, advisor_id, softConnectionUpdate);
   
    if (company.email && !validatedEmailSent) {        
        await emailService.softAdvisorAcceptNotifyAdmin(acceptedUsersDetails, company.company_name,pretty_id, company.email);
    }
    res.status(200).send({softConnection:softConnectionUpdate});
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};