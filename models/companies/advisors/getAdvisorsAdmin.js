const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const roles = require('../../services/auth/roles');
const adminAdvisorExtractor = require('./adminAdvisorExtractor');

exports.register = function register(app) {
    app.get('/companies/:pretty_id/advisors/admin',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(getAdvisorsAdmin));
};

async function getAdvisorsAdmin(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let advisorsToReturn = [];
    let adminAdvisors = [];

    if (company.advisors) {
        for (let advisor of company.advisors) {
            adminAdvisors.push([advisor,company._id]);
        }
    }
    advisorsToReturn =  await Promise.all(adminAdvisors.map((adminAdvisorsPair) =>{
        return extractAdvisorWrapper(adminAdvisorsPair[0],adminAdvisorsPair[1]);
    }));
    res.status(200).send(advisorsToReturn);
}

async function extractAdvisorWrapper(advisor, companyId){
    return await adminAdvisorExtractor.extractAdvisor(advisor, companyId);
}


const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};