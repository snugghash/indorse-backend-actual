const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const timestampService = require('../../services/common/timestampService');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const emailService = require('../../services/common/emailService');

exports.register = function register(app) {
    app.post('/companies/:pretty_id/advisors/:advisor_id/reject',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(rejectInvitation));
};

async function rejectInvitation(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let advisor_id = safeObjects.sanitize(req.params.advisor_id);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    let rejectingUser = await mongooseUserRepo.findOneByEmail(req.email);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let advisorToReject = company.advisors.find(advisor => advisor._id.toString() === advisor_id);

    if (!advisorToReject) {
        errorUtils.throwError("Invitation not found", 404);
    }

    if (advisorToReject.rejected_timestamp) {
        errorUtils.throwError("This invitation was already rejected", 400);
    }

    if (advisorToReject.verification_timestamp) {
        errorUtils.throwError("This invitation was already verified", 400);
    }

    if (!advisorToReject.user_id || advisorToReject.user_id !== rejectingUser._id.toString()) {
        errorUtils.throwError("No permission to reject other user's invitation", 403);
    }

    await mongooseCompanyRepo.setRejectedTimestamp(pretty_id, advisor_id, timestampService.createTimestamp());

    if (company.email) {
        await emailService.sendAdvisorAdvisorRejectedInvitationToCompanyEmail(rejectingUser.name, company.company_name, company.email);
    }

    await emailService.sendAdvisorAdvisorRejectedInvitationToUserEmail(rejectingUser.name, company.company_name, req.email, pretty_id);

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'advisor_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        advisor_id: {
            type: 'string',
            minLength: 1
        }
    }
};