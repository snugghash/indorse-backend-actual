const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const User = require('../../services/mongo/mongoose/schemas/user');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const authChecks = require('../../services/auth/authChecks');
const inviteUserAsAdvisorProxy = require('../../services/ethereum/connections/proxy/inviteUseProxy');
const roles = require('../../services/auth/roles');
const emailService = require('../../services/common/emailService');
const timestampService = require('../../services/common/timestampService');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');

const REQUEST_FIELD_LIST = ['email', 'user_id', 'username'];

exports.register = function register(app) {
    app.post(
        '/companies/:pretty_id/advisor',
        validate({body: BODY_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(inviteAdvisor),
    );
};

async function inviteAdvisor(req, res) {
    const inviteAdvisorRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    const pretty_id = safeObjects.sanitize(req.params.pretty_id);

    const company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    await checkIfAdvisorAlreadyExists(inviteAdvisorRequest, pretty_id);

    const userToInvite = await findInvitedUser(inviteAdvisorRequest);

    const creationTimestamp = timestampService.createTimestamp();

    let advisorID;

    if (!userToInvite) {

        if (!inviteAdvisorRequest.email) {
            errorUtils.throwError('Email must be provided for non-registered users', 400);
        }

        let advisor = {
            email: inviteAdvisorRequest.email,
            rejected: false,
            creation_timestamp: creationTimestamp
        };

        await inviteUserAsAdvisorProxy.inviteUser(undefined, company._id.toString(), ConnectionType.IS_ADVISOR_OF, req.user_id, advisor);

        advisorID = await mongooseCompanyRepo.addAdvisor(pretty_id, advisor);

        await emailService.sendNonExistentAdvisorInvitationEmail(company.company_name, inviteAdvisorRequest.email, pretty_id);
    } else {

        let advisor = {
            type: 'COMPANY_ADVISOR',
            user_id: userToInvite._id.toString(),
            rejected: false,
            creation_timestamp: creationTimestamp,
        };

        await inviteUserAsAdvisorProxy.inviteUser(userToInvite._id.toString(), company._id.toString(), ConnectionType.IS_ADVISOR_OF,
            req.user_id, advisor);

        advisorID = await mongooseCompanyRepo.addAdvisor(pretty_id, advisor);

        await emailService.sendAdvisorInvitationEmail(userToInvite.name, company.company_name, inviteAdvisorRequest.email,
            pretty_id);
    }

    const connectionToReturn = {
        _id: advisorID,
        creation_timestamp: creationTimestamp,
    };

    if (userToInvite) {
        connectionToReturn.user = User.toSafeObject(userToInvite);
    }

    res.status(200).send(connectionToReturn);
}

async function checkIfAdvisorAlreadyExists(inviteAdvisorRequest, pretty_id) {
    let existingAdvisor;

    if (inviteAdvisorRequest.email) {
        const orQuery = [];

        const user = await mongooseUserRepo.findOneByEmail(inviteAdvisorRequest.email);

        if (user) {
            orQuery.push({'advisors.user_id': user._id});
        }

        orQuery.push({'advisors.email': inviteAdvisorRequest.email});

        existingAdvisor = await mongooseCompanyRepo.findOne({
            $and: [
                {pretty_id},
                {$or: orQuery},
            ],
        });
    } else if (inviteAdvisorRequest.user_id) {
        existingAdvisor = await mongooseCompanyRepo.findOne({
            $and: [
                {pretty_id},
                {'advisors.user_id': inviteAdvisorRequest.user_id}],
        });
    } else if (inviteAdvisorRequest.username) {
        const user = await mongooseUserRepo.findOneByUsername(inviteAdvisorRequest.username);

        if (user) {
            existingAdvisor = await mongooseCompanyRepo.findOne({
                $and: [
                    {pretty_id},
                    {'advisors.user_id': user._id}],
            });
        }
    }

    if (existingAdvisor) {
        errorUtils.throwError('This company advisor already exists', 400);
    }
}

async function findInvitedUser(inviteAdvisorRequest) {
    if (inviteAdvisorRequest.email) {
        return await mongooseUserRepo.findOneByEmail(inviteAdvisorRequest.email);
    } else if (inviteAdvisorRequest.username) {
        return await mongooseUserRepo.findOneByUsername(inviteAdvisorRequest.username);
    } else if (inviteAdvisorRequest.user_id) {
        return await mongooseUserRepo.findOneById(inviteAdvisorRequest.user_id);
    }
    errorUtils.throwError('No query parameter provided', 400);
}

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        email: {
            type: 'string',
            minLength: 1,
        },
        user_id: {
            type: 'string',
            minLength: 1,
        },
        username: {
            type: 'string',
            minLength: 1,
        },
    },
    additionalProperties: false,
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
        },
    },
};

exports.checkIfAdvisorAlreadyExists = checkIfAdvisorAlreadyExists;
exports.findInvitedUser = findInvitedUser;