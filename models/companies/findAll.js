const safeObjects = require('../services/common/safeObjects');
const mongoCompanyRepo = require('../services/mongo/mongoRepository')('company_names');
const routeUtils = require('../services/common/routeUtils');
const errorUtils = require('../services/error/errorUtils');
const stringUtils = require('../services/blob/stringUtils')

exports.register = function register(app) {
    app.get('/companies',
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to list companies!', 403);
    }

    let searchParam, pageNumber, pageSize, sortObj;

    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.pageNumber) {
        pageNumber = safeObjects.sanitize(req.query.pageNumber);
    } else {
        pageNumber = 1;
    }

    if (req.query.pageSize) {
        pageSize = safeObjects.sanitize(req.query.pageSize);
    } else {
        pageSize = 10;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    if (req.query.sort) {
        let sortParam = safeObjects.sanitize(req.query.sort);
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            company_name: 1
        }
    }

    let companyCursor, matchingCompanies;

    if (searchParam) {
        companyCursor = await mongoCompanyRepo.findAllWithCursor(
            {
                $or: [{pretty_id: stringUtils.createSafeRegexObject(searchParam)},
                    {company_name: stringUtils.createSafeRegexObject(searchParam)}]
            });

        matchingCompanies = await companyCursor.count();
    } else {
        companyCursor = await mongoCompanyRepo.findAllWithCursor({pretty_id: {$exists: true}});
    }

    let sortedCompanies = await companyCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    let companyIndex = sortedCompanies.map((company) => {
        return {
            pretty_id: company.pretty_id,
            url: company.url,
            id: company._id,
            company_name: company.company_name,
            last_updated_by: company.last_updated_by,
            last_updated_timestamp: company.last_updated_timestamp
        }
    });

    let companyProfileCursor = await mongoCompanyRepo.findAllWithCursor({pretty_id: {$exists: true}});

    let responseObj = {
        companies: companyIndex,
        totalCompanies: await companyProfileCursor.count()
    };

    if (matchingCompanies) {
        responseObj.matchingCompanies = matchingCompanies;
    }

    res.status(200).send(200, responseObj);
}

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}

