const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const companyAdminService = require('../companyAdminService');

exports.register = function register(app) {
    app.delete('/companies/:pretty_id/social_links/:type',
        routeUtils.asyncMiddleware(deleteLink));
};

async function deleteLink(req, res) {
    let pretty_id = safeObjects.sanitize(req.param("pretty_id"));
    let type = safeObjects.sanitize(req.param("type"));

    if (!pretty_id) {
        errorUtils.throwError("Invalid pretty_id", 422);
    }

    if (!type) {
        errorUtils.throwError("Invalid type", 422);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError("Unauthorized", 403);
    }

    await companyAdminService.userIsCompanyAdmin(req.email, pretty_id);

    let linkExists = await mongooseCompanyRepo.findOne({$and: [{'social_links.type': type}, {pretty_id: pretty_id}]});

    if (!linkExists) {
        errorUtils.throwError("Link does not exist", 404);
    }

    let last_updated_by = req.username;
    let last_updated_timestamp = Date.now();

    await mongooseCompanyRepo.update({pretty_id: pretty_id}, {
        $set:
            {
                last_updated_by: last_updated_by,
                last_updated_timestamp: last_updated_timestamp
            },
        $pull: {social_links: {type: type}}
    });

    res.send(200);
}

function userIsAuthorized(req) {
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}
