const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const companyAdminService = require('../companyAdminService');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

const REQUEST_FIELD_LIST = ['type', 'url'];

exports.register = function register(app) {
    app.post('/companies/:pretty_id/social_links',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(add));
};

async function add(req, res) {
    let pretty_id = safeObjects.sanitize(req.param("pretty_id"));
    let addLinkRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!pretty_id) {
        errorUtils.throwError("Invalid pretty_id", 422);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError("Unauthorized", 403);
    }

    await companyAdminService.userIsCompanyAdmin(req.email, pretty_id);

    let updatedCompany = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (updatedCompany.social_links.find((link) => link.type === addLinkRequest.type)) {
        errorUtils.throwError("Link already exists, use PUT to replace", 400);
    }

    let last_updated_by = req.username;
    let last_updated_timestamp = Date.now();

    await mongooseCompanyRepo.update({pretty_id: pretty_id}, {
        $set:
            {
                last_updated_by: last_updated_by,
                last_updated_timestamp: last_updated_timestamp
            },
        $push: {social_links: addLinkRequest}
    });

    res.send(200);
}

function userIsAuthorized(req) {
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}


function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            type: {
                type: 'string',
                minLength: 1
            },
            url: {
                type: 'string',
                minLength: 1
            }
        },
        required: ['type', 'url'],
        additionalProperties: false
    };
}
