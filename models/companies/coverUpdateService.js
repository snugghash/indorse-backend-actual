const imageService = require('../services/blob/imageService');

exports.updateCover = async function updateCover(request, cover_data, pretty_id) {
    if (imageService.uploadsAreEnabled()) {
        let coverUploadResult = await imageService.uploadImage('company_cover_', cover_data, pretty_id);

        request.cover_s3 = coverUploadResult.s3;
        request.cover_ipfs = coverUploadResult.ipfs;
    }
};