const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const companyToUserChecker = require('../../services/ethereum/connections/proxy/companyToUserChecker');
const userToCompanyChecker = require('../../services/ethereum/connections/proxy/userToCompanyChecker');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const User = require('../../services/mongo/mongoose/schemas/user');

exports.extractTeamMember = async function extractTeamMember(teamMember, companyId) {
    let teamMemberToReturn = {};

    teamMemberToReturn._id = teamMember._id;

    if (teamMember.user_id) {
        let user = await mongooseUserRepo.findOneById(teamMember.user_id);

        teamMemberToReturn.user = User.toSafeObject(user);
    } else {
        teamMemberToReturn.email = teamMember.email;
    }

    if (teamMember.verification_timestamp) {
        teamMemberToReturn.verification_timestamp = teamMember.verification_timestamp;
    }

    if (teamMember.rejected_timestamp) {
        teamMemberToReturn.rejected_timestamp = teamMember.rejected_timestamp;
    }

    if (teamMember.accepted_timestamp) {
        teamMemberToReturn.accepted_timestamp = teamMember.accepted_timestamp;
    }

    teamMemberToReturn.creation_timestamp = teamMember.creation_timestamp;

    teamMemberToReturn.rejected = teamMember.rejected;

    teamMemberToReturn.designation = teamMember.designation;

    if (teamMember.user_id) {

        [teamMemberToReturn.signed_by_user ,teamMemberToReturn.signed_by_company] = await Promise.all(
            [userToCompanyChecker.checkIfConnectionExists(teamMember.user_id, companyId, ConnectionType.IS_TEAM_MEMBER_OF),
                companyToUserChecker.checkIfConnectionExists(teamMember.user_id, companyId, ConnectionType.IS_TEAM_MEMBER_OF)]);

        if (teamMember.company_tx_hash && teamMemberToReturn.signed_by_company) {
            teamMemberToReturn.company_tx_hash = teamMember.company_tx_hash;
        }

        if (teamMember.user_tx_hash && teamMemberToReturn.signed_by_user) {
            teamMemberToReturn.user_tx_hash = teamMember.user_tx_hash;
        }
    } else {
        teamMemberToReturn.signed_by_user = false;
        teamMemberToReturn.signed_by_company = false;
    }

    return teamMemberToReturn;
};