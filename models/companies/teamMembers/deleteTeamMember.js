const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const authChecks = require('../../services/auth/authChecks');
const emailService = require('../../services/common/emailService');
const removeConnectionProxy = require('../../services/ethereum/connections/proxy/removeConnectionProxy');
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');
const userToCompanyChecker = require('../../services/ethereum/connections/proxy/userToCompanyChecker');

const REQUEST_FIELD_LIST = ['tx_hash'];

exports.register = function register(app) {
    app.delete('/companies/:pretty_id/team_members/:teamMember_id',
        validate({params: PARAMS_SCHEMA, body: BODY_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(getTeamMembers));
};

async function getTeamMembers(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);
    let teamMember_id = safeObjects.sanitize(req.params.teamMember_id);

    let deleteTeamMemberRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    let company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);
    let deletingUser = await mongooseUserRepo.findOneByEmail(req.email);

    if (!company) {
        errorUtils.throwError("Company not found", 404);
    }

    let teamMemberToDelete = company.team_members.find(teamMember => teamMember._id.toString() === teamMember_id);

    if (!teamMemberToDelete) {
        errorUtils.throwError("TeamMember not found", 404);
    }

    if (teamMemberToDelete.user_id !== deletingUser._id.toString() && !req.permissions.admin.write) {
        errorUtils.throwError("No permission to delete this teamMember", 403);
    }

    if (teamMemberToDelete.user_id) {

        if (req.permissions && req.permissions.admin && !req.permissions.admin.write) {
            let userToCompanyConnectionExists =
                await userToCompanyChecker.checkIfConnectionExists(teamMemberToDelete.user_id, company._id, ConnectionType.IS_TEAM_MEMBER_OF);

            if (userToCompanyConnectionExists) {
                errorUtils.throwError("User must first remove their connection to company", 400);
            }
        }

        let correspondingUser = await mongooseUserRepo.findOneById(teamMemberToDelete.user_id);


        if (!req.permissions && !req.permissions.admin && !req.permissions.admin.write) { //Admin removing
            if (company.email) {
                await emailService.sendTeamMemberRemovedByAdminInfoToCompany(correspondingUser.name, company.company_name, company.email);
            }

            await emailService.sendTeamMemberRemovedInfoToTeamMember(correspondingUser.name, company.company_name, correspondingUser.email, pretty_id);

        } else { //User removing
            if (company.email) {
                await emailService.sendTeamMemberRemovedByUserInfoToCompany(correspondingUser.name, company.company_name, company.email);
            }

            await emailService.sendTeamMemberRemovedInfoToTeamMember(correspondingUser.name, company.company_name, correspondingUser.email, pretty_id);
        }

        await removeConnectionProxy.removeConnection(teamMemberToDelete.user_id, company._id, ConnectionType.IS_TEAM_MEMBER_OF,
            req.user_id);

    } else {
        await emailService.sendTeamMemberRemovedInfoToTeamMember("Sir/Madam", company.company_name, teamMemberToDelete.email);

        if (company.email) {
            await emailService.sendTeamMemberRemovedByAdminInfoToCompany("unregistered user", company.company_name, company.email, pretty_id);
        }
    }

    await mongooseCompanyRepo.teamMember.removeTeamMember(pretty_id, teamMember_id, deleteTeamMemberRequest.tx_hash);

    res.status(200).send();
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id', 'teamMember_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        },
        teamMember_id: {
            type: 'string',
            minLength: 1
        }
    }
};

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        tx_hash: {
            type: 'string',
            pattern: '^(0x)?[0-9aA-fF]{64}$'
        }
    },
    additionalProperties: false
};


