const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const User = require('../../services/mongo/mongoose/schemas/user');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const mongooseDesignationRepo = require('../../services/mongo/mongoose/mongooseDesignationRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const authChecks = require('../../services/auth/authChecks');
const inviteUserAsTeamMemberProxy = require('../../services/ethereum/connections/proxy/inviteUseProxy');
const roles = require('../../services/auth/roles');
const emailService = require('../../services/common/emailService');
const timestampService = require('../../services/common/timestampService');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');

const validator = new Validator({allErrors: true});
const validate = validator.validate;
const ConnectionType = require('../../../smart-contracts/services/connections/models/connectionType');

const REQUEST_FIELD_LIST = ['email', 'user_id', 'username', 'designation'];

exports.register = function register(app) {
    app.post(
        '/companies/:pretty_id/team_member',
        validate({body: BODY_SCHEMA, params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(authChecks.loginCheck()),
        routeUtils.asyncMiddleware(authChecks.roleCheck(roles.ADMIN)),
        routeUtils.asyncMiddleware(inviteTeamMember),
    );
};

async function inviteTeamMember(req, res) {
    const inviteTeamMemberRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    const pretty_id = safeObjects.sanitize(req.params.pretty_id);

    const company = await mongooseCompanyRepo.findOneByPrettyId(pretty_id);

    if (!company) {
        errorUtils.throwError('Company not found', 404);
    }

    await checkIfTeamMemberAlreadyExists(inviteTeamMemberRequest, pretty_id);

    if (!inviteTeamMemberRequest.designation) {
        errorUtils.throwError('Must provide designation', 400);
    }
    await checkThatDesignationExists(inviteTeamMemberRequest.designation);

    const userToInvite = await findInvitedUser(inviteTeamMemberRequest);

    const creationTimestamp = timestampService.createTimestamp();

    let teamMemberID;

    if (!userToInvite) {

        if (!inviteTeamMemberRequest.email) {
            errorUtils.throwError('Email must be provided for non-registered users', 400);
        }

        let teamMember = {
            email: inviteTeamMemberRequest.email,
            rejected: false,
            creation_timestamp: creationTimestamp,
            designation: inviteTeamMemberRequest.designation,
        };



        await inviteUserAsTeamMemberProxy.inviteUser(undefined, company._id.toString(), ConnectionType.IS_TEAM_MEMBER_OF, req.user_id, teamMember);

        teamMemberID = await mongooseCompanyRepo.teamMember.addTeamMember(pretty_id, teamMember);

        await emailService.sendNonExistentTeamMemberInvitationEmail(company.company_name, inviteTeamMemberRequest.email, pretty_id, inviteTeamMemberRequest.designation);
    } else {

        let teamMember = {
            user_id: userToInvite._id.toString(),
            rejected: false,
            creation_timestamp: creationTimestamp,
            designation: inviteTeamMemberRequest.designation,
            company_id: company._id.toString(),
            type: "COMPANY_TEAM_MEMBER"
        };


        await inviteUserAsTeamMemberProxy.inviteUser(userToInvite._id.toString(), company._id.toString(), ConnectionType.IS_TEAM_MEMBER_OF,
            req.user_id, teamMember);

        teamMemberID = await mongooseCompanyRepo.teamMember.addTeamMember(pretty_id, teamMember);

        await emailService.sendTeamMemberInvitationEmail(userToInvite.name, company.company_name, inviteTeamMemberRequest.email,
            pretty_id, inviteTeamMemberRequest.designation);
    }

    let connectionToReturn = {
        _id: teamMemberID,
        creation_timestamp: creationTimestamp,
    };

    if (userToInvite) {
        connectionToReturn.user = User.toSafeObject(userToInvite);
    }

    res.status(200).send(connectionToReturn);
}

async function checkIfTeamMemberAlreadyExists(inviteTeamMemberRequest, pretty_id) {
    let existingTeamMember;

    if (inviteTeamMemberRequest.email) {
        const orQuery = [];

        const user = await mongooseUserRepo.findOneByEmail(inviteTeamMemberRequest.email);

        if (user) {
            orQuery.push({'team_members.user_id': user._id});
        }

        orQuery.push({'team_members.email': inviteTeamMemberRequest.email});

        existingTeamMember = await mongooseCompanyRepo.findOne({
            $and: [
                {pretty_id},
                {$or: orQuery},
            ],
        });
    } else if (inviteTeamMemberRequest.user_id) {
        existingTeamMember = await mongooseCompanyRepo.findOne({
            $and: [
                {pretty_id},
                {'team_members.user_id': inviteTeamMemberRequest.user_id}],
        });
    } else if (inviteTeamMemberRequest.username) {
        const user = await mongooseUserRepo.findOneByUsername(inviteTeamMemberRequest.username);

        if (user) {
            existingTeamMember = await mongooseCompanyRepo.findOne({
                $and: [
                    {pretty_id},
                    {'team_members.user_id': user._id}],
            });
        }
    }

    if (existingTeamMember) {
        errorUtils.throwError('This company teamMember already exists', 400);
    }
}

async function checkThatDesignationExists(designatioName) {
    const results = await mongooseDesignationRepo.findAll({designation: designatioName})

    if(results.length === 0) errorUtils.throwError('Designation was not found', 400);
}

async function findInvitedUser(inviteTeamMemberRequest) {
    if (inviteTeamMemberRequest.email) {
        return await mongooseUserRepo.findOneByEmail(inviteTeamMemberRequest.email);
    } else if (inviteTeamMemberRequest.username) {
        return await mongooseUserRepo.findOneByUsername(inviteTeamMemberRequest.username);
    } else if (inviteTeamMemberRequest.user_id) {
        return await mongooseUserRepo.findOneById(inviteTeamMemberRequest.user_id);
    }
    errorUtils.throwError('No query parameter provided', 400);
}

const BODY_SCHEMA = {
    type: 'object',
    properties: {
        email: {
            type: 'string',
            minLength: 1,
        },
        user_id: {
            type: 'string',
            minLength: 1,
        },
        username: {
            type: 'string',
            minLength: 1,
        },
        designation: {
            type: 'string',
            minLength: 1,
        },
    },
    additionalProperties: false,
};

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1,
        },
    },
};
