const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseUserRepo = require('../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../services/common/safeObjects');
const errorUtils = require('../../services/error/errorUtils');
const routeUtils = require('../../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const teamMemberExtractor = require('./teamMemberExtractor');

exports.register = function register(app) {
    app.get('/users/:user_id/team_memberships',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getTeamMembers));
};

async function getTeamMembers(req, res) {
    let userId = safeObjects.sanitize(req.params.user_id);

    let user = await mongooseUserRepo.findOneById(userId);

    if (!user) {
        errorUtils.throwError("User does not exist!", 404);
    }

    let companiesInvitedTo = await mongooseCompanyRepo.findAll({'team_members.user_id': user._id});

    let teamMembersToReturn = [];

    let userTeamMemberArray = []; // it is an array of array. Subarray has 3 elements. [teamMember, companyId, company]

    for (let company of companiesInvitedTo) {
        let userTeamMemberies = company.team_members.filter(teamMember => teamMember.user_id === user._id.toString()); //Filter?
        for (let teamMember of userTeamMemberies) {
            userTeamMemberArray.push([teamMember,company._id,company]);
        }
    }

    teamMembersToReturn = await Promise.all(userTeamMemberArray.map((userCompnayTeamMemberPair,index) =>{
        return extractTeamMemberWrapper(userCompnayTeamMemberPair[0],userCompnayTeamMemberPair[1],userCompnayTeamMemberPair[2]);
    }));

    res.status(200).send(teamMembersToReturn);
}

async function extractTeamMemberWrapper(teamMember, companyId, company){
    let teamMemberToReturn = await teamMemberExtractor.extractTeamMember(teamMember, companyId);
    teamMemberToReturn.company = {
        pretty_id: company.pretty_id,
        id: company._id,
        logo_s3: company.logo_s3,
        logo_ipfs: company.logo_ipfs,
        company_name: company.company_name
    };
    return teamMemberToReturn;
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['user_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};