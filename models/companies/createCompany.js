const mongooseCompanyRepo = require('../services/mongo/mongoose/mongooseCompanyRepo');
const Company = require('../services/mongo/mongoose/schemas/company');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const coverUpdateService = require('./coverUpdateService');
const logoUpdateService = require('./logoUpdateService');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;
const companyAdminService = require('./companyAdminService');
const createVirtualEntityTxExecutor = require('../services/ethereum/connections/txExecutors/createVirtualEntityTxExecutor');

const REQUEST_FIELD_LIST = ['pretty_id', 'company_name', 'description', 'admin', 'logo_data', 'cover_data',
    'social_links', 'additional_data', 'email', 'visible_to_public'];

exports.register = function register(app) {
    app.post('/companies',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(createCompany));
};

async function createCompany(req, res) {
    let createCompanyRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let companyWithIdExists = await mongooseCompanyRepo.findOneByPrettyId(createCompanyRequest.pretty_id);

    if (companyWithIdExists) {
        errorUtils.throwError('This company id is already in use', 400);
    }

    if (createCompanyRequest.email) {
        let companyWithEmailExists = await mongooseCompanyRepo.findOneByEmail(createCompanyRequest.email);

        if (companyWithEmailExists) {
            errorUtils.throwError('This company email is already in use', 400);
        }
    }

    if (createCompanyRequest.admin) {
        let adminID = await companyAdminService.getAdminId(createCompanyRequest);

        createCompanyRequest.admin_username = createCompanyRequest.admin;
        createCompanyRequest.admin = adminID;
    }

    await logoUpdateService.updateLogo(createCompanyRequest, createCompanyRequest.logo_data, createCompanyRequest.pretty_id);
    delete createCompanyRequest.logo_data;

    if (createCompanyRequest.cover_data) {
        await coverUpdateService.updateCover(createCompanyRequest, createCompanyRequest.cover_data, createCompanyRequest.pretty_id);
        delete createCompanyRequest.cover_data;
    }

    createCompanyRequest.created_by_admin = true;
    createCompanyRequest.last_updated_by = req.username;
    createCompanyRequest.last_updated_timestamp = Date.now();

    if(createCompanyRequest.visible_to_public){
        createCompanyRequest.visible_to_public = true;
    }else{
        createCompanyRequest.visible_to_public = false;
    }
    let createdCompanyId = await mongooseCompanyRepo.insert(createCompanyRequest);

    let companyToReturn = await mongooseCompanyRepo.findOneById(createdCompanyId);

    await createVirtualEntityTxExecutor.execute(companyToReturn._id.toString(), req.user_id);

    res.status(200).send(Company.toSafeObject(companyToReturn));
}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company
    if (req.body.additional_data && !(req.permissions && req.permissions.admin && req.permissions.admin.write)) {
        return false;
    }

    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            pretty_id: {
                type: 'string',
                minLength: 1,
                maxLength: 64
            },
            email: {
                type: 'string',
                pattern: "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
            },
            logo_data: {
                type: 'string',
                minLength: 1,
                maxLength: 1000000
            },
            cover_data: {
                type: 'string',
                minLength: 1,
                maxLength: 1000000
            },
            company_name: {
                type: 'string',
                minLength: 1,
                maxLength: 255
            },
            description: {
                type: 'string',
                minLength: 1,
                maxLength: 1000
            },
            admin: {
                type: 'string',
                pattern: "\\w{12}|[aA-fF0-9]{24}"
            },
            visible_to_public: {
                type: 'boolean'
            },
            additional_data: {
                type: 'object'
            },
            social_links: {
                type: 'array',
                items: {
                    type: 'object',
                    properties: {
                        type: {
                            type: 'string',
                            minLength: 1,
                            maxLength: 64
                        },
                        url: {
                            type: 'string',
                            minLength: 1
                        }
                    },
                    required: ['type', 'url'],
                    additionalProperties: false
                }
            }
        },
        required: ['company_name', 'pretty_id', 'logo_data'],
        additionalProperties: false
    };
}


