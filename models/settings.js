// Settings module that other files can get settings from
var config = require('config');

// Load Server Credentials

module.exports.ENVIRONMENT = process.env.NODE_ENV;
module.exports.LAMBDA_ENABLED = config.get('aws.lambdaEnabled');
module.exports.AWS_LAMBDA_REGION = config.get('aws.lambdaRegion');

//CLAIMS
module.exports.CLAIM_VOTE_PERIOD = config.get('claims.votePeriod');
module.exports.CLAIM_VALIDATOR_COUNT = config.get('claims.validatorCount');

// RABBIT_MQ
module.exports.AMPLITUDE_API_TOKEN = config.get('amplitude.apiToken');
const RABBITMQ_USER = config.get('graphql.rabbitmq.user');
const RABBITMQ_PASS = config.get('graphql.rabbitmq.password');
const RABBITMQ_HOST = config.get('graphql.rabbitmq.host');
const RABBITMQ_PORT = config.get('graphql.rabbitmq.port');

if (config.get('server.termsVersion') && config.get('server.privacyVersion')) {
    module.exports.TERMS_VERSION = config.get('server.termsVersion');
    module.exports.PRIVACY_VERSION = config.get('server.privacyVersion');
} else {
    module.exports.TERMS_VERSION = 0;
    module.exports.PRIVACY_VERSION = 0;
}

//setup Ethereum environment
if (process.env.NODE_ENV === 'staging') {
    module.exports.ETH_NETWORK = 'RINKEBY';
} else if (process.env.NODE_ENV === 'production') {
    module.exports.ETH_NETWORK = 'MAINNET';
} else {
    module.exports.ETH_NETWORK = 'LOCAL';
}

if (process.env.NODE_ENV === 'production') {
    module.exports.SLACK_CLAIMS_WEBHOOK = config.get('slack.claimsWebhook');
    module.exports.SLACK_BUGS_WEBHOOK = config.get('slack.bugsWebhook');
}

if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'integration') {

    module.exports.SLACK_CLAIMS_WEBHOOK = config.get('slack.claimsWebhook');
    module.exports.SLACK_BUGS_WEBHOOK = config.get('slack.bugsWebhook');


    // Load AWS Credentials
    module.exports.AWS_REGION = config.get('aws.region');

    // Load Mailgun Credentials

    module.exports.MAILGUN_API_KEY = config.get('mailgun.apiKey');
    module.exports.MAILGUN_DOMAIN = config.get('mailgun.domain');

    //Mandrill API Key
    module.exports.MANDRILL_API_KEY = config.get('mandrill.apiKey');


    // Load Server Credentials

    const DATABASE_HOST = module.exports.DATABASE_HOST = config.get('server.databaseHost');
    const DATABASE_PORT = module.exports.DATABASE_PORT = config.get('server.databasePort');
    const DATABASE_USER = module.exports.DATABASE_USER = config.get('server.databaseUser');
    const DATABASE_PASSWORD = module.exports.DATABASE_PASSWORD = config.get('server.databasePassword');
    const DATABASE_NAME = module.exports.DATABASE_NAME = config.get('server.databaseName');

    if (config.get('server.databaseAuthenticationDisabled') == 1) {
        module.exports.DATABASE_CONNECTION_STRING = 'mongodb://' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_NAME;
    } else {
        module.exports.DATABASE_CONNECTION_STRING = 'mongodb://' + DATABASE_USER + ':' + DATABASE_PASSWORD + '@' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_NAME
    }


    module.exports.INDORSE_SIGN_MSG = config.get('ethereum.sign_data')

    module.exports.CONNECTIONS_SIGNER_LAMBDA_NAME = config.get('aws.connectionsContract.signerLambdaName');
    module.exports.INDORSETOKEN_SIGNER_LAMBDA_NAME = config.get('aws.indorseTokenContract.signerLambdaName');
    module.exports.CLAIMS_SIGNER_LAMBDA_NAME = config.get('aws.claimsContract.signerLambdaName');

    module.exports.ETHER_BALANCE_CHECK = config.get('ethereum.etherBalanceCheck');
    module.exports.IND_BALANCE_CHECK = config.get('ethereum.indorseTokenContract.indBalanceCheck');
    module.exports.BALANCE_CHECK_EMAIL_INTERVAL = config.get('ethereum.etherBalanceCheck');
    module.exports.ETHEREUM_TECH_SUPPORT_EMAIL = config.get('ethereum.techSupportEmail');

    module.exports.RABBITMQ_CONFIG = {
        host: RABBITMQ_HOST,
        port: RABBITMQ_PORT,
        login: RABBITMQ_USER,
        password: RABBITMQ_PASS,
        connectionTimeout: 10000,
        ssl: {
            enabled: false
        }
    }

} else {
    const DATABASE_HOST = module.exports.DATABASE_HOST = config.get('server.databaseHost');
    const DATABASE_PORT = module.exports.DATABASE_PORT = config.get('server.databasePort');
    const DATABASE_NAME = module.exports.DATABASE_NAME = config.get('server.databaseName');

    if (config.get('server.databaseAuthenticationDisabled') == 1) {
        module.exports.DATABASE_CONNECTION_STRING = 'mongodb://' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_NAME;
    } else {
        const DATABASE_USER = module.exports.DATABASE_USER = config.get('server.databaseUser');
        const DATABASE_PASSWORD = module.exports.DATABASE_PASSWORD = config.get('server.databasePassword');
        module.exports.DATABASE_CONNECTION_STRING = 'mongodb://' + DATABASE_USER + ':' + DATABASE_PASSWORD + '@' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_NAME
    }

    module.exports.INDORSE_SIGN_MSG = config.get('ethereum.sign_data')

    module.exports.INFURA_CONNECTION_URL = 'https://rinkeby.infura.io/' + config.get('ethereum.infuraToken')

    module.exports.CONNECTIONS_MOCK_PK = config.get('ethereum.connectionsContract.mockLambda.pk');
    module.exports.CONNECTIONS_MOCK_ADDRESS = config.get('ethereum.connectionsContract.mockLambda.address');
    module.exports.INDORSETOKEN_MOCK_PK = config.get('ethereum.indorseTokenContract.mockLambda.pk');
    module.exports.INDORSETOKEN_MOCK_ADDRESS = config.get('ethereum.indorseTokenContract.mockLambda.address');
    module.exports.CLAIMS_MOCK_PK = config.get('ethereum.claimsContract.mockLambda.pk');
    module.exports.CLAIMS_MOCK_ADDRESS = config.get('ethereum.claimsContract.mockLambda.address');
    //Mandrill API Key
    module.exports.MANDRILL_API_KEY = config.get('mandrill.apiKey');

    module.exports.RABBITMQ_CONFIG = {
        host: RABBITMQ_HOST,
        port: RABBITMQ_PORT,
        login: RABBITMQ_USER,
        password: RABBITMQ_PASS,
        connectionTimeout: 10000,
        ssl: {
            enabled: false
        }
    }
}

