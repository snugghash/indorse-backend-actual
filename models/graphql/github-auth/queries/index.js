const githubGetRepos = require('./githubGetRepos');

module.exports = {
    Query: {
        githubGetRepos: githubGetRepos
    }
}