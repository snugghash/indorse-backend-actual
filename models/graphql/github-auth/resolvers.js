const merge = require('lodash').merge;

const mutations = require('./mutations');
const queries = require('./queries');

module.exports = merge(mutations, queries);
