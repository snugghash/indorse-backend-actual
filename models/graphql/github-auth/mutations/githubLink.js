const errorUtils = require('../../../services/error/errorUtils');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const createClaimService = require('../../../services/claims/createClaimService');
const safeObjects = require('../../../services/common/safeObjects');
const socialSignup = require("../../../services/social/socialSignupService");
const jwt = require('jsonwebtoken');
const config = require('config');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const logger = require('../../../services/common/logger').getLogger();
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const githubUtils = require("../../../services/social/githubService");
const socialLogin = require("../../../services/social/socialLoginService");

const githubLink = async (root, {code, state, redirect_uri}, {req, res}) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let user = await socialLogin.findUserByEmail(req.email);
    if (!user) {
        errorUtils.throwError('Logged in user not found. Somethign went wrong', 403);
    }

    let linkRequest = safeObjects.safeObjectParse({code: code, state: state, redirect_uri: redirect_uri
    }, ['code', 'state', 'redirect_uri']);


    let accessToken = await githubUtils.exchangeAuthorizationCode(linkRequest.code, linkRequest.state, linkRequest.redirect_uri);
    let githubUserAndRepoDataObj = await githubUtils.getGithubUserAndRepoData(accessToken);
    let authType = "github";

    let githubUid = githubUserAndRepoDataObj.id;
    let profileUrl = githubUserAndRepoDataObj.avatarUrl;

    user.github_uid = githubUid;

    if (!githubUid || !profileUrl || !accessToken) {
        logger.debug('Github authentication error : githubUid ' + githubUid + ' profileUrl ' + profileUrl);
        errorUtils.throwError("Authentication failed", 401); // any error in decodedSignupToken will be catched here. Only valid token will lead to defined variable
    }
    await githubUtils.updateGithubDataOfUser(githubUserAndRepoDataObj, user);
    await mongooseUserRepo.update({email: user.email}, {$set: {githubProfileURL: profileUrl, github_uid: githubUid}});
    let token = await socialLogin.setToken(user, user.email);
    amplitudeTracker.publishData('link', {type: authType}, user._id);

    return {
        userExists: true,
        token: token
    }

};

function userIsAuthorized(req) {
    return req.login;
}

module.exports = githubLink;

