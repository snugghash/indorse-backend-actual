const githubLogin = require('./githubLogin');
const githubSignup = require('./githubSignup');
const githubLink = require('./githubLink');

module.exports = {
    Mutation: {
        githubLogin: githubLogin,
        githubSignup: githubSignup,
        githubLink: githubLink
    }
}
