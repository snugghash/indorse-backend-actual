const errorUtils = require('../../../services/error/errorUtils');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const createClaimService = require('../../../services/claims/createClaimService');
const safeObjects = require('../../../services/common/safeObjects');
const socialSignup = require("../../../services/social/socialSignupService");
const jwt = require('jsonwebtoken');
const config = require('config');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const newUserProcedures = require('../../../services/auth/newUserProcedure');
const logger = require('../../../services/common/logger').getLogger();
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const githubUtils = require("../../../services/social/githubService");

const githubSignup = async (root, {token,username,name,claimToken}, {req, res}) => {


    let newUserRequest = safeObjects.safeObjectParse({token: token, username: username, name: name,claimToken:claimToken
    }, ['token', 'username', 'name','claimToken']);

    let [email, githubUid, profileUrl, accessToken] = await socialSignup.decodeGithubSignupToken(newUserRequest.token);

    if(!githubUid|| !email || !profileUrl || !accessToken){
        logger.debug('Github authentication error : githubUid ' + githubUid + ' email ' + email + ' profileUrl ' + profileUrl);
        errorUtils.throwError("Authentication failed", 401); // any error in decodedSignupToken will be catched here. Only valid token will lead to defined variable
    }
    let githubUserAndRepoDataObj = await githubUtils.getGithubUserAndRepoData(accessToken);

    if(githubUserAndRepoDataObj.id !== githubUid){
        logger.debug("Access token for debug: " + accessToken);
        logger.debug("Github_uid from JWT " + githubUid);
        errorUtils.throwError("JWT token has wrong access token",500);
    }
    // TODO: Should get these 3 variables from githubUserAndRepoDataObj
    newUserRequest.github_uid = githubUid;
    newUserRequest.profileUrl = profileUrl;
    newUserRequest.email = email;

    if (newUserRequest.claimToken) {
        let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(newUserRequest.claimToken);

        if (!claimDraft) {
            errorUtils.throwError("Claim draft not found for this token!", 400);
        }

        if (claimDraft.email !== newUserRequest.email) {
            errorUtils.throwError("User email does not match claims email!", 400);
        }

        if (claimDraft.finalized) {
            errorUtils.throwError("This claim draft has already been finalized!", 400);
        }

        await socialSignup.completeSocialSignup(req, newUserRequest, 'github','claimToken');
        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);

        let userItem = Object.assign({}, userToLogin);
        let token = cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});
        await githubUtils.updateGithubDataOfUser(githubUserAndRepoDataObj, userToLogin);

        await newUserProcedures.startLinkingProcedures(newUserRequest);

        await mongooseUserRepo.update({_id: userToLogin._id}, {
            tokens: [],
            verified: true,
            role: 'full_access',
            approved: true,
            verify_token: ""
        });

        let claimCreationRequest = {
            title: claimDraft.title,
            desc: claimDraft.desc,
            proof: claimDraft.proof,
            level: claimDraft.level
        };

        await createClaimService.createClaim(claimCreationRequest, userToLogin);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);


        return{
            token:token,
            message : "Successful signup and claim creation!"
        }

    } else {

        await socialSignup.completeSocialSignup(req, newUserRequest, 'github');
        let userToLogin = await mongooseUserRepo.findOneByEmail(newUserRequest.email);
        let userItem = Object.assign({}, userToLogin);
        let token = cryptoUtils.generateJWT(userItem);
        userToLogin.tokens.push(token);

        await mongooseUserRepo.update({email: newUserRequest.email}, {$set: {tokens: userToLogin.tokens}});
        await githubUtils.updateGithubDataOfUser(githubUserAndRepoDataObj, userToLogin);

        await newUserProcedures.startLinkingProcedures(newUserRequest);
        console.log(token, userItem, 'token, userItem');
        return{
            token:token,
            message : "User successfully logged in"
        }
    }
    
};

function userIsAuthorized(req) {
    return req.login && req.permissions && req.permissions.full_access && req.permissions.full_access.write;
}

module.exports = githubSignup;

