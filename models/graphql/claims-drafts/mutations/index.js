const createClaimDraft = require('./createClaimDraft');
const submitClaimDraftToken = require('./submitClaimDraftToken');

module.exports = {
    Mutation: {
        createClaimDraft: createClaimDraft,
        submitClaimDraftToken: submitClaimDraftToken
    }
}
