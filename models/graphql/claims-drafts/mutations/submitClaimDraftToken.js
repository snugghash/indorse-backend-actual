const errorUtils = require('../../../services/error/errorUtils');
const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const createClaimService = require('../../../services/claims/createClaimService');
const safeObjects = require('../../../services/common/safeObjects');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');

const submitClaimDraftToken = async (root, {token, source}, {req, res}) => {

    if(!source) {
        source = "page";
    }

    let submitTokenRequest = safeObjects.safeObjectParse({token: token}, ['token']);

    let claimDraft = await mongooseClaimDraftsRepo.findOneByToken(submitTokenRequest.token);

    if (!claimDraft) {
        errorUtils.throwError("No claim draft exists for that token!", 400);
    }

    if (claimDraft.finalized) {
        errorUtils.throwError("This claim draft has already been finalized!", 400);
    }

    let claimant = await mongooseUserRepo.findOneByEmail(claimDraft.email);

    if (claimant) {
        let newclaimDraft = JSON.parse(JSON.stringify(claimDraft));
        delete newclaimDraft.email;
        await createClaimService.createClaim(newclaimDraft, claimant);
        await mongooseClaimDraftsRepo.markAsFinalized(claimDraft.email);

        amplitudeTracker.publishData('claim_draft_token_submitted', {source : source}, claimant._id.toString());

        return {
            email: claimDraft.email,
            draftFinalized: true,
            userExists: true
        }
    } else {

        amplitudeTracker.publishData('claim_draft_token_submitted_user_doesnt_exist', {source : source}, "unregistered");

        return {
            email: claimDraft.email,
            draftFinalized: false,
            userExists: false
        }
    }
};

module.exports = submitClaimDraftToken;
