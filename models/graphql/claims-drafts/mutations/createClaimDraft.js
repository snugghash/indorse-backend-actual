const mongooseClaimDraftsRepo = require('../../../services/mongo/mongoose/mongooseClaimDraftsRepo');
const claimsEmailService = require('../../../services/common/claimsEmailService');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const safeObjects = require('../../../services/common/safeObjects');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const randtoken = require('rand-token');

// Note: When you make a change in this function, please also check githubSignup.spec.js (should signup a user with Github and a claim) test case, and please modify logic there if needed
const createClaimDraft = async (root, {email, title, desc, proof, level, source}, {req, res}) => {

    if(!source) {
        source = "page";
    }

    let claimDraft = safeObjects.safeObjectParse({
        title:title,
        desc:desc,
        proof:proof,
        level:level,
        email: email,
        source : source
    },['title','desc','proof','level','email']);

    claimDraft.token = randtoken.generate(16);
    await mongooseClaimDraftsRepo.insert(claimDraft);

    let user = await mongooseUserRepo.findOneByEmail(email);
    let userExists = user ? true : false;

    claimsEmailService.sendClaimDraftLinkEmail(claimDraft.token,email, source);

    amplitudeTracker.publishData('claim_draft_created', {source: source}, req.user_id, req.device_id);

    return {
        userExists: userExists
    };
};

module.exports = createClaimDraft;
