module.exports = `
	enum ClaimLevel {
		beginner
		intermediate
		expert
	}
	
	type ClaimDraft {
	    title: String!
	    desc: String!
	    proof: URL
	    level: ClaimLevel!
	}
	
	type CreateDraftClaimResp {
		userExists : Boolean!
	}
	
	type SubmitClaimDraftTokenResp {
	    email: EmailAddress!
		draftFinalized: Boolean!
		userExists: Boolean!
	}
	
	type Mutation {
		createClaimDraft(email: EmailAddress!, title: String!, desc: String!, proof: URL, level: ClaimLevel!, source: String): CreateDraftClaimResp,         
		submitClaimDraftToken(token: String!, source: String): SubmitClaimDraftTokenResp  
	}
`;
