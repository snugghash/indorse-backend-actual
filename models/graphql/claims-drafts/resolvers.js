const merge = require('lodash').merge;

const mutations = require('./mutations');

module.exports = merge(mutations);
