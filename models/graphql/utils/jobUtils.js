const mongooseCompanyRepo = require('../../services/mongo/mongoose/mongooseCompanyRepo');
const mongooseSkillRepo = require('../../services/mongo/mongoose/mongooseSkillRepo');
const mongooseJobsRepo = require('../../services/mongo/mongoose/mongooseJobsRepo');
const mongoJobsRepo = require('../../services/mongo/mongoRepository')('jobs');
const mongooseUserJobRepo = require('../../services/mongo/mongoose/mongooseUserJobsRepo');
const errorUtils = require('../../services/error/errorUtils');
const settings = require('../../settings');
const s3Service = require('../../services/blob/s3Service');
const ipfsService = require('../../services/blob/ipfsService');
const config = require('config');
const ObjectID = require('mongodb').ObjectID;
const stringUtils = require('../../services/blob/stringUtils');

module.exports.addSkillsToJob = async function addSkillsToJob(form, jobToCreate) {
    let skillsToCreate = [];
    for(let i = 0; i < form.skills.length; i++) {
        const skillForm = form.skills[i];
        const skillFromMongo = await mongooseSkillRepo.findOneById(skillForm.id);

        if (!skillFromMongo) {
            errorUtils.throwError("Skill was not found", 400);
        }

        let skillToCreate = {
            id: skillForm.id,
            level: skillForm.level,
            required: true
        };
        skillsToCreate.push(skillToCreate);
    }
    jobToCreate.skills = skillsToCreate;
}

module.exports.formatCompanyFromMongo = function formatCompanyItem(companyForm, companyFromMongo) {
    let id, name
    id = companyFromMongo._id.toString();    
    name = companyFromMongo.company_name;
    return [id, name];
}

module.exports.addCompanyToJob = async function addCompanyToJob(form, jobToCreate) {
    const companyForm = form.company;
    let companyToCreate;
    let id, name;
    if (companyForm.id) {
        const companyFromMongo = await mongooseCompanyRepo.findOneById(form.company.id);

        if (!companyFromMongo) {
            errorUtils.throwError("Company was not found", 400);
        }

        [id, name] = this.formatCompanyFromMongo(companyForm, companyFromMongo);
    } else {
        const companyFromMongo = await mongooseCompanyRepo.findOne({
            company_name:
                {$regex: '^' + companyForm.name + '$', $options: 'i'}
        });

        if (companyFromMongo) {
            [id, name] = this.formatCompanyFromMongo(companyForm, companyFromMongo);
        } else {
            let companyNameToInsert = {
                company_name: companyForm.name
            };

            const company_id = await mongooseCompanyRepo.insert(companyNameToInsert);
            id = company_id.toString();
            name = companyForm.name;
        }
    }

    companyToCreate = {
        id: id,
        name: name,
        description: companyForm.description
    };

    await updateImage(form, companyToCreate);
    jobToCreate.company = companyToCreate;
}

module.exports.prepareJobObject =async function prepareJobObject(job){   
    //Needed as a guard for create , when id is already populated and it might not
    //have a _id attribute common to all mongo objects
    if (!job.id) {
        job.id = job._id.toString();
    }

    //Get company details //TODO error handling
    let company = await mongooseCompanyRepo.findOneById(job.company.id);
    job.company.name = company.company_name;
    job.company.pretty_id = company.pretty_id;

    //Get skill details 
    for (skill of job.skills) {
        let mongoSkill = await mongooseSkillRepo.findOneById(skill.id);
        skill.name = mongoSkill.name;
        skill.category = mongoSkill.category;
    }
}
/*
    This function matches the user skill to the required skill level
*/
function matchUserSkillLevel(userSkill,requiredSkillId,requiredSkillLevelArr){
    //Check if the required skill is same as we are iterating over
    if (userSkill.skill._id.toString() !== requiredSkillId){
        return false;
    }

    let requiredSkillLevel = requiredSkillLevelArr[userSkill.skill._id];

    let userSkillLevel = userSkill.level;
    //Ensure users skill level is same or better than
    if (requiredSkillLevel === "expert" && userSkillLevel=="expert"){
        return true
    } else if (requiredSkillLevel === "intermediate" && (userSkillLevel === "expert" ||
              userSkillLevel ==="intermediate")){
        return true;
    } else if (requiredSkillLevel === "beginner" && 
               ((userSkillLevel === "expert" || userSkillLevel === "intermediate" || userSkillLevel === "beginner"))){
        return true;
    }
    return false;
}

module.exports.userMeetsSkillRequirement = async function userMeetsSkillRequirement(userSkills,jobSkills){    
    
    if (!userSkills){
        return false;
    }

    var skillIds = jobSkills.map(function (e) {        
        return ObjectID(e.id);
    });

    let skillLevel = {};
    for (jobSkill of jobSkills){
        skillLevel[jobSkill.id] = jobSkill.level
    }   

    let allReqValidatedSkills = await mongooseSkillRepo.findAll( {$and :
    [
        { $or: [{ 'validation.chatbot': true }, { 'validation.aip': true }] },
        { _id: {$in : skillIds}}

    ]});
    for (requiredSkill of allReqValidatedSkills){
        let userIsValidated = false;
        let requiredSkillId = requiredSkill._id.toString();        

        for(userSkill of userSkills){
            //let requiredSkillLevel = skillLevel[userSkill.skill._id]; //Can be null if user skill is not a required skill
            if (matchUserSkillLevel(userSkill, requiredSkillId, skillLevel)){
                for(validation of userSkill.validations){
                    if(validation.validated){
                        userIsValidated = true;
                    }
                }
            }
        }
        if(!userIsValidated){
            return false;
        }            
    }   
    return true;
}

module.exports.userAppliedForJob = async function userAppliedForJob(userId, jobId) {
    let userJob = await mongooseUserJobRepo.findOne({
        $and: [{ user: userId} , {job : jobId}]           
    })

    if(userJob)
        return true;
    
    return false;
}



async function updateImage(form, companyToCreate) {
    
    if (form.company.logo && uploadsAreEnabled()) {
        let timeKey = Math.floor(Date.now() / 1000);
        let key = companyToCreate.id + timeKey;

        let bufferedData = new Buffer(form.company.logo.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        await s3Service.uploadFile(key, bufferedData);
        let hash = await ipfsService.uploadFile(bufferedData);

        companyToCreate.logo = {
            s3Url: config.get('aws.s3Bucket') + ".s3.amazonaws.com/" + key +
            "?t=" + timeKey,
            ipfsUrl: hash
        }       
    }
}

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production'
        || settings.ENVIRONMENT === 'test_tmp' || settings.ENVIRONMENT === 'integration';
}


module.exports.userIsAuthorized = function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}

module.exports.prepareSortObj = function prepareSortObj(sort) {
    let field = 'submitted.at';
    let order = -1;

    if (sort) {
        if (sort.startsWith('asc_')) {
            order = 1;
            field = sort.substring(4);
        } else if (sort.startsWith('desc_')) {
            order = -1;
            field = sort.substring(5);
        }

        if (order) {
            switch(field) {
                case 'experience_level':
                    field = 'experienceLevel';
                    break;
                case 'submitted_at':
                    field = 'submitted.at';
                    break;
                case 'updated_at':
                    field = 'updated.at';
                    break;
                case 'title':
                case 'location':
                    break;
                default:
                    field = 'submitted.at'
            }
        }
    }

    if (order && field) {
        return { [field]: order };
    }

    return {};
}

module.exports.searchJobs = async function searchJobs(search, onlyApprovedJobs) {
    const searchRegex = stringUtils.createSafeRegexObject(search);
    const searchDbFilters = [{title: searchRegex}, {location: searchRegex}, {description: searchRegex}];

    const companies = await mongooseCompanyRepo.findAllWithProjection({ company_name: searchRegex }, { _id: 1 });
    const companyIds = companies.map(c => c._id.toString());
    if (companyIds && companyIds.length && companyIds.length > 0) {
        searchDbFilters.push({ 'company.id': { $in: companyIds } });
    }

    const skills = await mongooseSkillRepo.findAllWithProjection({ name: searchRegex }, { _id: 1 });
    const skillIds = skills.map(s => s._id.toString());
    if (skillIds && skillIds.length && skillIds.length > 0) {
        searchDbFilters.push({ 'skills.id': { $in: skillIds } });
    }

    let jobsCursor;
    if (onlyApprovedJobs) {
        jobsCursor = await mongoJobsRepo.findAllWithCursor( { $and: [{'approved.approved': true}, { $or: searchDbFilters}] } );
    } else {
        jobsCursor = await mongoJobsRepo.findAllWithCursor( { $or: searchDbFilters} );
    }

    let matchingJobPosts = await jobsCursor.count();

    return [jobsCursor, matchingJobPosts]
}

module.exports.userIsLoggedIn = function userIsLoggedIn(req) {
    return req.login && req.permissions;
}
