const merge = require('lodash').merge
const ScalarResolvers = require('./scalar/resolvers');
const BadgeResolvers = require('./badge/resolvers');
const ClaimsDraftsResolvers = require('./claims-drafts/resolvers');
const GithubAuthResolvers = require('./github-auth/resolvers');
const JobsResolvers = require('./jobs/resolvers');
const UserJobsResolvers = require('./userJobs/resolvers');
const LinkedinArchiveResolvers = require('./linkedinarchive/resolvers');

module.exports = merge(
  ScalarResolvers,
  BadgeResolvers,
  ClaimsDraftsResolvers,
  GithubAuthResolvers,
  JobsResolvers,
  UserJobsResolvers,
  LinkedinArchiveResolvers
);

