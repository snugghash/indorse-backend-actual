const mergeTypes = require('merge-graphql-schemas').mergeTypes;
const ScalarModels = require('./scalar/models');
const BadgeModels = require('./badge/models');
const JobsModels = require('./jobs/models');
const ClaimsDraftsModels = require('./claims-drafts/models');
const GithubAuthModels = require('./github-auth/models');
const UserJobsModels = require('./userJobs/models');
const LinkedinArchiveModels = require('./linkedinarchive/models');

module.exports = mergeTypes([
	ScalarModels,
	BadgeModels,
	ClaimsDraftsModels,
	GithubAuthModels,
    JobsModels,
    UserJobsModels,
    LinkedinArchiveModels
 ], { all: true });

