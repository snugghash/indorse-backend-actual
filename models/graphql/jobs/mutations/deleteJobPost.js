const mongoJobsRepo = require('../../../../models/services/mongo/mongoose/mongooseJobsRepo');
const safeObjects = require('../../../services/common/safeObjects');
const errorUtils = require('../../../services/error/errorUtils');
const jobUtils = require('../../utils/jobUtils')
const ObjectID = require('mongodb').ObjectID;


const deleteJobPost = async (root, { id}, { req, res }) => {
    id = safeObjects.sanitize(id);

    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to delete job!', 403);
    }
    await mongoJobsRepo.deleteOne({ _id: ObjectID(id)});    
    return id;
}

module.exports = deleteJobPost;