const createJobPost = require('./createJobPost');
const updateJobPost = require('./updateJobPost');
const updateJobPostStatus = require('./updateJobPostStatus');
const deleteJobPost = require('./deleteJobPost')
const applyJobPost = require('./applyJobPost');

module.exports = {
    Mutation: {
        createJobPost: createJobPost,
        updateJobPost: updateJobPost,
        updateJobPostStatus: updateJobPostStatus,
        deleteJobPost: deleteJobPost,
        applyJobPost: applyJobPost
    }
}