const mongoJobsRepo = require('../../../../models/services/mongo/mongoose/mongooseJobsRepo'); //TODO 
const safeObjects = require('../../../services/common/safeObjects');
const errorUtils = require('../../../services/error/errorUtils');
const timestampService = require('../../../services/common/timestampService');
const jobUtils = require('../../utils/jobUtils')
const ObjectID = require('mongodb').ObjectID;


const updateJobPost = async (root, {id ,form}, { req, res }) => {
    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to update job!', 403);
    }

    safeObjects.sanitizeMultiple(id, form);

    //TODO Error handling
    form.updated ={}
    form.updated.at = timestampService.createTimestamp();
    form.updated.by = req.user_id;

    //Ensure right skills are being added
    if(form.skills){
        await jobUtils.addSkillsToJob(form, form);
    }

    //Ensure creation of company if not present on the object
    await jobUtils.addCompanyToJob(form, form);  
    await mongoJobsRepo.findOneAndUpdatePosting({ _id: ObjectID(id) }, form);    
    form.id =id;
    await jobUtils.prepareJobObject(form);

    return form;
}

module.exports = updateJobPost;