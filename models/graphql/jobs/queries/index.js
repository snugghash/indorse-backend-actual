const adminJobPosts = require('./adminJobPosts');
const jobPosts = require('./jobPosts');
const jobById = require('./jobById');

module.exports = {
    Query: {
        adminJobPosts: adminJobPosts,
        jobPosts: jobPosts,
        jobById: jobById
    }
}