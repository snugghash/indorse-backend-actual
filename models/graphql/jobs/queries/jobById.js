const safeObjects = require('../../../services/common/safeObjects');
const mongoJobsRepo = require('../../../../models/services/mongo/mongoose/mongooseJobsRepo');
const mongoUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongoUserJobsRepo = require('../../../../models/services/mongo/mongoose/mongooseUserJobsRepo');
const errorUtils = require('../../../services/error/errorUtils');
const jobUtils = require('../../utils/jobUtils')

const jobById = async (root, {id}, { req, res }) => {
    if (!jobUtils.userIsLoggedIn(req)) {
        errorUtils.throwError('Insufficient permission request job', 403);
    }

    let mongoId = safeObjects.sanitize(id);
    let job = await mongoJobsRepo.findOneById(mongoId);

    let skipApply = true;
    let isAdmin = jobUtils.userIsAuthorized(req);
    let user = await mongoUserRepo.findOneByEmail(req.email);

    if (!isAdmin) {
        if (!job.approved || !job.approved.approved)
            errorUtils.throwError('Job is not approved', 403);
        
        if (!await jobUtils.userMeetsSkillRequirement(user.skills, job.skills)) {
            skipApply = true;
        } else {
            skipApply = false;
        }
    }
    await jobUtils.prepareJobObject(job);        

    if (skipApply && !isAdmin){        
        delete job.contactEmail;
        delete job.applicationLink;
    }

    job.applied = await jobUtils.userAppliedForJob(user._id.toString(),id);

    return job;
}

module.exports = jobById;