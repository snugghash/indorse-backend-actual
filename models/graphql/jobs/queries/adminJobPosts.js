const mongoJobsRepo = require('../../../../models/services/mongo/mongoRepository')('jobs'); //TODO
const safeObjects = require('../../../services/common/safeObjects');
const errorUtils = require('../../../services/error/errorUtils');
const jobUtils = require('../../utils/jobUtils');

const adminJobPosts = async (root, { pageNumber, pageSize, sort, search } , {req, res}) => {
    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to list jobs!', 403);
    }

    safeObjects.sanitizeMultiple(pageNumber, pageSize, sort, search);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }
    
    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let allJobsCursor = await mongoJobsRepo.findAllWithCursor({ _id: { $exists: true } }); 
    let totalJobPosts = await allJobsCursor.count()

    let jobsCursor, matchingJobPosts;

    if (search) {
        [jobsCursor, matchingJobPosts] = await jobUtils.searchJobs(search, false);
    } else {
        jobsCursor = allJobsCursor;
        matchingJobPosts = totalJobPosts;
    }

    let sortObj = jobUtils.prepareSortObj(sort);
    let sortedJobs = await jobsCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    let jobsToReturn = [];

    for(job of sortedJobs){
        await jobUtils.prepareJobObject(job);
        jobsToReturn.push(job);
    }
    
    let responseObj = {
        jobPosts: jobsToReturn,
        totalJobPosts: totalJobPosts,
        matchingJobPosts: matchingJobPosts
    };
    
    return responseObj;
}

module.exports = adminJobPosts;