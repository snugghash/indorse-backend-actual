module.exports = `
    enum ExperienceLevel {
		intern
		junior
		intermediate
		senior
	}
	
	enum SkillLevel {
		beginner
		intermediate
		expert
	}
	
	enum JobsSortParam {
		asc_title
		desc_title
		asc_experience_level
		desc_experience_level
		asc_location
		desc_location
		asc_submitted_at
		desc_submitted_at
		asc_updated_at
		desc_updated_at
	}
	
	input JobsSkillForm {
        id: String!
        level: SkillLevel
    }
    
    input JobsCompany {
		id: String
		name: String
		logo: String
		description: String!
	}
	
	input JobPostForm {
		title: String!
		experienceLevel: ExperienceLevel!
		description: String!
		monthlySalary: String!
		company: JobsCompany!
		contactEmail: EmailAddress!
		applicationLink: String
		location:String
		skills: [JobsSkillForm]
		location: String
	}
	
	type JobLogo {
	    s3Url: String
	    ipfsUrl: String
	}
	
	type JobCompany {
        id: String!
        pretty_id: String
        name: String!
        logo: JobLogo
        description: String!
    }
	
	type JobPostApplicationResult {
		url: URL
		email: EmailAddress
	}
	
	type JobPostAuditItem {
		by: String
		at: Int
	}
	
	type JobPostApprovalStatus {
		by: String
		at: Int
		approved: Boolean
	}
	
	type JobSkill {
		id: String!
		name: String!
		category: String!
		level: SkillLevel!
	}
	
	type JobPost {
		id: String!
		title: String!
		experienceLevel: ExperienceLevel!
		description: String!
		monthlySalary: String!
		company: JobCompany!
		contactEmail: EmailAddress
		applicationLink: String
		skills: [JobSkill]
		location : String
		submitted: JobPostAuditItem
		approved: JobPostApprovalStatus
		updated: JobPostAuditItem
		applied : Boolean
	}
	
	type JobPostList {
		jobPosts: [JobPost]
		matchingJobPosts: Int
		totalJobPosts: Int!
	}
	
    type Query {
		adminJobPosts(pageNumber: Int, pageSize: Int, sort: JobsSortParam, search: String): JobPostList
		jobPosts(pageNumber: Int, pageSize: Int, sort: JobsSortParam, search: String): JobPostList
		jobById(id: String!): JobPost
	}
	
	type Mutation {
		createJobPost(form: JobPostForm): JobPost
		updateJobPost(id: String!, form: JobPostForm): JobPost
		updateJobPostStatus(id: String!, status : Boolean!): JobPost
		deleteJobPost(id: String!): String
		applyJobPost(id: String!): JobPostApplicationResult
	}`	