module.exports = `
    type Applicant {
        id: String!
        name: String
        username: String
        email: EmailAddress
        appliedAt: Int!
    }
	
	type Applicants {
	    applicants: [Applicant],
	    totalJobApplicants: Int
	}
	
    type Query {
		jobApplicantsById(jobId: String!, pageNumber: Int, pageSize: Int): Applicants
	}`