const safeObjects = require('../../../services/common/safeObjects');
const errorUtils = require('../../../services/error/errorUtils');
const jobUtils = require('../../utils/jobUtils');
const mongoUserJobsRepo = require('../../../../models/services/mongo/mongoRepository')('userjobs'); //TODO
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');

const jobApplicantsById = async (root, {jobId, pageNumber, pageSize}, { req, res }) => {
    if (!jobUtils.userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission request job', 403);
    }

    safeObjects.sanitizeMultiple(jobId, pageNumber, pageSize);

    if (!pageNumber) {
        pageNumber = 1;
    }

    if (!pageSize) {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    let jobApplicantsCursor = await mongoUserJobsRepo.findAllWithCursor({job: jobId});
    let totalJobApplicants = await jobApplicantsCursor.count();

    let jobApplicants = await jobApplicantsCursor.sort({appliedAt: 1}).skip(skip).limit(limit).toArray();

    let applicantsToReturn = [];

    for (userJob of jobApplicants) {
        let user = await mongooseUserRepo.findOneById(userJob.user);

        applicantsToReturn.push({
            id: userJob.user,
            name: user.name,
            username: user.username,
            email: user.email,
            appliedAt: userJob.appliedAt
        })
    }

    return {
        applicants: applicantsToReturn,
        totalJobApplicants: totalJobApplicants
    }
}

module.exports = jobApplicantsById;