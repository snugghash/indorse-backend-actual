const jobApplicantsById = require('./queries/jobApplicantsById');

module.exports = {
    Query: {
        jobApplicantsById: jobApplicantsById
    }
}