module.exports = `
  type Mutation {
    uploadLinkedinArchive(file: Upload!): Boolean
  }
`;