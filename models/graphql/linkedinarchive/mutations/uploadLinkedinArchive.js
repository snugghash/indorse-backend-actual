const s3Service = require('../../../services/blob/s3Service');
const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const mongooseImportedContactsRepo = require('../../../services/mongo/mongoose/mongooseImportedContactsRepo');
const mongooseConnectionsRepo = require('../../../services/mongo/mongoose/mongooseConnectionsRepo');
const mongooseSchoolRepo = require('../../../services/mongo/mongoose/mongooseSchoolRepo');
const mongooseSkillRepo = require('../../../services/mongo/mongoose/mongooseSkillRepo');
const mongooseCompanyRepo = require('../../../services/mongo/mongoose/mongooseCompanyRepo');
const cryptoUtils = require('../../../services/common/cryptoUtils');
const timestampService = require('../../../services/common/timestampService');
const amplitudeTracker = require('../../../services/tracking/amplitudeTracker');
const moment = require('moment');
const nodeZip = new require('node-zip');
const csv = require('csvtojson');
const fs = require('fs');


const uploadLinkedinArchive = async (root, {file}, {req, res}) => {
    const {stream, filename, mimetype, encoding} = await file;
    let pathToSave = req.user_id + "-" + timestampService.createTimestamp() + ".zip";

    // store file locally, remove later because of PDPA ;zzz
    await storeFS({
        stream,
        filename: pathToSave
    });

    let csvFound;

    amplitudeTracker.publishData('linkedinArchiveUpload', {feature_category: 'profile'}, req.user_id);


    let data = fs.readFileSync(pathToSave);

    console.log("LinkedinParse data ", data);

    s3Service.uploadLinkedinArchive(data, pathToSave);
    let zip = new nodeZip(data, {base64: false, checkCRC32: true});
    await fs.unlink(pathToSave);
    let requestingUser = await mongooseUserRepo.findOneByEmail(req.email);

    let newEducationEntries;
    let newWorkEntries;
    let newSkillEntries;
    let newContactEntries;
    let newConnectionEntries;

    console.log("LinkedinParse zip ", zip);
    console.log("LinkedinParse zip.files ", JSON.stringify(zip.files));

    if (zip.files["Education.csv"]) {
        csvFound = true;
        newEducationEntries = [];

        let educationCSVRows = await csv().fromString(zip.files["Education.csv"]._data);

        for (let csvRow of educationCSVRows) {
            console.log("LinkedinParse education row ", csvRow);
            let schoolName = csvRow['School Name'];
            let school_id = await getSchoolId(schoolName);
            let newEducationEntry = {
                school_id: school_id,
                school_name: schoolName,
                item_key: cryptoUtils.generateItemKey(),
                degree: csvRow['Degree Name'],
                activities: csvRow['Activities'],
                description: csvRow['Notes'],
            };

            if (csvRow['Start Date']) {
                newEducationEntry.start_date = {year : Number(csvRow['Start Date'])};
            }

            if (csvRow['End Date']) {
                newEducationEntry.end_date = {year : Number(csvRow['End Date'])};
            }

            newEducationEntries.push(newEducationEntry)
        }
    }

    if (zip.files["Positions.csv"]) {
        csvFound = true;
        newWorkEntries = [];

        let workCSVRows = await csv().fromString(zip.files["Positions.csv"]._data);

        for (let csvRow of workCSVRows) {
            console.log("LinkedinParse position row ", csvRow);

            let companyName = csvRow['Company Name'];
            let company_id = await getCopmanyID(companyName);
            let newWorkEntry = {
                company_id: company_id,
                item_key: cryptoUtils.generateItemKey(),
                title: csvRow['Title'],
                location: csvRow['Location'],
                description: csvRow['Description'],
            };

            if (csvRow['Started On']) {
                newWorkEntry.start_date = convertStringDate(csvRow['Started On']);
            }

            if (csvRow['Finished On']) {
                newWorkEntry.end_date = convertStringDate(csvRow['Finished On']);
            } else {
                newWorkEntry.currently_working = true;
            }
            newWorkEntries.push(newWorkEntry)
        }
    }

    if (zip.files["Skills.csv"]) {
        csvFound = true;
        newSkillEntries = [];

        let skillCSVRows = await csv().fromString(zip.files["Skills.csv"]._data);

        for (let csvRow of skillCSVRows) {
            console.log("LinkedinParse skill row ", csvRow);

            let skillName = csvRow['Name'];

            let existingSkill = await mongooseSkillRepo.findOne({name: skillName.toLowerCase()});
            let userHasSkill = requestingUser.skills.find(skill => skill.skill && skill.skill.name === skillName.toLowerCase());

            if (existingSkill && !userHasSkill) {
                newSkillEntries.push({skill: existingSkill, level: "beginner"});
            }
        }
    }

    if (zip.files["Imported Contacts.csv"]) {
        csvFound = true;
        newContactEntries = [];

        let contactsCSVRows = await csv().fromString(zip.files["Imported Contacts.csv"]._data);

        for (let csvRow of contactsCSVRows) {
            console.log("LinkedinParse contract row ", csvRow);

            let newContactEntry = {
                user_id: req.user_id,
                first_name: csvRow['First Name'],
                last_name: csvRow['Last Name'],
                companies: csvRow['Companies'],
                title: csvRow['Title'],
                email_address: csvRow['Email Address'],
                phone_numbers: csvRow['Phone Numbers'],
                created_at: csvRow['Created At'],
                addresses: csvRow['Addresses'],
                instant_message_handles: csvRow['Instant Message Handles'],
                sites: csvRow['Sites']
            };

            newContactEntries.push(newContactEntry);
        }
    }

    if (zip.files["Connections.csv"]) {
        csvFound = true;
        newConnectionEntries = [];

        let connectionsCSVRows = await csv().fromString(zip.files["Connections.csv"]._data);

        for (let csvRow of connectionsCSVRows) {

            let newConnectionEntry = {
                user_id: req.user_id,
                first_name: csvRow['First Name'],
                last_name: csvRow['Last Name'],
                email_address: csvRow['Email Address'],
                company: csvRow['Company'],
                position: csvRow['Position'],
                connected_on: csvRow['Connected On'],
            };

            newConnectionEntries.push(newConnectionEntry);
        }
    }

    if (newEducationEntries || newWorkEntries || newSkillEntries) {
        let updateObj = {
            $push: {}
        };

        if (newEducationEntries) {
            updateObj["$push"].education = {$each: newEducationEntries};
        }
        if (newWorkEntries) {
            updateObj["$push"].work = {$each: newWorkEntries};
        }
        if (newSkillEntries) {
            updateObj["$push"].skills = {$each: newSkillEntries};
        }

        console.log("LinkedinParse updateObj ", updateObj);

        await mongooseUserRepo.update({email: req.email}, updateObj);
    }

    if (newContactEntries) {
        await mongooseImportedContactsRepo.insertMany(newContactEntries);
    }

    if (newConnectionEntries) {
        await mongooseConnectionsRepo.insertMany(newConnectionEntries);
    }

    if (!csvFound) {
        throw new Error("No csvs found!");
    } else {
        return true;
    }
};

function convertStringDate(dateString) {
    let date = dateString.split(' ');
    let isYear = moment(date[1], "YYYY").isValid();
    let safeYear;
    if (isYear) {
        safeYear = date[1];
    }
    let safeMonth = moment().month(date[0]).format("M");
    return {month: safeMonth, year: safeYear};
}

async function getCopmanyID(company_name) {
    let company_id;
    let company = await mongooseCompanyRepo.findOne({'company_name': company_name});

    if (company) {
        company_id = company['_id'].toString();
    } else {
        let companyNameItem = {
            company_name: company_name
        };

        company_id = await mongooseCompanyRepo.insert(companyNameItem);
    }
    return company_id;
}

async function getSchoolId(school_name) {
    let school_id;
    let school = await mongooseCompanyRepo.findOne({'school_name': school_name});

    if (school) {
        school_id = school['_id'].toString();
    } else {
        let schoolNameItem = {
            school_name: school_name
        };

        school_id = await mongooseSchoolRepo.insert(schoolNameItem);
    }
    return school_id;
}

const storeFS = ({stream, filename}) => {
    return new Promise((resolve, reject) =>
        stream
            .on('error', error => {
                if (stream.truncated)
                // Delete the truncated file
                    fs.unlinkSync(filename);
                reject(error)
            })
            .pipe(fs.createWriteStream(filename))
            .on('error', error => reject(error))
            .on('finish', () => resolve({filename}))
    )
};

module.exports = uploadLinkedinArchive;
