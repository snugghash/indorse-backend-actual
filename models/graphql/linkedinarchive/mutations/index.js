const uploadLinkedinArchive = require('./uploadLinkedinArchive');

module.exports = {
    Mutation: {
        uploadLinkedinArchive: uploadLinkedinArchive
    }
};
