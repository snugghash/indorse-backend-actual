const badgeUtils = require('../utils/badgeUtils');

module.exports = `
	type Badge {
	    id: String!
	    name: String
	    type: String
	    description: String
	    logo_data: String
	    issuer: Issuer
	    criteria: Criteria
	    tags: [String]
	    extensions_cta_text: ExtensionsText
	    extensions_cta_link: ExtensionsLink
	    created_at_timestamp: String
	    last_updated_timestamp: String 
	    image: Image
	    chatbot_uuid: String
	}
	
	type AllBadge {
		badges: [Badge]
		totalBadges: Int
	}
	
	type Image {
		icon_s3: String
		icon_ipfs: String
	}

	# hehehe
	
	type Criteria {
		narrative: String
	}
	
	type Issuer {
		id: String
		type: String
		name: String
		url: URL
		email: EmailAddress
		verification: Verification
	}
	
	type Verification {
		allowedOrigins: String
	}
	
	type ExtensionsText {
		context: String
		type: String
		text: String
	}	
	
	type ExtensionsLink {
		context: String
		type: String
		link: URL
	}
	
	type User {
		username: String
		name: String
		bio: String
		img_url: String 
	}
	
	type AllBadgeUsersResp {
		users : [User]
		totalUsers: Int!
	}
	
	enum SortParam {
		  ${badgeUtils.sortParams.ascending.prettyId}
		  ${badgeUtils.sortParams.ascending.badgeName}
		  ${badgeUtils.sortParams.descending.prettyId}
		  ${badgeUtils.sortParams.descending.badgeName}
	}
	
	enum AssignType {
		ASSIGN
		REMOVE
	}
	
	type Query {
		badge(id: String!): Badge
		allBadges(fields: String, pageNumber: Int, pageSize: Int, sort: SortParam, search: String): AllBadge
		allBadgeUsers(id: String, fields: String, pageNumber: Int, pageSize: Int, sort: SortParam, search: String): AllBadgeUsersResp
	}
	
	type Mutation {
		createBadge(id: String! 
		            name: String!
		            description: String!
		            logo_data: String!
		            extensions_cta_text: String!
		            extensions_cta_link: URL!
		            chatbot_uuid: String!): Badge
		            
		updateBadge(id: String!
		 			name: String
		 			description: String
		 			logo_data: String
		 			extensions_cta_text: String
		 			extensions_cta_link: URL
		 			chatbot_uuid: String): Badge
		 			
		deleteBadge(id: String!): String
		
		massAssignBadges(badgeIds: [String]!
		 				assignType: AssignType
		 				emails: [EmailAddress]!): Boolean            
	}
	
	type MassAssignedSubscribe {
		badge: Badge
		email: EmailAddress
		assignType: AssignType
	}
	
	type CompanyCreatedTx {
		username: String
		companyName: String
		status: Boolean
	}
	
	type Subscription {
		massAssignBadgesSubscription(authorization: String!): MassAssignedSubscribe
		companyTxCompleted(authorization: String!): CompanyCreatedTx
	}
	`


