const errorUtils = require('../../../services/error/errorUtils');
const mongooseBadgeRepo = require('../../../services/mongo/mongoose/mongooseBadgeRepo');
const logoUpdateService = require('../../../badges/logoUpdateService');


const createBadge = async (root, {id, name, logo_data, description, extensions_cta_text, extensions_cta_link, chatbot_uuid}, { req, res }) => {

    // auth check
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let badgeWithIdExists = await mongooseBadgeRepo.findOneByPrettyId(id);

    if (badgeWithIdExists) {
        errorUtils.throwError('This badge id is already in use', 400);
    }

    let createBadgeRequest = {
        id:id,
        name:name,
        logo_data:logo_data,
        description:description,
        chatbot_uuid: chatbot_uuid
    };
    await logoUpdateService.updateLogo(createBadgeRequest, createBadgeRequest.logo_data, createBadgeRequest.pretty_id);
    delete createBadgeRequest.logo_data;
    createBadgeRequest.image = {icon_s3 : createBadgeRequest.logo_s3,icon_ipfs : createBadgeRequest.logo_ipfs};
    delete createBadgeRequest.logo_ipfs;
    delete createBadgeRequest.logo_s3;

    createBadgeRequest.tags = ['technology'];
    createBadgeRequest.criteria = {narrative : ''};
    createBadgeRequest.issuer = {id : 'https://indorse.io/issuer',type : 'badge',name : 'Indorse',url : 'https://indorse.io',email : 'badges@indorse.io',verification : {allowedOrigins : 'indorse.io'}};
    createBadgeRequest.extensions_cta_text = {context : '',type : '',text : extensions_cta_text};
    createBadgeRequest.extensions_cta_link = {context : '',type : '',link : extensions_cta_link};
    createBadgeRequest.created_at_timestamp = Date.now();
    createBadgeRequest.last_updated_timestamp = Date.now();

    let createdBadgeId = await mongooseBadgeRepo.insert(createBadgeRequest);

    let badgeToReturn =  await mongooseBadgeRepo.findOneById(createdBadgeId);
    return badgeToReturn;

}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

module.exports = createBadge;