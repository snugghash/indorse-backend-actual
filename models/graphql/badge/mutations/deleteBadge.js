const errorUtils = require('../../../services/error/errorUtils');
const mongooseBadgeRepo = require('../../../services/mongo/mongoose/mongooseBadgeRepo');

const deleteBadge = async (root, {id}, { req, res }) => {

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    await mongooseBadgeRepo.deleteOne({id: id});

    return id;

}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

module.exports = deleteBadge;