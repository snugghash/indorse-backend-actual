const mongooseUserRepo = require('../../../services/mongo/mongoose/mongooseUserRepo');
const mongooseBadgeRepo = require('../../../services/mongo/mongoose/mongooseBadgeRepo');
const safeObjects = require('../../../services/common/safeObjects');
const errorUtils = require('../../../services/error/errorUtils');
const routeUtils = require('../../../services/common/routeUtils');
const validate = require('../../../services/common/validate');
const authChecks = require('../../../services/auth/authChecks');
const roles = require('../../../services/auth/roles');
const pubsub = require('../../pubsub').pubsub;
const pubsubNative = require('amqp-pubsub');
const amqplib = require('amqplib');
const config = require('config');
const settings = require('../../../settings');
const rabbitMqPublish = require('../../../services/rabbitMq/publishToExchange');

/*
 ids = an array of badge ID
 emails = an array of users email
 assignType =
 ASSIGN: assign badges
 REMOVE: remove badges
 */
const massAssignBadge = async (root, {badgeIds, assignType, emails}, {req, res}) => {
    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    for (let email of emails) {
        let user = await mongooseUserRepo.findOneByEmail(email);
        if (!user) {
            errorUtils.throwError("User not found: " + email, 404);
        }
    }
    let badgeArray = [];
    for (let badgeId of badgeIds) {
        let badge = await mongooseBadgeRepo.findOneByPrettyId(badgeId);

        if (!badge) {
            errorUtils.throwError("Badge not found: " + badgeId, 404);
        }
        badgeArray.push(badge);
    }
    


    for (let email of emails) {
        if (assignType === "ASSIGN") {
            await mongooseUserRepo.update({email: email}, {$addToSet: {badges: {$each: badgeIds}}});
            for (let badge of badgeArray) {
                let message = {
                    email: email,
                    assignType: assignType,
                    badge: badge
                }

                await rabbitMqPublish.publishToExchange('Badge.Assigned.DLQ.Exchange', { massAssignBadgesSubscription: message });
            }
        } else if (assignType === "REMOVE") {
            await mongooseUserRepo.update({email: email}, {$pullAll: {badges: badgeIds}});
            for (let badge of badgeArray) {
                let message = {
                    email: email,
                    assignType: assignType,
                    badge: badge
                }

                await rabbitMqPublish.publishToExchange('Badge.Assigned.DLQ.Exchange', { massAssignBadgesSubscription: message });

            }
        }
    }

    return true;
}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

module.exports = massAssignBadge;