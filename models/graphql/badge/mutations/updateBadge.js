const errorUtils = require('../../../services/error/errorUtils');
const mongooseBadgeRepo = require('../../../services/mongo/mongoose/mongooseBadgeRepo');
const logoUpdateService = require('../../../badges/logoUpdateService');
const safeObjects = require('../../../services/common/safeObjects');

const updateBadge = async (root, {id, name, description, logo_data, extensions_cta_text, extensions_cta_link, chatbot_uuid}, { req, res }) => {

    let updateBadgeRequest = {};

    let safeId = safeObjects.sanitize(id);

    let badgeToUpdate = await mongooseBadgeRepo.findOneByPrettyId(safeId);

    if (!badgeToUpdate) {
        errorUtils.throwError("Badge not found!", 404);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    if (logo_data) {
        updateBadgeRequest.logo_data = logo_data;
        await
            logoUpdateService.updateLogo(updateBadgeRequest, updateBadgeRequest.logo_data, safeId);
        delete updateBadgeRequest.logo_data;
    }

    if (name) {
        updateBadgeRequest.name = name;
    }
    if (updateBadgeRequest.logo_s3) {
        updateBadgeRequest.image = {icon_s3: updateBadgeRequest.logo_s3, icon_ipfs: updateBadgeRequest.logo_ipfs};
        delete updateBadgeRequest.logo_ipfs;
        delete updateBadgeRequest.logo_s3;
    }

    if (extensions_cta_text) {
        updateBadgeRequest.extensions_cta_text = {context: '', type: '', text: extensions_cta_text};
    }
    if (extensions_cta_link) {
        updateBadgeRequest.extensions_cta_link = {context: '', type: '', link: extensions_cta_link};
    }
    updateBadgeRequest.last_updated_timestamp = Date.now();
    updateBadgeRequest.chatbot_uuid = chatbot_uuid;

    await mongooseBadgeRepo.update({id: safeId}, {$set: updateBadgeRequest});

    return await mongooseBadgeRepo.findOneByPrettyId(safeId);
}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

module.exports = updateBadge;