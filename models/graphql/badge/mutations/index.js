const createBadge = require('./createBadge');
const updateBadge = require('./updateBadge');
const deleteBadge = require('./deleteBadge');
const massAssignBadges = require('./massAssignBadges');

module.exports = {
    Mutation: {
        createBadge: createBadge,
        updateBadge: updateBadge,
        deleteBadge: deleteBadge,
        massAssignBadges: massAssignBadges
    }
}