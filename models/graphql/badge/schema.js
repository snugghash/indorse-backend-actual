const makeExecutableSchema = require('graphql-tools').makeExecutableSchema;
const resolvers = require('./resolvers');
const typeDefs = require('./models');

const schema = makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers,
});

module.exports = schema;
