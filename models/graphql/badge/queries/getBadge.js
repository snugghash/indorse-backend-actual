const mongoBadgeRepo = require('../../../services/mongo/mongoRepository')('badges');
const stringUtils = require('../../../services/blob/stringUtils')

const getBadge = async (root, {id}, { headers }) => {
    return await mongoBadgeRepo.findOne({id: stringUtils.createSafeRegexObject(id)});
}

module.exports = getBadge;