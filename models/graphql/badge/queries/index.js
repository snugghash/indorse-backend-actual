const getBadge = require('./getBadge');
const getAllBadges = require('./getAllBadges');
const allBadgeUsers = require('./getBadgeUsers');

module.exports = {
    Query: {
        badge: getBadge,
        allBadges: getAllBadges,
        allBadgeUsers:allBadgeUsers
    }
}