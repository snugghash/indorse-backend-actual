const merge = require('lodash').merge;

const mutations = require('./mutations');
const queries = require('./queries');
const subscriptions = require('./subscriptions');

module.exports = merge(mutations, queries, subscriptions);
