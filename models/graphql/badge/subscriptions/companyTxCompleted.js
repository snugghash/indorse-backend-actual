const withFilter = require('graphql-subscriptions').withFilter;
const pubsub = require('../../pubsub').pubsub;
const errorUtils = require('../../../services/error/errorUtils');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../../../services/auth/auth');

const companyTxCompleted = {
    subscribe: withFilter(
        (_, args, {authorization}) => {
            return pubsub.asyncIterator('Company.Created');
            },
        (payloads, {authorization}, context) => {
            let token = authorization.split(" ")[1];
            let req = {};
            req['token'] = token;
            auth(req,'',function(){return;});
            return req.username === payloads.companyTxCompleted.username;
        }
    )
};


module.exports = companyTxCompleted;