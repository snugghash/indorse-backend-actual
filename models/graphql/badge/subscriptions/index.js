const massAssignBadgesSubscription = require('./massAssignBadgesSubscription');
const companyTxCompleted = require('./companyTxCompleted');

module.exports = {
    Subscription: {
        massAssignBadgesSubscription: massAssignBadgesSubscription,
        companyTxCompleted: companyTxCompleted
    }
};
