const withFilter = require('graphql-subscriptions').withFilter;
const pubsub = require('../../pubsub').pubsub;
const errorUtils = require('../../../services/error/errorUtils');
const jwt = require('jsonwebtoken');
const config = require('config');
const auth = require('../../../services/auth/auth');

const massAssignBadgesSubscription = {
    subscribe: withFilter(
        (_, args, {authorization}) =>{
            return pubsub.asyncIterator('Badge.Assigned');
        } ,
        (payloads, {authorization}, context) => {
            let token = authorization.split(" ")[1];
            let req = {};
            req['token'] = token;
            auth(req,'',function(){return;});
            return req.email === payloads.massAssignBadgesSubscription.email;
        }
    )
};

module.exports = massAssignBadgesSubscription;
