const makeExecutableSchema = require('graphql-tools').makeExecutableSchema;
const mergeSchemas = require('graphql-tools').mergeSchemas;

const resolvers = require('./resolvers');
const typeDefs = require('./models');
const normalSchema = makeExecutableSchema({ typeDefs, resolvers });

module.exports = mergeSchemas({
   schemas: [normalSchema]
});
