const safeObjects = require('../services/common/safeObjects');
const mongoBadgeRepo = require('../services/mongo/mongoRepository')('badges');
const mongoUserRepo = require('../services/mongo/mongoRepository')('users');
const routeUtils = require('../services/common/routeUtils');
const errorUtils = require('../services/error/errorUtils');
const validate = require('../services/common/validate');
const stringUtils = require('../services/blob/stringUtils');

exports.register = function register(app) {
    app.get('/badges/:pretty_id/users',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {

    let searchParam, pageNumber, pageSize, sortObj, fields;

    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    if (req.query.fields) {
        fields = safeObjects.sanitize(req.query.fields).split(',');
    }


    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.pageNumber) {
        pageNumber = safeObjects.sanitize(req.query.pageNumber);
    } else {
        pageNumber = 1;
    }

    if (req.query.pageSize) {
        pageSize = safeObjects.sanitize(req.query.pageSize);
    } else {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    if (req.query.sort) {
        let sortParam = safeObjects.sanitize(req.query.sort);
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            company_name: 1
        }
    }

    let userCursor, matchingUsers;

    if (searchParam) {
        userCursor = await mongoUserRepo.findAllWithCursor(
            {
                badges : pretty_id,
                $or: [{username: stringUtils.createSafeRegexObject(searchParam)},
                    {name: stringUtils.createSafeRegexObject(searchParam)}]
            });

        matchingUsers = await userCursor.count();
    } else {
        userCursor = await mongoUserRepo.findAllWithCursor({badges: pretty_id});
    }

    let sortedUsers = await userCursor.sort(sortObj).skip(skip).limit(limit).toArray();

    fields = ['name','username','_id']; //hardcoding user fields to a limited set for now
    let usersToReturn = sortedUsers.map((user) => {
        if (fields) {
            let filteredUser = {};
            fields.forEach(field => {
                if (user[field]) {
                    filteredUser[field] = user[field];
                }
            });
            return filteredUser;
        } else {
            return user;
        }
    });

    let userProfileCursor = await mongoUserRepo.findAllWithCursor({badges: pretty_id});

    let responseObj = {
        users: usersToReturn,
        totalUsers: await userProfileCursor.count()
    };

    if (matchingUsers) {
        responseObj.matchingUsers = matchingUsers;
    }

    res.status(200).send(responseObj);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};

