const mongooseBadgeRepo = require('../services/mongo/mongoose/mongooseBadgeRepo');
const Badge = require('../services/mongo/mongoose/schemas/badge');
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const logoUpdateService = require('./logoUpdateService');
const routeUtils = require('../services/common/routeUtils');
const {Validator, ValidationError} = require('express-json-validator-middleware');
const validator = new Validator({allErrors: true});
const validate = validator.validate;

const REQUEST_FIELD_LIST = ['pretty_id', 'badge_name', 'description','logo_data','cta_text','cta_link'];

exports.register = function register(app) {
    app.post('/badges',
        validate({body: getRequestSchema()}),
        routeUtils.asyncMiddleware(createBadge));
};

async function createBadge(req, res) {
    let createBadgeRequest = safeObjects.safeReqestBodyParser(req, REQUEST_FIELD_LIST);

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    let badgeWithIdExists = await mongooseBadgeRepo.findOneByPrettyId(createBadgeRequest.pretty_id);

    if (badgeWithIdExists) {
        errorUtils.throwError('This badge id is already in use', 400);
    }

    await logoUpdateService.updateLogo(createBadgeRequest, createBadgeRequest.logo_data, createBadgeRequest.pretty_id);
    delete createBadgeRequest.logo_data;

    createBadgeRequest.name = createBadgeRequest.badge_name;
    createBadgeRequest.id = createBadgeRequest.pretty_id;
    createBadgeRequest.image = {icon_s3 : createBadgeRequest.logo_s3, icon_ipfs : createBadgeRequest.logo_ipfs};
    delete createBadgeRequest.logo_ipfs;
    delete createBadgeRequest.logo_s3;
    createBadgeRequest.tags = ['technology'];
    createBadgeRequest.criteria = {narrative : ''};
    createBadgeRequest.issuer = {id : 'https://indorse.io/issuer',type : 'badge',name : 'Indorse',url : 'https://indorse.io',email : 'badges@indorse.io',verification : {allowedOrigins : 'indorse.io'}};
    createBadgeRequest.extensions_cta_text = {context : '',type : '',text : createBadgeRequest.cta_text};
    createBadgeRequest.extensions_cta_link = {context : '',type : '',link : createBadgeRequest.cta_link};
    createBadgeRequest.created_at_timestamp = Date.now();
    createBadgeRequest.last_updated_timestamp = Date.now();

    let createdBadgeId = await mongooseBadgeRepo.insert(createBadgeRequest);

    let badgeToReturn = await mongooseBadgeRepo.findOneById(createdBadgeId);

    res.status(200).send(badgeToReturn);
}

function userIsAuthorized(req) {
    //Only superuser can add additional_data to company

    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}

function getRequestSchema() {
    return {
        type: 'object',
        properties: {
            pretty_id: {
                type: 'string',
                minLength: 1,
                maxLength: 64
            },
            logo_data: {
                type: 'string',
                minLength: 1,
                maxLength: 1000000
            },
            badge_name: {
                type: 'string',
                minLength: 1,
                maxLength: 255
            },
            description: {
                type: 'string',
                minLength: 1,
                maxLength: 1000
            },
            cta_text: {
                type: 'string',
                minLength: 1,
                maxLength: 64
            },
            cta_link: {
                type: 'string',
                minLength: 1,
                maxLength: 1000
            },
        },
        required: ['badge_name', 'pretty_id', 'logo_data','description','cta_text','cta_link'],
        additionalProperties: false
    };
}


