const mongoBadgeRepo = require('../services/mongo/mongoRepository')('badges');
const safeObjects = require('../services/common/safeObjects');
const validate = require('../services/common/validate');
const errorUtils = require('../services/error/errorUtils');
const stringUtils = require('../services/blob/stringUtils');

const routeUtils = require('../services/common/routeUtils');

exports.register = function register(app) {
    app.get('/badges/:pretty_id',
        validate({params: PARAMS_SCHEMA}),
        routeUtils.asyncMiddleware(getBadge));
};


async function getBadge(req, res) {
    let pretty_id = safeObjects.sanitize(req.params.pretty_id);

    let badge = await mongoBadgeRepo.findOne({id: stringUtils.createSafeRegexObject(pretty_id)});

    if(!badge) {
        errorUtils.throwError("Badge not found!", 404);
    }

    res.status(200).send(badge);
}

const PARAMS_SCHEMA = {
    type: 'object',
    required: ['pretty_id'],
    properties: {
        pretty_id: {
            type: 'string',
            minLength: 1
        }
    }
};