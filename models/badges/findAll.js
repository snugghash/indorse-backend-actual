const safeObjects = require('../services/common/safeObjects');
const mongoBadgeRepo = require('../services/mongo/mongoRepository')('badges');
const routeUtils = require('../services/common/routeUtils');
const errorUtils = require('../services/error/errorUtils');
const stringUtils = require('../services/blob/stringUtils');

exports.register = function register(app) {
    app.get('/badges',
        routeUtils.asyncMiddleware(findAll));
};

async function findAll(req, res) {

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Insufficient permission to list badges!', 403);
    }

    let searchParam, pageNumber, pageSize, sortObj, fields;

    if (req.query.fields) {
        fields = safeObjects.sanitize(req.query.fields).split(',');
    }


    if (req.query.search) {
        searchParam = safeObjects.sanitize(req.query.search);
    }

    if (req.query.pageNumber) {
        pageNumber = safeObjects.sanitize(req.query.pageNumber);
    } else {
        pageNumber = 1;
    }

    if (req.query.pageSize) {
        pageSize = safeObjects.sanitize(req.query.pageSize);
    } else {
        pageSize = 100;
    }

    let skip = (parseInt(pageNumber) - 1) * parseInt(pageSize);
    let limit = parseInt(pageSize);

    if (req.query.sort) {
        let sortParam = safeObjects.sanitize(req.query.sort);
        if (sortParam[0] === "-") {
            //Reverse order if starts with -
            sortObj = {
                [sortParam.substring(1)]: -1
            }
        } else {
            sortObj = {
                [sortParam]: 1
            }
        }
    } else {
        sortObj = {
            company_name: 1
        }
    }

    let badgeCursor, matchingBadges;

    if (searchParam) {
        badgeCursor = await mongoBadgeRepo.findAllWithCursor(
            {
                $or: [{id: stringUtils.createSafeRegexObject(searchParam)},
                    {name: stringUtils.createSafeRegexObject(searchParam)}]
            });

        matchingBadges = await badgeCursor.count();
    } else {
        badgeCursor = await mongoBadgeRepo.findAllWithCursor({id: {$exists: true}});
    }

    let sortedBadges = await badgeCursor.sort(sortObj).skip(skip).limit(limit).toArray();


    let badgesToReturn = sortedBadges.map((badge) => {
        if (fields) {
            let filteredBadge = {};
            fields.forEach(field => {
                if (badge[field]) {
                    filteredBadge[field] = badge[field];
                }
            });
            return filteredBadge;
        } else {
            return badge;
        }
    });

    let badgeProfileCursor = await mongoBadgeRepo.findAllWithCursor({id: {$exists: true}});

    let responseObj = {
        badges: badgesToReturn,
        totalBadges: await badgeProfileCursor.count()
    };

    if (matchingBadges) {
        responseObj.matchingBadges = matchingBadges;
    }

    res.status(200).send(responseObj);
}

function userIsAuthorized(req) {
    return req.login && req.permissions.admin.read;
}

