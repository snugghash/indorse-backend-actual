const mongooseBadgeRepo = require("../services/mongo/mongoose/mongooseBadgeRepo");
const safeObjects = require('../services/common/safeObjects');
const errorUtils = require('../services/error/errorUtils');
const routeUtils = require('../services/common/routeUtils');

exports.register = function register(app) {
    app.delete('/badges/:pretty_id',
        routeUtils.asyncMiddleware(deleteBadge));
};

async function deleteBadge(req, res) {
    let pretty_id = safeObjects.sanitize(req.param("pretty_id"));

    if (!pretty_id) {
        errorUtils.throwError("Invalid pretty_id", 422);
    }

    if (!userIsAuthorized(req)) {
        errorUtils.throwError('Unauthorized', 403);
    }

    await mongooseBadgeRepo.deleteOne({id: pretty_id});

    res.status(200).send();
}

function userIsAuthorized(req) {
    return req.login && req.permissions && req.permissions.admin && req.permissions.admin.write;
}