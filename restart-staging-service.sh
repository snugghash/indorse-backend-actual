#!/bin/bash

set -e

ECS_REGION="ap-southeast-1"

echo "Configuring AWS..."
aws --version
aws configure set default.region $ECS_REGION
aws configure set default.output json
echo "AWS configured!"


aws ecs update-service --cluster CI-staging --service staging-backend --task-definition staging-backend --force-new-deployment

aws ecs update-service --cluster CI-staging --service staging-cron --task-definition staging-cron --force-new-deployment
