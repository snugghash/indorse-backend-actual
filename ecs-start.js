const app = require('./server');
const mongoUserRepo = require('./models/services/mongo/mongoRepository')('users');
const routeUtils = require('./models/services/common/routeUtils');
const errorUtils = require('./models/services/error/errorUtils');

app.get('/verify_url/:username', routeUtils.asyncMiddleware(async function (req, res) {
    let username = req.param("username");
    if (!username) {
        errorUtils.throwError("Invalid username", 422)
    }

    let user = await mongoUserRepo.findOne({username: username});

    if (!user) {
        errorUtils.throwError("User not found", 404)
    }
    res.send("ecs-staging-frontend.attores.com/verify-email?email=" + user.email + "&token=" + user.verify_token);
}));

app.get('/fulluser/:username', routeUtils.asyncMiddleware(async function (req, res) {
    let username = req.param("username");
    if (!username) {
        errorUtils.throwError("Invalid username", 422)
    }

    let user = await mongoUserRepo.findOne({username: username});

    if (!user) {
        errorUtils.throwError("User not found", 404)
    }
    res.send(user);
}));