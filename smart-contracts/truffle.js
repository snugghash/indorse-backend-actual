module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" 
    },
    rinkeby: {
      host: "localhost",
      port: 8545,
      from: "0x4f0a650961A6Da6367491f497a72Ac49a7D29664",
      network_id: 4,
      gas: 4612388
    },
    kovan: {
      host: "localhost", 
      port: 8545,
      from: "0x4f0a650961A6Da6367491f497a72Ac49a7D29664", 
      network_id: 42,
      gas: 4612388 
    },
    ganache: {
      host: "localhost",
      port: 7545,
      from: "0x627306090abaB3A6e1400e9345bC60c78a8BEf57", 
      network_id: 5777,
      gas: 14612388 
    }            
  }
};
