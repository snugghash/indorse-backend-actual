pragma solidity ^0.4.11;

import "./library/Ownable.sol";
import "./library/SafeMath.sol";
import "./library/Pausable.sol";
import "./IndorseToken.sol";

/*
Variables guide

referee - The user who shared the referral code
newUser - New user to Indorse platform using the code generated above
refPool  -Address where the rewards for referral system are stored

*/

contract IndorseReferral is Ownable, SafeMath, Pausable{

  struct PendingReward{
      address referee;
      address newUser;
      uint256 reward;
      bool    isTransferred;
  }

  // Each user in Indorse can have only one reference code created against his address.
  mapping(address => mapping(bytes => uint256)) refereeCodeReward; //mapping of referee -> reference code -> rewards so far
  mapping(bytes => address) codeReferee; // mapping of reference code -> referee address
  mapping(address => bool) blackListedReferee;

  IndorseToken indorseContract = IndorseToken(0xf8e386eda857484f5a12e4b5daa9984e06e73705);
  address refPool; //Address where tokens rewards are stored

  //map of (users address => (address they referred to =>status if a pending entry exists))
  //using the dual write to prevent duplication of entries.
  //using an array to store the looping aspect
  mapping(address => mapping(address => bool)) pendingRewardsNewUser;

  PendingReward [] pendingRewards;
  uint256 balanceThreshold;
  uint256 lastPendingRewardIndex;

  //Possibly use a smaller data type than uint256
  event rewardUserSuccess(address indexed user, uint256 value);
  event balanceLow(uint256 value);

  //Constructor
  function IndorseReferral(address pool,uint256 bThreshold){
      refPool = pool;
      balanceThreshold = bThreshold;
      return;
  }

  function updateRefPoolAddress(address newRefPool) public onlyOwner{
      refPool =newRefPool;
  }

  function updateBalanceThreshold(uint256 bThreshold) public onlyOwner{
      balanceThreshold = bThreshold;
  }

  function addRefereeReward(address referee,bytes code,uint256 reward) public onlyOwner whenNotPaused{
      refereeCodeReward[referee][code] = safeAdd(refereeCodeReward[referee][code], reward);
      codeReferee[code] = referee;
      return;
  }


  function addRefereeToPendingRewards(address referee,address newUser,uint256 reward) public onlyOwner{
        if(!pendingRewardsNewUser[referee][newUser]){
          PendingReward memory pendingReward;
          pendingReward.referee = referee;
          pendingReward.newUser = newUser;
          pendingReward.reward = reward;
          pendingRewards.push(pendingReward);
          pendingRewardsNewUser[referee][newUser] = true;
      }
      return;
  }

  function distributePendingRewards(uint256 bucket) public onlyOwner whenNotPaused {
      uint256 refPoolBalance = indorseContract.balanceOf(refPool);
      uint256 endIndex = safeAdd(lastPendingRewardIndex,bucket);
      while((lastPendingRewardIndex <= endIndex) && (endIndex < pendingRewards.length)){ //TODO length -1
          PendingReward storage pendingReward = pendingRewards[lastPendingRewardIndex];
          require(!blackListedReferee[pendingReward.referee]);
          if(!pendingReward.isTransferred) {
              require(refPoolBalance > pendingReward.reward);
              refPoolBalance = safeSubtract(refPoolBalance,pendingReward.reward);
              lastPendingRewardIndex = safeAdd(lastPendingRewardIndex,1);
              pendingReward.isTransferred = true;
              require(indorseContract.transferFrom(refPool, pendingReward.referee,pendingReward.reward));
          }
      }
      return;
  }


  function payRewardToReferee(address newUser,bytes code, uint256 reward) public onlyOwner whenNotPaused{
        address referee = codeReferee[code];
        require(refereeCodeReward[referee][code] >= reward);
        require(!blackListedReferee[referee]);
        uint256 refPoolBalance = indorseContract.balanceOf(refPool);
        if(refPoolBalance < reward){
            addRefereeToPendingRewards(referee,newUser,reward);
            balanceLow(refPoolBalance);
            return;
        }
        refereeCodeReward[referee][code] = safeSubtract(refereeCodeReward[referee][code], reward);
        require(indorseContract.transferFrom(refPool,referee,reward));
        rewardUserSuccess(referee,reward);
        return;
  }

  function addBlackListedReferee(address referee) public onlyOwner{
      if(!blackListedReferee[referee]){
        blackListedReferee[referee] = true;
      }
      return;
  }

  function removeBlackListedReferee(address referee) public onlyOwner{
      delete blackListedReferee[referee];
      return;
  }

  //If accidentally ether was sent to it
 function withdraw() public onlyOwner {
        msg.sender.transfer(this.balance);
    }
}
