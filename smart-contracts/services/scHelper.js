// let Tx = require('ethereumjs-tx');
let fs = require("fs");
let settings = require('../../models/settings')
const mongooseEthNwRepo = require('../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
const mongooseContractsRepo = require('../../models/services/mongo/mongoose/mongooseContractsRepo');
let _ = require('lodash');
let path = require('path');
const logger = require("../../models/services/common/logger").getLogger();
const lambdaSigner = require('./lambdaSignerClient');
let web3Initializer = require('./initializers/web3')

const abiDefinitions = {};

async function getContractABI(contractName) {
    logger.debug("Getting ABI for contract " + contractName);
    let contractPath = path.join(__dirname, '../build/contracts/');

    if (!abiDefinitions[contractName]) {

        let file = await new Promise((res, rej) => {
            fs.readFile(contractPath + contractName + '.json', {}, (err, data) => {
                if (err) rej(err)
                else res(data)
            })
        });

        let abiDefinition = JSON.parse(file.toString());
        abiDefinitions[contractName] = abiDefinition.abi;

        return abiDefinitions[contractName];
    } else {
        return abiDefinitions[contractName];
    }
}

async function getContractInstance(web3, contractName, address) {
    let abiDefinition = await getContractABI(contractName);
    let contract = web3.eth.contract(abiDefinition);
    let contractInstance = contract.at(address);
    return contractInstance;
}


//Used only by testing framework , no longer used in production/staging system to send out transaction
//check resolverLoop for the sending of transaction on network.
async function sendTransaction(web3, callData, contractData, connectionsSigner, forceNonce = undefined) {
    let contractAddress = contractData.address;
    let nonceHex = web3.toHex(web3.eth.getTransactionCount(contractData.from)); //TODO : Handle synchro
    let gasLimit = contractData['gas'] ? contractData['gas'] : 200000;
    let ethNw = await mongooseEthNwRepo.findOneByNetwork(settings.ETH_NETWORK);
    let gasPrice = web3.toWei(ethNw.max_gas_price, 'gwei');
    let medianGWEIGasPrice = web3.fromWei(web3.eth.gasPrice, 'gwei') 

    logger.debug("Network median gas price in GWEI :" + medianGWEIGasPrice);
    logger.debug("Max default gas price in GWEI :" + ethNw.max_gas_price);

    if (ethNw.max_gas_price === undefined) {
        logger.error("Max gas price is undefined for the network " + settings.ETH_NETWORK);
        errorUtils.throwError("Max gas price is undefined for network " + settings.ETH_NETWORK, 500);
    }

    if (Number(medianGWEIGasPrice) < Number(ethNw.max_gas_price)) {
        gasPrice = web3.eth.gasPrice;
        logger.info("Setting gas price to the network median");
    }
    let gasPriceHex = web3.toHex(gasPrice);

    let rawTx = {
        nonce: nonceHex,
        gasPrice: gasPriceHex,
        gasLimit: gasLimit,
        to: contractAddress,
        lambda_signer: contractData.from,
        value: 0x0,
        data: callData
    }
    let hex_serialized = await connectionsSigner(rawTx);
    let result = {}
    let hash = await web3.eth.sendRawTransaction(hex_serialized);
    result["txHash"] = hash;
    result["rawTx"] = rawTx;
    logger.info("Sending transaction hash  = " + hash);
    return result;
}


async function getContractMetadata(contractName, fnGas = undefined) {
    let metadata = {};
    let contract = await mongooseContractsRepo.findOneByNameAndNetwork(contractName, settings.ETH_NETWORK);
    metadata['address'] = contract.contract_address;
    metadata['from'] = contract.caller_address;
    if (fnGas !== undefined) {
        let result = _.find(contract.function_metadata, {function_name: fnGas});
        if (result) {
            metadata['gas'] = result.gas;
        }
    }
    return metadata;
}

async function contractCallPromise(contractInstance, functionName, ...callArguments) {
    return new Promise((res, rej) => {
        contractInstance[functionName].call(...callArguments, (err, data) => {
            if (err) rej(err)
            else res(data)
        })
    });
}

async function getWeb3AndContract(contractName, functionName, ...callArguments) {
    // For explanation of ... behaviour: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters
    logger.info(contractName, ':', functionName, '(', callArguments.join(', '), ')');
    let web3 = await web3Initializer.getWeb3();
    let contractData = await getContractMetadata(contractName, functionName);
    let contractInstance = await getContractInstance(web3, contractName, contractData.address);

    return [web3, contractData, contractInstance];
}

exports.logAndSendContractTx = async function logAndSendContractTx(contractName, functionName, ...txArguments) {
    // For explanation of ... behaviour: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters
    let web3, contractData, contractInstance;
    [web3, contractData, contractInstance] = await getWeb3AndContract(contractName, functionName, txArguments)

    let callData = contractInstance[functionName].getData( ...txArguments)
    // For explanation of ... behaviour: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax#A_better_apply
    let signer = getSigner(contractName);
    let txDetails = await sendTransaction(web3, callData, contractData, signer);
    logger.debug("Transaction details : " + JSON.stringify(txDetails));
    return txDetails;
}

exports.logAndCallContract = async function logAndCallContract(contractName, functionName, ...callArguments) {
    let web3, contractData, contractInstance;
    [web3, contractData, contractInstance] = await getWeb3AndContract(contractName, functionName, ...callArguments)

    let response = await contractCallPromise(contractInstance, functionName, ...callArguments)
    return response;
}


async function getTransactionDetails(web3, callData, contractData, connectionsSigner){
    let gasLimit = contractData['gas'] ? contractData['gas'] : 500000;
    let ethNw = await mongooseEthNwRepo.findOneByNetwork(settings.ETH_NETWORK);

    let rawTx = {
        gasLimit: gasLimit,
        to: contractData.address,
        lambda_signer: contractData.from,
        value: 0x0,
        data: callData
    }
    return rawTx;
}


exports.logAndQueueContractTx = async function logAndQueueContractTx(contractName, functionName, ...txArguments){
    let web3, contractData, contractInstance;
    [web3, contractData, contractInstance] = await getWeb3AndContract(contractName, functionName, txArguments)

    let callData = contractInstance[functionName].getData(...txArguments)

    let signer = getSigner(contractName);
    let rawTx = await getTransactionDetails(web3, callData, contractData, signer);
    logger.debug("Transaction details : " + JSON.stringify(rawTx));
    return rawTx;
}

function getSigner(contractName) {
    switch (contractName) {
        case "NaiveClaims":
            return lambdaSigner.claimsLambda;
        case "Connections":
            return lambdaSigner.connectionsLambda;
        case "IndorseToken":
            return lambdaSigner.indorseTokenLambda;
    }
}

//Module exports
exports.getContractABI = getContractABI;
exports.getContractInstance = getContractInstance
exports.sendTransaction = sendTransaction
exports.getContractMetadata = getContractMetadata