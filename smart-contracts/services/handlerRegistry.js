module.exports = {
    Connections: {
        createVirtualEntity: require('./connections/handlers/createVirtualEntityTxHandler'),
        createVirtualEntityAndConnection: require('./connections/handlers/createVirtualEntityAndConnectionTxHandler'),
        removeConnection: require('./connections/handlers/removeConnectionTxHandler'),
        addConnection: require('./connections/handlers/addConnectionTxHandler'),
        createClaim: require('./claims/handlers/createClaimTxHandler')
    },
    IndorseToken: {
        transfer: require('./indorseToken/handlers/transferTxHandler')
    },
    NaiveClaims: {
        createClaim: require('./claims/handlers/createClaimTxHandler')
    }
};
