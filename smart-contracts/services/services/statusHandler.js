const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const timestampService = require('../../../models/services/common/timestampService');

//TODO : Check gas usage (might already be in receipt)
exports.handleFailure = async function handleFailure(txReceipt, transactionDocument) {
    const ERROR_MESSAGE = 'Transaction failed , status 0x0'; 
    await mongooseTransactionRepo.update({_id: transactionDocument._id}
        , {
            $set: {
                receipt: txReceipt,
                status: 'FAILED',
                error: {
                    message: ERROR_MESSAGE
                },
                finalized_timestamp : timestampService.createTimestamp()
            }
        });
};

exports.handleSuccess = async function handleSuccess(txReceipt, transactionDocument) {
    await mongooseTransactionRepo.update({ _id: transactionDocument._id }
        , {
            $set: {
                receipt: txReceipt,
                status: 'SUCCESSFUL',
                finalized_timestamp: timestampService.createTimestamp()
            }
        });
};