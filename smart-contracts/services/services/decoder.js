const SolidityCoder = require("web3/lib/solidity/coder.js");

module.exports = {

    ADDRESS : {
        decode : function decode(bytes) {
            return SolidityCoder.decodeParams(["address"], bytes.replace('0x',''))[0];
        }
    },

    UINT : {
        decode : function decode(bytes) {
            return SolidityCoder.decodeParams(["uint"], bytes.replace('0x',''))[0];
        }
    },

    BYTES_32 : {
        decode : function decode(bytes) {
            return bytes;
        }
    }
};