const connectionsEvents = require('../connections/models/events');
const indorseTokenEvents = require('../indorseToken/events');

const allEvents = connectionsEvents.eventsArray.concat(indorseTokenEvents.eventsArray);
if (hasDuplicateHashes(allEvents)) {
    throw new Error("There is a duplicate event hash, please make sure only one hash exists for each event");
}

function hasDuplicateHashes(arrayOfEvents) {
    let hashesSoFar = [];
    for (let i = 0; i < arrayOfEvents.length; i++) {
        let hash = arrayOfEvents[i].hash;
        if (hashesSoFar.indexOf(hash) !== -1) {
            return true;
        }
        hashesSoFar.push(hash);
    }
    return false;
}

function findByHash(hash) {
    return allEvents.find(event => event.hash === hash);
};

exports.decode = function decode(topics) {

    let eventSignature = topics[0];

    let eventType = findByHash(eventSignature);

    if (!eventType) {
        throw new Error("Unknown event:" + eventSignature);
    }

    let decodedEvent = {};
    decodedEvent.name = eventType.name;

    for (let index = 0; index < eventType.params.length; index++) {
        let param = eventType.params[index];
        decodedEvent[param.name] = eventType.params[index].type.decode(topics[index+1]);
    }

    return decodedEvent;
};