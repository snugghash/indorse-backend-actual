const mongooseTransactionRepo = require('../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const eventDecoder = require('./eventDecoder');
const timestampService = require('../../../models/services/common/timestampService');

exports.extractEvents = async function extractEvents(txReceipt, transactionDocument) {
    try {
        let decodedEvents = txReceipt.logs.map(log => eventDecoder.decode(log.topics));

        let result = {};

        decodedEvents.forEach(event => result[event.name] = event);

        return result;
    } catch (error) {
        await mongooseTransactionRepo.update({_id: transactionDocument._id}
            , {
                $set: {
                    receipt: txReceipt,
                    status: 'CLIENT_ERROR',
                    error: {message: error.message},
                    finalized_timestamp : timestampService.createTimestamp()
                }
            });
        throw error;
    }
};