const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');
const mongo = require('mongodb');
const ObjectID = mongo.ObjectID;

async function updateTransactionHash(txDocument, hash) {
    let txCallbackData = txDocument.tx_metadata.callback_data;

    if (txCallbackData && txCallbackData.type && txCallbackData.company_id && txCallbackData.user_id) {

        switch (txCallbackData.type) {
            case "COMPANY_ADVISOR":
                await mongooseCompanyRepo.setCompanyAdvisorTxHash(txCallbackData.company_id, txCallbackData.user_id, hash);
                break;
            case "COMPANY_TEAM_MEMBER":
                await mongooseCompanyRepo.teamMember.setCompanyTxHash(txCallbackData.company_id, txCallbackData.user_id, hash);                
                break;
        }
    }
    return;
}


exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    let txStatus = parseInt(txReceipt.status, 16);

    if (txStatus === 1) {
        let events = await eventExtractor.extractEvents(txReceipt, transactionDocument);
        let entityAddedEvent = events.entityAdded;
        let connectionAddedEvent = events.connectionAdded;

        if (!transactionDocument.tx_metadata.callback_data || !transactionDocument.tx_metadata.callback_data.company_id) {
            const ERROR_MESSAGE = 'Transaction document is missing company_id';
            await mongooseTransactionRepo.update({_id: transactionDocument._id}
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'CLIENT_ERROR',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }

        if (!connectionAddedEvent) {
            const ERROR_MESSAGE = 'Connection added event was not present';
            await mongooseTransactionRepo.update({_id: transactionDocument._id}
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'REVERT',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }

        if (!entityAddedEvent) {
            const ERROR_MESSAGE = 'Entity added event was not present';
            await mongooseTransactionRepo.update({_id: transactionDocument._id}
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'REVERT',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }
        let companyId = transactionDocument.tx_metadata.callback_data.company_id;
        await updateTransactionHash(transactionDocument, txReceipt.transactionHash);        
        await mongooseCompanyRepo.setEntityEthaddress(companyId, entityAddedEvent.entity);

        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);
    }
};