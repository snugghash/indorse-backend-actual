const mongooseCompanyRepo = require('../../../../models/services/mongo/mongoose/mongooseCompanyRepo');
const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseUserRepo = require('../../../../models/services/mongo/mongoose/mongooseUserRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');
const mongo = require('mongodb');
const ObjectID = mongo.ObjectID;
const rabbitMqPublish = require('../../../../models/services/rabbitMq/publishToExchange')


exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    let txStatus = parseInt(txReceipt.status, 16);

    if (txStatus === 1) {
        let events = await eventExtractor.extractEvents(txReceipt, transactionDocument);
        let entityAddedEvent = events.entityAdded;

        console.log("createVirtualEntityEvent:" + JSON.stringify(entityAddedEvent));

        if (!transactionDocument.tx_metadata.callback_data || !transactionDocument.tx_metadata.callback_data.company_id) {
            const ERROR_MESSAGE = 'Transaction document is missing company_id';
            await mongooseTransactionRepo.update({_id: transactionDocument._id}
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'CLIENT_ERROR',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);
        }

        let companyId = transactionDocument.tx_metadata.callback_data.company_id;

        await mongooseCompanyRepo.setEntityEthaddress(companyId, entityAddedEvent.entity);
        let companyDoc = await mongooseCompanyRepo.findOneById(companyId);
        let userDoc = await mongooseUserRepo.findOneById(transactionDocument.initiating_user_id);

        let message =  {
            companyName: companyDoc.company_name,
            username: userDoc.username,
            status: true
        }
        let exchangeName = 'Company.Created.DLQ.Exchange';
        rabbitMqPublish.publishToExchange(exchangeName, { companyTxCompleted: message });

        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);
    }
};