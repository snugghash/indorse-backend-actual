module.exports = {

    NOT_APPLICABLE : {
        name : 'NotApplicable',
        value : 0
    },

    FORWARDS : {
        name : 'Forwards',
        value : 1
    },

    BACKWARDS : {
        name : 'Backwards',
        value : 2
    },

    INVALID : {
        name : 'Invalid',
        value : 3
    }
};