const types = require('../../services/decoder');

const events = {
    ENTITY_ADDED : {
        name : 'entityAdded',
        hash : '0xbbb1598dca83166487facc52f9481f629b31e41e8f4d530378c58e86b05e843b',
        params : [
            {name : 'entity', type : types.ADDRESS},
            {name : 'owner', type : types.ADDRESS}
        ]
    },

    CONNECTION_ADDED : {
        name : 'connectionAdded',
        hash : '0x356e8b58ffa49eb58d48aa756231b01699dec71eb273a61c1ee94f1887a7c76c',
        params : [
            {name : 'entity', type : types.ADDRESS},
            {name : 'connectionTo', type : types.ADDRESS}
        ]
    },

    CONNECTION_REMOVED : {
        name : 'connectionRemoved',
        hash : '0x4b238bcbf7cb21a3e4744d42aed7c0fae81b61207320ff3f547e542497e3120d',
        params : [
            {name : 'entity', type : types.ADDRESS},
            {name : 'connectionTo', type : types.ADDRESS},
            {name : 'connectionType', type : types.BYTES_32},
        ]
    },
    CLAIM_CREATED: {
        name: 'claimCreated',
        hash: '0x3106ccab17a8e74387554d8ef984d75d6c305d51cbe629c038b0bec803c1aad3',
        params: [
            { name: 'claimId', type: types.UINT }
        ]
    }
};

const eventsArray = [events.ENTITY_ADDED, events.CONNECTION_ADDED, events.CONNECTION_REMOVED, events.CLAIM_CREATED];

module.exports.eventsArray = eventsArray;
module.exports.types = events;