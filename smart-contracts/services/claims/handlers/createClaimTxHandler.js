const mongooseTransactionRepo = require('../../../../models/services/mongo/mongoose/mongooseTransactionRepo');
const mongooseClaimsRepo = require('../../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const statusHandler = require('../../services/statusHandler');
const eventExtractor = require('../../services/eventExtractor');
const timestampService = require('../../../../models/services/common/timestampService');
const logger = require('../../../../models/services/common/logger').getLogger();
const mongo = require('mongodb');
const ObjectID = mongo.ObjectID;

async function updateClaimAddress(txDocument,claimId){    
    let txCallbackData = txDocument.tx_metadata.callback_data;
    let mongoClaimID = txCallbackData.mongoClaimID;
    await mongooseClaimsRepo.updateClaimID(new ObjectID(mongoClaimID), claimId)
    return;
}

exports.handleTx = async function handleTx(txReceipt, transactionDocument) {
    let txStatus = parseInt(txReceipt.status, 16);
    if (txStatus === 1) {
        let events = await eventExtractor.extractEvents(txReceipt, transactionDocument);
        let createClaimEvent = events.claimCreated;
        logger.info("CLAIM CREATION EVENT! " + JSON.stringify(events));
        if (!createClaimEvent){
            const ERROR_MESSAGE = 'Claim created event was not present';
            await mongooseTransactionRepo.update({ _id: transactionDocument._id }
                , {
                    $set: {
                        receipt: txReceipt,
                        status: 'REVERT',
                        error: {
                            message: ERROR_MESSAGE
                        },
                        finalized_timestamp: timestampService.createTimestamp()
                    }
                });
            throw new Error(ERROR_MESSAGE);

        }
        await updateClaimAddress(transactionDocument, createClaimEvent.claimId);
        logger.info("SETTING CLAIM ID TO " + JSON.stringify(createClaimEvent.claimId));
        await statusHandler.handleSuccess(txReceipt, transactionDocument);
    } else if (txStatus === 0) {
        await statusHandler.handleFailure(txReceipt, transactionDocument);
    }
};