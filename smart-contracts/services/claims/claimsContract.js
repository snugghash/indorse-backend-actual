var scHelper = require('../scHelper')
const logger = require("../../../models/services/common/logger").getLogger();
const settings = require("../../../models/settings")
 
module.exports.createClaim = async function createClaim(voters, deadline, claimantAddress) {
    return await scHelper.logAndQueueContractTx("NaiveClaims", "createClaim", voters, deadline, claimantAddress)
}

module.exports.calculateTally = async function calculateTaly(claimAddress,claimId){
    let response = await scHelper.logAndCallContract("NaiveTallyCalculator", "calculateTally", claimAddress,claimId);
    return response;
}

module.exports.getVote = async function getVote(claimId, voter){
    let response = await scHelper.logAndCallContract("NaiveClaims", "getVote", claimId, voter);
    return response;
}