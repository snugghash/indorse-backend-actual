const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const mongooseVoteRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const timestampService = require('../../../models/services/common/timestampService');
const claimEmailService = require('../../../models/services/common/claimsEmailService');

module.exports = {

    sendReminderEmails: async function sendReminderEmails() {
        try {
            console.log("SENDING REMINDER EMAILS");

            let now = timestampService.createTimestamp();

            let ongoingRounds = await mongooseVotingRoundRepo.findAll({
                $and: [
                    {voters_notified: true},
                    {status: 'in_progress'}
                ]
            });

            for (let roundIndex = 0; roundIndex < ongoingRounds.length; roundIndex++) {
                let round = ongoingRounds[roundIndex];

                let totalVotingTime = round.end_voting - round.end_registration;
                let timeRemaining = round.end_voting - now;

                if (timeRemaining < totalVotingTime / 4) {
                    if (!round.notification_75_sent) {
                        await remindVotingRound(round, "75%", round.end_voting);
                        await mongooseVotingRoundRepo.update({_id: round._id}, {$set: {notification_75_sent: true}});
                    }
                } else if (timeRemaining < totalVotingTime / 2) {
                    if (!round.notification_50_sent) {
                        await remindVotingRound(round, "50%", round.end_voting);
                        await mongooseVotingRoundRepo.update({_id: round._id}, {$set: {notification_50_sent: true}});
                    }
                } else if ((4 * timeRemaining) < (3 * totalVotingTime)) {
                    if (!round.notification_25_sent) {
                        await remindVotingRound(round, "25%", round.end_voting);
                        await mongooseVotingRoundRepo.update({_id: round._id}, {$set: {notification_25_sent: true}});
                    }
                }
            }
        } catch (err) {
            console.log("ERROR DURING SENDING REMINDER EMAILS " + err);
        }
    }
};

async function remindVotingRound(round, percentage, deadline) {
    let votes = await mongooseVoteRepo.findByVotingRoundId(round._id.toString());

    for (let vote of votes) {
        if (!vote.voted_at) {
            let userToRemind = await mongooseUserRepo.findOneById(vote.voter_id);
            claimEmailService.sendVoteReminder(vote.claim_id, userToRemind.email, percentage, new Date(deadline * 1000))
        }
    }
}