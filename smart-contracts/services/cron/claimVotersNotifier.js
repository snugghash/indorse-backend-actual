const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo')
const mongooseVotesRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const timestampService = require('../../../models/services/common/timestampService');
const invitationToVoteEmailService = require('../../../models/services/common/claimsEmailService')
const ObjectID = require('mongodb').ObjectID;
const amplitudeTracker = require('../../../models/services/tracking/amplitudeTracker');

/*
    1. Get all voting rounds which have started and not expired and do not have notifications sent out
    2. Send out notifications
    3. Mark notifications sent to true
*/
exports.notifyClaimVoters = async function notifyClaimVoters() {
    console.log("STARTING NOTIFYING VOTERS!");
    let now = timestampService.createTimestamp();
    let votingRoundsNotified = [];
    let votingRounds = await mongooseVotingRoundRepo.findAll({voters_notified: false, end_registration: {$lt: now}});
    console.log("Voting rounds " + JSON.stringify(votingRounds));
    for (let votingRound of votingRounds) {
        let mongoClaim = await mongooseClaimsRepo.findOneById(votingRound.claim_id);
        console.log("Mongo claim is " + JSON.stringify(mongoClaim));
        if (mongoClaim && ('sc_claim_id' in mongoClaim)) {
            votingRoundsNotified.push(votingRound._id.toString());
            let voters = await mongooseVotesRepo.findByVotingRoundId(votingRound._id.toString());
            console.log("Voters are " + JSON.stringify(voters));
            for (let vote of voters) {
                let user = await mongooseUserRepo.findOneById(vote.voter_id);
                console.log("Notifiying user " + user.name + " ID " + user.email + " for claim " + votingRound.claim_id);
                await invitationToVoteEmailService.sendVoteInvitationEmail(user.name, votingRound.claim_id, user.email);

                amplitudeTracker.publishData('validation_email_sent', {
                    claim_id: mongoClaim._id.toString(),
                    voting_round_id: votingRound._id.toString(),
                    vote_id: vote._id.toString()
                }, user._id.toString(), user.username, user.email);
            }
        }
    }
    let idsToUpdate = votingRoundsNotified.map(function (item) {
        return ObjectID(item)
    });
    await mongooseVotingRoundRepo.update({_id: {$in: idsToUpdate}}, {voters_notified: true})
};