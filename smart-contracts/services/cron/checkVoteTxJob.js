const mongooseVotesRepo = require('../../../models/services/mongo/mongoose/mongooseVoteRepo');
const getVoteService = require('../../../models/votes/getVoteService');

/*
    1. Get all voting rounds which have started and not expired and do not have notifications sent out
    2. Send out notifications
    3. Mark notifications sent to true
*/
exports.checkVoteTxStatus = async function checkVoteTxStatus() {
    console.log("Starting processing vote txns!")

    let pendingTxs = await mongooseVotesRepo.findAllWithStream({
        $and: [{tx_hash: {$exists: true}},
            {tx_status: 'PENDING'}]
    });

    return new Promise((resolve, reject) => pendingTxs.on('data', function (doc) {
            console.log("Processing vote " + JSON.stringify(doc));
            getVoteService.getVote(doc._id)
        })
        .on('error', function (err) {
            console.log("Vote processing error " + err.toString());
            reject(err);
        })
        .on('end', function () {
            console.log("Finished processing vote txns!");
            resolve();
        })
    );
};