const mongooseVotingRoundRepo = require('../../../models/services/mongo/mongoose/mongooseVotingRoundRepo');
const mongooseClaimsRepo = require('../../../models/services/mongo/mongoose/mongooseClaimsRepo');
const mongooseUserRepo = require('../../../models/services/mongo/mongoose/mongooseUserRepo');
const timestampService = require('../../../models/services/common/timestampService');
const claimsProxy = require('../../../models/services/ethereum/claims/proxy/claimsProxy');
const logger = require('../../../models/services/common/logger').getLogger();
const claimEmailService = require('../../../models/services/common/claimsEmailService');

module.exports = {

    processOutstandingClaims: async function processOutstandingClaims() {
        let now = timestampService.createTimestamp();

        let finishingRounds = await mongooseVotingRoundRepo.findAll({
            $and: [
                {end_voting: {$lt: now}},
                {status: 'in_progress'}
            ]
        });

        for (let roundIndex = 0; roundIndex < finishingRounds.length; roundIndex++) {
            let round = finishingRounds[roundIndex];

            let claim = await mongooseClaimsRepo.findOneById(round.claim_id);

            if (claim.sc_claim_id || claim.sc_claim_id === 0) {
                try {
                    let result = await claimsProxy.getClaimTally(claim.sc_claim_id);
                    await mongooseVotingRoundRepo.update({_id: round._id}, {$set: {status: 'finished'}});
                    await mongooseClaimsRepo.update({_id: round.claim_id}, {$set: {final_status: result}});

                    try {
                        let user = await mongooseUserRepo.findOneById(claim.ownerid);
                        if (result) {
                            await mongooseUserRepo.setValidationResult(user.email, round.claim_id, result);
                        }
                        if (!user)
                            logger.error("Unable to send email to user id " + claim.ownerid + ", Error :" + "User not found")
                        await claimEmailService.sendClaimTalliedEmail(user.name, round.claim_id, user.email)
                    } catch (e) {
                        logger.error("Unable to send email to user id " + claim.ownerid + ", Error :" + JSON.stringify(e))
                    }

                } catch (error) {
                    logger.info("Unable to finalize claim : " + JSON.stringify(claim) + " reason : "
                        + error.message + error.stack)
                }
            } else {
                logger.info("Claim has no sc_claim_id, postponing:" + JSON.stringify(claim));
            }
        }
    }
};