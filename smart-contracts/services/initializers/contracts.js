const fs = require("fs");
const truffleContract = require("truffle-contract");
const _ = require('lodash');
const path = require('path');
const mongooseContractsRepo = require('../../../models/services/mongo/mongoose/mongooseContractsRepo');
const web3Initializer = require('./web3')
const settings = require('../../../models/settings')

const contractPath = path.join(__dirname, '../../build/contracts/');

let connections, indorseToken, naiveClaims, naiveTallyCalculator;

module.exports.getConnectionsContract = async function getConnectionsContract() {
    if (connections) return connections;
    connections = await getContract("Connections");
}

module.exports.getIndorseTokenContract = async function getIndorseTokenContract() {
    if(indorseToken) return indorseToken;
    indorseToken = await getContract("IndorseToken");
}

module.exports.getNaiveClaimsContract = async function getNaiveClaimsContract() {
    if(naiveClaims) return naiveClaims;
    naiveClaims = await getContract("NaiveClaims");
}

module.exports.getNaiveTallyCalcultaorContract = async function getNaiveTallyCalcultaorContract() {
    if(naiveTallyCalculator) return naiveTallyCalculator;
    naiveTallyCalculator = await getContract("NaiveTallyCalculator");
}

async function getContractArtifact(contractName) {
    const filePath = contractPath + contractName + '.json';
    let file = await new Promise((res, rej) => {
        fs.readFile(filePath, {}, (err, data) => {
            if (err) rej(err)
            else res(data)
        })
    });

    return JSON.parse(file.toString());
}

async function getContract(contractName) {
    let web3 = await web3Initializer.getWeb3();

    let contractArtifact = await getContractArtifact(contractName);
    let contract = truffleContract(contractArtifact);
    contract.setProvider(web3.currentProvider);

    let contractDoc = await mongooseContractsRepo.findOneByNameAndNetwork(contractName, settings.ETH_NETWORK);
    contract.defaults({
        from: contractDoc.caller_address,
    })

    return contract.at(contractDoc.contract_address);
}