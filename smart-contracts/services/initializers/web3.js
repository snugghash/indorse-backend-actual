let settings = require('../../../models/settings')
const mongooseEthNwRepo = require('../../../models/services/mongo/mongoose/mongooseEthNetworkRepo');
let _ = require('lodash');
const logger = require('../../../models/services/common/logger').getLogger();
const Web3 = require('web3');

let web3;

module.exports.getWeb3 = async function getWeb3() {
    if (web3) return web3;

    logger.debug("Getting web3 for network " + settings.ETH_NETWORK);

    let ethNw = await mongooseEthNwRepo.findOneByNetwork(settings.ETH_NETWORK);

    web3 = new Web3(new Web3.providers.HttpProvider(ethNw.http_provider));
    logger.debug("Using provider " + ethNw.http_provider);
    return web3;
}