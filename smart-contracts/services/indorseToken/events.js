const types = require('../services/decoder');

const events = {
    PAUSE : {
        name : 'Pause',
        hash : '0x6985a02210a168e66602d3235cb6db0e70f92b3ba4d376a33c0f3d9434bff625',
        params : []
    },

    UNPAUSE : {
        name : 'Unpause',
        hash : '0x7805862f689e2f13df9f062ff482ad3ad112aca9e0847911ed832e158c525b33',
        params : []
    },

    TRANSFER : {
        name : 'Transfer',
        hash : '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
        params : [
            {name : 'from', type : types.ADDRESS},
            {name : 'to', type : types.ADDRESS},
            {name : 'value', type : types.UINT},
        ]
    },
};

const eventsArray = [events.PAUSE, events.UNPAUSE, events.TRANSFER];

module.exports.eventsArray = eventsArray;
module.exports.types = events;
