const scHelper = require('../scHelper')
const contractsInitializer = require('../initializers/contracts')
const errorUtils = require("../../../models/services/error/errorUtils");

// Tx executors
module.exports.transferFrom = transferFrom;
module.exports.unpause = unpause;
module.exports.pause = pause;
module.exports.transferOwnership = transferOwnership;
module.exports.transfer = transfer;
module.exports.approve = approve;

// Function calls
module.exports.name = name;
module.exports.totalSupply = totalSupply;
module.exports.decimals = decimals;
module.exports.version = version;
module.exports.paused = paused;
module.exports.balanceOf = balanceOf;
module.exports.owner = owner;
module.exports.symbol = symbol;
module.exports.allowance = allowance;
module.exports.indCommunity = indCommunity;
module.exports.indVestingDeposit = indVestingDeposit;
module.exports.indFutureDeposit = indFutureDeposit;
module.exports.indFuture = indFuture;
module.exports.indCommunityDeposit = indCommunityDeposit;
module.exports.indPreSale = indPreSale;
module.exports.indPresaleDeposit = indPresaleDeposit;
module.exports.indSeedDeposit = indSeedDeposit;
module.exports.indInflation = indInflation;
module.exports.indVesting = indVesting;
module.exports.indSale = indSale;
module.exports.indSaleDeposit = indSaleDeposit;
module.exports.indInflationDeposit = indInflationDeposit;
module.exports.indSeed = indSeed;

// Events
module.exports.Pause = Pause;
module.exports.Unpause = Unpause;
module.exports.Transfer = Transfer;


async function name() {
    return await scHelper.logAndCallContract("IndorseToken", "name");
}

async function totalSupply() {
    return await scHelper.logAndCallContract("IndorseToken", "totalSupply");
}

async function decimals(){
    return await scHelper.logAndCallContract("IndorseToken", "decimals");
};

async function version() {
    return await scHelper.logAndCallContract("IndorseToken", "version");
}

async function paused() {
    return await scHelper.logAndCallContract("IndorseToken", "paused");
}

async function balanceOf(owner) {
    return await scHelper.logAndCallContract("IndorseToken", "balanceOf", owner);
}

async function owner() {
    return await scHelper.logAndCallContract("IndorseToken", "owner");
}

async function symbol() {
    return await scHelper.logAndCallContract("IndorseToken", "symbol");
}

async function allowance(owner, spender) {
    return await scHelper.logAndCallContract("IndorseToken", "allowance", owner, spender);
}

async function indCommunity() {
    return await scHelper.logAndCallContract("IndorseToken", "indCommunity");
}

async function indVestingDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indVestingDeposit");
}

async function indFutureDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indFutureDeposit");
}

async function indFuture() {
    return await scHelper.logAndCallContract("IndorseToken", "indFuture");
}

async function indCommunityDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indCommunityDeposit");
}

async function indPreSale() {
    return await scHelper.logAndCallContract("IndorseToken", "indPreSale");
}

async function indPresaleDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indPresaleDeposit");
}

async function indSeedDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indSeedDeposit");
}

async function indInflation() {
    return await scHelper.logAndCallContract("IndorseToken", "indInflation");
}

async function indVesting() {
    return await scHelper.logAndCallContract("IndorseToken", "indVesting");
}

async function indSale() {
    return await scHelper.logAndCallContract("IndorseToken", "indSale");
}

async function indSaleDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indSaleDeposit");
}

async function indInflationDeposit() {
    return await scHelper.logAndCallContract("IndorseToken", "indInflationDeposit");
}

async function indSeed() {
    return await scHelper.logAndCallContract("IndorseToken", "indSeed");
}

// TODO: untested
async function transferFrom(from, to, value) {
    errorUtils.throwError("Back end is not authorized to execute this function", 403);
}

async function unpause() {
    errorUtils.throwError("Back end is not authorized to execute this function", 403);
}

async function pause() {
    errorUtils.throwError("Back end is not authorized to execute this function", 403);
}

async function transferOwnership(newOwner) {
    errorUtils.throwError("Back end is not authorized to execute this function", 403);
}

async function transfer(to, value) {
    return await scHelper.logAndQueueContractTx("IndorseToken", "transfer", to, value)
}

// TODO: untested
async function approve(spender, value) {
    errorUtils.throwError("Back end is not authorized to execute this function", 403);
}

// TODO: untested
async function Pause(...args) {
    let indorseTokenContract = await contractsInitializer.getIndorseTokenContractContract();
    return indorseTokenContract.Pause(...args);
}

// TODO: untested
async function Unpause(...args) {
    let indorseTokenContract = await contractsInitializer.getIndorseTokenContractContract();
    return indorseTokenContract.Unpause(...args);
}

// TODO: untested
// Transfer(from, to, value)
async function Transfer(...args) {
    let indorseTokenContract = await contractsInitializer.getIndorseTokenContractContract();
    return indorseTokenContract.Transfer(...args);
}
